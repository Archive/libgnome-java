package popupmenu;

import org.gnu.gnome.App;
import org.gnu.gnome.PopupMenu;
import org.gnu.gnome.Program;
import org.gnu.gnome.UIInfo;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.MenuItemEvent;
import org.gnu.gtk.event.MenuItemListener;

public class PopupMenuExample implements MenuItemListener {

    private App app;

    public PopupMenuExample() {

        app = new App("popupmenu", "Popup Menu");
        app.setBorderWidth(80);
        app.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DELETE)
                        || event.isOfType(LifeCycleEvent.Type.DESTROY))
                    Gtk.mainQuit();
                return false;
            }
        });

        UIInfo menuInfo[] = buildMyMenus();
        PopupMenu popup = new PopupMenu(menuInfo);
        popup.attach(app);

        app.showAll();
    }

    public void menuItemEvent(MenuItemEvent event) {
        app.message("Not implemented...");
    }

    // build the menus for this app.
    protected UIInfo[] buildMyMenus() {
        UIInfo showMenu[] = { UIInfo.item("Next", null, this),
                UIInfo.item("Previous", null, this),
                UIInfo.item("Zoom In", null, this),
                UIInfo.item("Zoom Out", null, this), UIInfo.end() };

        UIInfo editMenu[] = { UIInfo.cutItem(this), UIInfo.copyItem(this),
                UIInfo.pasteItem(this), UIInfo.item("Delete", null, this),
                UIInfo.end() };

        UIInfo mainMenu[] = { UIInfo.subtree("Show", showMenu),
                UIInfo.item("Reduce", null, this),
                UIInfo.item("Expand", null, this),
                UIInfo.subtree("Edit", editMenu), UIInfo.end() };

        return mainMenu;
    }

    public static void main(String[] args) {

        Program.initGnomeUI("PopupMenu", "1.0", args);

        PopupMenuExample pm = new PopupMenuExample();

        Gtk.main();
    }
}
