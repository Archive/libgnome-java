package radiomenu;

import org.gnu.gnome.App;
import org.gnu.gnome.Program;
import org.gnu.gnome.UIInfo;
import org.gnu.gtk.ButtonsType;
import org.gnu.gtk.DialogFlags;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.MessageDialog;
import org.gnu.gtk.MessageType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.MenuItemEvent;
import org.gnu.gtk.event.MenuItemListener;

public class RadioMenu implements MenuItemListener
{

	protected App		app;

	protected boolean	radioOneEnabled		= true;

	protected boolean	radioTwoEnabled		= false;

	protected boolean	radioThreeEnabled	= false;

	protected boolean	toggleOneEnabled	= false;

	protected boolean	toggleTwoEnabled	= false;

	protected boolean	toggleThreeEnabled	= false;

	public RadioMenu() {

		app = new App("radiomenu", "Radio Menu");
		app.setMinimumSize(200, 100);
		app.addListener(new LifeCycleListener() {
			public void lifeCycleEvent(LifeCycleEvent event) {}

			public boolean lifeCycleQuery(LifeCycleEvent event) {
				if (event.isOfType(LifeCycleEvent.Type.DELETE)
					|| event.isOfType(LifeCycleEvent.Type.DESTROY))
					Gtk.mainQuit();
				return false;
			}
		});

		UIInfo menuInfo[] = buildMyMenus();
		app.createMenus(menuInfo);

		app.showAll();
	}

	public void menuItemEvent(MenuItemEvent event) {
		showInfoMessage("Not implemented...");
	}

	public void radioOne() {
		radioOneEnabled = !radioOneEnabled;
		if (radioOneEnabled)
			showInfoMessage("Radio One Enabled");
	}

	public void radioTwo() {
		radioTwoEnabled = !radioTwoEnabled;
		if (radioTwoEnabled)
			showInfoMessage("Radio Two Enabled");
	}

	public void radioThree() {
		radioThreeEnabled = !radioThreeEnabled;
		if (radioThreeEnabled)
			showInfoMessage("Radio Three Enabled");
	}

	public void toggleOne() {
		toggleOneEnabled = !toggleOneEnabled;
		if (toggleOneEnabled)
			showInfoMessage("Toggle One Enabled");
		else
			showInfoMessage("Toggle One Disabled");
	}

	public void toggleTwo() {
		toggleTwoEnabled = !toggleTwoEnabled;
		if (toggleTwoEnabled)
			showInfoMessage("Toggle Two Enabled");
		else
			showInfoMessage("Toggle Two Disabled");
	}

	public void toggleThree() {
		toggleThreeEnabled = !toggleThreeEnabled;
		if (toggleThreeEnabled)
			showInfoMessage("Toggle Three Enabled");
		else
			showInfoMessage("Toggle Three Disabled");
	}

	protected UIInfo[] buildMyMenus() {
		UIInfo radioListMenu[] = {
			UIInfo.radioItem("Radio One", "The first radio button", new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					radioOne();
				}
			}),
			UIInfo.radioItem("Radio Two", "The second radio button", new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					radioTwo();
				}
			}),
			UIInfo.radioItem("Radio Three", "The third radio button", new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					radioThree();
				}
			}),
			UIInfo.end()
		};

		UIInfo radioMenu[] = {
			UIInfo.redoItem(this),
			UIInfo.separator(),
			UIInfo.radioList(radioListMenu),
			UIInfo.separator(),
			UIInfo.cutItem(this),
			UIInfo.end()
		};

		UIInfo toggleMenu[] = {
			UIInfo.newGameItem(this),
			UIInfo.separator(),
			UIInfo.toggleItem("Toggle One", "The first toggle button", new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					toggleOne();
				}
			}),
			UIInfo.toggleItem("Toggle Two", "The second toggle button", new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					toggleTwo();
				}
			}),
			UIInfo.toggleItem("Toggle Three", "The third toggle button", new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					toggleThree();
				}
			}),
			UIInfo.separator(),
			UIInfo.pauseGameItem(this),
			UIInfo.end()
		};

		UIInfo mainMenu[] = {
			UIInfo.subtree("Radio Buttons", radioMenu),
			UIInfo.subtree("Toggle Buttons", toggleMenu),
			UIInfo.end()
		};

		return mainMenu;
	}

	private void showInfoMessage(String message) {
		MessageDialog dialog = new MessageDialog(app, DialogFlags.MODAL, MessageType.INFO,
			ButtonsType.CLOSE, message, false);
		dialog.run();
		dialog.destroy();
	}

	public static void main(String[] args) {

		Program.initGnomeUI("RadioMenu", "1.0", args);

		new RadioMenu();

		Gtk.main();
	}
}
