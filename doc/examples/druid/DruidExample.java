package druid;

import org.gnu.gnome.App;
import org.gnu.gnome.Druid;
import org.gnu.gnome.DruidPageEdge;
import org.gnu.gnome.DruidPageStandard;
import org.gnu.gnome.EdgePosition;
import org.gnu.gnome.Program;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class DruidExample {

    public DruidExample() {
        App app = new App("druid", "Druid example");
        app.setBorderWidth(20);
        app.setDefaultSize(500, 300);
        app.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DELETE)
                        || event.isOfType(LifeCycleEvent.Type.DESTROY))
                    Gtk.mainQuit();
                return false;
            }
        });

        Druid druid = new Druid();
        druid.setButtonsSensitive(false, true, true, false);
        druid.getCancelButton().addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK))
                    Gtk.mainQuit();
            }
        });

        DruidPageEdge dpe = new DruidPageEdge(EdgePosition.START);
        dpe.setTitle("First page");
        dpe
                .setText("This is the first page of the Druid.\n\nYour info would be here.");
        druid.appendPage(dpe);

        DruidPageStandard dps = new DruidPageStandard();
        dps.setTitle("Second Page");
        druid.appendPage(dps);

        dpe = new DruidPageEdge(EdgePosition.FINISH);
        dpe.setTitle("Third page");
        dpe
                .setText("This is the final page of the Druid.\n\nYour info would be here.");
        druid.appendPage(dpe);

        app.setContent(druid);
        app.showAll();
    }

    public static void main(String[] args) {
        Program.initGnomeUI("Entry", "1.0", args);
        new DruidExample();
        Gtk.main();
    }
}
