package stocktoolbar;

import org.gnu.gnome.App;
import org.gnu.gnome.GnomeStockItem;
import org.gnu.gnome.Program;
import org.gnu.gnome.UIInfo;
import org.gnu.gtk.ButtonsType;
import org.gnu.gtk.DialogFlags;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.GtkStockItem;
import org.gnu.gtk.MessageDialog;
import org.gnu.gtk.MessageType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class StockToolbar implements ButtonListener
{

	private App				app;

	protected GtkStockItem	stock[]	= {
		GnomeStockItem.NEW,
		GnomeStockItem.OPEN,
		GnomeStockItem.CLOSE,
		GnomeStockItem.REVERT_TO_SAVED,
		GnomeStockItem.SAVE,
		GnomeStockItem.SAVE_AS,
		GnomeStockItem.CUT,
		GnomeStockItem.COPY,
		GnomeStockItem.PASTE,
		GnomeStockItem.CLEAR,
		GnomeStockItem.PROPERTIES,
		GnomeStockItem.PREFERENCES,
		GnomeStockItem.HELP,
		GnomeStockItem.SCORES,
		GnomeStockItem.PRINT,
		GnomeStockItem.FIND,
		GnomeStockItem.FIND_AND_REPLACE,
		GnomeStockItem.GO_BACK,
		GnomeStockItem.GO_FORWARD,
		GnomeStockItem.GOTO_FIRST,
		GnomeStockItem.GOTO_LAST,
		GnomeStockItem.HOME,
		GnomeStockItem.STOP,
		GnomeStockItem.REFRESH,
		GnomeStockItem.UNDO,
		GnomeStockItem.REDO,
		GnomeStockItem.TIMER,
		GnomeStockItem.TIMER_STOP,
		GnomeStockItem.MAIL,
		GnomeStockItem.MAIL_RCV,
		GnomeStockItem.MAIL_SND,
		GnomeStockItem.MAIL_RPL,
		GnomeStockItem.MAIL_FWD,
		GnomeStockItem.MAIL_NEW,
		GnomeStockItem.TRASH,
		GnomeStockItem.TRASH_FULL,
		GnomeStockItem.UNDELETE,
		GnomeStockItem.SPELL_CHECK,
		GnomeStockItem.MIC,
		GnomeStockItem.LINE_IN,
		GnomeStockItem.CDROM,
		GnomeStockItem.VOLUME,
		GnomeStockItem.MIDI,
		GnomeStockItem.BOOK_RED,
		GnomeStockItem.BOOK_GREEN,
		GnomeStockItem.BOOK_BLUE,
		GnomeStockItem.BOOK_YELLOW,
		GnomeStockItem.BOOK_OPEN,
		GnomeStockItem.ABOUT,
		GnomeStockItem.QUIT,
		GnomeStockItem.MULTIPLE_FILE,
		GnomeStockItem.NOT,
		GnomeStockItem.CONVERT,
		GnomeStockItem.JUMP_TO,
		GnomeStockItem.GO_UP,
		GnomeStockItem.GO_DOWN,
		GnomeStockItem.GOTO_TOP,
		GnomeStockItem.GOTO_BOTTOM,
		GnomeStockItem.ATTACH,
		GnomeStockItem.INDEX,
		GnomeStockItem.SELECT_FONT,
		GnomeStockItem.EXECUTE,
		GnomeStockItem.JUSTIFY_LEFT,
		GnomeStockItem.JUSTIFY_RIGHT,
		GnomeStockItem.JUSTIFY_CENTER,
		GnomeStockItem.JUSTIFY_FILL,
		GnomeStockItem.BOLD,
		GnomeStockItem.ITALIC,
		GnomeStockItem.UNDERLINE,
		GnomeStockItem.STRIKETHROUGH,
		GnomeStockItem.TEXT_INDENT,
		GnomeStockItem.TEXT_UNINDENT,
		GnomeStockItem.QUIT,
		GnomeStockItem.SELECT_COLOR,
		GnomeStockItem.ADD,
		GnomeStockItem.REMOVE,
		GnomeStockItem.TABLE_BORDERS,
		GnomeStockItem.TABLE_FILL,
		GnomeStockItem.TEXT_BULLETED_LIST,
		GnomeStockItem.TEXT_NUMBERED_LIST
									};

	public StockToolbar() {

		app = new App("stocktoolbar", "Stock Toolbar");
		app.addListener(new LifeCycleListener() {
			public void lifeCycleEvent(LifeCycleEvent event) {}

			public boolean lifeCycleQuery(LifeCycleEvent event) {
				if (event.isOfType(LifeCycleEvent.Type.DELETE)
					|| event.isOfType(LifeCycleEvent.Type.DESTROY))
					Gtk.mainQuit();
				return false;
			}
		});

		UIInfo toolbarInfo[] = new UIInfo[8];

		int s = 0;
		while (s < stock.length) {
			/*
			 * if (s + 8 > stock.length) { toolbarInfo = new UIInfo[stock.length -
			 * s + 1]; } else toolbarInfo = new UIInfo[8];
			 */
			for (int i = 0; i < 7; i++) {
				// strip of the prefix for the name
				String name = stock[s].getString();
				if (name.indexOf("gtk-") > -1)
					name = name.substring(4);
				else if (name.indexOf("gnome-") > -1)
					name = name.substring(6);
				toolbarInfo[i] = UIInfo.itemStock(name, name, this, (GtkStockItem) stock[s]);
				toolbarInfo[i + 1] = UIInfo.end();
				if (++s >= stock.length)
					break;
			}
			app.createToolBar(toolbarInfo);
			app.showAll();
		}
	}

	public void buttonEvent(ButtonEvent event) {
		if (event.isOfType(ButtonEvent.Type.CLICK)) {
			MessageDialog dialog = new MessageDialog(app, DialogFlags.MODAL, MessageType.INFO,
				ButtonsType.CLOSE, "Toolbar button pressed...", false);
			dialog.run();
			dialog.destroy();
		}
	}

	public static void main(String[] args) {

		Program.initGnomeUI("StockToolbar", "1.0", args);

		new StockToolbar();

		Gtk.main();
	}
}
