package fontpick;

import org.gnu.gnome.App;
import org.gnu.gnome.FontPicker;
import org.gnu.gnome.Program;
import org.gnu.gnome.event.FontPickerEvent;
import org.gnu.gnome.event.FontPickerListener;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.Label;
import org.gnu.gtk.VBox;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.pango.FontDescription;

public class FontPick {

    Label label;

    FontPicker fontPicker;

    public FontPick() {

        App app = new App("fontpick", "FontPicker example");
        app.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DELETE)
                        || event.isOfType(LifeCycleEvent.Type.DESTROY))
                    Gtk.mainQuit();
                return false;
            }
        });
        app.setBorderWidth(30);

        VBox box = new VBox(false, 0);

        label = new Label("Select the button below and the font picker\n"
                + "window will appear.\n"
                + "The font you select will be used to\n"
                + "rewrite this text.");
        box.packStart(label, false, false, 0);

        fontPicker = new FontPicker();
        fontPicker.addListener(new FontPickerListener() {
            public void fontPickerEvent(FontPickerEvent event) {
                fontSet(fontPicker.getFontName());
            }
        });
        box.packStart(fontPicker, false, false, 0);

        app.setContent(box);

        app.showAll();
    }

    public void fontSet(String fontname) {
        System.out.println("fontname is " + fontname);
        FontDescription fontDesc = new FontDescription(fontname);
        System.out.println("Created font description");
        label.setFont(fontDesc);
    }

    public static void main(String[] args) {

        Program.initGnomeUI("FontPicker", "1.0", args);

        FontPick fpick = new FontPick();

        Gtk.main();
    }
}
