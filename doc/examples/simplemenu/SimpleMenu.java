package simplemenu;

import org.gnu.gnome.App;
import org.gnu.gnome.Program;
import org.gnu.gnome.UIInfo;
import org.gnu.gtk.ButtonsType;
import org.gnu.gtk.DialogFlags;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.MessageDialog;
import org.gnu.gtk.MessageType;
import org.gnu.gtk.StatusBar;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.MenuItemEvent;
import org.gnu.gtk.event.MenuItemListener;

public class SimpleMenu
{

	private App	app;

	public SimpleMenu() {

		app = new App("simplemenu", "Simple Menu");
		app.setMinimumSize(200, 200);
		app.addListener(new LifeCycleListener() {
			public void lifeCycleEvent(LifeCycleEvent event) {}

			public boolean lifeCycleQuery(LifeCycleEvent event) {
				if (event.isOfType(LifeCycleEvent.Type.DELETE)
					|| event.isOfType(LifeCycleEvent.Type.DESTROY))
					Gtk.mainQuit();
				return false;
			}
		});

		UIInfo menuInfo[] = buildMyMenus();
		app.createMenus(menuInfo);

		StatusBar statusbar = new StatusBar();
		app.setStatusBar(statusbar);
		// app.installStatusBarMenuHints(statusbar, menuInfo);
		app.installMenuHints(menuInfo);

		app.showAll();
	}

	public void fileOpen() {
		showInfoMessage("\"File/Open\" selected.");
	}

	public void fileClose() {
		showInfoMessage("\"File/Close\" selected.");
	}

	public void fileExit() {
		Gtk.mainQuit();
	}

	public void editCut() {
		showInfoMessage("\"Edit/Cut\" selected.");
	}

	public void editCopy() {
		showInfoMessage("\"Edit/Copy\" selected.");
	}

	public void editPaste() {
		showInfoMessage("\"Edit/Paste\" selected.");
	}

	public void editMoveUp() {
		showInfoMessage("\"Edit/Move/Up\" selected.");
	}

	public void editMoveDown() {
		showInfoMessage("\"Edit/Move/Down\" selected.");
	}

	public void editDelete() {
		showInfoMessage("\"Edit/Delete\" selected.");
	}

	// build the menus for this app.
	protected UIInfo[] buildMyMenus() {
		UIInfo fileMenu[] = {
			// File / Open menu
			UIInfo.item("_Open", "Open an existing file", new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					fileOpen();
				}
			}),
			// File / Close menu
			UIInfo.item("_Close", "Close the current file", new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					fileClose();
				}
			}),
			// Separator
			UIInfo.separator(),
			// File / Exit menu
			UIInfo.item("E_xit", "Close the window and cease", new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					fileExit();
				}
			}),
			UIInfo.end()
		};

		UIInfo editMoveMenu[] = {
			// Edit / Move / Up menu
			UIInfo.item("_Up", "Move selection up", new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					editMoveUp();
				}
			}),
			// Edit / Move / Down menu
			UIInfo.item("D_own", "Move selection down", new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					editMoveDown();
				}
			}),
			UIInfo.end()
		};

		UIInfo editMenu[] = {
			// Edit / Cut menu
			UIInfo.item("C_ut", "Delete and copy selection to clipboard", new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					editCut();
				}
			}),
			// Edit / Copy menu
			UIInfo.item("C_opy", "Copy selection to clipboard", new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					editCopy();
				}
			}),
			// Edit / Paste menu
			UIInfo.item("_Paste", "Paste selection from clipboard", new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					editPaste();
				}
			}),
			// Edit / Move menu
			UIInfo.subtree("_Move", editMoveMenu),
			// Edit / Delete menu
			UIInfo.item("_Delete", "Delete the current selection", new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					editDelete();
				}
			}),
			UIInfo.end()
		};

		UIInfo mainMenu[] = {
			// File menu
			UIInfo.subtree("_File", fileMenu),
			// Edit menu
			UIInfo.subtree("_Edit", editMenu),
			UIInfo.end()
		};

		return mainMenu;
	}

	private void showInfoMessage(String message) {
		MessageDialog dialog = new MessageDialog(app, DialogFlags.MODAL, MessageType.INFO,
			ButtonsType.CLOSE, message, false);
		dialog.run();
		dialog.destroy();
	}

	public static void main(String[] args) {

		Program.initGnomeUI("SimpleMenu", "1.0", args);

		new SimpleMenu();

		Gtk.main();
	}
}
