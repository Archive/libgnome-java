package app;

import java.awt.Window;
import org.gnu.gdk.Pixbuf;
import org.gnu.gtk.AboutDialog;
import org.gnu.gnome.App;
import org.gnu.gnome.AppBar;
import org.gnu.gnome.PreferencesType;
import org.gnu.gnome.Program;
import org.gnu.gnome.UIInfo;
import org.gnu.gtk.Button;
import org.gnu.gtk.ButtonBox;
import org.gnu.gtk.ButtonsType;
import org.gnu.gtk.DialogFlags;
import org.gnu.gtk.Frame;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.GtkStockItem;
import org.gnu.gtk.HBox;
import org.gnu.gtk.HButtonBox;
import org.gnu.gtk.MessageDialog;
import org.gnu.gtk.MessageType;
import org.gnu.gtk.ShadowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.MenuItemEvent;
import org.gnu.gtk.event.MenuItemListener;

public class AppExample implements MenuItemListener, ButtonListener
{

	private App		app;

	private AppBar	appbar;

	public AppExample() {

		app = new App("appexample", "App Example");
		app.addListener(new LifeCycleListener() {
			public void lifeCycleEvent(LifeCycleEvent event) {}

			public boolean lifeCycleQuery(LifeCycleEvent event) {
				if (event.isOfType(LifeCycleEvent.Type.DELETE)
					|| event.isOfType(LifeCycleEvent.Type.DESTROY))
					Gtk.mainQuit();
				return false;
			}
		});

		// Create and add the AppBar
		appbar = new AppBar(true, true, PreferencesType.ALWAYS);
		app.setStatusBar(appbar);

		// Create and add the Menu
		UIInfo[] menuInfo = buildMyMenus();
		app.createMenus(menuInfo);
		// app.installMenuHints(menuInfo);

		// create the ToolBar
		app.createToolBar(buildMyToolbar());

		// create the main window area
		app.setContent(buildMainView());

		app.showAll();
	}

	// build the menus for this app.
	private UIInfo[] buildMyMenus() {

		UIInfo fileMenu[] = {
			UIInfo.newItem("New Window", "Open a new application window", (MenuItemListener) this),
			UIInfo.separator(),
			UIInfo.openItem((MenuItemListener) this),
			UIInfo.saveItem((MenuItemListener) this),
			UIInfo.saveAsItem((MenuItemListener) this),
			UIInfo.revertItem((MenuItemListener) this),
			UIInfo.printItem((MenuItemListener) this),
			UIInfo.printSetupItem((MenuItemListener) this),
			UIInfo.separator(),
			UIInfo.closeItem((MenuItemListener) this),
			UIInfo.quitItem((MenuItemListener) this),
			UIInfo.end()
		};

		UIInfo editMenu[] = {
			UIInfo.undoItem((MenuItemListener) this),
			UIInfo.redoItem((MenuItemListener) this),
			UIInfo.separator(),
			UIInfo.cutItem((MenuItemListener) this),
			UIInfo.copyItem((MenuItemListener) this),
			UIInfo.pasteItem((MenuItemListener) this),
			UIInfo.clearItem((MenuItemListener) this),
			UIInfo.selectAllItem((MenuItemListener) this),
			UIInfo.separator(),
			UIInfo.findItem((MenuItemListener) this),
			UIInfo.findAgainItem((MenuItemListener) this),
			UIInfo.replaceItem((MenuItemListener) this),
			UIInfo.propertiesItem((MenuItemListener) this),
			UIInfo.end()
		};

		UIInfo helpMenu[] = {
			UIInfo.aboutItem(new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					helpAbout();
				}
			}),
			UIInfo.end()
		};

		UIInfo mainMenu[] = {
			UIInfo.subtree("_File", fileMenu),
			UIInfo.subtree("_Edit", editMenu),
			UIInfo.subtree("_Help", helpMenu),
			UIInfo.end()
		};

		return mainMenu;
	}

	private UIInfo[] buildMyToolbar() {
		UIInfo toolbar[] = {
			UIInfo.itemStock("Cut", "Cut", (ButtonListener) this, GtkStockItem.CUT),
			UIInfo.itemStock("Copy", "Copy", (ButtonListener) this, GtkStockItem.COPY),
			UIInfo.itemStock("Paste", "Paste", (ButtonListener) this, GtkStockItem.PASTE),
			UIInfo.separator(),
			UIInfo.itemStock("Clear", "Clear", (ButtonListener) this, GtkStockItem.CLEAR),
			UIInfo.separator(),
			UIInfo.itemStock("Quit", "Quit", (ButtonListener) this, GtkStockItem.QUIT),
			UIInfo.end()
		};

		return toolbar;
	}

	private HBox buildMainView() {
		HBox main = new HBox(false, 10);

		Frame frame = new Frame("Popups");
		frame.setBorderWidth(10);
		frame.setShadow(ShadowType.ETCHED_IN);
		main.packStart(frame);

		ButtonBox bbox = new HButtonBox();
		// Add the message button
		Button button = new Button("_Message", true);
		button.addListener(new ButtonListener() {
			public void buttonEvent(ButtonEvent event) {
				if (event.isOfType(ButtonEvent.Type.CLICK)) {
					MessageDialog dialog = new MessageDialog(app, DialogFlags.MODAL, MessageType.INFO,
						ButtonsType.CLOSE, "This is a message", false);
					dialog.run();
					dialog.destroy();
				}
			}
		});
		bbox.add(button);
		// Add the warning button
		button = new Button("_Warning", true);
		button.addListener(new ButtonListener() {
			public void buttonEvent(ButtonEvent event) {
				if (event.isOfType(ButtonEvent.Type.CLICK)) {
					MessageDialog warningDialog = new MessageDialog(app, DialogFlags.MODAL,
						MessageType.WARNING, ButtonsType.CLOSE, "This is a warning", false);
					warningDialog.run();
					warningDialog.destroy();
				}
			}
		});
		bbox.add(button);
		// Add the error button
		button = new Button("_Error", true);
		button.addListener(new ButtonListener() {
			public void buttonEvent(ButtonEvent event) {
				if (event.isOfType(ButtonEvent.Type.CLICK)) {
					MessageDialog errorDialog = new MessageDialog(app, DialogFlags.MODAL,
						MessageType.ERROR, ButtonsType.CLOSE, "This is an error", false);
					errorDialog.run();
					errorDialog.destroy();
				};
			}
		});
		bbox.add(button);
		// Add the flase button
		button = new Button("_Flash", true);
		button.addListener(new ButtonListener() {
			public void buttonEvent(ButtonEvent event) {
				if (event.isOfType(ButtonEvent.Type.CLICK))
					// app.getStatusBar().getName();
					app.flash("This is a flash");
			}
		});
		bbox.add(button);

		frame.add(bbox);

		return main;
	}

	// Callback for the menu events
	public void menuItemEvent(MenuItemEvent event) {
		System.out.println("menuItemEvent");
		MessageDialog dialog = new MessageDialog(app, DialogFlags.MODAL, MessageType.INFO,
			ButtonsType.CLOSE, "menu callback", false);
		dialog.run();
		dialog.destroy();
	}

	public void buttonEvent(ButtonEvent event) {
		System.out.println("buttonEvent");
		MessageDialog dialog = new MessageDialog(app, DialogFlags.MODAL, MessageType.INFO,
			ButtonsType.CLOSE, "button callback", false);
		dialog.run();
		dialog.destroy();
	}

	// Callback for the help->about menu
	public void helpAbout() {
		System.out.println("helpAbout");
		String title = "My about";
		String version = "Version 0.0.0";
		String license = "GPL";
		String comments = "My comments go here";
		String[] authors = {
			"Me",
			"Myself",
			"I"
		};
		String[] documenters = {
			"Me",
			"Myself",
			"I"
		};
		String translator = "Porkey";
		AboutDialog about = new AboutDialog();

		about.setVersion(version);
		about.setAuthors(authors);
		about.setTranslatorCredits(translator);
		about.setTitle(title);
		about.setLicense(license);
		about.setComments(comments);
		about.setDocumenters(documenters);
		about.show();
	}

	public static void main(String[] args) {

		Program prog = Program.initGnomeUI("AppExample", "1.0", args);

		new AppExample();

		Gtk.main();
	}
}
