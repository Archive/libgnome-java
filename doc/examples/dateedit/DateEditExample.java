package dateedit;

import java.util.Date;

import org.gnu.gnome.App;
import org.gnu.gnome.DateEdit;
import org.gnu.gnome.Program;
import org.gnu.gnome.event.DateEditEvent;
import org.gnu.gnome.event.DateEditListener;
import org.gnu.gtk.Button;
import org.gnu.gtk.CheckButton;
import org.gnu.gtk.Frame;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.HBox;
import org.gnu.gtk.Label;
import org.gnu.gtk.ShadowType;
import org.gnu.gtk.VBox;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.ToggleEvent;
import org.gnu.gtk.event.ToggleListener;

public class DateEditExample
{

	private DateEdit	dateEdit;

	private CheckButton	dateFormat;

	private CheckButton	time;

	public DateEditExample() {
		App app = new App("entry", "Entry example");
		app.setBorderWidth(20);
		app.addListener(new LifeCycleListener() {
			public void lifeCycleEvent(LifeCycleEvent event) {}

			public boolean lifeCycleQuery(LifeCycleEvent event) {
				if (event.isOfType(LifeCycleEvent.Type.DELETE)
					|| event.isOfType(LifeCycleEvent.Type.DESTROY))
					Gtk.mainQuit();
				return false;
			}
		});

		VBox vbox = new VBox(false, 5);
		Date now = new Date();
		// Create the DateEdit control and a Label
		HBox dateEditBox = new HBox(false, 5);
		Label dateEditLabel = new Label("Date Edit:");
		dateEdit = new DateEdit(now, true, false);
		// Add a listener for the callbacks so we know when the values change
		dateEdit.addListener(new DateEditListener() {
			public void dateEditEvent(DateEditEvent event) {
				Date myDate = dateEdit.getTime();
				if (event.isOfType(DateEditEvent.Type.DATE_CHANGED))
					System.out.println("Date changed: " + myDate.toString());
				else if (event.isOfType(DateEditEvent.Type.TIME_CHANGED))
					System.out.println("Time changed: " + myDate.toString());
			}
		});
		dateEditBox.packStart(dateEditLabel);
		dateEditBox.packStart(dateEdit);
		vbox.packStart(dateEditBox);

		Frame frame = new Frame("Settings");
		frame.setBorderWidth(10);
		frame.setShadow(ShadowType.ETCHED_IN);
		vbox.packStart(frame);

		// Add a control to change the 24 hour setting of the DateEdit
		VBox settings = new VBox(false, 5);
		dateFormat = new CheckButton("24 hour format", false);
		dateFormat.setState(false);
		dateFormat.addListener(new ToggleListener() {
			public void toggleEvent(ToggleEvent event) {
				dateEdit.set24HourFormat(dateFormat.getState());
				dateEdit.setTime(dateEdit.getTime());
			}
		});
		settings.packStart(dateFormat);

		// Add a control to change the display of the time element of the
		// DateEdit
		time = new CheckButton("Display time", false);
		time.setState(true);
		time.addListener(new ToggleListener() {
			public void toggleEvent(ToggleEvent event) {
				dateEdit.setTimeDisplay(time.getState());
			}
		});
		settings.packStart(time);

		frame.add(settings);

		// Add button to quit application.
		Button button = new Button("_Quit", true);
		button.addListener(new ButtonListener() {
			public void buttonEvent(ButtonEvent event) {
				if (event.isOfType(ButtonEvent.Type.CLICK))
					Gtk.mainQuit();
			}
		});
		vbox.add(button);

		app.setContent(vbox);
		app.showAll();
	}

	public static void main(String[] args) {
		Program.initGnomeUI("Entry", "1.0", args);
		new DateEditExample();
		Gtk.main();
	}
}
