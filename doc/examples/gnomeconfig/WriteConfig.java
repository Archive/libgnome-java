package gnomeconfig;

import org.gnu.gnome.Config;
import org.gnu.gnome.Program;

public class WriteConfig {

    public WriteConfig() {

        // items to appear in .gnome directory
        Config.setString("writeconfig/cool/bindings", "java-gnome");
        Config.setInt("writeconfig/cool/width", 16);
        Config.setDouble("writeconfig/cool/version", 0.6);
        Config.setBoolean("writeconfig/cool/very_cool", true);

        // items to appear in .gnome_private directory
        Config.setPrivateString("writeconfig/user/password", "javaman");
        Config.setPrivateInt("writeconfig/user/age", 2);
        Config.setPrivateDouble("writeconfig/user/salary", 100.10);
        Config.setPrivateBoolean("writeconfig/user/is_lazy", true);

        Config.sync();
    }

    public static void main(String[] args) {

        Program.initGnome("WriteConfig Example", "1.0", args);

        WriteConfig write = new WriteConfig();

    }
}
