package gnomeconfig;

import org.gnu.gnome.Config;
import org.gnu.gnome.Program;

public class ReadConfig {

    public ReadConfig() {

        // items to appear in .gnome directory
        System.out.println("Bindings = "
                + Config.getString("writeconfig/cool/bindings"));
        System.out
                .println("Width = " + Config.getInt("writeconfig/cool/width"));
        System.out.println("Version = "
                + Config.getDouble("writeconfig/cool/version"));
        System.out.println("Very Cool = "
                + Config.getBoolean("writeconfig/cool/very_cool"));

        // items to appear in .gnome_private directory
        System.out.println("Private password = "
                + Config.getPrivateString("writeconfig/user/password"));
        System.out.println("Private age = "
                + Config.getPrivateInt("writeconfig/user/age"));
        System.out.println("Private salary = "
                + Config.getPrivateDouble("writeconfig/user/salary"));
        System.out.println("Private is lazy = "
                + Config.getPrivateBoolean("writeconfig/user/is_lazy"));
    }

    public static void main(String[] args) {

        Program.initGnome("ReadConfig Example", "1.0", args);
        ReadConfig read = new ReadConfig();
    }
}
