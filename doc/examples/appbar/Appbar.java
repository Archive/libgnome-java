package appbar;

import org.gnu.gnome.App;
import org.gnu.gnome.AppBar;
import org.gnu.gnome.PreferencesType;
import org.gnu.gnome.Program;
import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.VBox;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class Appbar implements ButtonListener {

    private App app;

    private AppBar appbar;

    public Appbar() {

        app = new App("appbar", "Appbar example");
        app.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DELETE)
                        || event.isOfType(LifeCycleEvent.Type.DESTROY))
                    Gtk.mainQuit();
                return false;
            }
        });

        // Create and add the AppBar
        appbar = new AppBar(true, true, PreferencesType.ALWAYS);
        appbar.setProgressPercentage(0.63);
        appbar.pushStack("Showing 63 Percent");
        app.setStatusBar(appbar);

        VBox box = new VBox(false, 0);
        // Create a button to activate an event.
        Button aButton = new Button("_Press Me", true);
        aButton.addListener(this);
        box.packStart(aButton, false, false, 10);
        app.setContent(box);

        app.showAll();
    }

    public void buttonEvent(ButtonEvent event) {
        if (event.isOfType(ButtonEvent.Type.CLICK)) {
            double rand = Math.random();
            // set the progress percentage to the random number
            appbar.setProgressPercentage(rand);
            // remove the previous message
            appbar.popStack();
            // show the updated message
            appbar.pushStack("Showing " + (int) (100 * rand) + " Percent");
        }
    }

    public static void main(String[] args) {

        Program prog = Program.initGnomeUI("Appbar", "1.0", args);

        Appbar abar = new Appbar();

        Gtk.main();
    }
}
