package canvas;

import org.gnu.gdk.Bitmap;
import org.gnu.gdk.Color;
import org.gnu.gnome.App;
import org.gnu.gnome.Canvas;
import org.gnu.gnome.CanvasEllipse;
import org.gnu.gnome.Program;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class CanvasExample {

    public CanvasExample() {
        App app = new App("Canvas", "Canvas App");
        app.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DELETE)
                        || event.isOfType(LifeCycleEvent.Type.DESTROY))
                    Gtk.mainQuit();
                return false;
            }
        });

        Canvas canvas = new Canvas(false);
        app.getVBox().add(canvas);
        CanvasEllipse ellipse = new CanvasEllipse(canvas.getRoot(), 20, 20,
                100, 140, Color.RED, Color.BLACK, null, null, 1.0);

        // Get the current setting.
        String propname = "fill-color-gdk";
        Object j = ellipse.getJavaObjectProperty(propname);
        // Cast this to a gdk.Color object.
        // NOTE: Some of properties can be null so in a real application,
        // you should check for that too.
        Color col = (Color) j;

        System.out.println("CanvasExample: " + j.getClass().getName());
        System.out.println("CanvasExample: red=" + col.getRed() + ";blue="
                + col.getBlue() + ";green=" + col.getGreen());

        // Create the replacement color.
        // Color colnew = Color.parse( "#0000ff" );
        // You could also just reuse the old color like this:
        // col.setRed( (short)0 );
        // col.setBlue( (short)65535 );
        // col.setGreen( (short)0 );
        // Or use one of the predefined colors:
        Color colnew = Color.BLUE;

        // Set the property and the color changes!.
        ellipse.setJavaObjectProperty(propname, colnew);

        // an integer Property returned as a java.lang.Integer object
        Object integerObject = ellipse.getJavaObjectProperty("fill-color-rgba");
        System.out.println("getJavaObjectProperty(\"fill-color-rgba\"): "
                + integerObject.toString());

        // create a fill stipple bitmap to test CanvasShape properties
        byte bytes[] = { 1, 2, 4, 8, 16, 32, 64, -128 };
        Bitmap fill_bitmap = new Bitmap(bytes, 8, 8);
        ellipse.setJavaObjectProperty("fill-stipple", fill_bitmap);

        // trying all the readable CanvasShape properties
        String[] properties = { "cap-style", "dash", // there's no way to
                                                        // test it, for now
                // anyway it gives no error/exception
                "fill-color-gdk", "fill-color-rgba", "fill-stipple",
                "join-style", "miterlimit", "outline-color-gdk",
                "outline-color-rgba", "outline-stipple", "width-pixels", "wind" };
        for (int i = 0; i < properties.length; i++) {
            System.out.print(properties[i] + ": ");
            Object property_object = ellipse
                    .getJavaObjectProperty(properties[i]);
            if (property_object == null) {
                System.out.println("null");
            } else {
                System.out.println(property_object.getClass().getName());
            }
        }

        app.show();
        canvas.show();
    }

    public static void main(String[] args) {

        Program prog = Program.initGnomeUI("CanvasExample", "1.0", args);
        CanvasExample abar = new CanvasExample();
        Gtk.main();
    }
}
