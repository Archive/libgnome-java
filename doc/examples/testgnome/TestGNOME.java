package testgnome;

import java.util.Date;

import org.gnu.gdk.Color;
import org.gnu.gnome.App;
import org.gnu.gnome.AppBar;
import org.gnu.gnome.Canvas;
import org.gnu.gnome.CanvasEllipse;
import org.gnu.gnome.CanvasGroup;
import org.gnu.gnome.DateEdit;
import org.gnu.gnome.HRef;
import org.gnu.gnome.PreferencesType;
import org.gnu.gnome.Program;
import org.gnu.gtk.AboutDialog;
import org.gnu.gtk.Button;
import org.gnu.gtk.ColorButton;
import org.gnu.gtk.Dialog;
import org.gnu.gtk.FontButton;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.HBox;
import org.gnu.gtk.HSeparator;
import org.gnu.gtk.Label;
import org.gnu.gtk.PolicyType;
import org.gnu.gtk.ResponseType;
import org.gnu.gtk.ScrolledWindow;
import org.gnu.gtk.VBox;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

/**
 * Based heavily on TestGTK.java written by Olivier
 * 
 * Not the very best example of good object-oriented Java programming, but well,
 * that's not the aim. :-)
 * 
 * @author C.J. van Wyk, Jan 2000
 * 
 */

public class TestGNOME extends Object implements ButtonListener, LifeCycleListener
{

	private Button				aboutButton	= null, appButton = null, appBarButton = null,
		canvasButton = null, closeButton = null, dateEditButton = null, dialogButton = null,
		entriesButton = null, hRefButton = null, miscButton = null, pickersButton = null;

	// private Program myProg;

	private org.gnu.gtk.Window	win;

	public TestGNOME(Program myProg) {
		super();
		// this.myProg = myProg;
		createMainWindow();
	}

	// Here the fun (and tests) starts
	public void createAbout() {
		String title = "My about";
		String version = "Version 0.0.0";
		String license = "GPL";
		String comments = "My comments go here";
		String[] authors = {
			"Me",
			"Myself",
			"I"
		};
		String[] documenters = {
			"Me",
			"Myself",
			"I"
		};
		String translator = "Porkey";
		AboutDialog about = new AboutDialog();
		about.setTitle(title);
		about.setVersion(version);
		about.setLicense(license);
		about.setComments(comments);
		about.setDocumenters(documenters);
		about.setTranslatorCredits(translator);
		about.setAuthors(authors);
		about.show();
	}

	public void createApp() {
		App app = new App("AppName", "My first App");
		app.show();
	}

	public void createAppBar() {
		AppBar app_bar = new AppBar(true, true, PreferencesType.USER);
		App app = new App("AppName", "My first App");
		app.setStatusBar(app_bar);
		app.showAll();
	}

	public void createCanvas() {
		new App("AppName", "Canvas Demo");
		Canvas canv = new Canvas(true);
		CanvasGroup root = canv.getRoot();
		new CanvasEllipse(root, 10.0, 10.0, 200.0, 150.0, 100000, 52300, 25);
		// Dialog and wrapping up
		Dialog dial = new Dialog();
		dial.setTitle("Canvas");
		dial.setModal(true);
		dial.addButton("OK", 1);
		(dial.getDialogLayout()).packStart(canv);
		dial.setDefaultResponse(0);
		dial.resize(350, 350);
		dial.showAll();
		dial.run();
		dial.destroy();
	}

	public void createColorSelection() {}

	public void createDateEdit() {
		Date now = new Date();
		// GnomeDateEdit
		HBox date_edit_box = new HBox(true, 0);
		Label date_edit_label = new Label("Date Edit:");
		DateEdit date_edit = new DateEdit(now, false, false);
		date_edit_box.packStart(date_edit_label);
		date_edit_box.packStart(date_edit);
		// GnomeDateEdit with time
		HBox time_date_edit_box = new HBox(true, 0);
		Label time_date_edit_label = new Label("Date Edit (with time):");
		DateEdit time_date_edit = new DateEdit(now, true, false);
		time_date_edit_box.packStart(time_date_edit_label);
		time_date_edit_box.packStart(time_date_edit);
		// GnomeDateEdit use 24 hr
		HBox hr_date_edit_box = new HBox(true, 0);
		Label hr_date_edit_label = new Label("Date Edit (using 24 hr):");
		DateEdit hr_date_edit = new DateEdit(now, true, true);
		hr_date_edit_box.packStart(hr_date_edit_label);
		hr_date_edit_box.packStart(hr_date_edit);
		// VBox used to pack everything
		VBox vbox = new VBox(true, 3);
		vbox.packStart(date_edit_box);
		vbox.packStart(time_date_edit_box);
		vbox.packStart(hr_date_edit_box);
		// Dialog and wrapping up
		Dialog dial = new Dialog();
		dial.setTitle("DateEdit");
		dial.setModal(true);
		dial.addButton("OK", 1);
		(dial.getDialogLayout()).packStart(vbox);
		// This is for when (if) we impliment method variables
		// dial.vbox.packStartDefaults(vbox);
		dial.setDefaultResponse(0);
		dial.showAll();
		dial.run();
		dial.destroy();
	}

	public void createDialog() {
		Label label = new Label("Test to see a dialog in Java-Gnome");
		Dialog dial = new Dialog();
		dial.setTitle("My first dialog");
		dial.setModal(true);
		dial.addButton("Ok", 1);
		dial.addButton("Cancel", 0);
		(dial.getDialogLayout()).packStart(label);
		// This is for when (if) we impliment method variables
		// dial.vbox.packStartDefaults(label);
		dial.setDefaultResponse(0);
		dial.showAll();
		dial.run();
		dial.destroy();
	}

	public void createEntries() {}

	public void createHRef() {
		HRef href = new HRef("http://www.gnome.org", "Gnome Site");
		Dialog dial = new Dialog();
		dial.setTitle("HRef");
		dial.setModal(true);
		dial.addButton("OK", ResponseType.OK.getValue());
		(dial.getDialogLayout()).packStart(href);
		// This is for when (if) we impliment method variables
		// dial.vbox.packStartDefaults(label);
		dial.setDefaultResponse(0);
		dial.showAll();
		dial.run();
		dial.destroy();
	}

	public void createPickers() {
		HBox color_picker_box = new HBox(true, 0);
		Label color_picker_label = new Label("Color Picker:");
		ColorButton color_button = new ColorButton();
		color_button.setColor(Color.BLUE);
		color_picker_box.packStart(color_picker_label);
		color_picker_box.packStart(color_button);
		HBox font_picker_box = new HBox(true, 0);
		Label font_picker_label = new Label("Font Picker:");
		FontButton font_button = new FontButton();
		font_picker_box.packStart(font_picker_label);
		font_picker_box.packStart(font_button);
		// VBox used to pack everything
		VBox vbox = new VBox(true, 3);
		vbox.packStart(color_picker_box);
		vbox.packStart(font_picker_box);
		// Dialog and wrapping up
		Dialog dial = new Dialog();
		dial.setTitle("Pickers");
		dial.setModal(true);
		dial.addButton("OK", 0);
		(dial.getDialogLayout()).packStart(vbox);
		// This is for when (if) we impliment method variables
		// dial.vbox.packStart(vbox);
		dial.setDefaultResponse(0);
		dial.showAll();
		dial.run();
		dial.destroy();
	}

	public void createMainWindow() {
		win = new org.gnu.gtk.Window(org.gnu.gtk.WindowType.TOPLEVEL);
		win.setName("main window");
		win.setDefaultSize(200, 400);
		win.addListener((LifeCycleListener) this);
		win.setTitle("gtktest");
		VBox box1 = new VBox(false, 0);
		win.add(box1);
		box1.show();
		ScrolledWindow scrolledWindow = new ScrolledWindow(null, null);
		scrolledWindow.setPolicy(PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
		scrolledWindow.setBorderWidth(10);

		box1.packStart(scrolledWindow, true, true, 0);

		scrolledWindow.show();
		VBox box2 = new VBox(false, 0); // Constructors without args fail
		box2.setBorderWidth(10);
		scrolledWindow.addWithViewport(box2);
		box2.show();

		// Buttons
		aboutButton = new Button("About");
		aboutButton.addListener((ButtonListener) this);
		box2.packStart(aboutButton, true, true, 0);
		aboutButton.show();

		appButton = new Button("App");
		appButton.addListener((ButtonListener) this);
		box2.packStart(appButton, true, true, 0);
		appButton.show();

		appBarButton = new Button("AppBar");
		appBarButton.addListener((ButtonListener) this);
		box2.packStart(appBarButton, true, true, 0);
		appBarButton.show();

		canvasButton = new Button("Canvas");
		canvasButton.addListener((ButtonListener) this);
		box2.packStart(canvasButton, true, true, 0);
		canvasButton.show();

		dateEditButton = new Button("DateEdit");
		dateEditButton.addListener((ButtonListener) this);
		box2.packStart(dateEditButton, true, true, 0);
		dateEditButton.show();

		dialogButton = new Button("Dialog");
		dialogButton.addListener((ButtonListener) this);
		box2.packStart(dialogButton, true, true, 0);
		dialogButton.show();

		entriesButton = new Button("Entries");
		entriesButton.addListener((ButtonListener) this);
		entriesButton.setSensitive(false);
		box2.packStart(entriesButton, true, true, 0);
		entriesButton.show();

		hRefButton = new Button("HRef");
		hRefButton.addListener((ButtonListener) this);
		box2.packStart(hRefButton, true, true, 0);
		hRefButton.show();

		miscButton = new Button("Misc");
		miscButton.addListener((ButtonListener) this);
		miscButton.setSensitive(false);
		box2.packStart(miscButton, true, true, 0);
		miscButton.show();

		pickersButton = new Button("Pickers");
		pickersButton.addListener((ButtonListener) this);
		box2.packStart(pickersButton, true, true, 0);
		pickersButton.show();
		// End of Buttons

		HSeparator separator = new HSeparator();
		box1.packStart(separator, false, true, 0);
		separator.show();
		VBox box3 = new VBox(false, 10);
		box3.setBorderWidth(10);
		box1.packStart(box3, false, true, 0);
		box3.show();
		closeButton = new Button("close");
		closeButton.addListener((ButtonListener) this);
		// button.setFlags(.GTK_CAN_DEFAULT);
		box3.packStart(closeButton, true, true, 0);
		// button.grabDefault();
		closeButton.show();
		win.show();
	}

	public static void main(String[] args) {
		Program myProg = Program.initGnomeUI("TestGNOME", "0.0.1", args);
		new TestGNOME(myProg);
		Gtk.main();
	}

	public void buttonEvent(ButtonEvent be) {
		if (be.isOfType(ButtonEvent.Type.CLICK)) {
			Object source = be.getSource();
			if (source == aboutButton) {
				createAbout();
			} else if (source == appButton) {
				createApp();
			} else if (source == appBarButton) {
				createAppBar();
			} else if (source == canvasButton) {
				createCanvas();
			} else if (source == closeButton) {
				exit(0);
			} else if (source == dateEditButton) {
				createDateEdit();
			} else if (source == dialogButton) {
				createDialog();
			} else if (source == entriesButton) {
				createEntries();
			} else if (source == hRefButton) {
				createHRef();
			} else if (source == miscButton) {
				// Nothing
			} else if (source == pickersButton) {
				createPickers();
			} else {
				System.err.println("TestGNOME.buttonEvent buttonEvent source unknown");
			}
		}
	}

	public void lifeCycleEvent(LifeCycleEvent lce) {}

	public boolean lifeCycleQuery(LifeCycleEvent lce) {
		if (lce.isOfType(LifeCycleEvent.Type.DESTROY) || lce.isOfType(LifeCycleEvent.Type.DELETE))
			Gtk.mainQuit();
		return false;
	}

	private void exit(int returnVal) {
		Gtk.mainQuit();
		System.exit(returnVal);
	}
}
