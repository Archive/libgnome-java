package iconentry;

import org.gnu.gnome.App;
import org.gnu.gnome.IconEntry;
import org.gnu.gnome.Program;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class IconEntryExample {

    IconEntry iconentry;

    public IconEntryExample() {

        App app = new App("iconentry", "IconEntry example");
        app.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DELETE)
                        || event.isOfType(LifeCycleEvent.Type.DESTROY))
                    Gtk.mainQuit();
                return false;
            }
        });
        app.setBorderWidth(20);

        iconentry = new IconEntry("historyID", "Select an Icon");
        app.setContent(iconentry);

        app.showAll();
    }

    public static void main(String[] args) {

        Program.initGnomeUI("IconEntry", "1.0", args);

        IconEntryExample ie = new IconEntryExample();

        Gtk.main();
    }
}
