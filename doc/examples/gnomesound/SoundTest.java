package gnomesound;

import org.gnu.gnome.Program;
import org.gnu.gnome.Sound;

public class SoundTest {

    public SoundTest() {

        Sound.play("gnomesound/startup.wav");
        Sound.shutdown();
    }

    public static void main(String[] args) {

        Program.initGnome("Sound Example", "1.0", args);

        SoundTest sound = new SoundTest();

    }
}
