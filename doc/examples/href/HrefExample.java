package href;

import org.gnu.gnome.App;
import org.gnu.gnome.HRef;
import org.gnu.gnome.Program;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

/**
 * Example demonstrating the usage of the HRef widget.
 */
public class HrefExample {

    public HrefExample() {

        App app = new App("href", "Href example");
        app.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DELETE)
                        || event.isOfType(LifeCycleEvent.Type.DESTROY))
                    Gtk.mainQuit();
                return false;
            }
        });
        app.setBorderWidth(20);

        HRef hr = new HRef("http://java-gnome.sourceforge.net",
                "Java-GNOME site");
        app.setContent(hr);

        app.showAll();
    }

    public static void main(String[] args) {

        Program.initGnomeUI("Href", "1.0", args);

        HrefExample href = new HrefExample();

        Gtk.main();
    }
}
