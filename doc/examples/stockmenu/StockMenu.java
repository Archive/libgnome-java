package stockmenu;

import org.gnu.gnome.App;
import org.gnu.gnome.Program;
import org.gnu.gnome.UIInfo;
import org.gnu.gtk.AboutDialog;
import org.gnu.gtk.ButtonsType;
import org.gnu.gtk.DialogFlags;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.MessageDialog;
import org.gnu.gtk.MessageType;
import org.gnu.gtk.StatusBar;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.MenuItemEvent;
import org.gnu.gtk.event.MenuItemListener;

public class StockMenu implements MenuItemListener
{

	private App	app;

	public StockMenu() {

		app = new App("stockmenu", "Stock Menu");
		app.setMinimumSize(400, 300);
		app.addListener(new LifeCycleListener() {
			public void lifeCycleEvent(LifeCycleEvent event) {}

			public boolean lifeCycleQuery(LifeCycleEvent event) {
				if (event.isOfType(LifeCycleEvent.Type.DELETE)
					|| event.isOfType(LifeCycleEvent.Type.DESTROY))
					Gtk.mainQuit();
				return false;
			}
		});

		UIInfo menuInfo[] = buildMyMenus();
		app.createMenus(menuInfo);
		StatusBar statusbar = new StatusBar();
		app.setStatusBar(statusbar);
		// app.installMenuHints(menuInfo);

		app.showAll();
	}

	public void menuItemEvent(MenuItemEvent event) {
		MessageDialog dialog = new MessageDialog(app, DialogFlags.MODAL, MessageType.INFO,
			ButtonsType.CLOSE, "Not implemented...", false);
		dialog.run();
		dialog.destroy();
	}

	public void helpAbout() {
		String title = "My about";
		String version = "Version 0.0.0";
		String license = "GPL";
		String comments = "My comments go here";
		String[] authors = {
			"Me",
			"Myself",
			"I"
		};
		String[] documenters = {
			"Me",
			"Myself",
			"I"
		};
		String translator = "Porkey";

		AboutDialog about = new AboutDialog();

		about.setVersion(version);
		about.setAuthors(authors);
		about.setTranslatorCredits(translator);
		about.setTitle(title);
		about.setLicense(license);
		about.setComments(comments);
		about.setDocumenters(documenters);
		about.show();
		about.show();
	}

	// build the menus for this app.
	protected UIInfo[] buildMyMenus() {

		UIInfo dummyMenu[] = {
			UIInfo.item("Dummy", "A placeholder menu entry", this),
			UIInfo.end()
		};

		UIInfo fileMenu[] = {
			UIInfo.newItem("New Window", "Open a new application window", this),
			UIInfo.newSubtree(dummyMenu),
			UIInfo.separator(),
			UIInfo.openItem(this),
			UIInfo.saveItem(this),
			UIInfo.saveAsItem(this),
			UIInfo.revertItem(this),
			UIInfo.printItem(this),
			UIInfo.printSetupItem(this),
			UIInfo.separator(),
			UIInfo.closeItem(this),
			UIInfo.quitItem(this),
			UIInfo.end()
		};

		UIInfo editMenu[] = {
			UIInfo.undoItem(this),
			UIInfo.redoItem(this),
			UIInfo.separator(),
			UIInfo.cutItem(this),
			UIInfo.copyItem(this),
			UIInfo.pasteItem(this),
			UIInfo.clearItem(this),
			UIInfo.selectAllItem(this),
			UIInfo.separator(),
			UIInfo.findItem(this),
			UIInfo.findAgainItem(this),
			UIInfo.replaceItem(this),
			UIInfo.propertiesItem(this),
			UIInfo.end()
		};

		UIInfo gameMenu[] = {
			UIInfo.newGameItem(this),
			UIInfo.pauseGameItem(this),
			UIInfo.restartGameItem(this),
			UIInfo.undoMoveItem(this),
			UIInfo.redoMoveItem(this),
			UIInfo.hintItem(this),
			UIInfo.scoresItem(this),
			UIInfo.endGameItem(this),
			UIInfo.end()
		};

		UIInfo settingsMenu[] = {
			UIInfo.preferencesItem(this),
			UIInfo.end()
		};

		UIInfo windowsMenu[] = {
			UIInfo.newWindowItem(this),
			UIInfo.closeWindowItem(this),
			UIInfo.end()
		};

		UIInfo helpMenu[] = {
			// UIInfo.help("stockmenu"),
			UIInfo.aboutItem(new MenuItemListener() {
				public void menuItemEvent(MenuItemEvent event) {
					helpAbout();
				}
			}),
			UIInfo.end()
		};

		UIInfo mainMenu[] = {
			UIInfo.subtree("_File", fileMenu),
			UIInfo.subtree("_Edit", editMenu),
			UIInfo.subtree("_Game", gameMenu),
			UIInfo.subtree("_Settings", settingsMenu),
			UIInfo.subtree("_Windows", windowsMenu),
			UIInfo.subtree("_Help", helpMenu),
			UIInfo.end()
		};

		return mainMenu;
	}

	public static void main(String[] args) {

		Program.initGnomeUI("StockMenu", "1.0", args);

		new StockMenu();

		Gtk.main();
	}
}
