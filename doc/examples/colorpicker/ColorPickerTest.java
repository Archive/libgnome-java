package colorpicker;

/*
 * The class org.gnu.gnome.ColorPicker, that was in 
 * previous version of this example is deprecated.
 * Instead of it we will use org.gnu.gtk.ColorButton.
 * 
 * 10 Aug 2006 - michal.pryc@sun.com
 */
import java.text.NumberFormat;

import org.gnu.gdk.Color;
import org.gnu.gnome.App;
import org.gnu.gtk.ColorButton;
import org.gnu.gnome.Program;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class ColorPickerTest implements ButtonListener
{

	ColorButton	colorPicker;

	public ColorPickerTest() {

		App app = new App("colorpicker", "ColorPicker example");
		app.addListener(new LifeCycleListener() {
			public void lifeCycleEvent(LifeCycleEvent event) {}

			public boolean lifeCycleQuery(LifeCycleEvent event) {
				if (event.isOfType(LifeCycleEvent.Type.DELETE)
					|| event.isOfType(LifeCycleEvent.Type.DESTROY))
					Gtk.mainQuit();
				return false;
			}
		});
		app.setBorderWidth(20);

		colorPicker = new ColorButton();
		colorPicker.setColor(Color.BLUE);
		colorPicker.addListener(this);
		app.setContent(colorPicker);

		System.out.println("The initial values are: 0.3, 0.2, 0.75, 1.0");

		app.showAll();
	}

	public void buttonEvent(ButtonEvent be) {
		Object o = be.getSource();
		if (o == colorPicker) {
			StringBuffer sb = new StringBuffer("The updated values are: \n");
			NumberFormat nf = NumberFormat.getInstance();
			nf.setMaximumFractionDigits(3);
			sb.append(colorPicker.getColor());
			System.out.println(sb.toString());
		}
	}

	public static void main(String[] args) {

		Program.initGnomeUI("ColorPickerTest", "1.0", args);

		new ColorPickerTest();

		Gtk.main();
	}

}
