#!/bin/bash
#
# A small script to help run example apps
#

prefix=@prefix@
exec_prefix=@exec_prefix@
libdir=@libdir@
classpath=@INSTALLED_CLASSPATH@

GNOMEJAVA_LIB=$libdir
GNOMEJAVA_JAR=$classpath

# You should not need to modify anything below here

error_message ()
{
	echo
	echo
	echo "Usage: ./runExample.sh CLASSNAME [ARGS]"
		echo "Please first ensure you have a valid java interpreter and compiler" 
	echo "in your \$PATH and you have compiled java-gnome libraries and jars, then" 
	echo "run an application by passing it's classname to this script, e.g.:"
	echo
	echo "$0 progressbar/ProgressBarTest"
	echo
	exit 1;
}

echo "Java-Gnome Example Application Launcher"
echo

GLIBJAVA_JAR=`pkg-config --variable classpath glib-java`
if ! test -n "$GLIBJAVA_JAR"; then
	echo "Package glib-java was not found in the pkg-config search path."
	echo "Perhaps you should add the directory containing 'glib-java.pc'"
	echo "to the PKG_CONFIG_PATH environment variable"
	echo "No package 'glib-java' found"
	exit 1
fi

GLIBJAVA_LIB=`pkg-config --variable libdir glib-java`
if ! test -n "$GLIBJAVA_LIB"; then
	echo "Library requirements (libglibjni >= 0.2) not met;"
	echo "consider adjusting the PKG_CONFIG_PATH environment" 
	echo "variable if your libraries are in a nonstandard prefix" 
	echo "so pkg-config can find them."
	exit 1
fi

GTKJAVA_JAR=`pkg-config --variable classpath gtk2-java`
if ! test -n "$GTKJAVA_JAR"; then
	echo "Package gtk2-java was not found in the pkg-config search path."
	echo "Perhaps you should add the directory containing 'gtk2-java.pc'"
	echo "to the PKG_CONFIG_PATH environment variable"
	echo "No package 'gtk2-java' found"
	exit 1
fi

GTKJAVA_LIB=`pkg-config --variable libdir gtk2-java`
if ! test -n "$GTKJAVA_LIB"; then
	echo "Library requirements (libgtkjava2.4) not met;"
	echo "consider adjusting the PKG_CONFIG_PATH environment" 
	echo "variable if your libraries are in a nonstandard prefix" 
	echo "so pkg-config can find them."
	exit 1
fi

NEED_LIB="$GLIBJAVA_LIB $GTKJAVA_LIB $GNOMEJAVA_LIB"
NEED_JAR="$GLIBJAVA_JAR $GTKJAVA_JAR $GNOMEJAVA_JAR"

echo "Checking for jvm and compiler availability..."

TESTGCJ="`which gcj 2> /dev/null`"
TESTGCJ="`basename "$TESTGCJ"`"
TESTJAVAC="`which javac 2> /dev/null`"
TESTJAVAC="`basename "$TESTJAVAC"`"

if [ "$TESTGCJ" == "gcj" ]; then
	JAVAC="gcj -C"
elif [ "$TESTJAVAC" == "javac" ]; then
	JAVAC=javac
else
	echo "ERROR: no valid compiler was found in your \$PATH"
	error_message
fi

TESTGIJ="`which gij 2> /dev/null`"
TESTGIJ="`basename "$TESTGIJ"`"
TESTJAVA="`which java 2> /dev/null`"
TESTJAVA="`basename "$TESTJAVA"`"

#Try to use gij first, then java
if [ "$TESTGIJ" == "gij" ]; then
	JAVA=gij
elif [ "$TESTJAVA" == "java" ]; then
	JAVA=java
else
	echo "ERROR: no valid interpreter was found in your \$PATH"
	error_message
fi

echo "	`echo $JAVAC | awk '{print $1}'` and $JAVA - appear to be ok"

#Praise the users who use Free Software
if [ "$JAVAC" == "gcj -C" -a "$JAVA" == "gij" ]; then
	echo
	echo "	You are using Open Source java" 
	echo "	virtual machine and compiler: this is so good!"
fi

echo
echo "Checking if library paths exist..."

for NEEDLIB in $NEED_LIB; do
	if test -e $NEEDLIB; then echo "	$NEEDLIB - appears to be ok"; fi;
	if ! test -e $NEEDLIB; then echo "	$NEEDLIB - DOES NOT EXIST" && error_message; fi;
done;

echo
echo "Testing JAR archives..."
for JAR in $NEED_JAR; do
	if test -e $JAR; then
		echo "	$JAR - appears to be ok";
	else
		echo "	$JAR - DOES NOT EXIST";
	error_message
	fi;
done;

echo

if ! test -e $1.java ; then
	echo "ERROR: unexpected example app";
	echo
	error_message
fi

#do the real work
export CLASSPATH=.:$CLASSPATH:`echo $NEED_JAR | sed s'/ /:/g'`
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`echo $NEED_LIB | sed s'/ /:/g'`

($JAVAC $1.java && echo "Compilation successful") || (echo "ERROR: Compilation failed"; error_message)

TARGET=`echo $1 | sed 's/\//\./g'`
shift
($JAVA $TARGET $@ && echo "Application successfully terminated") || (echo
"Application terminated abnormally"; error_message)
