/**
 *This program uses Class FileEntry(deprecated)that can be replaced by Interface FileChooser.
 *It won't stop the program compiling
 *The same function can be implemented as the examples-gtk.filechooser
 *4 Aug 2006
 *xusheng.hou@sun.com
 */
package fileentry;

import org.gnu.gnome.App;
import org.gnu.gnome.FileEntry;
import org.gnu.gnome.Program;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class FileEntryTest
{

	public FileEntryTest() {

		App app = new App("fileentry", "FileEntry example");
		app.addListener(new LifeCycleListener() {
			public void lifeCycleEvent(LifeCycleEvent event) {}

			public boolean lifeCycleQuery(LifeCycleEvent event) {
				if (event.isOfType(LifeCycleEvent.Type.DELETE)
					|| event.isOfType(LifeCycleEvent.Type.DESTROY))
					Gtk.mainQuit();
				return false;
			}
		});
		app.setBorderWidth(20);

		// The "fseekid" is the ID used to save the list of
		// previously selected filenames. the second string
		// is for the title bar on the GtkFileSelection window.
		FileEntry fileentry = new FileEntry("fseekid", "File Seeking");
		app.setContent(fileentry);

		app.showAll();
	}

	public static void main(String[] args) {

		Program.initGnomeUI("FileEntry", "1.0", args);

		FileEntryTest fe = new FileEntryTest();

		Gtk.main();
	}
}
