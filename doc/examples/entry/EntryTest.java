package entry;

/*
 * 
 * This example uses deprecated class  org.gnu.gnome.Entry
 * Please look at the EntryDemo.java example under
 * examples-gtk/entry folder
 * 
 *  Fri Aug 04 2006 - michal.pryc@sun.com
 *  
 */

import org.gnu.gnome.App;
import org.gnu.gnome.Entry;
import org.gnu.gnome.Program;
import org.gnu.gtk.Button;
import org.gnu.gtk.ButtonBox;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.HButtonBox;
import org.gnu.gtk.HSeparator;
import org.gnu.gtk.VBox;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.EntryEvent;
import org.gnu.gtk.event.EntryListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class EntryTest
{

	Entry	entry;

	public EntryTest() {
		App app = new App("entry", "Entry example");
		app.setBorderWidth(20);
		app.addListener(new LifeCycleListener() {
			public void lifeCycleEvent(LifeCycleEvent event) {}

			public boolean lifeCycleQuery(LifeCycleEvent event) {
				if (event.isOfType(LifeCycleEvent.Type.DELETE)
					|| event.isOfType(LifeCycleEvent.Type.DESTROY))
					Gtk.mainQuit();
				return false;
			}
		});

		VBox vbox = new VBox(false, 10);
		// The Entry widget accepts user input; once
		// accepted, the entry is included in the history
		// of items previously entered. You can save
		// the history information so it can be restored
		// the enxt time the widget appears. In this example
		// you will notice that the items appended will
		// appear twice the second time, etc... This is
		// because we save the hisotry with the call to
		// saveHistory(). Each new invocation three new
		// entries are added to the three entries save from
		// the previous execution.
		entry = new Entry("idstring");
		entry.appendHistory("First Append", true);
		entry.appendHistory("Second Apped", true);
		entry.appendHistory("Third Append", true);

		// Handling of the entryChanged event is not yet implemented
		entry.getEntry().addListener(new EntryListener() {
			public void entryEvent(EntryEvent event) {
				if (event.isOfType(EntryEvent.Type.CHANGED))
					entryChanged();
			}
		});
		vbox.add(entry);

		// Add a separator
		HSeparator hsep = new HSeparator();
		vbox.add(hsep);

		// Add a pair of buttons
		ButtonBox bbox = new HButtonBox();

		Button button = new Button("_Clear History", true);
		button.addListener(new ButtonListener() {
			public void buttonEvent(ButtonEvent event) {
				if (event.isOfType(ButtonEvent.Type.CLICK))
					entry.clearHistory();
			}
		});
		bbox.add(button);

		button = new Button("_Quit", true);
		button.addListener(new ButtonListener() {
			public void buttonEvent(ButtonEvent event) {
				if (event.isOfType(ButtonEvent.Type.CLICK))
					Gtk.mainQuit();
			}
		});
		bbox.add(button);
		vbox.add(bbox);

		app.setContent(vbox);
		app.showAll();
	}

	public void entryChanged() {
		System.out.println("value changed - new value is: " + entry.getEntry().getText());
	}

	public static void main(String[] args) {
		Program.initGnomeUI("Entry", "1.0", args);

		EntryTest ent = new EntryTest();

		Gtk.main();
	}
}
