package radiotoolbar;

import org.gnu.gnome.App;
import org.gnu.gnome.GnomeStockItem;
import org.gnu.gnome.Program;
import org.gnu.gnome.UIInfo;
import org.gnu.gtk.ButtonsType;
import org.gnu.gtk.DialogFlags;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.MessageDialog;
import org.gnu.gtk.MessageType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class RadioToolbar implements ButtonListener
{

	protected App		app;

	protected boolean	radioOneEnabled		= true;

	protected boolean	radioTwoEnabled		= false;

	protected boolean	radioThreeEnabled	= false;

	protected boolean	toggleOneEnabled	= false;

	protected boolean	toggleTwoEnabled	= false;

	protected boolean	toggleThreeEnabled	= false;

	public RadioToolbar() {

		app = new App("radiotoolbar", "Radio Toolbar");
		app.addListener(new LifeCycleListener() {
			public void lifeCycleEvent(LifeCycleEvent event) {}

			public boolean lifeCycleQuery(LifeCycleEvent event) {
				if (event.isOfType(LifeCycleEvent.Type.DELETE)
					|| event.isOfType(LifeCycleEvent.Type.DESTROY))
					Gtk.mainQuit();
				return false;
			}
		});

		UIInfo toolbar[] = buildMyToolbar();
		app.createToolBar(toolbar);

		app.showAll();
	}

	public void buttonEvent(ButtonEvent event) {
		if (event.isOfType(ButtonEvent.Type.CLICK))
			showInfoMessage("Not implemented...");
	}

	public void radioOne() {
		radioOneEnabled = !radioOneEnabled;
		if (radioOneEnabled)
			showInfoMessage("Radio One Enabled");
	}

	public void radioTwo() {
		radioTwoEnabled = !radioTwoEnabled;
		if (radioTwoEnabled)
			showInfoMessage("Radio Two Enabled");
	}

	public void radioThree() {
		radioThreeEnabled = !radioThreeEnabled;
		if (radioThreeEnabled)
			showInfoMessage("Radio Three Enabled");
	}

	public void toggleOne() {
		toggleOneEnabled = !toggleOneEnabled;
		if (toggleOneEnabled)
			showInfoMessage("Toggle One Enabled");
		else
			showInfoMessage("Toggle One Disabled");
	}

	public void toggleTwo() {
		toggleTwoEnabled = !toggleTwoEnabled;
		if (toggleTwoEnabled)
			showInfoMessage("Toggle Two Enabled");
		else
			showInfoMessage("Toggle Two Disabled");
	}

	public void toggleThree() {
		toggleThreeEnabled = !toggleThreeEnabled;
		if (toggleThreeEnabled)
			showInfoMessage("Toggle Three Enabled");
		else
			showInfoMessage("Toggle Three Disabled");
	}

	protected UIInfo[] buildMyToolbar() {
		UIInfo radioList[] = {
			UIInfo.radioItemStock("Radio One", "The first radio button", new ButtonListener() {
				public void buttonEvent(ButtonEvent event) {
					if (event.isOfType(ButtonEvent.Type.CLICK)) {
						radioOne();
					}
				}
			}, GnomeStockItem.GO_UP),
			UIInfo.radioItemStock("Radio Two", "The second radio button", new ButtonListener() {
				public void buttonEvent(ButtonEvent event) {
					if (event.isOfType(ButtonEvent.Type.CLICK))
						radioTwo();
				}
			}, GnomeStockItem.GO_DOWN),
			UIInfo.radioItemStock("Radio Three", "The third radio button", new ButtonListener() {
				public void buttonEvent(ButtonEvent event) {
					if (event.isOfType(ButtonEvent.Type.CLICK))
						radioThree();
				}
			}, GnomeStockItem.ATTACH),
			UIInfo.end()
		};

		UIInfo toolbar[] = {
			UIInfo.radioList(radioList),
			UIInfo.separator(),
			UIInfo.toggleItemStock("Toggle One", "The first toggle button", new ButtonListener() {
				public void buttonEvent(ButtonEvent event) {
					if (event.isOfType(ButtonEvent.Type.CLICK))
						toggleOne();
				}
			}, GnomeStockItem.GO_UP),
			UIInfo.toggleItemStock("Toggle Two", "The second toggle button", new ButtonListener() {
				public void buttonEvent(ButtonEvent event) {
					if (event.isOfType(ButtonEvent.Type.CLICK))
						toggleTwo();
				}
			}, GnomeStockItem.GO_DOWN),
			UIInfo.toggleItemStock("Toggle Three", "The third toggle button", new ButtonListener() {
				public void buttonEvent(ButtonEvent event) {
					if (event.isOfType(ButtonEvent.Type.CLICK))
						toggleThree();
				}
			}, GnomeStockItem.ATTACH),
			UIInfo.end()
		};

		return toolbar;
	}

	private void showInfoMessage(String message) {
		MessageDialog dialog = new MessageDialog(app, DialogFlags.MODAL, MessageType.INFO,
			ButtonsType.CLOSE, message, false);
		dialog.run();
		dialog.destroy();
	}

	public static void main(String[] args) {

		Program.initGnomeUI("RadioToolbar", "1.0", args);

		new RadioToolbar();

		Gtk.main();
	}
}
