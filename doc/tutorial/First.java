import org.gnu.gnome.App;
import org.gnu.gnome.Program;
import org.gnu.gtk.Gtk;

public class First {
    public static void main(String[] args) {
        // Initialization
        Program.initGnomeUI("First", "0.1", args);

        App app = new App("First", "First App");
        app.show();

        Gtk.main();
    }
}
