/**
 * This is the second application used in the Java-GNOME tutorial.
 * It demonstrates the use of callbacks and events.  It also
 * introduces the user to menus.
 */

import org.gnu.gnome.App;
import org.gnu.gnome.Program;
import org.gnu.gnome.UIInfo;
import org.gnu.gtk.AboutDialog;
import org.gnu.gtk.ButtonsType;
import org.gnu.gtk.DialogFlags;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.MessageDialog;
import org.gnu.gtk.MessageType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.MenuItemEvent;
import org.gnu.gtk.event.MenuItemListener;

public class Second implements MenuItemListener {
    private App app = null;

    public static final String appVersion = "0.1";

    public Second() {
        createMainWindow();
        createMenus();
        app.showAll();
    }

    private void createMainWindow() {
        app = new App("Second", "Second App");
        app.setDefaultSize(200, 200);
        app.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                Gtk.mainQuit();
                return false;
            }
        });
    }

    private void createMenus() {

        UIInfo fileMenu[] = {
                UIInfo.newItem("New Window", "Open a new application window",
                        this), UIInfo.separator(), UIInfo.openItem(this),
                UIInfo.saveItem(this), UIInfo.saveAsItem(this),
                UIInfo.separator(), UIInfo.closeItem(this),
                UIInfo.quitItem(new MenuItemListener() {
                    public void menuItemEvent(MenuItemEvent event) {
                        fileExit();
                    }
                }), UIInfo.end() };

        UIInfo editMenu[] = { UIInfo.undoItem(this), UIInfo.redoItem(this),
                UIInfo.separator(), UIInfo.cutItem(this),
                UIInfo.copyItem(this), UIInfo.pasteItem(this),
                UIInfo.separator(), UIInfo.findItem(this),
                UIInfo.findAgainItem(this), UIInfo.replaceItem(this),
                UIInfo.propertiesItem(this), UIInfo.end() };

        UIInfo moveMenu[] = { UIInfo.item("_Up", "Move selection up", this),
                UIInfo.item("D_own", "Move selection down", this), UIInfo.end() };

        UIInfo helpMenu[] = { UIInfo.help("second"),
                UIInfo.aboutItem(new MenuItemListener() {
                    public void menuItemEvent(MenuItemEvent event) {
                        helpAbout();
                    }
                }), UIInfo.end() };

        UIInfo mainMenu[] = { UIInfo.subtree("_File", fileMenu),
                UIInfo.subtree("_Edit", editMenu),
                UIInfo.subtree("_Move", moveMenu),
                UIInfo.subtree("_Help", helpMenu), UIInfo.end() };

        app.createMenus(mainMenu);
    }

    public void helpAbout() {
        String appname = "Java-GNOME Tutorial";
        String version = "0.1";
        String license = "GPL";
        String description = "Java-GNOME Tutorial.";
        String authors[] = { "http://java-gnome.sf.net", "http://www.gtk.org" };
        String documentors[] = { "<java-gnome-developer@lists.sf.net>",
                "http://www.gnome.org" };
        String translators = "Language Guys Inc.";
        String website = "http://java-gnome.sf.net";

        AboutDialog about = new AboutDialog();
        about.setName(appname);
        about.setVersion(version);
        about.setLicense(license);
        about.setComments(description);
        about.setAuthors(authors);
        about.setDocumenters(documentors);
        about.setTranslatorCredits(translators);
        about.setWebsite(website);
        about.show();
    }

    public void fileExit() {
        Gtk.mainQuit();
    }

    public void menuItemEvent(MenuItemEvent event) {
        MessageDialog dialog = new MessageDialog(app, DialogFlags.MODAL,
                MessageType.INFO, ButtonsType.OK, "Not implemented", false);
        dialog.run();
        dialog.destroy();
    }

    public static void main(String[] args) {
        Program.initGnomeUI("Second", Second.appVersion, args);
        new Second();
        Gtk.main();
    }
}
