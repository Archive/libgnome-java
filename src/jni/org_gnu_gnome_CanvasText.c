/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <libgnomecanvas/libgnomecanvas.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_CanvasText.h"

#ifdef __cplusplus
extern "C" 
{
#endif

static gchar * GnomeCanvasText_get_text (GnomeCanvasText * cptr) 
{
    return cptr->text;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getText
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_CanvasText_getText (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    gchar *result_g = (gchar*)GnomeCanvasText_get_text (cptr_g);
    return (*env)->NewStringUTF(env, result_g);
}

static void GnomeCanvasText_set_text (GnomeCanvasText * cptr, gchar * text) 
{
    cptr->text = text;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setText
 */
  JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setText (JNIEnv *env, jclass cls, jobject cptr, 
    jstring text) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    gchar* text_g = (gchar*)(*env)->GetStringUTFChars(env, text, 0);
    GnomeCanvasText_set_text (cptr_g, text_g);
    (*env)->ReleaseStringUTFChars(env, text, text_g);
}

static gdouble GnomeCanvasText_get_x (GnomeCanvasText * cptr) 
{
    return cptr->x;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getX
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasText_getX (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasText_get_x (cptr_g));
}

static void GnomeCanvasText_set_x (GnomeCanvasText * cptr, gdouble x) 
{
    cptr->x = x;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setX
 */
  JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setX (JNIEnv *env, jclass cls,  jobject cptr, jdouble x) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    gdouble x_g = (gdouble) x;
    GnomeCanvasText_set_x (cptr_g, x_g);
}

static gdouble GnomeCanvasText_get_y (GnomeCanvasText * cptr) 
{
    return cptr->y;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getY
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasText_getY (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasText_get_y (cptr_g));
}

static void GnomeCanvasText_set_y (GnomeCanvasText * cptr, gdouble y) 
{
    cptr->y = y;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setY
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setY (JNIEnv *env, jclass cls, jobject cptr, jdouble y) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    gdouble y_g = (gdouble) y;
    GnomeCanvasText_set_y (cptr_g, y_g);
}

static GtkAnchorType GnomeCanvasText_get_anchor (GnomeCanvasText * cptr) 
{
    return cptr->anchor;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getAnchor
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasText_getAnchor (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasText_get_anchor (cptr_g));
}

static void GnomeCanvasText_set_anchor (GnomeCanvasText * cptr, GtkAnchorType anchor) 
{
    cptr->anchor = anchor;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setAnchor
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setAnchor (JNIEnv *env, jclass cls, jobject cptr, jint 
    anchor) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    GtkAnchorType anchor_g = (GtkAnchorType) anchor;
    GnomeCanvasText_set_anchor (cptr_g, anchor_g);
}

static GtkJustification GnomeCanvasText_get_justification (GnomeCanvasText * cptr) 
{
    return cptr->justification;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getJustification
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasText_getJustification (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasText_get_justification (cptr_g));
}

static void GnomeCanvasText_set_justification (GnomeCanvasText * cptr, GtkJustification justification) 
{
    cptr->justification = justification;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setJustification
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setJustification (JNIEnv *env,jclass cls, jobject cptr, 
    jint justification) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    GtkJustification justification_g = (GtkJustification) justification;
    GnomeCanvasText_set_justification (cptr_g, justification_g);
}

static gdouble GnomeCanvasText_get_clip_width (GnomeCanvasText * cptr) 
{
    return cptr->clip_width;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getClipWidth
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasText_getClipWidth (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasText_get_clip_width (cptr_g));
}

static void GnomeCanvasText_set_clip_width (GnomeCanvasText * cptr, gdouble clip_width) 
{
    cptr->clip_width = clip_width;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setClipWidth
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setClipWidth (JNIEnv *env, jobject cptr, jclass cls,
    jdouble clip_width) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    gdouble clip_width_g = (gdouble) clip_width;
    GnomeCanvasText_set_clip_width (cptr_g, clip_width_g);
}

static gdouble GnomeCanvasText_get_clip_height (GnomeCanvasText * cptr) 
{
    return cptr->clip_height;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getClipHeight
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasText_getClipHeight (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasText_get_clip_height (cptr_g));
}

static void GnomeCanvasText_set_clip_height (GnomeCanvasText * cptr, gdouble clip_height) 
{
    cptr->clip_height = clip_height;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setClipHeight
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setClipHeight (JNIEnv *env, jobject cptr, jclass cls,
    jdouble clip_height) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    gdouble clip_height_g = (gdouble) clip_height;
    GnomeCanvasText_set_clip_height (cptr_g, clip_height_g);
}

static glong GnomeCanvasText_get_pixel (GnomeCanvasText * cptr) 
{
    return cptr->pixel;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getPixel
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnome_CanvasText_getPixel (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jlong) (GnomeCanvasText_get_pixel (cptr_g));
}

static void GnomeCanvasText_set_pixel (GnomeCanvasText * cptr, glong pixel) 
{
    cptr->pixel = pixel;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setPixel
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setPixel (JNIEnv *env, jclass cls, jobject cptr, jlong 
    pixel) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    glong pixel_g = (glong) pixel;
    GnomeCanvasText_set_pixel (cptr_g, pixel_g);
}

static GdkBitmap * GnomeCanvasText_get_stipple (GnomeCanvasText * cptr) 
{
    return cptr->stipple;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getStipple
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasText_getStipple (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return getGObjectHandleAndRef(env, G_OBJECT(GnomeCanvasText_get_stipple (cptr_g)));
}

static void GnomeCanvasText_set_stipple (GnomeCanvasText * cptr, GdkBitmap * stipple) 
{
    cptr->stipple = stipple;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setStipple
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setStipple (JNIEnv *env, jclass cls, jobject cptr, jobject 
    stipple) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    GdkBitmap *stipple_g = (GdkBitmap *)getPointerFromHandle(env, stipple);
    GnomeCanvasText_set_stipple (cptr_g, stipple_g);
}

static GdkGC * GnomeCanvasText_get_gc (GnomeCanvasText * cptr) 
{
    return cptr->gc;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getGc
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasText_getGc (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return getGObjectHandleAndRef(env, G_OBJECT(GnomeCanvasText_get_gc (cptr_g)));
}

static gint32 GnomeCanvasText_get_max_width (GnomeCanvasText * cptr) 
{
    return cptr->max_width;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getMaxWidth
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasText_getMaxWidth (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasText_get_max_width (cptr_g));
}

static gint32 GnomeCanvasText_get_height (GnomeCanvasText * cptr) 
{
    return cptr->height;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getHeight
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasText_getHeight (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasText_get_height (cptr_g));
}

static gboolean GnomeCanvasText_get_clip (GnomeCanvasText * cptr) 
{
    return cptr->clip;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getClip
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_CanvasText_getClip (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jboolean) (GnomeCanvasText_get_clip (cptr_g));
}

static void GnomeCanvasText_set_clip (GnomeCanvasText * cptr, gboolean clip) 
{
    cptr->clip = clip;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setClip
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setClip (JNIEnv *env, jclass cls, jobject cptr, jboolean 
    clip) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    gboolean clip_g = (gboolean) clip;
    GnomeCanvasText_set_clip (cptr_g, clip_g);
}

static PangoFontDescription * GnomeCanvasText_get_font_desc (GnomeCanvasText * cptr) 
{
    return cptr->font_desc;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getFontDesc
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasText_getFontDesc (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return getGBoxedHandle(env, GnomeCanvasText_get_font_desc (cptr_g),
    		PANGO_TYPE_FONT_DESCRIPTION,
    		(GBoxedCopyFunc) pango_font_description_copy,
    		(GBoxedFreeFunc) pango_font_description_free);
}

static void GnomeCanvasText_set_font_desc (GnomeCanvasText * cptr, PangoFontDescription * font_desc) 
{
    cptr->font_desc = font_desc;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setFontDesc
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setFontDesc (JNIEnv *env, jclass cls, jobject cptr, jobject 
    font_desc) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    PangoFontDescription *font_desc_g = (PangoFontDescription *)getPointerFromHandle(env, font_desc);
    GnomeCanvasText_set_font_desc (cptr_g, font_desc_g);
}

static PangoAttrList * GnomeCanvasText_get_attr_list (GnomeCanvasText * cptr) 
{
    return cptr->attr_list;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getAttrList
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasText_getAttrList (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return getGBoxedHandle(env, GnomeCanvasText_get_attr_list (cptr_g),
    		PANGO_TYPE_ATTR_LIST, (GBoxedCopyFunc) pango_attr_list_copy,
    		(GBoxedFreeFunc) pango_attr_list_unref);
}

static void GnomeCanvasText_set_attr_list (GnomeCanvasText * cptr, PangoAttrList * attr_list) 
{
    cptr->attr_list = attr_list;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setAttrList
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setAttrList (JNIEnv *env, jclass cls, jobject cptr, jobject 
    attr_list) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    PangoAttrList *attr_list_g = (PangoAttrList *)getPointerFromHandle(env, attr_list);
    GnomeCanvasText_set_attr_list (cptr_g, attr_list_g);
}

static PangoUnderline GnomeCanvasText_get_underline (GnomeCanvasText * cptr) 
{
    return cptr->underline;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getUnderline
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasText_getUnderline (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasText_get_underline (cptr_g));
}

static void GnomeCanvasText_set_underline (GnomeCanvasText * cptr, PangoUnderline underline) 
{
    cptr->underline = underline;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setUnderline
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setUnderline (JNIEnv *env, jclass cls, jobject cptr, jint 
    underline) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    PangoUnderline underline_g = (PangoUnderline) underline;
    GnomeCanvasText_set_underline (cptr_g, underline_g);
}

static gboolean GnomeCanvasText_get_strikethrough (GnomeCanvasText * cptr) 
{
    return cptr->strikethrough;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getStrikethrough
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_CanvasText_getStrikethrough (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jboolean) (GnomeCanvasText_get_strikethrough (cptr_g));
}

static void GnomeCanvasText_set_strikethrough (GnomeCanvasText * cptr, gboolean strikethrough) 
{
    cptr->strikethrough = strikethrough;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setStrikethrough
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setStrikethrough (JNIEnv *env, jclass cls, jobject cptr, 
    jboolean strikethrough) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    gboolean strikethrough_g = (gboolean) strikethrough;
    GnomeCanvasText_set_strikethrough (cptr_g, strikethrough_g);
}

static gint32 GnomeCanvasText_get_rise (GnomeCanvasText * cptr) 
{
    return cptr->rise;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getRise
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasText_getRise (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasText_get_rise (cptr_g));
}

static void GnomeCanvasText_set_rise (GnomeCanvasText * cptr, gint32 rise) 
{
    cptr->rise = rise;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setRise
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setRise (JNIEnv *env, jclass cls, jobject cptr, jint rise)
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    gint32 rise_g = (gint32) rise;
    GnomeCanvasText_set_rise (cptr_g, rise_g);
}

static gdouble GnomeCanvasText_get_scale (GnomeCanvasText * cptr) 
{
    return cptr->scale;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getScale
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasText_getScale (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasText_get_scale (cptr_g));
}

static void GnomeCanvasText_set_scale (GnomeCanvasText * cptr, gdouble scale) 
{
    cptr->scale = scale;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setScale
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setScale (JNIEnv *env, jclass cls, jobject cptr, jdouble 
    scale) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    gdouble scale_g = (gdouble) scale;
    GnomeCanvasText_set_scale (cptr_g, scale_g);
}

static PangoLayout * GnomeCanvasText_get_layout (GnomeCanvasText * cptr) 
{
    return cptr->layout;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getLayout
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasText_getLayout (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return getGObjectHandleAndRef(env, G_OBJECT(GnomeCanvasText_get_layout (cptr_g)));
}

static void GnomeCanvasText_set_layout (GnomeCanvasText * cptr, PangoLayout * layout) 
{
    cptr->layout = layout;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    setLayout
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasText_setLayout (JNIEnv *env, jclass cls, jobject cptr, jobject 
    layout) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    GnomeCanvasText_set_layout (cptr_g, layout_g);
}

static gdouble GnomeCanvasText_get_xofs (GnomeCanvasText * cptr) 
{
    return cptr->xofs;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getXofs
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasText_getXofs (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasText_get_xofs (cptr_g));
}

static gdouble GnomeCanvasText_get_yofs (GnomeCanvasText * cptr) 
{
    return cptr->yofs;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getYofs
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasText_getYofs (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasText_get_yofs (cptr_g));
}

static gint32 GnomeCanvasText_get_cx (GnomeCanvasText * cptr) 
{
    return cptr->cx;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getCx
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasText_getCx (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasText_get_cx (cptr_g));
}

static gint32 GnomeCanvasText_get_cy (GnomeCanvasText * cptr) 
{
    return cptr->cy;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getCy
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasText_getCy (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasText_get_cy (cptr_g));
}

static gboolean GnomeCanvasText_get_underline_set (GnomeCanvasText * cptr) 
{
    return cptr->underline_set;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getUnderlineSet
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_CanvasText_getUnderlineSet (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jboolean) (GnomeCanvasText_get_underline_set (cptr_g));
}

static gboolean GnomeCanvasText_get_strike_set (GnomeCanvasText * cptr) 
{
    return cptr->strike_set;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getStrikeSet
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_CanvasText_getStrikeSet (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jboolean) (GnomeCanvasText_get_strike_set (cptr_g));
}

static gboolean GnomeCanvasText_get_rise_set (GnomeCanvasText * cptr) 
{
    return cptr->rise_set;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getRiseSet
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_CanvasText_getRiseSet (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jboolean) (GnomeCanvasText_get_rise_set (cptr_g));
}

static gboolean GnomeCanvasText_get_scale_set (GnomeCanvasText * cptr) 
{
    return cptr->scale_set;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getScaleSet
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_CanvasText_getScaleSet (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jboolean) (GnomeCanvasText_get_scale_set (cptr_g));
}

static gint32 GnomeCanvasText_get_clip_cx (GnomeCanvasText * cptr) 
{
    return cptr->clip_cx;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getClipCx
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasText_getClipCx (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasText_get_clip_cx (cptr_g));
}

static gint32 GnomeCanvasText_get_clip_cy (GnomeCanvasText * cptr) 
{
    return cptr->clip_cy;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getClipCy
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasText_getClipCy (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasText_get_clip_cy (cptr_g));
}

static gint32 GnomeCanvasText_get_clip_cheight (GnomeCanvasText * cptr) 
{
    return cptr->clip_cheight;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getClipCheight
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasText_getClipCheight (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasText_get_clip_cheight (cptr_g));
}

static glong GnomeCanvasText_get_rgba (GnomeCanvasText * cptr) 
{
    return cptr->rgba;
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    getRgba
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnome_CanvasText_getRgba (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasText *cptr_g = (GnomeCanvasText *)getPointerFromHandle(env, cptr);
    return (jlong) (GnomeCanvasText_get_rgba (cptr_g));
}

/*
 * Class:     org.gnu.gnome.CanvasText
 * Method:    gnome_canvas_text_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasText_gnome_1canvas_1text_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gnome_canvas_text_get_type ();
}


#ifdef __cplusplus
}

#endif
