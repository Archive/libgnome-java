/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomecanvas/libgnomecanvas.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_CanvasShape.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.CanvasShape
 * Method:    gnome_canvas_shape_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasShape_gnome_1canvas_1shape_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gnome_canvas_shape_get_type ();
}

/*
 * Class:     org.gnu.gnome.CanvasShape
 * Method:    gnome_canvas_shape_set_path_def
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasShape_gnome_1canvas_1shape_1set_1path_1def (
    JNIEnv *env, jclass cls, jobject shape, jobject def) 
{
    GnomeCanvasShape *shape_g = (GnomeCanvasShape *)getPointerFromHandle(env, shape);
    GnomeCanvasPathDef *def_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, def);
    gnome_canvas_shape_set_path_def (shape_g, def_g);
}

/*
 * Class:     org.gnu.gnome.CanvasShape
 * Method:    gnome_canvas_shape_get_path_def
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasShape_gnome_1canvas_1shape_1get_1path_1def (
    JNIEnv *env, jclass cls, jobject shape) 
{
    GnomeCanvasShape *shape_g = (GnomeCanvasShape *)getPointerFromHandle(env, shape);
    return getStructHandle(env, gnome_canvas_shape_get_path_def (shape_g), NULL,
    		(JGFreeFunc) gnome_canvas_path_def_unref);
}


#ifdef __cplusplus
}

#endif
