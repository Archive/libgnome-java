/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_App.h"
#ifdef __cplusplus
extern "C" 
{
#endif
  
static gchar * GnomeApp_get_name (GnomeApp * cptr) 
{
    return cptr->name;
}
  
/*
 * Class:     org.gnu.gnome.App
 * Method:    getName
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_App_getName (JNIEnv *env, jclass cls, jobject cptr)
{
    GnomeApp *cptr_g = (GnomeApp *)getPointerFromHandle(env, cptr);
    return (*env)->NewStringUTF(env, (gchar*)GnomeApp_get_name (cptr_g));
}
  
static gchar * GnomeApp_get_prefix (GnomeApp * cptr) 
{
    return cptr->prefix;
}
  
/*
 * Class:     org.gnu.gnome.App
 * Method:    getPrefix
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_App_getPrefix (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeApp *cptr_g = (GnomeApp *)getPointerFromHandle(env, cptr);
    return (*env)->NewStringUTF(env, (gchar*)GnomeApp_get_prefix (cptr_g));
}

static GtkWidget * GnomeApp_get_dock (GnomeApp * cptr) 
{
    return cptr->dock;
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    getDock
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_App_getDock (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeApp *cptr_g = (GnomeApp *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, G_OBJECT(GnomeApp_get_dock (cptr_g)));
}

static GtkWidget * GnomeApp_get_statusbar (GnomeApp * cptr) 
{
    return cptr->statusbar;
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    getStatusbar
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_App_getStatusbar (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeApp *cptr_g = (GnomeApp *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, G_OBJECT(GnomeApp_get_statusbar (cptr_g)));
}

static GtkWidget * GnomeApp_get_vbox (GnomeApp * cptr) 
{
    return cptr->vbox;
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    getVbox
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_App_getVbox (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeApp *cptr_g = (GnomeApp *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, G_OBJECT(GnomeApp_get_vbox (cptr_g)));
}

static GtkWidget * GnomeApp_get_menubar (GnomeApp * cptr) 
{
    return cptr->menubar;
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    getMenubar
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_App_getMenubar (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeApp *cptr_g = (GnomeApp *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, G_OBJECT(GnomeApp_get_menubar (cptr_g)));
}

static GtkWidget * GnomeApp_get_contents (GnomeApp * cptr) 
{
    return cptr->contents;
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    getContents
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_App_getContents (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeApp *cptr_g = (GnomeApp *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, G_OBJECT(GnomeApp_get_contents (cptr_g)));
}

static GtkAccelGroup * GnomeApp_get_accel_group (GnomeApp * cptr) 
{
    return cptr->accel_group;
}
  
/*
 * Class:     org.gnu.gnome.App
 * Method:    getAccelGroup
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_App_getAccelGroup (JNIEnv *env, jclass cls, jobject cptr)
{
    GnomeApp *cptr_g = (GnomeApp *)getPointerFromHandle(env, cptr);
    return getGObjectHandleAndRef(env, G_OBJECT(GnomeApp_get_accel_group (cptr_g)));
}

static gboolean GnomeApp_get_enable_layout_config (GnomeApp * cptr) 
{
    return cptr->enable_layout_config;
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    getEnableLayoutConfig
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_App_getEnableLayoutConfig (JNIEnv *env, jclass 
                                                                         cls, jobject cptr) 
{
    GnomeApp *cptr_g = (GnomeApp *)getPointerFromHandle(env, cptr);
    return (jboolean) (GnomeApp_get_enable_layout_config (cptr_g));
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_App_gnome_1app_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gnome_app_get_type ();
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_App_gnome_1app_1new (JNIEnv *env, jclass cls, 
                                                                  jstring appname, jstring title) 
{
    const gchar* appname_g = (*env)->GetStringUTFChars(env, appname, NULL);
    const gchar* title_g = (*env)->GetStringUTFChars(env, title, NULL);
    jobject retval =  getGObjectHandle(env, G_OBJECT(gnome_app_new (appname_g, title_g)));
    (*env)->ReleaseStringUTFChars(env, appname, appname_g);
    (*env)->ReleaseStringUTFChars(env, title, title_g);
    return retval;
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_set_menus
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1set_1menus (JNIEnv *env, jclass cls, 
                                                                      jobject app, jobject menubar) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    GtkMenuBar *menubar_g = (GtkMenuBar *)getPointerFromHandle(env, menubar);
    gnome_app_set_menus (app_g, menubar_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_set_toolbar
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1set_1toolbar (JNIEnv *env, jclass cls, jobject app, jobject toolbar) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    gnome_app_set_toolbar (app_g, toolbar_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_set_statusbar
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1set_1statusbar (JNIEnv *env, jclass cls, jobject app, jobject statusbar) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    GtkWidget *statusbar_g = (GtkWidget *)getPointerFromHandle(env, statusbar);
    gnome_app_set_statusbar (app_g, statusbar_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_set_statusbar_custom
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1set_1statusbar_1custom (JNIEnv *env, jclass cls, jobject app, jobject container, jobject statusbar) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    GtkWidget *container_g = (GtkWidget *)getPointerFromHandle(env, container);
    GtkWidget *statusbar_g = (GtkWidget *)getPointerFromHandle(env, statusbar);
    gnome_app_set_statusbar_custom (app_g, container_g, statusbar_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_set_contents
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1set_1contents (JNIEnv *env, jclass cls, jobject app, jobject contents) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    GtkWidget *contents_g = (GtkWidget *)getPointerFromHandle(env, contents);
    gnome_app_set_contents (app_g, contents_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_enable_layout_config
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1enable_1layout_1config (JNIEnv *env, jclass cls, jobject app, jboolean enable) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    gboolean enable_g = (gboolean) enable;
    gnome_app_enable_layout_config (app_g, enable_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_ui_configure_configurable
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1ui_1configure_1configurable (JNIEnv *env, jclass cls, jobject uiinfo) 
{
    GnomeUIInfo *uiinfo_g = (GnomeUIInfo *)getPointerFromHandle(env, uiinfo);
    gnome_app_ui_configure_configurable (uiinfo_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_create_menus
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1create_1menus (JNIEnv *env, jclass 
                                                                         cls, jobject app, jobjectArray uiinfo) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    GnomeUIInfo* menu = 
        (GnomeUIInfo*)getArrayFromHandles(env, uiinfo, 
                                          sizeof(GnomeUIInfo), 1, 0);
    gnome_app_create_menus(app_g, menu);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_create_menus_custom
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1create_1menus_1custom (JNIEnv *env, 
                                                                                 jclass cls, jobject app, jobjectArray uiinfo, jobject uibdate) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    GnomeUIInfo *uiinfo_g = 
        (GnomeUIInfo *) getArrayFromHandles(env, uiinfo, 
                                            sizeof(GnomeUIInfo), 1, 0);
    GnomeUIBuilderData *uibdate_g = 
        (GnomeUIBuilderData *)getPointerFromHandle(env, uibdate);
    gnome_app_create_menus_custom (app_g, uiinfo_g, uibdate_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_create_toolbar
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1create_1toolbar (JNIEnv *env, jclass 
                                                                           cls, jobject app, jobjectArray uiinfo)
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    GnomeUIInfo* menu = 
        (GnomeUIInfo*)getArrayFromHandles(env, uiinfo, 
                                          sizeof(GnomeUIInfo), 1, 0);
    gnome_app_create_toolbar(app_g, menu);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_create_toolbar_custom
 * Signature: (I[Lint ;I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1create_1toolbar_1custom (JNIEnv *env, 
                                                                                   jclass cls, jobject app, jobjectArray uiinfo, jobject uibdata) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    GnomeUIInfo *uiinfo_g = 
        (GnomeUIInfo*)getArrayFromHandles(env, uiinfo, 
                                          sizeof(GnomeUIInfo), 1, 0);
    GnomeUIBuilderData *uibdata_g = 
        (GnomeUIBuilderData *)getPointerFromHandle(env, uibdata);
    gnome_app_create_toolbar_custom (app_g, uiinfo_g, uibdata_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_find_menu_pos
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_App_gnome_1app_1find_1menu_1pos (JNIEnv *env, jclass 
                                                                              cls, jobject parent, jstring path, jint pos) 
{
    GtkWidget *parent_g = (GtkWidget *)getPointerFromHandle(env, parent);
    gint32 pos_g = (gint32) pos;
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, NULL);
    jobject retval = getGObjectHandle(env, G_OBJECT(gnome_app_find_menu_pos (parent_g, path_g, &pos_g)));
    (*env)->ReleaseStringUTFChars(env, path, path_g);
    return retval;
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_remove_menus
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1remove_1menus (JNIEnv *env, jclass 
                                                                         cls, jobject app, jstring path, jint items) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    gint32 items_g = (gint32) items;
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, NULL);
    gnome_app_remove_menus (app_g, path_g, items_g);
    (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_remove_menu_range
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1remove_1menu_1range (JNIEnv *env, 
                                                                               jclass cls, jobject app, jstring path, jint start, jint items) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    gint32 start_g = (gint32) start;
    gint32 items_g = (gint32) items;
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, NULL);
    gnome_app_remove_menu_range (app_g, path_g, start_g, items_g);
    (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_insert_menus_custom
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1insert_1menus_1custom (JNIEnv *env, 
                                                                                 jclass cls, jobject app, jstring path, jobject menuinfo, jobject uibdata) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    GnomeUIInfo *menuinfo_g = (GnomeUIInfo *)getPointerFromHandle(env, menuinfo);
    GnomeUIBuilderData *uibdata_g = (GnomeUIBuilderData *)getPointerFromHandle(env, uibdata);
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, NULL);
    gnome_app_insert_menus_custom (app_g, path_g, menuinfo_g, uibdata_g);
    (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_insert_menus
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1insert_1menus (JNIEnv *env, jclass 
                                                                         cls, jobject app, jstring path, jobjectArray menuinfo) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, NULL);
    GnomeUIInfo* menu = 
        (GnomeUIInfo*)getArrayFromHandles(env, menuinfo, 
                                          sizeof(GnomeUIInfo), 1, 0);
    gnome_app_insert_menus (app_g, path_g, menu);
    (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_install_appbar_menu_hints
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1install_1appbar_1menu_1hints (JNIEnv 
                                                                                        *env, jclass cls, jobject appbar, jobjectArray uiinfo) 
{
    GnomeAppBar *appbar_g = (GnomeAppBar *)getPointerFromHandle(env, appbar);
    GnomeUIInfo* menu = 
        (GnomeUIInfo*)getArrayFromHandles(env, uiinfo, 
                                          sizeof(GnomeUIInfo), 1, 0);
    gnome_app_install_appbar_menu_hints(appbar_g, menu);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_install_statusbar_menu_hints
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1install_1statusbar_1menu_1hints (
                                                                                           JNIEnv *env, jclass cls, jobject bar, jobjectArray uiinfo) 
{
    GtkStatusbar *bar_g = (GtkStatusbar *)getPointerFromHandle(env, bar);
    GnomeUIInfo* menu = 
        (GnomeUIInfo*)getArrayFromHandles(env, uiinfo, 
                                          sizeof(GnomeUIInfo), 1, 0);
    gnome_app_install_statusbar_menu_hints (bar_g, menu);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_install_menu_hints
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1install_1menu_1hints (JNIEnv *env, 
                                                                                jclass cls, jobject app, jobjectArray uiinfo) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    GnomeUIInfo* menu = 
        (GnomeUIInfo*)getArrayFromHandles(env, uiinfo, 
                                          sizeof(GnomeUIInfo), 1, 0);
    gnome_app_install_menu_hints(app_g, menu);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_fill_menu
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1fill_1menu (JNIEnv *env, jclass cls, 
                                                                      jobject menuShell, jobjectArray uiInfo, jobject accelGroup, jboolean ulineAccels, jint pos) 
{
    GtkMenuShell *menuShell_g = 
        (GtkMenuShell *)getPointerFromHandle(env, menuShell);
    GnomeUIInfo *uiInfo_g = 
        (GnomeUIInfo *)getArrayFromHandles(env, uiInfo, 
                                           sizeof(GnomeUIInfo), 1, 0);
    GtkAccelGroup *accelGroup_g = 
        (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    gboolean ulineAccels_g = (gboolean) ulineAccels;
    gint32 pos_g = (gint32) pos;
    gnome_app_fill_menu (menuShell_g, uiInfo_g, 
                         accelGroup_g, ulineAccels_g, pos_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_fill_menu_with_data
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1fill_1menu_1with_1data (JNIEnv *env, jclass cls, jobject menuShell, jobjectArray uiInfo, jobject accelGroup, jboolean ulineAccels, jint pos, jobject userData) 
{
    GtkMenuShell *menuShell_g = 
        (GtkMenuShell *)getPointerFromHandle(env, menuShell);
    GnomeUIInfo *uiInfo_g = 
        (GnomeUIInfo *)getArrayFromHandles(env, uiInfo, 
                                           sizeof(GnomeUIInfo), 1, 0);
    GtkAccelGroup *accelGroup_g = 
        (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    gboolean ulineAccels_g = (gboolean) ulineAccels;
    gint32 pos_g = (gint32) pos;
    gpointer *userData_g = (gpointer *)userData;
    gnome_app_fill_menu_with_data (menuShell_g, uiInfo_g, 
                                   accelGroup_g, ulineAccels_g, 
                                   pos_g, userData_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_fill_menu_custom
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1fill_1menu_1custom (JNIEnv *env, jclass cls, jobject menuShell, jobjectArray uiInfo, jobject uiBData, jobject accelGroup, jboolean ulineAccels, jint pos) 
{
    GtkMenuShell *menuShell_g = 
        (GtkMenuShell *)getPointerFromHandle(env, menuShell);
    GnomeUIInfo *uiInfo_g = 
        (GnomeUIInfo *)getArrayFromHandles(env, uiInfo, 
                                           sizeof(GnomeUIInfo), 1, 0);
    GnomeUIBuilderData *uiBData_g = 
        (GnomeUIBuilderData *)getPointerFromHandle(env, uiBData);
    GtkAccelGroup *accelGroup_g = 
        (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    gboolean ulineAccels_g = (gboolean) ulineAccels;
    gint32 pos_g = (gint32) pos;
    gnome_app_fill_menu_custom (menuShell_g, uiInfo_g, 
                                uiBData_g, accelGroup_g, 
                                ulineAccels_g, pos_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_fill_toolbar
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1fill_1toolbar (JNIEnv *env, jclass 
                                                                         cls, jobject toolbar, jobjectArray uiInfo, jobject accelGroup) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    GnomeUIInfo *uiInfo_g = 
        (GnomeUIInfo *)getArrayFromHandles(env, uiInfo, 
                                           sizeof(GnomeUIInfo), 1, 0);
    GtkAccelGroup *accelGroup_g = 
        (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    gnome_app_fill_toolbar (toolbar_g, uiInfo_g, accelGroup_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_fill_toolbar_with_data
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1fill_1toolbar_1with_1data (JNIEnv 
                                                                                     *env, jclass cls, jobject toolbar, jobjectArray uiInfo, jobject accelGroup, jobject userData) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    GnomeUIInfo *uiInfo_g = 
        (GnomeUIInfo *)getArrayFromHandles(env, uiInfo, 
                                           sizeof(GnomeUIInfo), 1, 0);
    GtkAccelGroup *accelGroup_g = 
        (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    gpointer *userData_g = (gpointer *)userData;
    gnome_app_fill_toolbar_with_data (toolbar_g, uiInfo_g, 
                                      accelGroup_g, userData_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_fill_toolbar_custom
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1fill_1toolbar_1custom (JNIEnv *env, 
                                                                                 jclass cls, jobject toolbar, jobjectArray uiInfo, jobject uiBData, jobject accelGroup) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    GnomeUIInfo *uiInfo_g = 
        (GnomeUIInfo *)getArrayFromHandles(env, uiInfo, 
                                           sizeof(GnomeUIInfo), 1, 0);
    GnomeUIBuilderData *uiBData_g = 
        (GnomeUIBuilderData *)getPointerFromHandle(env, uiBData);
    GtkAccelGroup *accelGroup_g = 
        (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    gnome_app_fill_toolbar_custom (toolbar_g, uiInfo_g, 
                                   uiBData_g, accelGroup_g);
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_message
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_App_gnome_1app_1message (JNIEnv *env, jclass cls, 
                                                                      jobject app, jstring message) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    const gchar* message_g = (*env)->GetStringUTFChars(env, message, NULL);
    jobject retval = getGObjectHandle(env, G_OBJECT(gnome_app_message (app_g, message_g)));
    (*env)->ReleaseStringUTFChars( env, message, message_g );
    return retval;
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_flash
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_App_gnome_1app_1flash (JNIEnv *env, jclass cls, jobject 
                                                                 app, jstring flash) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    const gchar* flash_g = (*env)->GetStringUTFChars(env, flash, NULL);
    gnome_app_flash (app_g, flash_g);
    (*env)->ReleaseStringUTFChars( env, flash, flash_g );
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_error
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_App_gnome_1app_1error (JNIEnv *env, jclass cls, jobject 
                                                                    app, jstring error) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    const gchar* error_g = (*env)->GetStringUTFChars(env, error, NULL);
    jobject retval = getGObjectHandle(env, G_OBJECT(gnome_app_error (app_g, error_g)));
    (*env)->ReleaseStringUTFChars( env, error, error_g );
    return retval;
}

/*
 * Class:     org.gnu.gnome.App
 * Method:    gnome_app_warning
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_App_gnome_1app_1warning (JNIEnv *env, jclass cls, 
                                                                      jobject app, jstring warning) 
{
    GnomeApp *app_g = (GnomeApp *)getPointerFromHandle(env, app);
    const gchar* warning_g = (*env)->GetStringUTFChars(env, warning, NULL);
    jobject retval = getGObjectHandle(env, G_OBJECT(gnome_app_warning (app_g, warning_g)));
    (*env)->ReleaseStringUTFChars( env, warning, warning_g );
    return retval;
}


#ifdef __cplusplus
}

#endif
