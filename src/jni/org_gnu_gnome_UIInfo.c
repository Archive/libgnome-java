/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_UIInfo.h"

#ifdef __cplusplus
extern "C" 
{
#endif

typedef struct {
    JNIEnv *env;
    jobject obj;  /* object to recieve the signal */
    jmethodID methodID;  /* methodID of callback method */
    jobject methodData;  /* user defined data to pass to callback method */
} UIInfo_CallbackInfo;

/*
 * Class:     org_gnu_gnome_UIInfo
 * Method:    uiinfo_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_UIInfo_uiinfo_1new(JNIEnv *env, jclass cls, jint type, jstring label, jstring hint, jint pmType, jint acceleratorKey, jint acMods) {
    GnomeUIInfo *cptr = g_new(GnomeUIInfo, 1);
	GnomeUIInfoType infoType = (GnomeUIInfoType)type;
	GnomeUIPixmapType pixmapType = (GnomeUIPixmapType)pmType;
	guint accelKey = (guint)acceleratorKey;
	GdkModifierType modifierType = (GdkModifierType)acMods;
    
	const gchar* label_g = (*env)->GetStringUTFChars(env, label, 0);
    const gchar* hint_g = (*env)->GetStringUTFChars(env, hint, 0);

	cptr->type = infoType;
	cptr->label = label_g;
	cptr->hint = hint_g;
	cptr->pixmap_type = pixmapType;
	cptr->accelerator_key = accelKey;
	cptr->ac_mods = modifierType;

    return getHandleFromPointer(env, cptr);
}

/*
 * Class:     org_gnu_gnome_UIInfo
 * Method:    setPixmapInfo
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_UIInfo_setPixmapInfo__Lorg_gnu_glib_Handle_2Ljava_lang_String_2
  (JNIEnv *env, jclass cls, jobject hndl, jstring data) {
  	GnomeUIInfo *uiinfo = (GnomeUIInfo*)getPointerFromHandle(env, hndl);
    const gchar* data_g = (*env)->GetStringUTFChars(env, data, 0);
  	uiinfo->pixmap_info = (gconstpointer)data_g;
}

/*
 * Class:     org_gnu_gnome_UIInfo
 * Method:    setPixmapInfo
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_UIInfo_setPixmapInfo__Lorg_gnu_glib_Handle_2_3B
  (JNIEnv *env, jclass cls, jobject hndl, jbyteArray data) {
  	GnomeUIInfo *uiinfo = (GnomeUIInfo*)getPointerFromHandle(env, hndl);
	jint data_len = (*env)->GetArrayLength(env, data);
    gchar* data_g = (gchar*)g_malloc(data_len + 1);
	(*env)->GetByteArrayRegion(env, data, 0, data_len, (jbyte*)data_g);
	data_g[data_len] = 0;
  	uiinfo->pixmap_info = (gconstpointer)data_g;
}

/*
 * This is the callback method that recieves all callbacks
 * associated with the GnomeUIInfo activate.  It calls out
 * to the java method.
 */
static void
activateCallback(GtkWidget *widget, gpointer data)
{
  	UIInfo_CallbackInfo *cbi = data;
  	jvalue *jargs = g_new(jvalue, 1);

  	if (data == NULL) {
    	g_critical("Java-GNOME - unable to determine the callback method\n");
    	return;
  	}

  	jargs->l = cbi->methodData;
    (* cbi->env)->CallVoidMethodA(cbi->env, cbi->obj, cbi->methodID, jargs);
}

/*
 * Class:     org_gnu_gnome_UIInfo
 * Method:    setCallbackInfo
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_UIInfo_setCallbackInfo
  (JNIEnv *env, jclass cls, jobject hndl, jstring method, jobject obj) {
  	GnomeUIInfo *uiinfo = (GnomeUIInfo*)getPointerFromHandle(env, hndl);
  	UIInfo_CallbackInfo *callbackData;
  	jthrowable exc;

  	callbackData = g_new(UIInfo_CallbackInfo, 1);
  	callbackData->env = env;
  	callbackData->obj = (*env)->NewGlobalRef(env, obj);
  	callbackData->methodID = (*env)->GetMethodID(env, 
			(*env)->GetObjectClass(env, obj), 
			(*env)->GetStringUTFChars(env, method, 0),
			"()V");
  	exc = (*env)->ExceptionOccurred(env);
  	if (exc) {
    	g_critical("Java-GNOME - cannot find callback method %s with signature %s in the specified object.\n",
   		(*env)->GetStringUTFChars(env, method, 0), "()V");
    	g_critical("Java-GNOME - exception is:\n");
    	(*env)->ExceptionDescribe(env);
    	(*env)->ExceptionClear(env);
    	g_warning("\n\nJava-GNOME - signal will not be mapped\n\n");
   	 	return;
  	}
  	uiinfo->user_data = callbackData;
  	uiinfo->moreinfo = activateCallback;
}

/*
 * Class:     org_gnu_gnome_UIInfo
 * Method:    setMoreInfo
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_UIInfo_setMoreInfo
  (JNIEnv *env, jclass cls, jobject hndl, jstring info) {
  	GnomeUIInfo *uiinfo = (GnomeUIInfo*)getPointerFromHandle(env, hndl);
    const gchar* info_g = (*env)->GetStringUTFChars(env, info, 0);
  	uiinfo->moreinfo = (gpointer)info_g;
}

/*
 * Class:     org_gnu_gnome_UIInfo
 * Method:    setSubmenuInfo
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_UIInfo_setSubmenuInfo
  (JNIEnv *env, jclass cls, jobject hndl, jobjectArray info)
{
    GnomeUIInfo *uiinfo = (GnomeUIInfo*)getPointerFromHandle(env, hndl);
    GnomeUIInfo* submenu = (GnomeUIInfo*)getArrayFromHandles(env, info, sizeof(GnomeUIInfo), 1, 0);

    uiinfo->moreinfo = (gpointer)submenu;
    uiinfo->user_data = NULL;
}
                                                                                

static GnomeUIInfoType GnomeUIInfo_get_type (GnomeUIInfo * cptr) 
{
    return cptr->type;
}

/*
 * Class:     org.gnu.gnome.UIInfo
 * Method:    getType
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_UIInfo_getType (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeUIInfo *cptr_g = (GnomeUIInfo *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeUIInfo_get_type (cptr_g));
}

static gchar * GnomeUIInfo_get_label (GnomeUIInfo * cptr) 
{
    return (gchar *)cptr->label;
}

/*
 * Class:     org.gnu.gnome.UIInfo
 * Method:    getLabel
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_UIInfo_getLabel (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeUIInfo *cptr_g = (GnomeUIInfo *)getPointerFromHandle(env, cptr);
	return (*env)->NewStringUTF(env, (gchar*)GnomeUIInfo_get_label (cptr_g));
}

static gchar * GnomeUIInfo_get_hint (GnomeUIInfo * cptr) 
{
    return (gchar *)cptr->hint;
}

/*
 * Class:     org.gnu.gnome.UIInfo
 * Method:    getHint
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_UIInfo_getHint (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeUIInfo *cptr_g = (GnomeUIInfo *)getPointerFromHandle(env, cptr);
	return (*env)->NewStringUTF(env,(gchar*)GnomeUIInfo_get_hint (cptr_g));
}

static gpointer GnomeUIInfo_get_moreinfo (GnomeUIInfo * cptr) 
{
    return cptr->moreinfo;
}

/*
 * Class:     org.gnu.gnome.UIInfo
 * Method:    getMoreinfo
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_UIInfo_getMoreinfo (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeUIInfo *cptr_g = (GnomeUIInfo *)getPointerFromHandle(env, cptr);
    return getHandleFromPointer(env, GnomeUIInfo_get_moreinfo (cptr_g));
}

static gpointer GnomeUIInfo_get_user_data (GnomeUIInfo * cptr) 
{
    return cptr->user_data;
}

/*
 * Class:     org.gnu.gnome.UIInfo
 * Method:    getUserData
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_UIInfo_getUserData (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeUIInfo *cptr_g = (GnomeUIInfo *)getPointerFromHandle(env, cptr);
    return getHandleFromPointer(env,GnomeUIInfo_get_user_data (cptr_g));
}

static GnomeUIPixmapType GnomeUIInfo_get_pixmap_type (GnomeUIInfo * cptr) 
{
    return cptr->pixmap_type;
}

/*
 * Class:     org.gnu.gnome.UIInfo
 * Method:    getPixmapType
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_UIInfo_getPixmapType (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeUIInfo *cptr_g = (GnomeUIInfo *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeUIInfo_get_pixmap_type (cptr_g));
}

static gpointer GnomeUIInfo_get_pixmap_info (GnomeUIInfo * cptr) 
{
    return (gpointer)cptr->pixmap_info;
}

/*
 * Class:     org.gnu.gnome.UIInfo
 * Method:    getPixmapInfo
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_UIInfo_getPixmapInfo (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeUIInfo *cptr_g = (GnomeUIInfo *)getPointerFromHandle(env, cptr);
    return getPointerFromHandle(env, GnomeUIInfo_get_pixmap_info (cptr_g));
}

static gint32 GnomeUIInfo_get_accelerator_key (GnomeUIInfo * cptr) 
{
    return cptr->accelerator_key;
}

/*
 * Class:     org.gnu.gnome.UIInfo
 * Method:    getAcceleratorKey
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_UIInfo_getAcceleratorKey (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeUIInfo *cptr_g = (GnomeUIInfo *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeUIInfo_get_accelerator_key (cptr_g));
}

static GdkModifierType GnomeUIInfo_get_ac_mods (GnomeUIInfo * cptr) 
{
    return cptr->ac_mods;
}

/*
 * Class:     org.gnu.gnome.UIInfo
 * Method:    getAcMods
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_UIInfo_getAcMods (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeUIInfo *cptr_g = (GnomeUIInfo *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeUIInfo_get_ac_mods (cptr_g));
}

/*
 * Class:     org.gnu.gnome.UIInfo
 * Method:    getWidget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_UIInfo_getWidget (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeUIInfo *cptr_g = (GnomeUIInfo *)getPointerFromHandle(env, cptr);
    return getHandleFromPointer(env, cptr_g->widget);
}


#ifdef __cplusplus
}

#endif
