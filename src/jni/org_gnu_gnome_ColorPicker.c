/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_ColorPicker.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.ColorPicker
 * Method:    gnome_color_picker_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_ColorPicker_gnome_1color_1picker_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gnome_color_picker_get_type ();
}

/*
 * Class:     org.gnu.gnome.ColorPicker
 * Method:    gnome_color_picker_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_ColorPicker_gnome_1color_1picker_1new (JNIEnv *env, 
    jclass cls) 
{
    return getHandleFromPointer(env, gnome_color_picker_new ());
}

/*
 * Class:     org.gnu.gnome.ColorPicker
 * Method:    gnome_color_picker_set_d
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_ColorPicker_gnome_1color_1picker_1set_1d (JNIEnv 
    *env, jclass cls, jobject cp, jdouble red, jdouble green, jdouble blue, jdouble alpha) 
{
    GnomeColorPicker *cp_g = (GnomeColorPicker *)getPointerFromHandle(env, cp);
    gdouble red_g = (gdouble) red;
    gdouble green_g = (gdouble) green;
    gdouble blue_g = (gdouble) blue;
    gdouble alpha_g = (gdouble) alpha;
    gnome_color_picker_set_d (cp_g, red_g, green_g, blue_g, alpha_g);
}

/*
 * Class:     org.gnu.gnome.ColorPicker
 * Method:    gnome_color_picker_get_d
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_ColorPicker_gnome_1color_1picker_1get_1d (JNIEnv 
    *env, jclass cls, jobject cp, jdoubleArray red, jdoubleArray green, jdoubleArray blue, 
    jdoubleArray alpha) 
{
    GnomeColorPicker *cp_g = (GnomeColorPicker *)getPointerFromHandle(env, cp);
    gdouble *red_g = (gdouble *) (*env)->GetDoubleArrayElements (env, red, NULL);
    gdouble *green_g = (gdouble *) (*env)->GetDoubleArrayElements (env, green, NULL);
    gdouble *blue_g = (gdouble *) (*env)->GetDoubleArrayElements (env, blue, NULL);
    gdouble *alpha_g = (gdouble *) (*env)->GetDoubleArrayElements (env, alpha, NULL);
    gnome_color_picker_get_d (cp_g, red_g, green_g, blue_g, alpha_g);
    (*env)->ReleaseDoubleArrayElements (env, red, (jdouble *) red_g, 0);
    (*env)->ReleaseDoubleArrayElements (env, green, (jdouble *) green_g, 0);
    (*env)->ReleaseDoubleArrayElements (env, blue, (jdouble *) blue_g, 0);
    (*env)->ReleaseDoubleArrayElements (env, alpha, (jdouble *) alpha_g, 0);
}

/*
 * Class:     org.gnu.gnome.ColorPicker
 * Method:    gnome_color_picker_set_i8
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_ColorPicker_gnome_1color_1picker_1set_1i8 (JNIEnv 
    *env, jclass cls, jobject cp, jint red, jint green, jint blue, jint alpha) 
{
    GnomeColorPicker *cp_g = (GnomeColorPicker *)getPointerFromHandle(env, cp);
    gint32 red_g = (gint32) red;
    gint32 green_g = (gint32) green;
    gint32 blue_g = (gint32) blue;
    gint32 alpha_g = (gint32) alpha;
    gnome_color_picker_set_i8 (cp_g, red_g, green_g, blue_g, alpha_g);
}

/*
 * Class:     org.gnu.gnome.ColorPicker
 * Method:    gnome_color_picker_get_i8
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_ColorPicker_gnome_1color_1picker_1get_1i8 (JNIEnv 
    *env, jclass cls, jobject cp, jintArray red, jintArray green, jintArray blue, jintArray alpha) 
{
    GnomeColorPicker *cp_g = (GnomeColorPicker *)getPointerFromHandle(env, cp);
    guint8 *red_g = (guint8 *) (*env)->GetIntArrayElements (env, red, NULL);
    guint8 *green_g = (guint8 *) (*env)->GetIntArrayElements (env, green, NULL);
    guint8 *blue_g = (guint8 *) (*env)->GetIntArrayElements (env, blue, NULL);
    guint8 *alpha_g = (guint8 *) (*env)->GetIntArrayElements (env, alpha, NULL);
    gnome_color_picker_get_i8 (cp_g, red_g, green_g, blue_g, alpha_g);
    (*env)->ReleaseIntArrayElements (env, red, (jint *) red_g, 0);
    (*env)->ReleaseIntArrayElements (env, green, (jint *) green_g, 0);
    (*env)->ReleaseIntArrayElements (env, blue, (jint *) blue_g, 0);
    (*env)->ReleaseIntArrayElements (env, alpha, (jint *) alpha_g, 0);
}

/*
 * Class:     org.gnu.gnome.ColorPicker
 * Method:    gnome_color_picker_set_i16
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_ColorPicker_gnome_1color_1picker_1set_1i16 (JNIEnv 
    *env, jclass cls, jobject cp, jint red, jint green, jint blue, jint alpha) 
{
    GnomeColorPicker *cp_g = (GnomeColorPicker *)getPointerFromHandle(env, cp);
    gint32 red_g = (gint32) red;
    gint32 green_g = (gint32) green;
    gint32 blue_g = (gint32) blue;
    gint32 alpha_g = (gint32) alpha;
    gnome_color_picker_set_i16 (cp_g, red_g, green_g, blue_g, alpha_g);
}

/*
 * Class:     org.gnu.gnome.ColorPicker
 * Method:    gnome_color_picker_get_i16
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_ColorPicker_gnome_1color_1picker_1get_1i16 (JNIEnv 
    *env, jclass cls, jobject cp, jintArray red, jintArray green, jintArray blue, jintArray alpha) 
{
    GnomeColorPicker *cp_g = (GnomeColorPicker *)getPointerFromHandle(env, cp);
    gushort *red_g = (gushort *) (*env)->GetIntArrayElements (env, red, NULL);
    gushort *green_g = (gushort *) (*env)->GetIntArrayElements (env, green, NULL);
    gushort *blue_g = (gushort *) (*env)->GetIntArrayElements (env, blue, NULL);
    gushort *alpha_g = (gushort *) (*env)->GetIntArrayElements (env, alpha, NULL);
    gnome_color_picker_get_i16 (cp_g, red_g, green_g, blue_g, alpha_g);
    (*env)->ReleaseIntArrayElements (env, red, (jint *) red_g, 0);
    (*env)->ReleaseIntArrayElements (env, green, (jint *) green_g, 0);
    (*env)->ReleaseIntArrayElements (env, blue, (jint *) blue_g, 0);
    (*env)->ReleaseIntArrayElements (env, alpha, (jint *) alpha_g, 0);
}

/*
 * Class:     org.gnu.gnome.ColorPicker
 * Method:    gnome_color_picker_set_dither
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_ColorPicker_gnome_1color_1picker_1set_1dither (JNIEnv 
    *env, jclass cls, jobject cp, jboolean dither) 
{
    GnomeColorPicker *cp_g = (GnomeColorPicker *)getPointerFromHandle(env, cp);
    gboolean dither_g = (gboolean) dither;
    gnome_color_picker_set_dither (cp_g, dither_g);
}

/*
 * Class:     org.gnu.gnome.ColorPicker
 * Method:    gnome_color_picker_get_dither
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_ColorPicker_gnome_1color_1picker_1get_1dither (
    JNIEnv *env, jclass cls, jobject cp) 
{
    GnomeColorPicker *cp_g = (GnomeColorPicker *)getPointerFromHandle(env, cp);
    return (jboolean) (gnome_color_picker_get_dither (cp_g));
}

/*
 * Class:     org.gnu.gnome.ColorPicker
 * Method:    gnome_color_picker_set_use_alpha
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_ColorPicker_gnome_1color_1picker_1set_1use_1alpha (
    JNIEnv *env, jclass cls, jobject cp, jboolean useAlpha) 
{
    GnomeColorPicker *cp_g = (GnomeColorPicker *)getPointerFromHandle(env, cp);
    gboolean useAlpha_g = (gboolean) useAlpha;
    gnome_color_picker_set_use_alpha (cp_g, useAlpha_g);
}

/*
 * Class:     org.gnu.gnome.ColorPicker
 * Method:    gnome_color_picker_get_use_alpha
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_ColorPicker_gnome_1color_1picker_1get_1use_1alpha (
    JNIEnv *env, jclass cls, jobject cp) 
{
    GnomeColorPicker *cp_g = (GnomeColorPicker *)getPointerFromHandle(env, cp);
    return (jboolean) (gnome_color_picker_get_use_alpha (cp_g));
}

/*
 * Class:     org.gnu.gnome.ColorPicker
 * Method:    gnome_color_picker_set_title
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_ColorPicker_gnome_1color_1picker_1set_1title (JNIEnv 
    *env, jclass cls, jobject cp, jstring title) 
{
    GnomeColorPicker *cp_g = (GnomeColorPicker *)getPointerFromHandle(env, cp);
    const gchar* title_g = (*env)->GetStringUTFChars(env, title, 0);
    gnome_color_picker_set_title(cp_g, title_g);
    if (title) 
    	(*env)->ReleaseStringUTFChars(env, title, title_g);
}

/*
 * Class:     org.gnu.gnome.ColorPicker
 * Method:    gnome_color_picker_get_title
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_ColorPicker_gnome_1color_1picker_1get_1title (
    JNIEnv *env, jclass cls, jobject cp) 
{
    GnomeColorPicker *cp_g = (GnomeColorPicker *)getPointerFromHandle(env, cp);
    gchar *result_g = (gchar*)gnome_color_picker_get_title(cp_g);
    return (*env)->NewStringUTF(env, result_g);
}


#ifdef __cplusplus
}

#endif
