/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomecanvas/libgnomecanvas.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_CanvasLine.h"

#ifdef __cplusplus
extern "C" 
{
#endif

static gint32 GnomeCanvasLine_get_num_points (GnomeCanvasLine * cptr) 
{
    return cptr->num_points;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getNumPoints
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasLine_getNumPoints (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasLine_get_num_points (cptr_g));
}

static gdouble GnomeCanvasLine_get_width (GnomeCanvasLine * cptr) 
{
    return cptr->width;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getWidth
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasLine_getWidth (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasLine_get_width (cptr_g));
}

static GdkBitmap * GnomeCanvasLine_get_stipple (GnomeCanvasLine * cptr) 
{
    return cptr->stipple;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getStipple
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasLine_getStipple (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, G_OBJECT(GnomeCanvasLine_get_stipple (cptr_g)));
}

static void GnomeCanvasLine_set_stipple (GnomeCanvasLine * cptr, GdkBitmap * stipple) 
{
    cptr->stipple = stipple;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    setStipple
 */
  JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasLine_setStipple (JNIEnv *env, jclass cls, jobject cptr, jobject 
    stipple) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    GdkBitmap *stipple_g = (GdkBitmap *)getPointerFromHandle(env, stipple);
    GnomeCanvasLine_set_stipple (cptr_g, stipple_g);
}

static glong GnomeCanvasLine_get_fill_pixel (GnomeCanvasLine * cptr) 
{
    return cptr->fill_pixel;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getFillPixel
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnome_CanvasLine_getFillPixel (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jlong) (GnomeCanvasLine_get_fill_pixel (cptr_g));
}

static void GnomeCanvasLine_set_fill_pixel (GnomeCanvasLine * cptr, glong fill_pixel) 
{
    cptr->fill_pixel = fill_pixel;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    setFillPixel
 */
  JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasLine_setFillPixel (JNIEnv *env, jclass cls, jobject cptr, 
    jlong fill_pixel) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    glong fill_pixel_g = (glong) fill_pixel;
    GnomeCanvasLine_set_fill_pixel (cptr_g, fill_pixel_g);
}

static gint32 GnomeCanvasLine_get_fill_color (GnomeCanvasLine * cptr) 
{
    return cptr->fill_color;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getFillColor
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasLine_getFillColor (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jint) GnomeCanvasLine_get_fill_color (cptr_g);
}

static void GnomeCanvasLine_set_fill_color (GnomeCanvasLine * cptr, gint32 fill_color) 
{
    cptr->fill_color = fill_color;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    setFillColor
 */
  JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasLine_setFillColor (JNIEnv *env, jclass cls, jobject cptr, jint 
    fill_color) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    gint32 fill_color_g = (gint32) fill_color;
    GnomeCanvasLine_set_fill_color (cptr_g, fill_color_g);
}

static GdkGC * GnomeCanvasLine_get_gc (GnomeCanvasLine * cptr) 
{
    return cptr->gc;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getGc
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasLine_getGc (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, G_OBJECT(GnomeCanvasLine_get_gc (cptr_g)));
}

static gboolean GnomeCanvasLine_get_width_pixels (GnomeCanvasLine * cptr) 
{
    return cptr->width_pixels;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getWidthPixels
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_CanvasLine_getWidthPixels (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jboolean) (GnomeCanvasLine_get_width_pixels (cptr_g));
}

static gboolean GnomeCanvasLine_get_first_arrow (GnomeCanvasLine * cptr) 
{
    return cptr->first_arrow;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getFirstArrow
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_CanvasLine_getFirstArrow (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jboolean) (GnomeCanvasLine_get_first_arrow (cptr_g));
}

static void GnomeCanvasLine_set_first_arrow (GnomeCanvasLine * cptr, gboolean first_arrow) 
{
    cptr->first_arrow = first_arrow;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    setFirstArrow
 */
  JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasLine_setFirstArrow (JNIEnv *env, jclass cls, jobject cptr, 
    jboolean first_arrow) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    gboolean first_arrow_g = (gboolean) first_arrow;
    GnomeCanvasLine_set_first_arrow (cptr_g, first_arrow_g);
}

static gboolean GnomeCanvasLine_get_last_arrow (GnomeCanvasLine * cptr) 
{
    return cptr->last_arrow;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getLastArrow
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_CanvasLine_getLastArrow (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jboolean) (GnomeCanvasLine_get_last_arrow (cptr_g));
}

static void GnomeCanvasLine_set_last_arrow (GnomeCanvasLine * cptr, gboolean last_arrow) 
{
    cptr->last_arrow = last_arrow;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    setLastArrow
 */
  JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasLine_setLastArrow (JNIEnv *env, jclass cls, jobject cptr, 
    jboolean last_arrow) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    gboolean last_arrow_g = (gboolean) last_arrow;
    GnomeCanvasLine_set_last_arrow (cptr_g, last_arrow_g);
}

static gboolean GnomeCanvasLine_get_smooth (GnomeCanvasLine * cptr) 
{
    return cptr->smooth;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getSmooth
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_CanvasLine_getSmooth (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jboolean) (GnomeCanvasLine_get_smooth (cptr_g));
}

static void GnomeCanvasLine_set_smooth (GnomeCanvasLine * cptr, gboolean smooth) 
{
    cptr->smooth = smooth;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    setSmooth
 */
  JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasLine_setSmooth (JNIEnv *env, jclass cls, jobject cptr, 
    jboolean smooth) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    gboolean smooth_g = (gboolean) smooth;
    GnomeCanvasLine_set_smooth (cptr_g, smooth_g);
}

static GdkCapStyle GnomeCanvasLine_get_cap (GnomeCanvasLine * cptr) 
{
    return cptr->cap;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getCap
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasLine_getCap (JNIEnv *env, jclass cls, jobject cptr)
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasLine_get_cap (cptr_g));
}

static void GnomeCanvasLine_set_cap (GnomeCanvasLine * cptr, GdkCapStyle cap) 
{
    cptr->cap = cap;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    setCap
 */
  JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasLine_setCap (JNIEnv *env, jclass cls, jobject cptr, jint cap) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    GdkCapStyle cap_g = (GdkCapStyle) cap;
    GnomeCanvasLine_set_cap (cptr_g, cap_g);
}

static GdkLineStyle GnomeCanvasLine_get_line_style (GnomeCanvasLine * cptr) 
{
    return cptr->line_style;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getLineStyle
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasLine_getLineStyle (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasLine_get_line_style (cptr_g));
}

static void GnomeCanvasLine_set_line_style (GnomeCanvasLine * cptr, GdkLineStyle line_style) 
{
    cptr->line_style = line_style;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    setLineStyle
 */
  JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasLine_setLineStyle (JNIEnv *env, jclass cls, jobject cptr, jint 
    line_style) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    GdkLineStyle line_style_g = (GdkLineStyle) line_style;
    GnomeCanvasLine_set_line_style (cptr_g, line_style_g);
}

static GdkJoinStyle GnomeCanvasLine_get_join (GnomeCanvasLine * cptr) 
{
    return cptr->join;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getJoin
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasLine_getJoin (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasLine_get_join (cptr_g));
}

static void GnomeCanvasLine_set_join (GnomeCanvasLine * cptr, GdkJoinStyle join) 
{
    cptr->join = join;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    setJoin
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasLine_setJoin (JNIEnv *env, jclass cls, jobject cptr, jint join)
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    GdkJoinStyle join_g = (GdkJoinStyle) join;
    GnomeCanvasLine_set_join (cptr_g, join_g);
}

static gint32 GnomeCanvasLine_get_fill_rgba (GnomeCanvasLine * cptr) 
{
    return cptr->fill_rgba;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getFillRgba
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasLine_getFillRgba (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasLine_get_fill_rgba (cptr_g));
}

static void GnomeCanvasLine_set_fill_rgba (GnomeCanvasLine * cptr, gint32 fill_rgba) 
{
    cptr->fill_rgba = fill_rgba;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    setFillRgba
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasLine_setFillRgba (JNIEnv *env, jclass cls, jobject cptr, jint 
    fill_rgba) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    gint32 fill_rgba_g = (gint32) fill_rgba;
    GnomeCanvasLine_set_fill_rgba (cptr_g, fill_rgba_g);
}

static gint32 GnomeCanvasLine_get_spline_steps (GnomeCanvasLine * cptr) 
{
    return cptr->spline_steps;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getSplineSteps
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasLine_getSplineSteps (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasLine_get_spline_steps (cptr_g));
}

static void GnomeCanvasLine_set_spline_steps (GnomeCanvasLine * cptr, gint32 spline_steps) 
{
    cptr->spline_steps = spline_steps;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    setSplineSteps
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasLine_setSplineSteps (JNIEnv *env, jclass cls, jobject cptr, 
    jint spline_steps) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    gint32 spline_steps_g = (gint32) spline_steps;
    GnomeCanvasLine_set_spline_steps (cptr_g, spline_steps_g);
}

static gdouble GnomeCanvasLine_get_shape_a (GnomeCanvasLine * cptr) 
{
    return cptr->shape_a;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getShapeA
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasLine_getShapeA (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasLine_get_shape_a (cptr_g));
}

static void GnomeCanvasLine_set_shape_a (GnomeCanvasLine * cptr, gdouble shape_a) 
{
    cptr->shape_a = shape_a;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    setShapeA
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasLine_setShapeA (JNIEnv *env, jclass cls, jobject cptr, jdouble 
    shape_a) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    gdouble shape_a_g = (gdouble) shape_a;
    GnomeCanvasLine_set_shape_a (cptr_g, shape_a_g);
}

static gdouble GnomeCanvasLine_get_shape_b (GnomeCanvasLine * cptr) 
{
    return cptr->shape_b;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getShapeB
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasLine_getShapeB (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasLine_get_shape_b (cptr_g));
}

static void GnomeCanvasLine_set_shape_b (GnomeCanvasLine * cptr, gdouble shape_b) 
{
    cptr->shape_b = shape_b;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    setShapeB
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasLine_setShapeB (JNIEnv *env, jclass cls, jobject cptr, jdouble 
    shape_b) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    gdouble shape_b_g = (gdouble) shape_b;
    GnomeCanvasLine_set_shape_b (cptr_g, shape_b_g);
}

static gdouble GnomeCanvasLine_get_shape_c (GnomeCanvasLine * cptr) 
{
    return cptr->shape_c;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    getShapeC
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasLine_getShapeC (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasLine_get_shape_c (cptr_g));
}

static void GnomeCanvasLine_set_shape_c (GnomeCanvasLine * cptr, gdouble shape_c) 
{
    cptr->shape_c = shape_c;
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    setShapeC
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasLine_setShapeC (JNIEnv *env, jclass cls, jobject cptr, jdouble 
    shape_c) 
{
    GnomeCanvasLine *cptr_g = (GnomeCanvasLine *)getPointerFromHandle(env, cptr);
    gdouble shape_c_g = (gdouble) shape_c;
    GnomeCanvasLine_set_shape_c (cptr_g, shape_c_g);
}

/*
 * Class:     org.gnu.gnome.CanvasLine
 * Method:    gnome_canvas_line_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasLine_gnome_1canvas_1line_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gnome_canvas_line_get_type ();
}


#ifdef __cplusplus
}

#endif
