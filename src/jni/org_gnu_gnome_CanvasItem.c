/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomecanvas/libgnomecanvas.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_CanvasItem.h"

#ifdef __cplusplus

extern "C" 
{
#endif

static GnomeCanvas * GnomeCanvasItem_get_canvas (GnomeCanvasItem * cptr) 
{
    return cptr->canvas;
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    getCanvas
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasItem_getCanvas (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasItem *cptr_g = (GnomeCanvasItem *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, G_OBJECT(GnomeCanvasItem_get_canvas (cptr_g)));
}

static GnomeCanvasItem * GnomeCanvasItem_get_parent (GnomeCanvasItem * cptr) 
{
    return cptr->parent;
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    getParent
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasItem_getParent (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasItem *cptr_g = (GnomeCanvasItem *)getPointerFromHandle(env, cptr);
    return getGObjectHandleAndRef(env, G_OBJECT(GnomeCanvasItem_get_parent (cptr_g)));
}

static gdouble GnomeCanvasItem_get_x1 (GnomeCanvasItem * cptr) 
{
    return cptr->x1;
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    getX1
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasItem_getX1 (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasItem *cptr_g = (GnomeCanvasItem *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasItem_get_x1 (cptr_g));
}

static gdouble GnomeCanvasItem_get_y1 (GnomeCanvasItem * cptr) 
{
    return cptr->y1;
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    getY1
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasItem_getY1 (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasItem *cptr_g = (GnomeCanvasItem *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasItem_get_y1 (cptr_g));
}

static gdouble GnomeCanvasItem_get_x2 (GnomeCanvasItem * cptr) 
{
    return cptr->x2;
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    getX2
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasItem_getX2 (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasItem *cptr_g = (GnomeCanvasItem *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasItem_get_x2 (cptr_g));
}

static gdouble GnomeCanvasItem_get_y2 (GnomeCanvasItem * cptr) 
{
    return cptr->y2;
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    getY2
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasItem_getY2 (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasItem *cptr_g = (GnomeCanvasItem *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasItem_get_y2 (cptr_g));
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    gnome_canvas_item_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gnome_canvas_item_get_type ();
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    gnome_canvas_item_move
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1move (JNIEnv *env, 
    jclass cls, jobject item, jdouble dx, jdouble dy) 
{
    GnomeCanvasItem *item_g = (GnomeCanvasItem *)getPointerFromHandle(env, item);
    gdouble dx_g = (gdouble) dx;
    gdouble dy_g = (gdouble) dy;
    gnome_canvas_item_move (item_g, dx_g, dy_g);
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    gnome_canvas_item_affine_relative
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1affine_1relative (
    JNIEnv *env, jclass cls, jobject item, jdoubleArray affine) 
{
    GnomeCanvasItem *item_g = (GnomeCanvasItem *)getPointerFromHandle(env, item);
    gdouble *affine_g = (gdouble *) (*env)->GetDoubleArrayElements (env, affine, NULL);
    gnome_canvas_item_affine_relative (item_g, affine_g);
    (*env)->ReleaseDoubleArrayElements (env, affine, (jdouble *) affine_g, 0);
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    gnome_canvas_item_affine_absolute
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1affine_1absolute (
    JNIEnv *env, jclass cls, jobject item, jdoubleArray affine) 
{
    GnomeCanvasItem *item_g = (GnomeCanvasItem *)getPointerFromHandle(env, item);
    gdouble *affine_g = (gdouble *) (*env)->GetDoubleArrayElements (env, affine, NULL);
    gnome_canvas_item_affine_absolute (item_g, affine_g);
    (*env)->ReleaseDoubleArrayElements (env, affine, (jdouble *) affine_g, 0);
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    gnome_canvas_item_raise
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1raise (JNIEnv *env, 
    jclass cls, jobject item, jint position) 
{
    GnomeCanvasItem *item_g = (GnomeCanvasItem *)getPointerFromHandle(env, item);
    gint32 position_g = (gint32) position;
    gnome_canvas_item_raise (item_g, position_g);
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    gnome_canvas_item_raise_to_top
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1raise_1to_1top (
    JNIEnv *env, jclass cls, jobject item) 
{
    GnomeCanvasItem *item_g = (GnomeCanvasItem *)getPointerFromHandle(env, item);
    gnome_canvas_item_raise_to_top (item_g);
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    gnome_canvas_item_lower
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1lower (JNIEnv *env, 
    jclass cls, jobject item, jint positions) 
{
    GnomeCanvasItem *item_g = (GnomeCanvasItem *)getPointerFromHandle(env, item);
    gint32 positions_g = (gint32) positions;
    gnome_canvas_item_lower (item_g, positions_g);
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    gnome_canvas_item_lower_to_bottom
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1lower_1to_1bottom (
    JNIEnv *env, jclass cls, jobject item) 
{
    GnomeCanvasItem *item_g = (GnomeCanvasItem *)getPointerFromHandle(env, item);
    gnome_canvas_item_lower_to_bottom (item_g);
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    gnome_canvas_item_show
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1show (JNIEnv *env, 
    jclass cls, jobject item) 
{
    GnomeCanvasItem *item_g = (GnomeCanvasItem *)getPointerFromHandle(env, item);
    gnome_canvas_item_show (item_g);
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    gnome_canvas_item_hide
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1hide (JNIEnv *env, 
    jclass cls, jobject item) 
{
    GnomeCanvasItem *item_g = (GnomeCanvasItem *)getPointerFromHandle(env, item);
    gnome_canvas_item_hide (item_g);
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    gnome_canvas_item_grab
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1grab (JNIEnv *env, 
    jclass cls, jobject item, jint event_mask, jobject cursor, jint etime) 
{
    GnomeCanvasItem *item_g = (GnomeCanvasItem *)getPointerFromHandle(env, item);
    gint32 event_mask_g = (gint32) event_mask;
    GdkCursor *cursor_g = (GdkCursor *)getPointerFromHandle(env, cursor);
    gint32 etime_g = (gint32) etime;
    return (jint)gnome_canvas_item_grab (item_g, event_mask_g, cursor_g, etime_g);
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    gnome_canvas_item_ungrab
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1ungrab (JNIEnv *env, 
    jclass cls, jobject item, jint etime) 
{
    GnomeCanvasItem *item_g = (GnomeCanvasItem *)getPointerFromHandle(env, item);
    gint32 etime_g = (gint32) etime;
    gnome_canvas_item_ungrab (item_g, etime_g);
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    gnome_canvas_item_reparent
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1reparent (JNIEnv 
    *env, jclass cls, jobject item, jobject new_group) 
{
    GnomeCanvasItem *item_g = (GnomeCanvasItem *)getPointerFromHandle(env, item);
    GnomeCanvasGroup *new_group_g = (GnomeCanvasGroup *)getPointerFromHandle(env, new_group);
    gnome_canvas_item_reparent (item_g, new_group_g);
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    gnome_canvas_item_grab_focus
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1grab_1focus (JNIEnv 
    *env, jclass cls, jobject item) 
{
    GnomeCanvasItem *item_g = (GnomeCanvasItem *)getPointerFromHandle(env, item);
    gnome_canvas_item_grab_focus (item_g);
}

/*
 * Class:     org.gnu.gnome.CanvasItem
 * Method:    gnome_canvas_item_request_update
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1request_1update (
    JNIEnv *env, jclass cls, jobject item) 
{
    GnomeCanvasItem *item_g = (GnomeCanvasItem *)getPointerFromHandle(env, item);
    gnome_canvas_item_request_update (item_g);
}

/*
 * Class:     org_gnu_gnome_CanvasItem
 * Method:    gnome_canvas_item_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasItem_gnome_1canvas_1item_1new
  (JNIEnv *env, jclass cls, jobject group, jint type)
{
    GnomeCanvasGroup *group_g = (GnomeCanvasGroup *)getPointerFromHandle(env, group);
	return getHandleFromPointer(env, gnome_canvas_item_new(group_g, (GType)type, NULL));
}
                                                                              

#ifdef __cplusplus
}

#endif
