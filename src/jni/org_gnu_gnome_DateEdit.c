/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_DateEdit.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.DateEdit
 * Method:    gnome_date_edit_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_DateEdit_gnome_1date_1edit_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gnome_date_edit_get_type ();
}

/*
 * Class:     org.gnu.gnome.DateEdit
 * Method:    gnome_date_edit_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DateEdit_gnome_1date_1edit_1new (JNIEnv *env, jclass 
    cls, jlong the_time, jboolean show_time, jboolean use_24_format) 
{
    time_t the_time_g = (time_t) the_time;
    gboolean show_time_g = (gboolean) show_time;
    gboolean use_24_format_g = (gboolean) use_24_format;
    return getGObjectHandle(env,
    		G_OBJECT(gnome_date_edit_new (the_time_g, show_time_g, use_24_format_g)));
}

/*
 * Class:     org.gnu.gnome.DateEdit
 * Method:    gnome_date_edit_new_flags
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DateEdit_gnome_1date_1edit_1new_1flags (JNIEnv *env, 
    jclass cls, jlong the_time, jint flags) 
{
    time_t the_time_g = (time_t) the_time;
    GnomeDateEditFlags flags_g = (GnomeDateEditFlags) flags;
    return getGObjectHandle(env,
    		G_OBJECT(gnome_date_edit_new_flags (the_time_g, flags_g)));
}

/*
 * Class:     org.gnu.gnome.DateEdit
 * Method:    gnome_date_edit_set_time
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_DateEdit_gnome_1date_1edit_1set_1time (JNIEnv *env, 
    jclass cls, jobject gde, jlong the_time) 
{
    GnomeDateEdit *gde_g = (GnomeDateEdit *)getPointerFromHandle(env, gde);
    time_t the_time_g = (time_t) the_time;
    gnome_date_edit_set_time (gde_g, the_time_g);
}

/*
 * Class:     org.gnu.gnome.DateEdit
 * Method:    gnome_date_edit_get_time
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnome_DateEdit_gnome_1date_1edit_1get_1time (JNIEnv *env, 
    jclass cls, jobject gde) 
{
    GnomeDateEdit *gde_g = (GnomeDateEdit *)getPointerFromHandle(env, gde);
    return (jlong)gnome_date_edit_get_time (gde_g);
}

/*
 * Class:     org.gnu.gnome.DateEdit
 * Method:    gnome_date_edit_set_popup_range
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_DateEdit_gnome_1date_1edit_1set_1popup_1range (JNIEnv 
    *env, jclass cls, jobject gde, jint low_hour, jint up_hour) 
{
    GnomeDateEdit *gde_g = (GnomeDateEdit *)getPointerFromHandle(env, gde);
    gint32 low_hour_g = (gint32) low_hour;
    gint32 up_hour_g = (gint32) up_hour;
    gnome_date_edit_set_popup_range (gde_g, low_hour_g, up_hour_g);
}

/*
 * Class:     org.gnu.gnome.DateEdit
 * Method:    gnome_date_edit_set_flags
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_DateEdit_gnome_1date_1edit_1set_1flags (JNIEnv *env, 
    jclass cls, jobject gde, jint flags) 
{
    GnomeDateEdit *gde_g = (GnomeDateEdit *)getPointerFromHandle(env, gde);
    GnomeDateEditFlags flags_g = (GnomeDateEditFlags) flags;
    gnome_date_edit_set_flags (gde_g, flags_g);
}

/*
 * Class:     org.gnu.gnome.DateEdit
 * Method:    gnome_date_edit_get_flags
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_DateEdit_gnome_1date_1edit_1get_1flags (JNIEnv *env, 
    jclass cls, jobject gde) 
{
    GnomeDateEdit *gde_g = (GnomeDateEdit *)getPointerFromHandle(env, gde);
    return (jint) (gnome_date_edit_get_flags (gde_g));
}

/*
 * Class:     org.gnu.gnome.DateEdit
 * Method:    gnome_date_edit_get_initial_time
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnome_DateEdit_gnome_1date_1edit_1get_1initial_1time (
    JNIEnv *env, jclass cls, jobject gde) 
{
    GnomeDateEdit *gde_g = (GnomeDateEdit *)getPointerFromHandle(env, gde);
    return (jlong)gnome_date_edit_get_initial_time (gde_g);
}


#ifdef __cplusplus
}

#endif
