/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_IconList.h"

#ifdef __cplusplus

extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gnome_icon_list_get_type ();
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1new (JNIEnv *env, jclass 
    cls, jint iconWidth, jobject adj, jint flags) 
{
    gint32 iconWidth_g = (gint32) iconWidth;
    GtkAdjustment *adj_g = (GtkAdjustment *)getPointerFromHandle(env, adj);
    gint32 flags_g = (gint32) flags;
    return getHandleFromPointer(env, gnome_icon_list_new (iconWidth_g, adj_g, flags_g));
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_set_hadjustment
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1set_1hadjustment (JNIEnv 
    *env, jclass cls, jobject gil, jobject hadj) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    GtkAdjustment *hadj_g = (GtkAdjustment *)getPointerFromHandle(env, hadj);
    gnome_icon_list_set_hadjustment (gil_g, hadj_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_set_vadjustment
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1set_1vadjustment (JNIEnv 
    *env, jclass cls, jobject gil, jobject vadj) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    GtkAdjustment *vadj_g = (GtkAdjustment *)getPointerFromHandle(env, vadj);
    gnome_icon_list_set_vadjustment (gil_g, vadj_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_freeze
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1freeze (JNIEnv *env, 
    jclass cls, jobject gil) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gnome_icon_list_freeze (gil_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_thaw
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1thaw (JNIEnv *env, jclass 
    cls, jobject gil) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gnome_icon_list_thaw (gil_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_insert
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1insert (JNIEnv *env, 
    jclass cls, jobject gil, jint idx, jstring iconFilename, jstring text) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gchar* iconFilename_g = (gchar*)(*env)->GetStringUTFChars(env, iconFilename, 0);
    gchar* text_g = (gchar*)(*env)->GetStringUTFChars(env, text, 0);
    gnome_icon_list_insert (gil_g, (gint32)idx, iconFilename_g, text_g);
    if (iconFilename) 
    	(*env)->ReleaseStringUTFChars(env, iconFilename, iconFilename_g);
    if (text) 
    	(*env)->ReleaseStringUTFChars(env, text, text_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_insert_pixbuf
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1insert_1pixbuf (JNIEnv 
    *env, jclass cls, jobject gil, jint idx, jobject im, jstring iconFilename, jstring text) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gchar* iconFilename_g = (gchar*)(*env)->GetStringUTFChars(env, iconFilename, 0);
    gchar* text_g = (gchar*)(*env)->GetStringUTFChars(env, text, 0);
    gnome_icon_list_insert_pixbuf (gil_g, (gint32)idx, (GdkPixbuf*)getPointerFromHandle(env, im), iconFilename_g, text_g);
    if (iconFilename) 
    	(*env)->ReleaseStringUTFChars(env, iconFilename, iconFilename_g);
    if (text) 
    	(*env)->ReleaseStringUTFChars(env, text, text_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_append
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1append (JNIEnv *env, 
    jclass cls, jobject gil, jstring iconFilename, jstring text) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gchar* iconFilename_g = (gchar*)(*env)->GetStringUTFChars(env, iconFilename, 0);
    gchar* text_g = (gchar*)(*env)->GetStringUTFChars(env, text, 0);
    jint result_j = (jint)gnome_icon_list_append (gil_g, iconFilename_g, text_g);
    if (iconFilename) 
    	(*env)->ReleaseStringUTFChars(env, iconFilename, iconFilename_g);
    if (text) 
    	(*env)->ReleaseStringUTFChars(env, text, text_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_append_pixbuf
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1append_1pixbuf (JNIEnv 
    *env, jclass cls, jobject gil, jobject im, jstring iconFilename, jstring text) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    GdkPixbuf* im_g = (GdkPixbuf*)getPointerFromHandle(env, im);
    gchar* iconFilename_g = (gchar*)(*env)->GetStringUTFChars(env, iconFilename, 0);
    gchar* text_g = (gchar*)(*env)->GetStringUTFChars(env, text, 0);
    jint result_j = (jint)gnome_icon_list_append_pixbuf (gil_g, im_g, iconFilename_g, text_g);
    if (iconFilename) 
    	(*env)->ReleaseStringUTFChars(env, iconFilename, iconFilename_g);
    if (text) 
    	(*env)->ReleaseStringUTFChars(env, text, text_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_clear
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1clear (JNIEnv *env, 
    jclass cls, jobject gil) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gnome_icon_list_clear (gil_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_remove
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1remove (JNIEnv *env, 
    jclass cls, jobject gil, jint idx) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gint32 idx_g = (gint32) idx;
    gnome_icon_list_remove (gil_g, idx_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_get_num_icons
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1get_1num_1icons (JNIEnv 
    *env, jclass cls, jobject gil) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    return (jint) (gnome_icon_list_get_num_icons (gil_g));
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_get_selection_mode
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1get_1selection_1mode (
    JNIEnv *env, jclass cls, jobject gil) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    return (jint) (gnome_icon_list_get_selection_mode (gil_g));
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_set_selection_mode
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1set_1selection_1mode (
    JNIEnv *env, jclass cls, jobject gil, jint mode) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    GtkSelectionMode mode_g = (GtkSelectionMode) mode;
    gnome_icon_list_set_selection_mode (gil_g, mode_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_select_icon
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1select_1icon (JNIEnv 
    *env, jclass cls, jobject gil, jint idx) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gint32 idx_g = (gint32) idx;
    gnome_icon_list_select_icon (gil_g, idx_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_unselect_icon
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1unselect_1icon (JNIEnv 
    *env, jclass cls, jobject gil, jint idx) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gint32 idx_g = (gint32) idx;
    gnome_icon_list_unselect_icon (gil_g, idx_g);
}

/*
 * Class:     org_gnu_gnome_IconList  
 * Method:    gnome_icon_list_select_all
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1select_1all
  (JNIEnv *env, jclass cls, jobject gil) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
	gnome_icon_list_select_all(gil_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_unselect_all
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1unselect_1all (JNIEnv 
    *env, jclass cls, jobject gil) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    return (jint) (gnome_icon_list_unselect_all (gil_g));
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_get_selection
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1get_1selection (JNIEnv 
    *env, jclass cls, jobject gil) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    return getHandleArrayFromGList(env, gnome_icon_list_get_selection (gil_g));
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_focus_icon
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1focus_1icon (JNIEnv *env, 
    jclass cls, jobject gil, jint idx) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gint32 idx_g = (gint32) idx;
    gnome_icon_list_focus_icon (gil_g, idx_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_set_icon_width
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1set_1icon_1width (JNIEnv 
    *env, jclass cls, jobject gil, jint w) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gint32 w_g = (gint32) w;
    gnome_icon_list_set_icon_width (gil_g, w_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_set_row_spacing
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1set_1row_1spacing (JNIEnv 
    *env, jclass cls, jobject gil, jint pixels) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gint32 pixels_g = (gint32) pixels;
    gnome_icon_list_set_row_spacing (gil_g, pixels_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_set_col_spacing
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1set_1col_1spacing (JNIEnv 
    *env, jclass cls, jobject gil, jint pixels) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gint32 pixels_g = (gint32) pixels;
    gnome_icon_list_set_col_spacing (gil_g, pixels_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_set_text_spacing
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1set_1text_1spacing (
    JNIEnv *env, jclass cls, jobject gil, jint pixels) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gint32 pixels_g = (gint32) pixels;
    gnome_icon_list_set_text_spacing (gil_g, pixels_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_set_icon_border
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1set_1icon_1border (JNIEnv 
    *env, jclass cls, jobject gil, jint pixels) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gint32 pixels_g = (gint32) pixels;
    gnome_icon_list_set_icon_border (gil_g, pixels_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_set_separators
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1set_1separators (JNIEnv 
    *env, jclass cls, jobject gil, jstring sep) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gchar* sep_g = (gchar*)(*env)->GetStringUTFChars(env, sep, 0);
    gnome_icon_list_set_separators (gil_g, sep_g);
    if (sep) 
    	(*env)->ReleaseStringUTFChars(env, sep, sep_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_get_icon_filename
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1get_1icon_1filename (
    JNIEnv *env, jclass cls, jobject gil, jint idx) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gchar *result_g = (gchar*)gnome_icon_list_get_icon_filename (gil_g, (gint32)idx);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_find_icon_from_filename
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_gnome_IconList_gnome_1icon_1list_1find_1icon_1from_1filename (JNIEnv *env, jclass 
    cls, jobject gil, jstring filename) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
    jint result_j = (jint) (gnome_icon_list_find_icon_from_filename (gil_g, filename_g));
    if (filename) 
    	(*env)->ReleaseStringUTFChars(env, filename, filename_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_moveto
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1moveto (JNIEnv *env, 
    jclass cls, jobject gil, jint idx, jdouble yalign) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gint32 idx_g = (gint32) idx;
    gdouble yalign_g = (gdouble) yalign;
    gnome_icon_list_moveto (gil_g, idx_g, yalign_g);
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_icon_is_visible
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1icon_1is_1visible (JNIEnv 
    *env, jclass cls, jobject gil, jint idx) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gint32 idx_g = (gint32) idx;
    return (jint) (gnome_icon_list_icon_is_visible (gil_g, idx_g));
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_get_icon_at
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1get_1icon_1at (JNIEnv 
    *env, jclass cls, jobject gil, jint x, jint y) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    return (jint) (gnome_icon_list_get_icon_at (gil_g, x_g, y_g));
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_get_items_per_line
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1get_1items_1per_1line (
    JNIEnv *env, jclass cls, jobject gil) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    return (jint) (gnome_icon_list_get_items_per_line (gil_g));
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_get_icon_text_item
 * Signature: (II)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1get_1icon_1text_1item (
    JNIEnv *env, jclass cls, jobject gil, jint idx) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gint32 idx_g = (gint32) idx;
    return getHandleFromPointer(env, gnome_icon_list_get_icon_text_item (gil_g, idx_g));
}

/*
 * Class:     org.gnu.gnome.IconList
 * Method:    gnome_icon_list_get_icon_pixbuf_item
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_IconList_gnome_1icon_1list_1get_1icon_1pixbuf_1item (
    JNIEnv *env, jclass cls, jobject gil, jint idx) 
{
    GnomeIconList *gil_g = (GnomeIconList *)getPointerFromHandle(env, gil);
    gint32 idx_g = (gint32) idx;
    return getHandleFromPointer(env, gnome_icon_list_get_icon_pixbuf_item (gil_g, idx_g));
}

#ifdef __cplusplus
}

#endif
