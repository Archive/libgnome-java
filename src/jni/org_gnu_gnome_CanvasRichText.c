/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomecanvas/libgnomecanvas.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_CanvasRichText.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.CanvasRichText
 * Method:    gnome_canvas_rich_text_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasRichText_gnome_1canvas_1rich_1text_1get_1type (
    JNIEnv *env, jclass cls) 
{
    return (jint)gnome_canvas_rich_text_get_type ();
}

/*
 * Class:     org.gnu.gnome.CanvasRichText
 * Method:    gnome_canvas_rich_text_cut_clipboard
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_CanvasRichText_gnome_1canvas_1rich_1text_1cut_1clipboard (JNIEnv *env, 
    jclass cls, jobject text) 
{
    GnomeCanvasRichText *text_g = (GnomeCanvasRichText *)getPointerFromHandle(env, text);
    gnome_canvas_rich_text_cut_clipboard (text_g);
}

/*
 * Class:     org.gnu.gnome.CanvasRichText
 * Method:    gnome_canvas_rich_text_copy_clipboard
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_CanvasRichText_gnome_1canvas_1rich_1text_1copy_1clipboard (JNIEnv *env, 
    jclass cls, jobject text) 
{
    GnomeCanvasRichText *text_g = (GnomeCanvasRichText *)getPointerFromHandle(env, text);
    gnome_canvas_rich_text_copy_clipboard (text_g);
}

/*
 * Class:     org.gnu.gnome.CanvasRichText
 * Method:    gnome_canvas_rich_text_paste_clipboard
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_CanvasRichText_gnome_1canvas_1rich_1text_1paste_1clipboard (JNIEnv *env, 
    jclass cls, jobject text) 
{
    GnomeCanvasRichText *text_g = (GnomeCanvasRichText *)getPointerFromHandle(env, text);
    gnome_canvas_rich_text_paste_clipboard (text_g);
}

/*
 * Class:     org.gnu.gnome.CanvasRichText
 * Method:    gnome_canvas_rich_text_set_buffer
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasRichText_gnome_1canvas_1rich_1text_1set_1buffer (
    JNIEnv *env, jclass cls, jobject text, jobject buffer) 
{
    GnomeCanvasRichText *text_g = (GnomeCanvasRichText *)getPointerFromHandle(env, text);
    GtkTextBuffer *buffer_g = (GtkTextBuffer *)getPointerFromHandle(env, buffer);
    gnome_canvas_rich_text_set_buffer (text_g, buffer_g);
}

/*
 * Class:     org.gnu.gnome.CanvasRichText
 * Method:    gnome_canvas_rich_text_get_buffer
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasRichText_gnome_1canvas_1rich_1text_1get_1buffer (
    JNIEnv *env, jclass cls, jobject text) 
{
    GnomeCanvasRichText *text_g = (GnomeCanvasRichText *)getPointerFromHandle(env, text);
    return getGObjectHandleAndRef(env, G_OBJECT(gnome_canvas_rich_text_get_buffer (text_g)));
}

/*
 * Class:     org.gnu.gnome.CanvasRichText
 * Method:    gnome_canvas_rich_text_get_iter_location
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_CanvasRichText_gnome_1canvas_1rich_1text_1get_1iter_1location (JNIEnv *env, 
    jclass cls, jobject text, jobject iter, jobject location) 
{
    GnomeCanvasRichText *text_g = (GnomeCanvasRichText *)getPointerFromHandle(env, text);
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    GdkRectangle *location_g = (GdkRectangle *)getPointerFromHandle(env, location);
    gnome_canvas_rich_text_get_iter_location (text_g, iter_g, location_g);
}

/*
 * Class:     org.gnu.gnome.CanvasRichText
 * Method:    gnome_canvas_rich_text_get_iter_at_location
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_CanvasRichText_gnome_1canvas_1rich_1text_1get_1iter_1at_1location (JNIEnv 
    *env, jclass cls, jobject text, jobject iter, jint x, jint y) 
{
    GnomeCanvasRichText *text_g = (GnomeCanvasRichText *)getPointerFromHandle(env, text);
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gnome_canvas_rich_text_get_iter_at_location (text_g, iter_g, x_g, y_g);
}


#ifdef __cplusplus
}

#endif
