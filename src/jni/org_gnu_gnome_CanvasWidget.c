/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomecanvas/libgnomecanvas.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_CanvasWidget.h"

#ifdef __cplusplus
extern "C" 
{
#endif

static GtkWidget * GnomeCanvasWidget_get_widget (GnomeCanvasWidget * cptr) 
{
    return cptr->widget;
}

/*
 * Class:     org.gnu.gnome.CanvasWidget
 * Method:    getWidget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasWidget_getWidget (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasWidget *cptr_g = (GnomeCanvasWidget *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, G_OBJECT(GnomeCanvasWidget_get_widget (cptr_g)));
}

static void GnomeCanvasWidget_set_widget (GnomeCanvasWidget * cptr, GtkWidget * widget) 
{
    cptr->widget = widget;
}

/*
 * Class:     org.gnu.gnome.CanvasWidget
 * Method:    setWidget
 */
  JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasWidget_setWidget (JNIEnv *env, jclass cls, jobject cptr, jobject 
    widget) 
{
    GnomeCanvasWidget *cptr_g = (GnomeCanvasWidget *)getPointerFromHandle(env, cptr);
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GnomeCanvasWidget_set_widget (cptr_g, widget_g);
}

static gdouble GnomeCanvasWidget_get_x (GnomeCanvasWidget * cptr) 
{
    return cptr->x;
}

/*
 * Class:     org.gnu.gnome.CanvasWidget
 * Method:    getX
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasWidget_getX (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasWidget *cptr_g = (GnomeCanvasWidget *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasWidget_get_x (cptr_g));
}

static void GnomeCanvasWidget_set_x (GnomeCanvasWidget * cptr, gdouble x) 
{
    cptr->x = x;
}

/*
 * Class:     org.gnu.gnome.CanvasWidget
 * Method:    setX
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasWidget_setX (JNIEnv *env, jclass cls, jobject cptr, jdouble x) 
{
    GnomeCanvasWidget *cptr_g = (GnomeCanvasWidget *)getPointerFromHandle(env, cptr);
    gdouble x_g = (gdouble) x;
    GnomeCanvasWidget_set_x (cptr_g, x_g);
}

static gdouble GnomeCanvasWidget_get_y (GnomeCanvasWidget * cptr) 
{
    return cptr->y;
}

/*
 * Class:     org.gnu.gnome.CanvasWidget
 * Method:    getY
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasWidget_getY (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasWidget *cptr_g = (GnomeCanvasWidget *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasWidget_get_y (cptr_g));
}

static void GnomeCanvasWidget_set_y (GnomeCanvasWidget * cptr, gdouble y) 
{
    cptr->y = y;
}

/*
 * Class:     org.gnu.gnome.CanvasWidget
 * Method:    setY
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasWidget_setY (JNIEnv *env, jclass cls, jobject cptr, jdouble y) 
{
    GnomeCanvasWidget *cptr_g = (GnomeCanvasWidget *)getPointerFromHandle(env, cptr);
    gdouble y_g = (gdouble) y;
    GnomeCanvasWidget_set_y (cptr_g, y_g);
}

static gdouble GnomeCanvasWidget_get_width (GnomeCanvasWidget * cptr) 
{
    return cptr->width;
}

/*
 * Class:     org.gnu.gnome.CanvasWidget
 * Method:    getWidth
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasWidget_getWidth (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasWidget *cptr_g = (GnomeCanvasWidget *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasWidget_get_width (cptr_g));
}

static void GnomeCanvasWidget_set_width (GnomeCanvasWidget * cptr, gdouble width) 
{
    cptr->width = width;
}

/*
 * Class:     org.gnu.gnome.CanvasWidget
 * Method:    setWidth
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasWidget_setWidth (JNIEnv *env, jclass cls, jobject cptr, 
    jdouble width) 
{
    GnomeCanvasWidget *cptr_g = (GnomeCanvasWidget *)getPointerFromHandle(env, cptr);
    gdouble width_g = (gdouble) width;
    GnomeCanvasWidget_set_width (cptr_g, width_g);
}

static gdouble GnomeCanvasWidget_get_height (GnomeCanvasWidget * cptr) 
{
    return cptr->height;
}

/*
 * Class:     org.gnu.gnome.CanvasWidget
 * Method:    getHeight
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_CanvasWidget_getHeight (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvasWidget *cptr_g = (GnomeCanvasWidget *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvasWidget_get_height (cptr_g));
}

static void GnomeCanvasWidget_set_height (GnomeCanvasWidget * cptr, gdouble height) 
{
    cptr->height = height;
}

/*
 * Class:     org.gnu.gnome.CanvasWidget
 * Method:    setHeight
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasWidget_setHeight (JNIEnv *env, jclass cls, jobject cptr, 
    jdouble height) 
{
    GnomeCanvasWidget *cptr_g = (GnomeCanvasWidget *)getPointerFromHandle(env, cptr);
    gdouble height_g = (gdouble) height;
    GnomeCanvasWidget_set_height (cptr_g, height_g);
}

static GtkAnchorType GnomeCanvasWidget_get_anchor (GnomeCanvasWidget * cptr) 
{
    return cptr->anchor;
}

/*
 * Class:     org.gnu.gnome.CanvasWidget
 * Method:    getAnchor
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasWidget_getAnchor (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvasWidget *cptr_g = (GnomeCanvasWidget *)getPointerFromHandle(env, cptr);
    return (jint) (GnomeCanvasWidget_get_anchor (cptr_g));
}

static void GnomeCanvasWidget_set_anchor (GnomeCanvasWidget * cptr, GtkAnchorType anchor) 
{
    cptr->anchor = anchor;
}

/*
 * Class:     org.gnu.gnome.CanvasWidget
 * Method:    setAnchor
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasWidget_setAnchor (JNIEnv *env, jclass cls, jobject cptr, jint 
    anchor) 
{
    GnomeCanvasWidget *cptr_g = (GnomeCanvasWidget *)getPointerFromHandle(env, cptr);
    GtkAnchorType anchor_g = (GtkAnchorType) anchor;
    GnomeCanvasWidget_set_anchor (cptr_g, anchor_g);
}

static gboolean GnomeCanvasWidget_get_size_pixels (GnomeCanvasWidget * cptr) 
{
    return cptr->size_pixels;
}

/*
 * Class:     org.gnu.gnome.CanvasWidget
 * Method:    getSizePixels
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_CanvasWidget_getSizePixels (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GnomeCanvasWidget *cptr_g = (GnomeCanvasWidget *)getPointerFromHandle(env, cptr);
    return (jboolean) (GnomeCanvasWidget_get_size_pixels (cptr_g));
}

static void GnomeCanvasWidget_set_size_pixels (GnomeCanvasWidget * cptr, gboolean size_pixels) 
{
    cptr->size_pixels = size_pixels;
}

/*
 * Class:     org.gnu.gnome.CanvasWidget
 * Method:    setSizePixels
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasWidget_setSizePixels (JNIEnv *env, jclass cls, jobject cptr,
    jboolean size_pixels) 
{
    GnomeCanvasWidget *cptr_g = (GnomeCanvasWidget *)getPointerFromHandle(env, cptr);
    gboolean size_pixels_g = (gboolean) size_pixels;
    GnomeCanvasWidget_set_size_pixels (cptr_g, size_pixels_g);
}

/*
 * Class:     org.gnu.gnome.CanvasWidget
 * Method:    gnome_canvas_widget_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasWidget_gnome_1canvas_1widget_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gnome_canvas_widget_get_type ();
}


#ifdef __cplusplus
}

#endif
