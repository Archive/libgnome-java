/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_PasswordDialog.h"

#ifndef _Included_org_gnu_gnome_PasswordDialog
#define _Included_org_gnu_gnome_PasswordDialog
#ifdef __cplusplus



extern "C" {
#endif

/*
 * Class:     org_gnu_gnome_PasswordDialog
 * Method:    gnome_password_dialog_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_PasswordDialog_gnome_1password_1dialog_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gnome_password_dialog_get_type();
}

/*
 * Class:     org_gnu_gnome_PasswordDialog
 * Method:    gnome_password_dialog_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_PasswordDialog_gnome_1password_1dialog_1new
  (JNIEnv *env, jclass cls, jstring title, jstring message, jstring username, jstring password, jboolean readonly)
{
	const char *t = title ? (*env)->GetStringUTFChars(env, title, NULL) : NULL;
	const char *m = message ? (*env)->GetStringUTFChars(env, message, NULL) : NULL;
	const char *u = username ? (*env)->GetStringUTFChars(env, username, NULL) : NULL;
	const char *p = password ? (*env)->GetStringUTFChars(env, password, NULL) : NULL;
	GtkWidget *ret = gnome_password_dialog_new(t, m, u, p, (gboolean)readonly);
	if (t)
		(*env)->ReleaseStringUTFChars(env, title, t);
	if (m)
		(*env)->ReleaseStringUTFChars(env, message, m);
	if (u)
		(*env)->ReleaseStringUTFChars(env, username, u);
	if (p)
		(*env)->ReleaseStringUTFChars(env, password, p);
	return getGObjectHandle(env, G_OBJECT(ret));
}

/*
 * Class:     org_gnu_gnome_PasswordDialog
 * Method:    gnome_password_dialog_run_and_block
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_PasswordDialog_gnome_1password_1dialog_1run_1and_1block
  (JNIEnv *env, jclass cls, jobject dialog)
{
	GnomePasswordDialog* dialog_g = (GnomePasswordDialog*)getPointerFromHandle(env, dialog);
	return (jboolean)gnome_password_dialog_run_and_block(dialog_g);
}

/*
 * Class:     org_gnu_gnome_PasswordDialog
 * Method:    gnome_password_dialog_set_username
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_PasswordDialog_gnome_1password_1dialog_1set_1username
  (JNIEnv *env, jclass cls, jobject dialog, jstring username)
{
	GnomePasswordDialog* dialog_g = (GnomePasswordDialog*)getPointerFromHandle(env, dialog);
	const char *u = (*env)->GetStringUTFChars(env, username, NULL);
	gnome_password_dialog_set_username(dialog_g, u);
	(*env)->ReleaseStringUTFChars(env, username, u);
}

/*
 * Class:     org_gnu_gnome_PasswordDialog
 * Method:    gnome_password_dialog_set_password
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_PasswordDialog_gnome_1password_1dialog_1set_1password
  (JNIEnv *env, jclass cls, jobject dialog, jstring password)
{
	GnomePasswordDialog* dialog_g = (GnomePasswordDialog*)getPointerFromHandle(env, dialog);
	const char *p = (*env)->GetStringUTFChars(env, password, NULL);
	gnome_password_dialog_set_password(dialog_g, p);
	(*env)->ReleaseStringUTFChars(env, password, p);
}

/*
 * Class:     org_gnu_gnome_PasswordDialog
 * Method:    gnome_password_dialog_set_readonly_username
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_PasswordDialog_gnome_1password_1dialog_1set_1readonly_1username
  (JNIEnv *env, jclass cls, jobject dialog, jboolean readonly)
{
	GnomePasswordDialog* dialog_g = (GnomePasswordDialog*)getPointerFromHandle(env, dialog);
	gnome_password_dialog_set_readonly_username(dialog_g, (gboolean)readonly);
}

/*
 * Class:     org_gnu_gnome_PasswordDialog
 * Method:    gnome_password_dialog_set_remember
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_PasswordDialog_gnome_1password_1dialog_1set_1remember
  (JNIEnv *env, jclass cls, jobject dialog, jboolean remember)
{
	GnomePasswordDialog* dialog_g = (GnomePasswordDialog*)getPointerFromHandle(env, dialog);
	gnome_password_dialog_set_remember(dialog_g, (gboolean)remember);
}

/*
 * Class:     org_gnu_gnome_PasswordDialog  
 * Method:    gnome_password_dialog_set_show_userpass_buttons
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_PasswordDialog_gnome_1password_1dialog_1set_1show_1userpass_1buttons
  (JNIEnv *env, jclass cls, jobject dialog, jboolean show)                                                                               
{
	GnomePasswordDialog* dialog_g = (GnomePasswordDialog*)getPointerFromHandle(env, dialog);
	gnome_password_dialog_set_show_userpass_buttons(dialog_g, (gboolean)show);
}

/*
 * Class:     org_gnu_gnome_PasswordDialog
 * Method:    gnome_password_dialog_get_username
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_PasswordDialog_gnome_1password_1dialog_1get_1username
  (JNIEnv *env, jclass cls, jobject dialog)
{
	GnomePasswordDialog* dialog_g = (GnomePasswordDialog*)getPointerFromHandle(env, dialog);
	char *username = gnome_password_dialog_get_username(dialog_g);
	return (*env)->NewStringUTF(env, username);
}

/*
 * Class:     org_gnu_gnome_PasswordDialog
 * Method:    gnome_password_dialog_get_password
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_PasswordDialog_gnome_1password_1dialog_1get_1password
  (JNIEnv *env, jclass cls, jobject dialog)
{
	GnomePasswordDialog* dialog_g = (GnomePasswordDialog*)getPointerFromHandle(env, dialog);
	char *password = gnome_password_dialog_get_password(dialog_g);
	return (*env)->NewStringUTF(env, password);
}

/*
 * Class:     org_gnu_gnome_PasswordDialog
 * Method:    gnome_password_dialog_get_remember
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_PasswordDialog_gnome_1password_1dialog_1get_1remember
  (JNIEnv *env, jclass cls, jobject dialog)
{
	GnomePasswordDialog* dialog_g = (GnomePasswordDialog*)getPointerFromHandle(env, dialog);
	return (jboolean)gnome_password_dialog_get_remember(dialog_g);
}

#ifdef __cplusplus
}
#endif
#endif
