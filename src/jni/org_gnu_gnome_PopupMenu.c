/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_PopupMenu.h"

#ifdef __cplusplus
extern "C" 
{
#endif


/*
 * Class:     org.gnu.gnome.PopupMenu
 * Method:    gnome_popup_menu_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_PopupMenu_gnome_1popup_1menu_1new (JNIEnv *env, 
    jclass cls, jobjectArray uiinfo) 
{
    GnomeUIInfo *uiinfo_g = 
        (GnomeUIInfo*)getArrayFromHandles(env, uiinfo, 
                                          sizeof(GnomeUIInfo), 1, 0);
    return getHandleFromPointer(env, gnome_popup_menu_new (uiinfo_g));
}

/*
 * Class:     org.gnu.gnome.PopupMenu
 * Method:    gnome_popup_menu_new_with_accelgroup
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_PopupMenu_gnome_1popup_1menu_1new_1with_1accelgroup (
    JNIEnv *env, jclass cls, jobjectArray uiinfo, jobject accelgroup) 
{
    GnomeUIInfo *uiinfo_g = 
        (GnomeUIInfo*)getArrayFromHandles(env, uiinfo, 
                                          sizeof(GnomeUIInfo), 1, 0);
    GtkAccelGroup *accelgroup_g = (GtkAccelGroup *)getPointerFromHandle(env, accelgroup);
    return getHandleFromPointer(env, gnome_popup_menu_new_with_accelgroup (uiinfo_g, accelgroup_g));
}

/*
 * Class:     org.gnu.gnome.PopupMenu
 * Method:    gnome_popup_menu_get_accel_group
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_PopupMenu_gnome_1popup_1menu_1get_1accel_1group (
    JNIEnv *env, jclass cls, jobject menu) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    return getHandleFromPointer(env, gnome_popup_menu_get_accel_group (menu_g));
}

/*
 * Class:     org.gnu.gnome.PopupMenu
 * Method:    gnome_popup_menu_attach
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_PopupMenu_gnome_1popup_1menu_1attach (JNIEnv *env, 
    jclass cls, jobject menu, jobject widget, jobject userData) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gpointer *userData_g = (gpointer *)userData;
    gnome_popup_menu_attach ((GtkWidget*)menu_g, widget_g, userData_g);
}

/*
 * Class:     org.gnu.gnome.PopupMenu
 * Method:    gnome_popup_menu_append
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_PopupMenu_gnome_1popup_1menu_1append (JNIEnv *env, 
    jclass cls, jobject menu, jobjectArray uiinfo) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    GnomeUIInfo *uiinfo_g = 
        (GnomeUIInfo*)getArrayFromHandles(env, uiinfo, 
                                          sizeof(GnomeUIInfo), 1, 0);
    gnome_popup_menu_append ((GtkWidget*)menu_g, uiinfo_g);
}


#ifdef __cplusplus
}

#endif
