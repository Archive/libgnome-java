/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_Entry.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.Entry
 * Method:    gnome_entry_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_Entry_gnome_1entry_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)gnome_entry_get_type ();
}

/*
 * Class:     org.gnu.gnome.Entry
 * Method:    gnome_entry_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Entry_gnome_1entry_1new (JNIEnv *env, jclass cls, 
    jstring historyId) 
{
    gchar* historyId_g = (gchar*)(*env)->GetStringUTFChars(env, historyId, 0);
    jobject result = getHandleFromPointer(env, gnome_entry_new (historyId_g));
    if (historyId) 
    	(*env)->ReleaseStringUTFChars(env, historyId, historyId_g);
    return result;
}

/*
 * Class:     org.gnu.gnome.Entry
 * Method:    gnome_entry_gtk_entry
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Entry_gnome_1entry_1gtk_1entry (JNIEnv *env, jclass 
    cls, jobject gentry) 
{
	GnomeEntry* gentry_g = (GnomeEntry*)getPointerFromHandle(env, gentry);
    return getHandleFromPointer(env, gnome_entry_gtk_entry (gentry_g));
}

/*
 * Class:     org.gnu.gnome.Entry
 * Method:    gnome_entry_set_history_id
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Entry_gnome_1entry_1set_1history_1id (JNIEnv *env, 
    jclass cls, jobject gentry, jstring historyId) 
{
	GnomeEntry* gentry_g = (GnomeEntry*)getPointerFromHandle(env, gentry);
    gchar* historyId_g = (gchar*)(*env)->GetStringUTFChars(env, historyId, 0);
    gnome_entry_set_history_id (gentry_g, historyId_g);
    if (historyId) 
    	(*env)->ReleaseStringUTFChars(env, historyId, historyId_g);
}

/*
 * Class:     org.gnu.gnome.Entry
 * Method:    gnome_entry_get_history_id
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_Entry_gnome_1entry_1get_1history_1id (JNIEnv 
    *env, jclass cls, jobject gentry) 
{
	GnomeEntry* gentry_g = (GnomeEntry*)getPointerFromHandle(env, gentry);
    gchar *result_g = (gchar*)gnome_entry_get_history_id (gentry_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.Entry
 * Method:    gnome_entry_set_max_saved
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Entry_gnome_1entry_1set_1max_1saved (JNIEnv *env, 
    jclass cls, jobject gentry, jint maxSaved) 
{
	GnomeEntry* gentry_g = (GnomeEntry*)getPointerFromHandle(env, gentry);
    guint32 maxSaved_g = (guint32) maxSaved;
    gnome_entry_set_max_saved (gentry_g, maxSaved_g);
}

/*
 * Class:     org.gnu.gnome.Entry
 * Method:    gnome_entry_get_max_saved
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_Entry_gnome_1entry_1get_1max_1saved (JNIEnv *env, 
    jclass cls, jobject gentry) 
{
	GnomeEntry* gentry_g = (GnomeEntry*)getPointerFromHandle(env, gentry);
    return (jint) (gnome_entry_get_max_saved (gentry_g));
}

/*
 * Class:     org.gnu.gnome.Entry
 * Method:    gnome_entry_prepend_history
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Entry_gnome_1entry_1prepend_1history (JNIEnv *env, 
    jclass cls, jobject gentry, jboolean save, jstring text) 
{
	GnomeEntry* gentry_g = (GnomeEntry*)getPointerFromHandle(env, gentry);
    gchar* text_g = (gchar*)(*env)->GetStringUTFChars(env, text, 0);
    gnome_entry_prepend_history (gentry_g, (gboolean)save, text_g);
    if (text) 
    	(*env)->ReleaseStringUTFChars(env, text, text_g);
}

/*
 * Class:     org.gnu.gnome.Entry
 * Method:    gnome_entry_append_history
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Entry_gnome_1entry_1append_1history (JNIEnv *env, 
    jclass cls, jobject gentry, jboolean save, jstring text) 
{
	GnomeEntry* gentry_g = (GnomeEntry*)getPointerFromHandle(env, gentry);
    gchar* text_g = (gchar*)(*env)->GetStringUTFChars(env, text, 0);
    gnome_entry_append_history (gentry_g, (gboolean)save, text_g);
    if (text) 
    	(*env)->ReleaseStringUTFChars(env, text, text_g);
}

/*
 * Class:     org.gnu.gnome.Entry
 * Method:    gnome_entry_clear_history
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Entry_gnome_1entry_1clear_1history (JNIEnv *env, 
    jclass cls, jobject gentry) 
{
	GnomeEntry* gentry_g = (GnomeEntry*)getPointerFromHandle(env, gentry);
    gnome_entry_clear_history (gentry_g);
}


#ifdef __cplusplus
}

#endif
