/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_Gnome.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.Gnome
 * Method:    gnome_master_client
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Gnome_gnome_1master_1client (JNIEnv *env, jclass cls) 
{
    return getGObjectHandleAndRef(env, G_OBJECT(gnome_master_client ()));
}

/*
 * Class:     org.gnu.gnome.Gnome
 * Method:    gnome_accelerators_sync
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Gnome_gnome_1accelerators_1sync (JNIEnv *env, jclass 
    cls) 
{
    gnome_accelerators_sync ();
}


#ifdef __cplusplus
}

#endif
