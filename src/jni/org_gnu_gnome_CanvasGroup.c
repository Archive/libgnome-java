/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomecanvas/libgnomecanvas.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_CanvasGroup.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.CanvasGroup
 * Method:    gnome_canvas_group_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasGroup_gnome_1canvas_1group_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gnome_canvas_group_get_type ();
}

/*
 * Class:     org_gnu_gnome_CanvasGroup
 * Method:    get_item_list
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gnome_CanvasGroup_get_1item_1list
  (JNIEnv *env, jclass cls, jobject group)
{
	GnomeCanvasGroup* group_g = (GnomeCanvasGroup*)getPointerFromHandle(env, group);
	GList *list = group_g->item_list;
	return getGObjectHandlesFromGListAndRef(env, list);
}
                                                                              

#ifdef __cplusplus
}

#endif
