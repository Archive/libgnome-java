/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_HRef.h"

#ifdef __cplusplus

extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.HRef
 * Method:    gnome_href_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_HRef_gnome_1href_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gnome_href_get_type ();
}

/*
 * Class:     org.gnu.gnome.HRef
 * Method:    gnome_href_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_HRef_gnome_1href_1new (JNIEnv *env, jclass cls, 
    jstring url, jstring lable) 
{
    const gchar* url_g = (*env)->GetStringUTFChars(env, url, NULL);
    const gchar* lable_g = (*env)->GetStringUTFChars(env, lable, NULL);
	jobject retval =  getGObjectHandle(env, G_OBJECT(gnome_href_new (url_g, lable_g)));
	(*env)->ReleaseStringUTFChars( env, url, url_g);
	(*env)->ReleaseStringUTFChars( env, lable, lable_g );
	return retval;
}

/*
 * Class:     org.gnu.gnome.HRef
 * Method:    gnome_href_set_url
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_HRef_gnome_1href_1set_1url (JNIEnv *env, jclass cls, 
    jobject href, jstring url) 
{
    GnomeHRef *href_g = (GnomeHRef *)getPointerFromHandle(env, href);
    const gchar* url_g = (*env)->GetStringUTFChars(env, url, NULL);
	gnome_href_set_url (href_g, url_g);
	(*env)->ReleaseStringUTFChars(env, url, url_g );
}

/*
 * Class:     org.gnu.gnome.HRef
 * Method:    gnome_href_get_url
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_HRef_gnome_1href_1get_1url (JNIEnv *env, jclass 
    cls, jobject href) 
{
    GnomeHRef *href_g = (GnomeHRef *)getPointerFromHandle(env, href);
	return (*env)->NewStringUTF(env, (gchar*)gnome_href_get_url (href_g));
}

/*
 * Class:     org.gnu.gnome.HRef
 * Method:    gnome_href_set_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_HRef_gnome_1href_1set_1text (JNIEnv *env, jclass cls, 
    jobject href, jstring text) 
{
    GnomeHRef *href_g = (GnomeHRef *)getPointerFromHandle(env, href);
    const gchar* text_g = (*env)->GetStringUTFChars(env, text, NULL);
	gnome_href_set_text (href_g, text_g);
	(*env)->ReleaseStringUTFChars(env, text, text_g);
}

/*
 * Class:     org.gnu.gnome.HRef
 * Method:    gnome_href_get_text
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_HRef_gnome_1href_1get_1text (JNIEnv *env, 
    jclass cls, jobject label) 
{
    GnomeHRef *label_g = (GnomeHRef *)getPointerFromHandle(env, label);
	return (*env)->NewStringUTF(env, (gchar*)gnome_href_get_text (label_g));
}


#ifdef __cplusplus
}

#endif
