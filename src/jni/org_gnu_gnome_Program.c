/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_Program.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     gnu.gnome.Program
 * Method:    initLibgnomeui0
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Program_initLibgnome
    (JNIEnv *env, jclass cls, jstring appId, jstring appVersion, 
    jint argc, jobjectArray argv, jstring prefix, jstring sysconfdir, 
    jstring datadir, jstring libdir, jint type) 
{
    const gchar *appId_g = (*env)->GetStringUTFChars(env, appId, NULL);
    const gchar *appVersion_g = (*env)->GetStringUTFChars(env, appVersion, NULL);
    const gchar *prefix_g = (*env)->GetStringUTFChars(env, prefix, NULL);
    const gchar *sysconfdir_g = (*env)->GetStringUTFChars(env, sysconfdir, NULL);
    const gchar *datadir_g = (*env)->GetStringUTFChars(env, datadir, NULL); 
    const gchar *libdir_g = (*env)->GetStringUTFChars(env, libdir, NULL);
    gint32 argc_g = (gint32) argc;
    gchar **argv_g = getStringArray(env, argv);
	const GnomeModuleInfo *module_info;

	if (type == 0)
		module_info = LIBGNOMEUI_MODULE;
	else
		module_info = LIBGNOME_MODULE;

  	jobject retval;
  	if (prefix_g)
      	retval = getHandleFromPointer(env, gnome_program_init
				(appId_g, appVersion_g, module_info, argc_g, argv_g,
	 			GNOME_PARAM_APP_PREFIX, prefix_g,
	 			GNOME_PARAM_APP_SYSCONFDIR, sysconfdir_g,
	 			GNOME_PARAM_APP_DATADIR, datadir_g,
	 			GNOME_PARAM_APP_LIBDIR, libdir_g,
	 			NULL));
  	else
      	retval = getHandleFromPointer(env, gnome_program_init(appId_g, appVersion_g,
				module_info, argc_g, argv_g, NULL));
  	(*env)->ReleaseStringUTFChars(env, appId, appId_g );
  	(*env)->ReleaseStringUTFChars(env, appVersion, appVersion_g );
  	(*env)->ReleaseStringUTFChars(env, prefix, prefix_g );
  	(*env)->ReleaseStringUTFChars(env, sysconfdir, sysconfdir_g );
  	(*env)->ReleaseStringUTFChars(env, datadir, datadir_g );
  	(*env)->ReleaseStringUTFChars(env, libdir, libdir_g );
  	freeStringArray(env, argv, argv_g);
  	return retval;
}

/*
 * Class:     org.gnu.gnome.Program
 * Method:    gnome_program_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_Program_gnome_1program_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gnome_program_get_type ();
}

/*
 * Class:     org.gnu.gnome.Program
 * Method:    gnome_program_get
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Program_gnome_1program_1get (JNIEnv *env, jclass cls) 
{
    return getHandleFromPointer(env, gnome_program_get ());
}

/*
 * Class:     org.gnu.gnome.Program
 * Method:    gnome_program_get_human_readable_name
 */
JNIEXPORT jstring JNICALL 
Java_org_gnu_gnome_Program_gnome_1program_1get_1human_1readable_1name (JNIEnv *env, jclass cls, 
    jobject program) 
{
    GnomeProgram *program_g = (GnomeProgram *)getPointerFromHandle(env, program);
	return (*env)->NewStringUTF(env, (gchar*) gnome_program_get_human_readable_name (program_g));
}

/*
 * Class:     org.gnu.gnome.Program
 * Method:    gnome_program_get_app_id
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_Program_gnome_1program_1get_1app_1id (JNIEnv 
    *env, jclass cls, jobject program) 
{
    GnomeProgram *program_g = (GnomeProgram *)getPointerFromHandle(env, program);
	return (*env)->NewStringUTF(env, (gchar*)gnome_program_get_app_id (program_g) );
}

/*
 * Class:     org.gnu.gnome.Program
 * Method:    gnome_program_get_app_version
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_Program_gnome_1program_1get_1app_1version (
    JNIEnv *env, jclass cls, jobject program) 
{
    GnomeProgram *program_g = (GnomeProgram *)getPointerFromHandle(env, program);
	return (*env)->NewStringUTF(env, (gchar*)gnome_program_get_app_version (program_g));
}

/*
 * Class:     org.gnu.gnome.Program
 * Method:    gnome_program_module_register
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Program_gnome_1program_1module_1register (JNIEnv 
    *env, jclass cls, jobject moduleInfo) 
{
    GnomeModuleInfo *moduleInfo_g = (GnomeModuleInfo *)getPointerFromHandle(env, moduleInfo);
    gnome_program_module_register (moduleInfo_g);
}

/*
 * Class:     org.gnu.gnome.Program
 * Method:    gnome_program_module_registered
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_Program_gnome_1program_1module_1registered (
    JNIEnv *env, jclass cls, jobject moduleInfo) 
{
    GnomeModuleInfo *moduleInfo_g = (GnomeModuleInfo *)getPointerFromHandle(env, moduleInfo);
   	return (jboolean) (gnome_program_module_registered (moduleInfo_g));
}

/*
 * Class:     org.gnu.gnome.Program
 * Method:    gnome_program_module_load
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Program_gnome_1program_1module_1load (JNIEnv *env, 
    jclass cls, jstring modName) 
{
    const gchar* modName_g = (*env)->GetStringUTFChars(env, modName, NULL);
	jobject retval =  getHandleFromPointer(env, (GnomeModuleInfo*)gnome_program_module_load (modName_g));
    (*env)->ReleaseStringUTFChars( env, modName, modName_g );
	return retval;
}


#ifdef __cplusplus
}

#endif
