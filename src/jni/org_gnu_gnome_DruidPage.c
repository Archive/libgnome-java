/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_DruidPage.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.DruidPage
 * Method:    gnome_druid_page_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_DruidPage_gnome_1druid_1page_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gnome_druid_page_get_type ();
}

/*
 * Class:     org.gnu.gnome.DruidPage
 * Method:    gnome_druid_page_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPage_gnome_1druid_1page_1new (JNIEnv *env, 
    jclass cls) 
{
    return getGObjectHandle(env, G_OBJECT(gnome_druid_page_new ()));
}

/*
 * Class:     org.gnu.gnome.DruidPage
 * Method:    gnome_druid_page_next
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_DruidPage_gnome_1druid_1page_1next (JNIEnv *env, 
    jclass cls, jobject page) 
{
    GnomeDruidPage *page_g = (GnomeDruidPage *)getPointerFromHandle(env, page);
    return (jboolean) (gnome_druid_page_next (page_g));
}

/*
 * Class:     org.gnu.gnome.DruidPage
 * Method:    gnome_druid_page_prepare
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_DruidPage_gnome_1druid_1page_1prepare (JNIEnv *env, 
    jclass cls, jobject page) 
{
    GnomeDruidPage *page_g = (GnomeDruidPage *)getPointerFromHandle(env, page);
    gnome_druid_page_prepare (page_g);
}

/*
 * Class:     org.gnu.gnome.DruidPage
 * Method:    gnome_druid_page_back
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_DruidPage_gnome_1druid_1page_1back (JNIEnv *env, 
    jclass cls, jobject page) 
{
    GnomeDruidPage *page_g = (GnomeDruidPage *)getPointerFromHandle(env, page);
    return (jboolean) (gnome_druid_page_back (page_g));
}

/*
 * Class:     org.gnu.gnome.DruidPage
 * Method:    gnome_druid_page_cancel
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_DruidPage_gnome_1druid_1page_1cancel (JNIEnv 
    *env, jclass cls, jobject page) 
{
    GnomeDruidPage *page_g = (GnomeDruidPage *)getPointerFromHandle(env, page);
    return (jboolean) (gnome_druid_page_cancel (page_g));
}

/*
 * Class:     org.gnu.gnome.DruidPage
 * Method:    gnome_druid_page_finish
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_DruidPage_gnome_1druid_1page_1finish (JNIEnv *env, 
    jclass cls, jobject page) 
{
    GnomeDruidPage *page_g = (GnomeDruidPage *)getPointerFromHandle(env, page);
    gnome_druid_page_finish (page_g);
}


#ifdef __cplusplus
}

#endif
