/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_UIBuilderData.h"

#ifdef __cplusplus
extern "C" 
{
#endif

static gpointer GnomeUIBuilderData_get_data (GnomeUIBuilderData * cptr) 
{
    return cptr->data;
}

/*
 * Class:     org.gnu.gnome.UIBuilderData
 * Method:    getData
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_UIBuilderData_getData (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeUIBuilderData *cptr_g = (GnomeUIBuilderData *)getPointerFromHandle(env, cptr);
    return getHandleFromPointer(env, GnomeUIBuilderData_get_data (cptr_g));
}

static gboolean GnomeUIBuilderData_get_is_interp (GnomeUIBuilderData * cptr) 
{
    return cptr->is_interp;
}

/*
 * Class:     org.gnu.gnome.UIBuilderData
 * Method:    getIsInterp
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_UIBuilderData_getIsInterp (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GnomeUIBuilderData *cptr_g = (GnomeUIBuilderData *)getPointerFromHandle(env, cptr);
    return (jboolean) (GnomeUIBuilderData_get_is_interp (cptr_g));
}


#ifdef __cplusplus
}

#endif
