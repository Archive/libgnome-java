/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomecanvas/libgnomecanvas.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_CanvasPoints.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.CanvasPoints
 * Method:    gnome_canvas_points_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasPoints_gnome_1canvas_1points_1new (JNIEnv *env, 
    jclass cls, jint numPoints) 
{
    gint32 numPoints_g = (gint32) numPoints;
    return getStructHandle(env, gnome_canvas_points_new (numPoints_g), NULL,
    		(JGFreeFunc) gnome_canvas_points_free);
}

/*
 * Class:     org.gnu.gnome.CanvasPoints
 * Method:    gnome_canvas_points_ref
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasPoints_gnome_1canvas_1points_1ref (JNIEnv *env, 
    jclass cls, jobject points) 
{
    GnomeCanvasPoints *points_g = (GnomeCanvasPoints *)getPointerFromHandle(env, points);
    gnome_canvas_points_ref (points_g);
}

/*
 * Class:     org.gnu.gnome.CanvasPoints
 * Method:    gnome_canvas_points_free
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasPoints_gnome_1canvas_1points_1free (JNIEnv 
    *env, jclass cls, jobject points) 
{
    GnomeCanvasPoints *points_g = (GnomeCanvasPoints *)getPointerFromHandle(env, points);
    gnome_canvas_points_free (points_g);
}

/*
 * Class:     org.gnu.gnome.CanvasPoints
 * Method:    gnome_canvas_points_set_point
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasPoints_gnome_1canvas_1points_1set_1point (
    JNIEnv *env, jclass cls, jobject points, jint index, jdouble x, jdouble y) 
{
  	// points are stored in an array with x and y alternating.  Thus the x
  	// coordinate of point 0 is at points_g->coords[0], the y coordinate of
  	// point 0 is at points_g->coords[1].  The x coordinate of point 1 is at
  	// points_g->coords[2], the y coordinate of point 1 is at
  	// points_g->coords[3].  Etc.
    GnomeCanvasPoints *points_g = (GnomeCanvasPoints *)getPointerFromHandle(env, points);
  	gint32 index_g = (gint32) index;
  	gdouble x_g = (gdouble) x;
  	gdouble y_g = (gdouble) y;
  	// Make sure a good index value was passed in.  We don't want to attmept
  	// to access memory outside of the array.
  	if (index_g > -1 && index_g < points_g->num_points) {
		gint32 xindex = index_g * 2;
		gint32 yindex = xindex + 1;
		points_g->coords[xindex] = x_g;
  		points_g->coords[yindex] = y_g;
  	}
}


#ifdef __cplusplus
}

#endif
