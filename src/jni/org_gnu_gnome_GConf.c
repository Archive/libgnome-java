/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_GConf.h"

#ifdef __cplusplus

extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.GConf
 * Method:    gnome_gconf_get_gnome_libs_settings_relative
 */
JNIEXPORT jstring JNICALL 
Java_org_gnu_gnome_GConf_gnome_1gconf_1get_1gnome_1libs_1settings_1relative (JNIEnv *env, 
    jclass cls, jstring subkey) 
{
    gchar* subkey_g = (gchar*)(*env)->GetStringUTFChars(env, subkey, 0);
    gchar *result_g = (gchar*)gnome_gconf_get_gnome_libs_settings_relative (subkey_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.GConf
 * Method:    gnome_gconf_get_app_settings_relative
 */
JNIEXPORT jstring JNICALL 
Java_org_gnu_gnome_GConf_gnome_1gconf_1get_1app_1settings_1relative (JNIEnv *env, jclass cls, 
    jobject program, jstring subkey) 
{
	GnomeProgram* program_g = (GnomeProgram*)getPointerFromHandle(env, program);
    gchar* subkey_g = (gchar*)(*env)->GetStringUTFChars(env, subkey, 0);
    gchar *result_g = (gchar*)gnome_gconf_get_app_settings_relative (program_g, subkey_g);
    return (*env)->NewStringUTF(env, result_g);
}


#ifdef __cplusplus
}

#endif
