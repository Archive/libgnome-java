/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_Url.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.Url
 * Method:    gnome_url_show
 * Signature: (java.lang.String;[Lint ;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_Url_gnome_1url_1show (JNIEnv *env, jclass cls, 
    jstring url, jintArray error) 
{
    const gchar* url_g = (*env)->GetStringUTFChars(env, url, 0);
    gint *error_g_g = (gint *) (*env)->GetIntArrayElements (env, error, 0);
    GError *error_g = (GError *)error_g_g;
    jboolean result_j = (jboolean) (gnome_url_show (url_g, &error_g));
    (*env)->ReleaseIntArrayElements (env, error, (jint*)error_g_g, 0);
    (*env)->ReleaseStringUTFChars(env, url, url_g);
    return result_j;
}


#ifdef __cplusplus
}

#endif
