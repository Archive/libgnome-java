/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomecanvas/libgnomecanvas.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_CanvasPathDef.h"

#ifdef __cplusplus

extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1new (JNIEnv 
    *env, jclass cls) 
{
    return getGObjectHandle(env, G_OBJECT(gnome_canvas_path_def_new ()));
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_new_sized
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1new_1sized (
    JNIEnv *env, jclass cls, jint length) 
{
    gint32 length_g = (gint32) length;
    return getGObjectHandle(env, G_OBJECT(gnome_canvas_path_def_new_sized (length_g)));
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_ref
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1ref (JNIEnv 
    *env, jclass cls, jobject path) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    gnome_canvas_path_def_ref (path_g);
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_finish
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1finish (
    JNIEnv *env, jclass cls, jobject path) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    gnome_canvas_path_def_finish (path_g);
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_ensure_space
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1ensure_1space (
    JNIEnv *env, jclass cls, jobject path, jint space) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    gint32 space_g = (gint32) space;
    gnome_canvas_path_def_ensure_space (path_g, space_g);
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_copy
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1copy (JNIEnv 
    *env, jclass cls, jobject dst, jobject src) 
{
    GnomeCanvasPathDef *dst_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, dst);
    GnomeCanvasPathDef *src_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, src);
    gnome_canvas_path_def_copy (dst_g, src_g);
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_duplicate
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1duplicate (
    JNIEnv *env, jclass cls, jobject path) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    return getGObjectHandle(env, G_OBJECT(gnome_canvas_path_def_duplicate (path_g)));
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_unref
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1unref (JNIEnv 
    *env, jclass cls, jobject path) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    gnome_canvas_path_def_unref (path_g);
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_reset
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1reset (JNIEnv 
    *env, jclass cls, jobject path) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    gnome_canvas_path_def_reset (path_g);
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_moveto
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1moveto (
    JNIEnv *env, jclass cls, jobject path, jdouble x, jdouble y) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    gdouble x_g = (gdouble) x;
    gdouble y_g = (gdouble) y;
    gnome_canvas_path_def_moveto (path_g, x_g, y_g);
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_lineto
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1lineto (
    JNIEnv *env, jclass cls, jobject path, jdouble x, jdouble y) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    gdouble x_g = (gdouble) x;
    gdouble y_g = (gdouble) y;
    gnome_canvas_path_def_lineto (path_g, x_g, y_g);
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_lineto_moving
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1lineto_1moving (JNIEnv *env, jclass 
    cls, jobject path, jdouble x, jdouble y) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    gdouble x_g = (gdouble) x;
    gdouble y_g = (gdouble) y;
    gnome_canvas_path_def_lineto_moving (path_g, x_g, y_g);
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_curveto
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1curveto (
    JNIEnv *env, jclass cls, jobject path, jdouble x0, jdouble y0, jdouble x1, jdouble y1, jdouble 
    x2, jdouble y2) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    gdouble x0_g = (gdouble) x0;
    gdouble y0_g = (gdouble) y0;
    gdouble x1_g = (gdouble) x1;
    gdouble y1_g = (gdouble) y1;
    gdouble x2_g = (gdouble) x2;
    gdouble y2_g = (gdouble) y2;
    gnome_canvas_path_def_curveto (path_g, x0_g, y0_g, x1_g, y1_g, x2_g, y2_g);
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_closepath
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1closepath (
    JNIEnv *env, jclass cls, jobject path) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    gnome_canvas_path_def_closepath (path_g);
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_closepath_current
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1closepath_1current (JNIEnv *env, 
    jclass cls, jobject path) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    gnome_canvas_path_def_closepath_current (path_g);
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_length
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1length (
    JNIEnv *env, jclass cls, jobject path) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    return (jint) (gnome_canvas_path_def_length (path_g));
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_is_empty
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1is_1empty (
    JNIEnv *env, jclass cls, jobject path) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    return (jboolean) (gnome_canvas_path_def_is_empty (path_g));
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_has_currentpoint
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1has_1currentpoint (JNIEnv *env, 
    jclass cls, jobject path) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    return (jboolean) (gnome_canvas_path_def_has_currentpoint (path_g));
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_any_open
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1any_1open (
    JNIEnv *env, jclass cls, jobject path) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    return (jboolean) (gnome_canvas_path_def_any_open (path_g));
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_all_open
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1all_1open (
    JNIEnv *env, jclass cls, jobject path) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    return (jboolean) (gnome_canvas_path_def_all_open (path_g));
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_any_closed
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1any_1closed (JNIEnv *env, jclass 
    cls, jobject path) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    return (jboolean) (gnome_canvas_path_def_any_closed (path_g));
}

/*
 * Class:     org.gnu.gnome.CanvasPathDef
 * Method:    gnome_canvas_path_def_all_closed
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gnome_CanvasPathDef_gnome_1canvas_1path_1def_1all_1closed (JNIEnv *env, jclass 
    cls, jobject path) 
{
    GnomeCanvasPathDef *path_g = (GnomeCanvasPathDef *)getPointerFromHandle(env, path);
    return (jboolean) (gnome_canvas_path_def_all_closed (path_g));
}


#ifdef __cplusplus
}

#endif
