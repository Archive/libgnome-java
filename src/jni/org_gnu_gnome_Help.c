/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_Help.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.Help
 * Method:    gnome_help_display
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_Help_gnome_1help_1display(JNIEnv *env, jclass 
    cls, jstring filename, jstring linkId, jintArray error) 
{
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
    gchar* linkId_g = (gchar*)(*env)->GetStringUTFChars(env, linkId, 0);
    GError *error_g = (GError *) (*env)->GetIntArrayElements (env, error, NULL);
    jboolean result_j = (jboolean) (gnome_help_display (filename_g, linkId_g, &error_g));
  	(*env)->ReleaseIntArrayElements (env, error, (jint *) error_g, 0);
    if (filename) 
    	(*env)->ReleaseStringUTFChars(env, filename, filename_g);
    if (linkId) 
    	(*env)->ReleaseStringUTFChars(env, linkId, linkId_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.Help
 * Method:    gnome_help_display_with_doc_id
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_Help_gnome_1help_1display_1with_1doc_1id (JNIEnv 
    *env, jclass cls, jobject program, jstring docId, jstring filename, jstring lineId, jintArray error) 
{
	GnomeProgram* program_g = (GnomeProgram*)getPointerFromHandle(env, program);
    gchar* docId_g = (gchar*)(*env)->GetStringUTFChars(env, docId, 0);
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
    gchar* lineId_g = (gchar*)(*env)->GetStringUTFChars(env, lineId, 0);
    GError *error_g = (GError *) (*env)->GetIntArrayElements (env, error, NULL);
    jboolean result_j = (jboolean) (gnome_help_display_with_doc_id
        	(program_g, docId_g, filename_g, lineId_g, &error_g));
    (*env)->ReleaseIntArrayElements (env, error, (jint *) error_g, 0);
    if (docId) 
    	(*env)->ReleaseStringUTFChars(env, docId, docId_g);
    if (filename) 
    	(*env)->ReleaseStringUTFChars(env, filename, filename_g);
    if (lineId) 
    	(*env)->ReleaseStringUTFChars(env, lineId, lineId_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.Help
 * Method:    gnome_help_display_desktop
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_Help_gnome_1help_1display_1desktop (JNIEnv *env, 
    jclass cls, jobject program, jstring docId, jstring filename, jstring linkId, jintArray error) 
{
	GnomeProgram* program_g = (GnomeProgram*)getPointerFromHandle(env, program);
    gchar* docId_g = (gchar*)(*env)->GetStringUTFChars(env, docId, 0);
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
    gchar* linkId_g = (gchar*)(*env)->GetStringUTFChars(env, linkId, 0);
    GError *error_g = (GError *) (*env)->GetIntArrayElements (env, error, NULL);
    jboolean result_j = (jboolean) gnome_help_display_desktop (program_g, docId_g, 
                filename_g, linkId_g, &error_g);
    (*env)->ReleaseIntArrayElements (env, error, (jint *) error_g, 0);
    if (docId) 
    	(*env)->ReleaseStringUTFChars(env, docId, docId_g);
    if (filename) 
    	(*env)->ReleaseStringUTFChars(env, filename, filename_g);
    if (linkId) 
    	(*env)->ReleaseStringUTFChars(env, linkId, linkId_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.Help
 * Method:    gnome_help_display_uri
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_Help_gnome_1help_1display_1uri (JNIEnv *env, 
    jclass cls, jstring helpUri, jintArray error) 
{
    gchar* helpUri_g = (gchar*)(*env)->GetStringUTFChars(env, helpUri, 0);
    GError *error_g = (GError *) (*env)->GetIntArrayElements (env, error, NULL);
    jboolean result_j = (jboolean) gnome_help_display_uri (helpUri_g, &error_g);
    (*env)->ReleaseIntArrayElements (env, error, (jint *) error_g, 0);
    if (helpUri) 
    	(*env)->ReleaseStringUTFChars(env, helpUri, helpUri_g);
    return result_j;
}


#ifdef __cplusplus
}

#endif
