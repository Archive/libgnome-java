/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_ModuleInfo.h"

#ifdef __cplusplus
extern "C" 
{
#endif

static gchar * GnomeModuleInfo_get_name (GnomeModuleInfo * cptr) 
{
    return (gchar *)cptr->name;
}

/*
 * Class:     org.gnu.gnome.ModuleInfo
 * Method:    getName
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_ModuleInfo_getName (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
	GnomeModuleInfo* cptr_g = (GnomeModuleInfo*)getPointerFromHandle(env, cptr);
    gchar *result_g = (gchar*)GnomeModuleInfo_get_name (cptr_g);
    return (*env)->NewStringUTF(env, result_g);
}

static gchar * GnomeModuleInfo_get_version (GnomeModuleInfo * cptr) 
{
    return (gchar *)cptr->version;
}

/*
 * Class:     org.gnu.gnome.ModuleInfo
 * Method:    getVersion
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_ModuleInfo_getVersion (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
	GnomeModuleInfo* cptr_g = (GnomeModuleInfo*)getPointerFromHandle(env, cptr);
    gchar *result_g = (gchar*)GnomeModuleInfo_get_version (cptr_g);
    return (*env)->NewStringUTF(env, result_g);
}

static gchar * GnomeModuleInfo_get_description (GnomeModuleInfo * cptr) 
{
    return (gchar *)cptr->description;
}

/*
 * Class:     org.gnu.gnome.ModuleInfo
 * Method:    getDescription
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_ModuleInfo_getDescription (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
	GnomeModuleInfo* cptr_g = (GnomeModuleInfo*)getPointerFromHandle(env, cptr);
    gchar *result_g = (gchar*)GnomeModuleInfo_get_description (cptr_g);
    return (*env)->NewStringUTF(env, result_g);
}


#ifdef __cplusplus
}

#endif
