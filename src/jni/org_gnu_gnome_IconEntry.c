/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_IconEntry.h"

#ifdef __cplusplus

extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.IconEntry
 * Method:    gnome_icon_entry_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_IconEntry_gnome_1icon_1entry_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gnome_icon_entry_get_type ();
}

/*
 * Class:     org.gnu.gnome.IconEntry
 * Method:    gnome_icon_entry_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_IconEntry_gnome_1icon_1entry_1new (JNIEnv *env, 
    jclass cls, jstring historyId, jstring browserDialogTitle) 
{
    gchar* historyId_g = (gchar*)(*env)->GetStringUTFChars(env, historyId, 0);
    gchar* browserDialogTitle_g = (gchar*)(*env)->GetStringUTFChars(env, browserDialogTitle, 0);
    jobject result = getGObjectHandle(env, G_OBJECT(gnome_icon_entry_new (historyId_g,
    		browserDialogTitle_g)));
    if (historyId) 
    	(*env)->ReleaseStringUTFChars(env, historyId, historyId_g);
    if (browserDialogTitle) 
    	(*env)->ReleaseStringUTFChars(env, browserDialogTitle, browserDialogTitle_g);
    return result;
}

/*
 * Class:     org.gnu.gnome.IconEntry
 * Method:    gnome_icon_entry_set_pixmap_subdir
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconEntry_gnome_1icon_1entry_1set_1pixmap_1subdir (
    JNIEnv *env, jclass cls, jobject ientry, jstring subdir) 
{
	GnomeIconEntry* ientry_g = (GnomeIconEntry*)getPointerFromHandle(env, ientry);
    gchar* subdir_g = (gchar*)(*env)->GetStringUTFChars(env, subdir, 0);
    gnome_icon_entry_set_pixmap_subdir (ientry_g, subdir_g);
    if (subdir) 
    	(*env)->ReleaseStringUTFChars(env, subdir, subdir_g);
}

/*
 * Class:     org.gnu.gnome.IconEntry
 * Method:    gnome_icon_entry_get_filename
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_IconEntry_gnome_1icon_1entry_1get_1filename (
    JNIEnv *env, jclass cls, jobject ientry) 
{
	GnomeIconEntry* ientry_g = (GnomeIconEntry*)getPointerFromHandle(env, ientry);
    gchar *result_g = (gchar*)gnome_icon_entry_get_filename (ientry_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.IconEntry
 * Method:    gnome_icon_entry_set_filename
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_IconEntry_gnome_1icon_1entry_1set_1filename (
    JNIEnv *env, jclass cls, jobject ientry, jstring filename) 
{
	GnomeIconEntry* ientry_g = (GnomeIconEntry*)getPointerFromHandle(env, ientry);
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
    jboolean result_j = (jboolean) (gnome_icon_entry_set_filename(ientry_g, filename_g));
    if (filename) 
    	(*env)->ReleaseStringUTFChars(env, filename, filename_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.IconEntry
 * Method:    gnome_icon_entry_set_browse_dialog_title
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_IconEntry_gnome_1icon_1entry_1set_1browse_1dialog_1title (JNIEnv *env, 
    jclass cls, jobject ientry, jstring browseDialogTitle) 
{
	GnomeIconEntry* ientry_g = (GnomeIconEntry*)getPointerFromHandle(env, ientry);
    gchar* browseDialogTitle_g = (gchar*)(*env)->GetStringUTFChars(env, browseDialogTitle, 0);
    gnome_icon_entry_set_browse_dialog_title (ientry_g, browseDialogTitle_g);
    if (browseDialogTitle) 
    	(*env)->ReleaseStringUTFChars(env, browseDialogTitle, browseDialogTitle_g);
}

/*
 * Class:     org.gnu.gnome.IconEntry
 * Method:    gnome_icon_entry_set_history_id
 * Signature: (Ijava.lang.String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconEntry_gnome_1icon_1entry_1set_1history_1id (
    JNIEnv *env, jclass cls, jobject ientry, jstring historyId) 
{
	GnomeIconEntry* ientry_g = (GnomeIconEntry*)getPointerFromHandle(env, ientry);
    gchar* historyId_g = (gchar*)(*env)->GetStringUTFChars(env, historyId, 0);
    gnome_icon_entry_set_history_id (ientry_g, historyId_g);
    if (historyId) 
    	(*env)->ReleaseStringUTFChars(env, historyId, historyId_g);
}

/*
 * Class:     org.gnu.gnome.IconEntry
 * Method:    gnome_icon_entry_pick_dialog
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_IconEntry_gnome_1icon_1entry_1pick_1dialog (JNIEnv 
    *env, jclass cls, jobject ientry) 
{
	GnomeIconEntry* ientry_g = (GnomeIconEntry*)getPointerFromHandle(env, ientry);
    return getGObjectHandle(env, G_OBJECT(gnome_icon_entry_pick_dialog (ientry_g)));
}

/*
 * Class:     org_gnu_gnome_IconEntry
 * Method:    gnome_icon_entry_set_max_saved
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconEntry_gnome_1icon_1entry_1set_1max_1saved
  (JNIEnv *env, jclass cls, jobject ientry, jint max)
{
	GnomeIconEntry* ientry_g = (GnomeIconEntry*)getPointerFromHandle(env, ientry);
	gnome_icon_entry_set_max_saved(ientry_g, (guint)max);
}

#ifdef __cplusplus
}

#endif
