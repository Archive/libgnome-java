/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_Client.h"

#ifdef __cplusplus
extern "C" 
{
#endif


/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_Client_gnome_1client_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)gnome_client_get_type ();
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_get_config_prefix
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_Client_gnome_1client_1get_1config_1prefix (
    JNIEnv *env, jclass cls, jobject client) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gchar *result_g = (gchar*)gnome_client_get_config_prefix (client_g);
   	return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_get_global_config_prefix
 */
JNIEXPORT jstring JNICALL 
Java_org_gnu_gnome_Client_gnome_1client_1get_1global_1config_1prefix (JNIEnv *env, jclass cls, 
    jobject client) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gchar *result_g = (gchar*)gnome_client_get_global_config_prefix (client_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_set_global_config_prefix
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1set_1global_1config_1prefix (
    JNIEnv *env, jclass cls, jobject client, jstring prefix) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    const gchar* prefix_g = (*env)->GetStringUTFChars(env, prefix, 0);
    gnome_client_set_global_config_prefix (client_g, prefix_g);
    if (prefix) 
    	(*env)->ReleaseStringUTFChars(env, prefix, prefix_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_get_flags
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_Client_gnome_1client_1get_1flags (JNIEnv *env, jclass 
    cls, jobject client) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    return (jint) (gnome_client_get_flags (client_g));
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_set_restart_style
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1set_1restart_1style (JNIEnv 
    *env, jclass cls, jobject client, jint style) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    GnomeRestartStyle style_g = (GnomeRestartStyle) style;
    gnome_client_set_restart_style (client_g, style_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_set_priority
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1set_1priority (JNIEnv *env, 
    jclass cls, jobject client, jint priority) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gint32 priority_g = (gint32) priority;
    gnome_client_set_priority (client_g, priority_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_set_restart_command
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1set_1restart_1command (JNIEnv 
    *env, jclass cls, jobject client, jint argc, jobjectArray args) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gint32 argc_g_int = ((gint32) argc) + 1;
    gchar **args_g_arr = getStringArray(env, args);
    gnome_client_set_restart_command (client_g, argc_g_int, args_g_arr);
    freeStringArray(env, args, args_g_arr);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_set_discard_command
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1set_1discard_1command (JNIEnv 
    *env, jclass cls, jobject client, jint argc, jobjectArray args) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gint32 argc_g_int = ((gint32) argc) + 1;
    gchar **args_g_arr = getStringArray(env, args);
    gnome_client_set_discard_command (client_g, argc_g_int, args_g_arr);
    freeStringArray(env, args, args_g_arr);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_set_resign_command
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1set_1resign_1command (JNIEnv 
    *env, jclass cls, jobject client, jint argc, jobjectArray args) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gint32 argc_g_int = ((gint32) argc) + 1;
    gchar **args_g_arr = getStringArray(env, args);
    gnome_client_set_resign_command (client_g, argc_g_int, args_g_arr);
    freeStringArray(env, args, args_g_arr);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_set_shutdown_command
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1set_1shutdown_1command (JNIEnv 
    *env, jclass cls, jobject client, jint argc, jobjectArray args) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gint32 argc_g = ((gint32) argc) + 1;
	gchar **args_g = getStringArray(env, args);
    gnome_client_set_shutdown_command (client_g, argc_g, args_g);
    freeStringArray(env, args, args_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_set_current_directory
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1set_1current_1directory (JNIEnv 
    *env, jclass cls, jobject client, jstring dir) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    const gchar* dir_g = (*env)->GetStringUTFChars(env, dir, 0);
    gnome_client_set_current_directory (client_g, dir_g);
    if (dir) 
    	(*env)->ReleaseStringUTFChars(env, dir, dir_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_set_environment
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1set_1environment (JNIEnv *env, 
    jclass cls, jobject client, jstring name, jstring value) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    const gchar* name_g = (*env)->GetStringUTFChars(env, name, 0);
    const gchar* value_g = (*env)->GetStringUTFChars(env, value, 0);
    gnome_client_set_environment (client_g, name_g, value_g);
    if (name) 
    	(*env)->ReleaseStringUTFChars(env, name, name_g);
    if (value) 
    	(*env)->ReleaseStringUTFChars(env, value, value_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_set_clone_command
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1set_1clone_1command (JNIEnv 
    *env, jclass cls, jobject client, jint argc, jobjectArray args) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gint32 argc_g = ((gint32) argc) + 1;
	gchar **args_g = getStringArray(env, args);
    gnome_client_set_clone_command (client_g, argc_g, args_g);
    freeStringArray(env, args, args_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_set_process_id
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1set_1process_1id (JNIEnv *env, 
    jclass cls, jobject client, jint id) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gint32 id_g = (gint32) id;
    gnome_client_set_process_id (client_g, id_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_set_program
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1set_1program (JNIEnv *env, 
    jclass cls, jobject client, jstring program) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    const gchar* program_g = (*env)->GetStringUTFChars(env, program, 0);
    gnome_client_set_program (client_g, program_g);
    if (program) 
    	(*env)->ReleaseStringUTFChars(env, program, program_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_set_user_id
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1set_1user_1id (JNIEnv *env, 
    jclass cls, jobject client, jstring id) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    const gchar* id_g = (*env)->GetStringUTFChars(env, id, 0);
    gnome_client_set_user_id (client_g, id_g);
    if (id) 
    	(*env)->ReleaseStringUTFChars(env, id, id_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_save_any_dialog
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1save_1any_1dialog (JNIEnv *env, 
    jclass cls, jobject client, jobject dialog) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    GtkDialog *dialog_g = (GtkDialog *)getPointerFromHandle(env, dialog);
    gnome_client_save_any_dialog (client_g, dialog_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_save_error_dialog
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1save_1error_1dialog (JNIEnv 
    *env, jclass cls, jobject client, jobject dialog) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    GtkDialog *dialog_g = (GtkDialog *)getPointerFromHandle(env, dialog);
    gnome_client_save_error_dialog (client_g, dialog_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_request_phase_2
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1request_1phase_12 (JNIEnv *env, 
    jclass cls, jobject client) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gnome_client_request_phase_2 (client_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_request_save
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1request_1save (JNIEnv *env, 
    jclass cls, jobject client, jint save_style, jboolean shutdown, jint interact_style, jboolean 
    fast, jboolean global) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    GnomeSaveStyle save_style_g = (GnomeSaveStyle) save_style;
    gboolean shutdown_g = (gboolean) shutdown;
    GnomeInteractStyle interact_style_g = (GnomeInteractStyle) interact_style;
    gboolean fast_g = (gboolean) fast;
    gboolean global_g = (gboolean) global;
    gnome_client_request_save (client_g, save_style_g, shutdown_g, interact_style_g, 
            fast_g, global_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_flush
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1flush (JNIEnv *env, jclass cls, 
    jobject client) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gnome_client_flush (client_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Client_gnome_1client_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, G_OBJECT(gnome_client_new ()));
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_connect
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1connect (JNIEnv *env, jclass 
    cls, jobject client) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gnome_client_connect (client_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_disconnect
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1disconnect (JNIEnv *env, jclass 
    cls, jobject client) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gnome_client_disconnect (client_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_set_id
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Client_gnome_1client_1set_1id (JNIEnv *env, jclass 
    cls, jobject client, jstring id) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    const gchar* id_g = (*env)->GetStringUTFChars(env, id, 0);
    gnome_client_set_id (client_g, id_g);
    if (id) 
    	(*env)->ReleaseStringUTFChars(env, id, id_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_get_id
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_Client_gnome_1client_1get_1id (JNIEnv *env, 
    jclass cls, jobject client) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gchar *result_g = (gchar*)gnome_client_get_id (client_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_get_previous_id
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_Client_gnome_1client_1get_1previous_1id (JNIEnv 
    *env, jclass cls, jobject client) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gchar *result_g = (gchar*)gnome_client_get_previous_id (client_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.Client
 * Method:    gnome_client_get_desktop_id
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_Client_gnome_1client_1get_1desktop_1id (JNIEnv 
    *env, jclass cls, jobject client) 
{
	GnomeClient* client_g = (GnomeClient*)getPointerFromHandle(env, client);
    gchar *result_g = (gchar*)gnome_client_get_desktop_id (client_g);
    return (*env)->NewStringUTF(env, result_g);
}


#ifdef __cplusplus
}

#endif
