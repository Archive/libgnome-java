/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_Sound.h"

#ifdef __cplusplus

extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.Sound
 * Method:    gnome_sound_connection_get
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_Sound_gnome_1sound_1connection_1get (JNIEnv *env, 
    jclass cls) 
{
    return (jint) (gnome_sound_connection_get ());
}

/*
 * Class:     org.gnu.gnome.Sound
 * Method:    gnome_sound_init
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Sound_gnome_1sound_1init (JNIEnv *env, jclass cls, 
    jstring hostname) 
{
    gchar* hostname_g = (gchar*)(*env)->GetStringUTFChars(env, hostname, 0);
    gnome_sound_init (hostname_g);
    if (hostname) 
    	(*env)->ReleaseStringUTFChars(env, hostname, hostname_g);
}

/*
 * Class:     org.gnu.gnome.Sound
 * Method:    gnome_sound_shutdown
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Sound_gnome_1sound_1shutdown (JNIEnv *env, jclass cls)
{
    gnome_sound_shutdown ();
}

/*
 * Class:     org.gnu.gnome.Sound
 * Method:    gnome_sound_sample_load
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_Sound_gnome_1sound_1sample_1load (JNIEnv *env, jclass 
    cls, jstring sample_name, jstring filename) 
{
    gchar* sample_name_g = (gchar*)(*env)->GetStringUTFChars(env, sample_name, 0);
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
    jint result_j = (jint) (gnome_sound_sample_load (sample_name_g, filename_g));
    if (sample_name) 
    	(*env)->ReleaseStringUTFChars(env, sample_name, sample_name_g);
    if (filename) 
    	(*env)->ReleaseStringUTFChars(env, filename, filename_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.Sound
 * Method:    gnome_sound_play
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Sound_gnome_1sound_1play (JNIEnv *env, jclass cls, 
    jstring filename) 
{
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
    gnome_sound_play (filename_g);
    if (filename) 
    	(*env)->ReleaseStringUTFChars(env, filename, filename_g);
}


#ifdef __cplusplus
}

#endif
