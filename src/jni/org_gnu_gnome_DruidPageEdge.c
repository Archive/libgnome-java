/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_DruidPageEdge.h"

#ifdef __cplusplus

extern "C" 
{
#endif

static gchar * GnomeDruidPageEdge_get_title (GnomeDruidPageEdge * cptr) 
{
    return cptr->title;
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    getTitle
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_DruidPageEdge_getTitle (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
	GnomeDruidPageEdge* cptr_g = (GnomeDruidPageEdge*)getPointerFromHandle(env, cptr);
    gchar *result_g = (gchar*)GnomeDruidPageEdge_get_title (cptr_g);
    return (*env)->NewStringUTF(env, result_g);
}

static gchar * GnomeDruidPageEdge_get_text (GnomeDruidPageEdge * cptr) 
{
    return cptr->text;
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    getText
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_DruidPageEdge_getText (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
	GnomeDruidPageEdge* cptr_g = (GnomeDruidPageEdge*)getPointerFromHandle(env, cptr);
    gchar *result_g = (gchar*)GnomeDruidPageEdge_get_text (cptr_g);
    return (*env)->NewStringUTF(env, result_g);
}

static GdkPixbuf * GnomeDruidPageEdge_get_logo_image (GnomeDruidPageEdge * cptr) 
{
    return cptr->logo_image;
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    getLogoImage
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageEdge_getLogoImage (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
	GnomeDruidPageEdge* cptr_g = (GnomeDruidPageEdge*)getPointerFromHandle(env, cptr);
    return getGBoxedHandle(env, GnomeDruidPageEdge_get_logo_image (cptr_g),
    		GDK_TYPE_COLOR, (GBoxedCopyFunc) gdk_color_copy,
    		(GBoxedFreeFunc) gdk_color_free);
}

static GdkPixbuf * GnomeDruidPageEdge_get_watermark_image (GnomeDruidPageEdge * cptr) 
{
    return cptr->watermark_image;
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    getWatermarkImage
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageEdge_getWatermarkImage (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
	GnomeDruidPageEdge* cptr_g = (GnomeDruidPageEdge*)getPointerFromHandle(env, cptr);
    return getGBoxedHandle(env, GnomeDruidPageEdge_get_watermark_image (cptr_g),
    		GDK_TYPE_COLOR, (GBoxedCopyFunc) gdk_color_copy,
    		(GBoxedFreeFunc) gdk_color_free);
}

static GdkPixbuf * GnomeDruidPageEdge_get_top_watermark_image (GnomeDruidPageEdge * cptr) 
{
    return cptr->top_watermark_image;
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    getTopWatermarkImage
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageEdge_getTopWatermarkImage (JNIEnv *env, 
    jclass cls, jobject cptr) 
{
	GnomeDruidPageEdge* cptr_g = (GnomeDruidPageEdge*)getPointerFromHandle(env, cptr);
    return getGBoxedHandle(env, GnomeDruidPageEdge_get_top_watermark_image (cptr_g),
    		GDK_TYPE_COLOR, (GBoxedCopyFunc) gdk_color_copy,
    		(GBoxedFreeFunc) gdk_color_free);
}

static GdkColor * GnomeDruidPageEdge_get_background_color (GnomeDruidPageEdge * cptr) 
{
    return (GdkColor *)&cptr->background_color;
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    getBackgroundColor
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageEdge_getBackgroundColor (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
	GnomeDruidPageEdge* cptr_g = (GnomeDruidPageEdge*)getPointerFromHandle(env, cptr);
    return getHandleFromPointer(env, GnomeDruidPageEdge_get_background_color (cptr_g));
}

static GdkColor * GnomeDruidPageEdge_get_textbox_color (GnomeDruidPageEdge * cptr) 
{
    return (GdkColor *)&cptr->textbox_color;
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    getTextboxColor
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageEdge_getTextboxColor (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
	GnomeDruidPageEdge* cptr_g = (GnomeDruidPageEdge*)getPointerFromHandle(env, cptr);
    return getGBoxedHandle(env, GnomeDruidPageEdge_get_textbox_color (cptr_g),
    		GDK_TYPE_COLOR, (GBoxedCopyFunc) gdk_color_copy,
    		(GBoxedFreeFunc) gdk_color_free);
}

static GdkColor * GnomeDruidPageEdge_get_logo_background_color (GnomeDruidPageEdge * cptr) 
{
    return (GdkColor *)&cptr->logo_background_color;
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    getLogoBackgroundColor
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageEdge_getLogoBackgroundColor (JNIEnv *env, 
    jclass cls, jobject cptr) 
{
	GnomeDruidPageEdge* cptr_g = (GnomeDruidPageEdge*)getPointerFromHandle(env, cptr);
    return getGBoxedHandle(env, GnomeDruidPageEdge_get_logo_background_color (cptr_g),
    		GDK_TYPE_COLOR, (GBoxedCopyFunc) gdk_color_copy,
    		(GBoxedFreeFunc) gdk_color_free);
}

static GdkColor * GnomeDruidPageEdge_get_title_color (GnomeDruidPageEdge * cptr) 
{
    return (GdkColor *)&cptr->title_color;
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    getTitleColor
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageEdge_getTitleColor (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
	GnomeDruidPageEdge* cptr_g = (GnomeDruidPageEdge*)getPointerFromHandle(env, cptr);
    return getGBoxedHandle(env, GnomeDruidPageEdge_get_title_color (cptr_g),
    		GDK_TYPE_COLOR, (GBoxedCopyFunc) gdk_color_copy,
    		(GBoxedFreeFunc) gdk_color_free);
}

static GdkColor * GnomeDruidPageEdge_get_text_color (GnomeDruidPageEdge * cptr) 
{
    return (GdkColor *)&cptr->text_color;
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    getTextColor
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageEdge_getTextColor (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
	GnomeDruidPageEdge* cptr_g = (GnomeDruidPageEdge*)getPointerFromHandle(env, cptr);
    return getGBoxedHandle(env, GnomeDruidPageEdge_get_text_color (cptr_g),
    		GDK_TYPE_COLOR, (GBoxedCopyFunc) gdk_color_copy,
    		(GBoxedFreeFunc) gdk_color_free);
}

static GnomeEdgePosition GnomeDruidPageEdge_get_position (GnomeDruidPageEdge * cptr) 
{
    return cptr->position;
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    getPosition
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_DruidPageEdge_getPosition (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
	GnomeDruidPageEdge* cptr_g = (GnomeDruidPageEdge*)getPointerFromHandle(env, cptr);
    return(jint) (GnomeDruidPageEdge_get_position (cptr_g));
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    gnome_druid_page_edge_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_DruidPageEdge_gnome_1druid_1page_1edge_1get_1type (
    JNIEnv *env, jclass cls) 
{
    return (jint)gnome_druid_page_edge_get_type ();
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    gnome_druid_page_edge_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageEdge_gnome_1druid_1page_1edge_1new (JNIEnv 
    *env, jclass cls, jint position) 
{
    GnomeEdgePosition position_g = (GnomeEdgePosition) position;
    return getGObjectHandle(env, G_OBJECT(gnome_druid_page_edge_new (position_g)));
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    gnome_druid_page_edge_new_with_vals
 */
JNIEXPORT jobject JNICALL 
Java_org_gnu_gnome_DruidPageEdge_gnome_1druid_1page_1edge_1new_1with_1vals (JNIEnv *env, jclass 
    cls, jint position, jboolean antialiased, jstring title, jstring text, jobject logo, 
    jobject watermark, jobject topWatermark) 
{
    GnomeEdgePosition position_g = (GnomeEdgePosition) position;
    gchar* title_g = (gchar*)(*env)->GetStringUTFChars(env, title, 0);
    gchar* text_g = (gchar*)(*env)->GetStringUTFChars(env, text, 0);
    GdkPixbuf *logo_g = (GdkPixbuf *)getPointerFromHandle(env, logo);
    GdkPixbuf *watermark_g = (GdkPixbuf *)getPointerFromHandle(env, watermark);
    GdkPixbuf *topWatermark_g = (GdkPixbuf *)getPointerFromHandle(env, topWatermark);
    jobject result = getGObjectHandle(env,
    		G_OBJECT(gnome_druid_page_edge_new_with_vals (position_g,
    		(gboolean)antialiased, title_g, 
            text_g, logo_g, watermark_g, topWatermark_g)));
    if (title) 
    	(*env)->ReleaseStringUTFChars(env, title, title_g);
    if (text) 
    	(*env)->ReleaseStringUTFChars(env, text, text_g);
    return result;
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    gnome_druid_page_edge_set_bg_color
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_DruidPageEdge_gnome_1druid_1page_1edge_1set_1bg_1color (JNIEnv *env, jclass 
    cls, jobject dpe, jobject color) 
{
    GnomeDruidPageEdge *dpe_g = (GnomeDruidPageEdge *)getPointerFromHandle(env, dpe);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gnome_druid_page_edge_set_bg_color (dpe_g, color_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    gnome_druid_page_edge_set_textbox_color
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_DruidPageEdge_gnome_1druid_1page_1edge_1set_1textbox_1color (JNIEnv *env, 
    jclass cls, jobject dpe, jobject color) 
{
    GnomeDruidPageEdge *dpe_g = (GnomeDruidPageEdge *)getPointerFromHandle(env, dpe);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gnome_druid_page_edge_set_textbox_color (dpe_g, color_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    gnome_druid_page_edge_set_logo_bg_color
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_DruidPageEdge_gnome_1druid_1page_1edge_1set_1logo_1bg_1color (JNIEnv *env, 
    jclass cls, jobject dpe, jobject color) 
{
    GnomeDruidPageEdge *dpe_g = (GnomeDruidPageEdge *)getPointerFromHandle(env, dpe);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gnome_druid_page_edge_set_logo_bg_color (dpe_g, color_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    gnome_druid_page_edge_set_title_color
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_DruidPageEdge_gnome_1druid_1page_1edge_1set_1title_1color (JNIEnv *env, 
    jclass cls, jobject dpe, jobject color) 
{
    GnomeDruidPageEdge *dpe_g = (GnomeDruidPageEdge *)getPointerFromHandle(env, dpe);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gnome_druid_page_edge_set_title_color (dpe_g, color_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    gnome_druid_page_edge_set_text_color
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_DruidPageEdge_gnome_1druid_1page_1edge_1set_1text_1color (JNIEnv *env, 
    jclass cls, jobject dpe, jobject color) 
{
    GnomeDruidPageEdge *dpe_g = (GnomeDruidPageEdge *)getPointerFromHandle(env, dpe);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gnome_druid_page_edge_set_text_color (dpe_g, color_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    gnome_druid_page_edge_set_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_DruidPageEdge_gnome_1druid_1page_1edge_1set_1text (
    JNIEnv *env, jclass cls, jobject dpe, jstring text) 
{
    GnomeDruidPageEdge *dpe_g = (GnomeDruidPageEdge *)getPointerFromHandle(env, dpe);
    gchar* text_g = (gchar*)(*env)->GetStringUTFChars(env, text, 0);
    gnome_druid_page_edge_set_text(dpe_g, text_g);
    if (text) 
    	(*env)->ReleaseStringUTFChars(env, text, text_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    gnome_druid_page_edge_set_title
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_DruidPageEdge_gnome_1druid_1page_1edge_1set_1title (
    JNIEnv *env, jclass cls, jobject dpe, jstring title) 
{
    GnomeDruidPageEdge *dpe_g = (GnomeDruidPageEdge *)getPointerFromHandle(env, dpe);
    gchar* title_g = (gchar*)(*env)->GetStringUTFChars(env, title, 0);
    gnome_druid_page_edge_set_title(dpe_g, title_g);
    if (title) 
    	(*env)->ReleaseStringUTFChars(env, title, title_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    gnome_druid_page_edge_set_logo
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_DruidPageEdge_gnome_1druid_1page_1edge_1set_1logo (
    JNIEnv *env, jclass cls, jobject dpe, jobject logoImage) 
{
    GnomeDruidPageEdge *dpe_g = (GnomeDruidPageEdge *)getPointerFromHandle(env, dpe);
    GdkPixbuf *logoImage_g = (GdkPixbuf *)getPointerFromHandle(env, logoImage);
    gnome_druid_page_edge_set_logo (dpe_g, logoImage_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    gnome_druid_page_edge_set_watermark
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_DruidPageEdge_gnome_1druid_1page_1edge_1set_1watermark (JNIEnv *env, jclass 
    cls, jobject dpe, jobject watermark) 
{
    GnomeDruidPageEdge *dpe_g = (GnomeDruidPageEdge *)getPointerFromHandle(env, dpe);
    GdkPixbuf *watermark_g = (GdkPixbuf *)getPointerFromHandle(env, watermark);
    gnome_druid_page_edge_set_watermark (dpe_g, watermark_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageEdge
 * Method:    gnome_druid_page_edge_set_top_watermark
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_DruidPageEdge_gnome_1druid_1page_1edge_1set_1top_1watermark (JNIEnv *env, 
    jclass cls, jobject dpe, jobject topWatermark) 
{
    GnomeDruidPageEdge *dpe_g = (GnomeDruidPageEdge *)getPointerFromHandle(env, dpe);
    GdkPixbuf *topWatermark_g = (GdkPixbuf *)getPointerFromHandle(env, topWatermark);
    gnome_druid_page_edge_set_top_watermark (dpe_g, topWatermark_g);
}


#ifdef __cplusplus
}

#endif
