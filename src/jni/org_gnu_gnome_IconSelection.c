/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_IconSelection.h"

#ifdef __cplusplus

extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.IconSelection
 * Method:    gnome_icon_selection_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_IconSelection_gnome_1icon_1selection_1get_1type (
    JNIEnv *env, jclass cls) 
{
    return (jint)gnome_icon_selection_get_type ();
}

/*
 * Class:     org.gnu.gnome.IconSelection
 * Method:    gnome_icon_selection_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_IconSelection_gnome_1icon_1selection_1new (JNIEnv 
    *env, jclass cls) 
{
    return getGObjectHandle(env, G_OBJECT(gnome_icon_selection_new ()));
}

/*
 * Class:     org.gnu.gnome.IconSelection
 * Method:    gnome_icon_selection_add_defaults
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconSelection_gnome_1icon_1selection_1add_1defaults (
    JNIEnv *env, jclass cls, jobject gis) 
{
    GnomeIconSelection *gis_g = (GnomeIconSelection *)getPointerFromHandle(env, gis);
    gnome_icon_selection_add_defaults (gis_g);
}

/*
 * Class:     org.gnu.gnome.IconSelection
 * Method:    gnome_icon_selection_add_directory
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconSelection_gnome_1icon_1selection_1add_1directory (
    JNIEnv *env, jclass cls, jobject gis, jstring dir) 
{
    GnomeIconSelection *gis_g = (GnomeIconSelection *)getPointerFromHandle(env, gis);
    gchar* dir_g = (gchar*)(*env)->GetStringUTFChars(env, dir, 0);
    gnome_icon_selection_add_directory (gis_g, dir_g);
    if (dir) 
    	(*env)->ReleaseStringUTFChars(env, dir, dir_g);
}

/*
 * Class:     org.gnu.gnome.IconSelection
 * Method:    gnome_icon_selection_show_icons
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconSelection_gnome_1icon_1selection_1show_1icons (
    JNIEnv *env, jclass cls, jobject gis) 
{
    GnomeIconSelection *gis_g = (GnomeIconSelection *)getPointerFromHandle(env, gis);
    gnome_icon_selection_show_icons (gis_g);
}

/*
 * Class:     org.gnu.gnome.IconSelection
 * Method:    gnome_icon_selection_clear
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconSelection_gnome_1icon_1selection_1clear (JNIEnv 
    *env, jclass cls, jobject gis, jboolean notShown) 
{
    GnomeIconSelection *gis_g = (GnomeIconSelection *)getPointerFromHandle(env, gis);
    gboolean notShown_g = (gboolean) notShown;
    gnome_icon_selection_clear (gis_g, notShown_g);
}

/*
 * Class:     org.gnu.gnome.IconSelection
 * Method:    gnome_icon_selection_get_icon
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_IconSelection_gnome_1icon_1selection_1get_1icon (
    JNIEnv *env, jclass cls, jobject gis, jboolean fullPath) 
{
    GnomeIconSelection *gis_g = (GnomeIconSelection *)getPointerFromHandle(env, gis);
    gchar *result_g = (gchar*)gnome_icon_selection_get_icon(gis_g, (gboolean)fullPath);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.IconSelection
 * Method:    gnome_icon_selection_select_icon
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconSelection_gnome_1icon_1selection_1select_1icon (
    JNIEnv *env, jclass cls, jobject gis, jstring filename) 
{
    GnomeIconSelection *gis_g = (GnomeIconSelection *)getPointerFromHandle(env, gis);
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
    gnome_icon_selection_select_icon (gis_g, filename_g);
    if (filename) 
    	(*env)->ReleaseStringUTFChars(env, filename, filename_g);
}

/*
 * Class:     org.gnu.gnome.IconSelection
 * Method:    gnome_icon_selection_stop_loading
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconSelection_gnome_1icon_1selection_1stop_1loading (
    JNIEnv *env, jclass cls, jobject gis) 
{
    GnomeIconSelection *gis_g = (GnomeIconSelection *)getPointerFromHandle(env, gis);
    gnome_icon_selection_stop_loading (gis_g);
}

/*
 * Class:     org.gnu.gnome.IconSelection
 * Method:    gnome_icon_selection_get_gil
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_IconSelection_gnome_1icon_1selection_1get_1gil (
    JNIEnv *env, jclass cls, jobject gis) 
{
    GnomeIconSelection *gis_g = (GnomeIconSelection *)getPointerFromHandle(env, gis);
    return getGObjectHandle(env, G_OBJECT(gnome_icon_selection_get_gil (gis_g)));
}

/*
 * Class:     org.gnu.gnome.IconSelection
 * Method:    gnome_icon_selection_get_box
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_IconSelection_gnome_1icon_1selection_1get_1box (
    JNIEnv *env, jclass cls, jobject gis) 
{
    GnomeIconSelection *gis_g = (GnomeIconSelection *)getPointerFromHandle(env, gis);
    return getGObjectHandle(env, G_OBJECT(gnome_icon_selection_get_box (gis_g)));
}


#ifdef __cplusplus
}

#endif
