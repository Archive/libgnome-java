/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_AppBar.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.AppBar
 * Method:    gnome_appbar_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_AppBar_gnome_1appbar_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)gnome_appbar_get_type ();
}

/*
 * Class:     org.gnu.gnome.AppBar
 * Method:    gnome_appbar_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_AppBar_gnome_1appbar_1new (JNIEnv *env, jclass cls, 
    jboolean hasProgress, jboolean hasStatus, jint interactivity) 
{
    gboolean hasProgress_g = (gboolean) hasProgress;
    gboolean hasStatus_g = (gboolean) hasStatus;
    GnomePreferencesType interactivity_g = (GnomePreferencesType) interactivity;
    return getGObjectHandle(env, 
    		G_OBJECT(gnome_appbar_new (hasProgress_g, hasStatus_g, interactivity_g)));
}

/*
 * Class:     org.gnu.gnome.AppBar
 * Method:    gnome_appbar_set_status
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_AppBar_gnome_1appbar_1set_1status (JNIEnv *env, 
    jclass cls, jobject appbar, jstring status) 
{
    GnomeAppBar *appbar_g = (GnomeAppBar *)getPointerFromHandle(env, appbar);
    const gchar* status_g = (*env)->GetStringUTFChars(env, status, NULL);
	gnome_appbar_set_status (appbar_g, status_g);
	(*env)->ReleaseStringUTFChars( env, status, status_g );
}

/*
 * Class:     org.gnu.gnome.AppBar
 * Method:    gnome_appbar_get_status
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_AppBar_gnome_1appbar_1get_1status (JNIEnv *env, 
    jclass cls, jobject appbar) 
{
    GnomeAppBar *appbar_g = (GnomeAppBar *)getPointerFromHandle(env, appbar);
    return getGObjectHandle(env, G_OBJECT(gnome_appbar_get_status (appbar_g)));
}

/*
 * Class:     org.gnu.gnome.AppBar
 * Method:    gnome_appbar_set_default
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_AppBar_gnome_1appbar_1set_1default (JNIEnv *env, 
    jclass cls, jobject appbar, jstring defaultStatus) 
{
    GnomeAppBar *appbar_g = (GnomeAppBar *)getPointerFromHandle(env, appbar);
    const gchar* defaultStatus_g = (*env)->GetStringUTFChars(env, defaultStatus, NULL);
	gnome_appbar_set_default (appbar_g, defaultStatus_g);
	(*env)->ReleaseStringUTFChars( env, defaultStatus, defaultStatus_g );
}

/*
 * Class:     org.gnu.gnome.AppBar
 * Method:    gnome_appbar_push
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_AppBar_gnome_1appbar_1push (JNIEnv *env, jclass cls, 
    jobject appbar, jstring status) 
{
    GnomeAppBar *appbar_g = (GnomeAppBar *)getPointerFromHandle(env, appbar);
    const gchar* status_g = (*env)->GetStringUTFChars(env, status, NULL);
	gnome_appbar_push (appbar_g, status_g);
	(*env)->ReleaseStringUTFChars( env, status, status_g );
}

/*
 * Class:     org.gnu.gnome.AppBar
 * Method:    gnome_appbar_pop
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_AppBar_gnome_1appbar_1pop (JNIEnv *env, jclass cls, 
    jobject appbar) 
{
    GnomeAppBar *appbar_g = (GnomeAppBar *)getPointerFromHandle(env, appbar);
    gnome_appbar_pop (appbar_g);
}

/*
 * Class:     org.gnu.gnome.AppBar
 * Method:    gnome_appbar_clear_stack
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_AppBar_gnome_1appbar_1clear_1stack (JNIEnv *env, 
    jclass cls, jobject appbar) 
{
    GnomeAppBar *appbar_g = (GnomeAppBar *)getPointerFromHandle(env, appbar);
    gnome_appbar_clear_stack (appbar_g);
}

/*
 * Class:     org.gnu.gnome.AppBar
 * Method:    gnome_appbar_set_progress_percentage
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_AppBar_gnome_1appbar_1set_1progress_1percentage (
    JNIEnv *env, jclass cls, jobject appbar, jdouble percentage) 
{
    GnomeAppBar *appbar_g = (GnomeAppBar *)getPointerFromHandle(env, appbar);
    gdouble percentage_g = (gdouble) percentage;
    gnome_appbar_set_progress_percentage (appbar_g, percentage_g);
}

/*
 * Class:     org.gnu.gnome.AppBar
 * Method:    gnome_appbar_get_progress
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_AppBar_gnome_1appbar_1get_1progress (JNIEnv *env, 
    jclass cls, jobject appbar) 
{
    GnomeAppBar *appbar_g = (GnomeAppBar *)getPointerFromHandle(env, appbar);
    return getGObjectHandle(env, G_OBJECT(gnome_appbar_get_progress (appbar_g)));
}

/*
 * Class:     org.gnu.gnome.AppBar
 * Method:    gnome_appbar_refresh
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_AppBar_gnome_1appbar_1refresh (JNIEnv *env, jclass 
    cls, jobject appbar) 
{
    GnomeAppBar *appbar_g = (GnomeAppBar *)getPointerFromHandle(env, appbar);
    gnome_appbar_refresh (appbar_g);
}

/*
 * Class:     org.gnu.gnome.AppBar
 * Method:    gnome_appbar_set_prompt
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_AppBar_gnome_1appbar_1set_1prompt (JNIEnv *env, 
    jclass cls, jobject appbar, jstring prompt, jboolean modal) 
{
    GnomeAppBar *appbar_g = (GnomeAppBar *)getPointerFromHandle(env, appbar);
    gboolean modal_g = (gboolean) modal;
    const gchar* prompt_g = (*env)->GetStringUTFChars(env, prompt, NULL);
	gnome_appbar_set_prompt (appbar_g, prompt_g, modal_g);
	(*env)->ReleaseStringUTFChars( env, prompt, prompt_g );
}

/*
 * Class:     org.gnu.gnome.AppBar
 * Method:    gnome_appbar_clear_prompt
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_AppBar_gnome_1appbar_1clear_1prompt (JNIEnv *env, 
    jclass cls, jobject appbar) 
{
    GnomeAppBar *appbar_g = (GnomeAppBar *)getPointerFromHandle(env, appbar);
    gnome_appbar_clear_prompt (appbar_g);
}

/*
 * Class:     org.gnu.gnome.AppBar
 * Method:    gnome_appbar_get_response
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_AppBar_gnome_1appbar_1get_1response (JNIEnv 
    *env, jclass cls, jobject appbar) 
{
    GnomeAppBar *appbar_g = (GnomeAppBar *)getPointerFromHandle(env, appbar);
	return (*env)->NewStringUTF( env, (gchar*)gnome_appbar_get_response (appbar_g) );
}


#ifdef __cplusplus
}

#endif
