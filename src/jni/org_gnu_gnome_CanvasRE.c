/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomecanvas/libgnomecanvas.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_CanvasRE.h"

#ifdef __cplusplus

extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.CanvasRE
 * Method:    gnome_canvas_request_redraw
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_CanvasRE_gnome_1canvas_1request_1redraw (JNIEnv *env, 
    jclass cls, jobject canvas, jint x1, jint y1, jint x2, jint y2) 
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
    gint32 x1_g = (gint32) x1;
    gint32 y1_g = (gint32) y1;
    gint32 x2_g = (gint32) x2;
    gint32 y2_g = (gint32) y2;
    gnome_canvas_request_redraw (canvas_g, x1_g, y1_g, x2_g, y2_g);
}

/*
 * Class:     org.gnu.gnome.CanvasRE
 * Method:    gnome_canvas_re_new_gdk_wpix
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasRE_gnome_1canvas_1re_1new_1gdk_1wpix (JNIEnv 
    *env, jclass cls, jobject group, jint type, jdouble x1, jdouble y1, jdouble x2, jdouble y2, 
    jobject fill_color, jobject outline_color, jobject fill_stipple, jobject outline_stipple, jint 
    width_pixels) 
{
    GnomeCanvasGroup *group_g = (GnomeCanvasGroup *)getPointerFromHandle(env, group);
    GType type_g = (GType)type;
    gdouble x1_g = (gdouble) x1;
    gdouble y1_g = (gdouble) y1;
    gdouble x2_g = (gdouble) x2;
    gdouble y2_g = (gdouble) y2;
    GdkColor *fill_color_g = (GdkColor *)getPointerFromHandle(env, fill_color);
    GdkColor *outline_color_g = (GdkColor *)getPointerFromHandle(env, outline_color);
    GdkBitmap *fill_stipple_g = (GdkBitmap *)getPointerFromHandle(env, fill_stipple);
    GdkBitmap *outline_stipple_g = (GdkBitmap *)getPointerFromHandle(env, outline_stipple);
    gint32 width_pixels_g = (gint32) width_pixels;
	return getGObjectHandle(env, G_OBJECT(gnome_canvas_item_new(group_g, type_g,
	       "x1", x1_g, "y1", y1_g, "x2", x2_g, "y2", y2_g,
       		"fill_color_gdk", fill_color_g, "outline_color_gdk", outline_color_g,
       		"fill_stipple", fill_stipple_g, "outline_stipple", outline_stipple_g,
       		"width_pixels", width_pixels_g, NULL)));
}

/*
 * Class:     org.gnu.gnome.CanvasRE
 * Method:    gnome_canvas_re_new_gdk_wunit
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasRE_gnome_1canvas_1re_1new_1gdk_1wunit (JNIEnv 
    *env, jclass cls, jobject group, jint type, jdouble x1, jdouble y1, jdouble x2, jdouble y2, 
    jobject fill_color, jobject outline_color, jobject fill_stipple, jobject outline_stipple, jdouble 
    width_units) 
{
    GnomeCanvasGroup *group_g = (GnomeCanvasGroup *)getPointerFromHandle(env, group);
    GType type_g = (GType)type;
    gdouble x1_g = (gdouble) x1;
    gdouble y1_g = (gdouble) y1;
    gdouble x2_g = (gdouble) x2;
    gdouble y2_g = (gdouble) y2;
    GdkColor *fill_color_g = (GdkColor *)getPointerFromHandle(env, fill_color);
    GdkColor *outline_color_g = (GdkColor *)getPointerFromHandle(env, outline_color);
    GdkBitmap *fill_stipple_g = (GdkBitmap *)getPointerFromHandle(env, fill_stipple);
    GdkBitmap *outline_stipple_g = (GdkBitmap *)getPointerFromHandle(env, outline_stipple);
    gdouble width_units_g = (gdouble) width_units;
    return getGObjectHandle(env, G_OBJECT(gnome_canvas_item_new(group_g, type_g,
       		"x1", x1_g, "y1", y1_g, "x2", x2_g, "y2", y2_g,
       		"fill_color_gdk", fill_color_g, "outline_color_gdk", outline_color_g,
       		"fill_stipple", fill_stipple_g, "outline_stipple", outline_stipple_g,
       		"width_units", width_units_g, NULL)));
}

/*
 * Class:     org.gnu.gnome.CanvasRE
 * Method:    gnome_canvas_re_new_rgba_wpix
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasRE_gnome_1canvas_1re_1new_1rgba_1wpix (JNIEnv 
    *env, jclass cls, jobject group, jint type, jdouble x1, jdouble y1, jdouble x2, jdouble y2, 
    jint fill_color, jint outline_color, jint width_pixels) 
{
    GnomeCanvasGroup *group_g = (GnomeCanvasGroup *)getPointerFromHandle(env, group);
    GType type_g = (GType )type;
    gdouble x1_g = (gdouble) x1;
    gdouble y1_g = (gdouble) y1;
    gdouble x2_g = (gdouble) x2;
    gdouble y2_g = (gdouble) y2;
    gint32 fill_color_g = (gint32) fill_color;
    gint32 outline_color_g = (gint32) outline_color;
    gint32 width_pixels_g = (gint32) width_pixels;
    return getGObjectHandle(env, G_OBJECT(gnome_canvas_item_new(group_g, type_g,
       		"x1", x1_g, "y1", y1_g, "x2", x2_g, "y2", y2_g,
       		"fill_color_rgba", fill_color_g, "outline_color_rgba", outline_color_g,
       		"width_pixels", width_pixels_g, NULL)));
}

/*
 * Class:     org.gnu.gnome.CanvasRE
 * Method:    gnome_canvas_re_new_rgba_wunit
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_CanvasRE_gnome_1canvas_1re_1new_1rgba_1wunit (JNIEnv 
    *env, jclass cls, jobject group, jint type, jdouble x1, jdouble y1, jdouble x2, jdouble y2, 
    jint fill_color, jint outline_color, jdouble width_units) 
{
    GnomeCanvasGroup *group_g = (GnomeCanvasGroup *)getPointerFromHandle(env, group);
    GType type_g = (GType)type;
    gdouble x1_g = (gdouble) x1;
    gdouble y1_g = (gdouble) y1;
    gdouble x2_g = (gdouble) x2;
    gdouble y2_g = (gdouble) y2;
    gint32 fill_color_g = (gint32) fill_color;
    gint32 outline_color_g = (gint32) outline_color;
    gdouble width_units_g = (gdouble) width_units;
	return getGObjectHandle(env, G_OBJECT(gnome_canvas_item_new(group_g, type_g,
       		"x1", x1_g, "y1", y1_g, "x2", x2_g, "y2", y2_g,
       		"fill_color_rgba", fill_color_g, "outline_color_rgba", outline_color_g,
       		"width_units", width_units_g, NULL)));
}

#ifdef __cplusplus
}

#endif
