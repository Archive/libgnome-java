/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_Config.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_get_string
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_Config_gnome_1config_1get_1string (JNIEnv *env, 
    jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gchar *result_g = (gchar*)gnome_config_get_string (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_get_translated_string
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_Config_gnome_1config_1get_1translated_1string (
    JNIEnv *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gchar *result_g = (gchar*)gnome_config_get_translated_string (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_get_int
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_Config_gnome_1config_1get_1int (JNIEnv *env, jclass 
    cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    jint result_j = (jint) (gnome_config_get_int (path_g));
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_get_float
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_Config_gnome_1config_1get_1float (JNIEnv *env, 
    jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    jdouble result_j = (jdouble) (gnome_config_get_float (path_g));
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_get_bool
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_Config_gnome_1config_1get_1bool (JNIEnv *env, 
    jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    jboolean result_j = (jboolean) (gnome_config_get_bool (path_g));
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_get_string
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_Config_gnome_1config_1private_1get_1string (
    JNIEnv *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gchar *result_g = (gchar*)gnome_config_private_get_string (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_get_translated_string
 */
JNIEXPORT jstring JNICALL 
Java_org_gnu_gnome_Config_gnome_1config_1private_1get_1translated_1string (JNIEnv *env, jclass 
    cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gchar *result_g = (gchar*)gnome_config_private_get_translated_string (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_get_int
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_Config_gnome_1config_1private_1get_1int (JNIEnv *env, 
    jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    jint result_j = (jint) (gnome_config_private_get_int (path_g));
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_get_float
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_Config_gnome_1config_1private_1get_1float (JNIEnv 
    *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    jdouble result_j = (jdouble) (gnome_config_private_get_float (path_g));
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_get_bool
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_Config_gnome_1config_1private_1get_1bool (JNIEnv 
    *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    jboolean result_j = (jboolean) (gnome_config_private_get_bool (path_g));
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_set_string
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1set_1string (JNIEnv *env, 
    jclass cls, jstring path, jstring value) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    const gchar* value_g = (*env)->GetStringUTFChars(env, value, 0);
    gnome_config_set_string (path_g, value_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    if (value) (*env)->ReleaseStringUTFChars(env, value, value_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_set_translated_string
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1set_1translated_1string (JNIEnv 
    *env, jclass cls, jstring path, jstring value) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    const gchar* value_g = (*env)->GetStringUTFChars(env, value, 0);
    gnome_config_set_translated_string (path_g, value_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    if (value) (*env)->ReleaseStringUTFChars(env, value, value_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_set_int
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1set_1int (JNIEnv *env, jclass 
    cls, jstring path, jint value) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gnome_config_set_int (path_g, (gint)value);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_set_float
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1set_1float (JNIEnv *env, jclass 
    cls, jstring path, jdouble value) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gnome_config_set_float (path_g, (gfloat)value);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_set_bool
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1set_1bool (JNIEnv *env, jclass 
    cls, jstring path, jboolean value) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gnome_config_set_bool (path_g, (gboolean)value);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_set_string
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1private_1set_1string (JNIEnv 
    *env, jclass cls, jstring path, jstring value) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    const gchar* value_g = (*env)->GetStringUTFChars(env, value, 0);
    gnome_config_private_set_string (path_g, value_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    if (value) (*env)->ReleaseStringUTFChars(env, value, value_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_set_translated_string
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_Config_gnome_1config_1private_1set_1translated_1string (JNIEnv *env, jclass 
    cls, jstring path, jstring value) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    const gchar* value_g = (*env)->GetStringUTFChars(env, value, 0);
    gnome_config_private_set_translated_string (path_g, value_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    if (value) (*env)->ReleaseStringUTFChars(env, value, value_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_set_int
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1private_1set_1int (JNIEnv *env, 
    jclass cls, jstring path, jint value) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gnome_config_private_set_int (path_g, (gint)value);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_set_float
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1private_1set_1float (JNIEnv 
    *env, jclass cls, jstring path, jdouble value) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gnome_config_private_set_float (path_g, (gdouble)value);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_set_bool
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1private_1set_1bool (JNIEnv 
    *env, jclass cls, jstring path, jboolean value) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gnome_config_private_set_bool (path_g, (gboolean)value);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_has_section
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_Config_gnome_1config_1has_1section (JNIEnv *env, 
    jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    jboolean result_j = (jboolean) (gnome_config_has_section (path_g));
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_has_section
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_Config_gnome_1config_1private_1has_1section (
    JNIEnv *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    jboolean result_j = (jboolean) (gnome_config_private_has_section (path_g));
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_drop_all
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1drop_1all (JNIEnv *env, jclass 
    cls) 
{
    gnome_config_drop_all ();
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_sync
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1sync (JNIEnv *env, jclass cls) 
{
    gnome_config_sync ();
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_sync_file
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1sync_1file
(JNIEnv *env, jclass cls, jstring path) 
{
    gchar* path_g = (gchar*)(*env)->GetStringUTFChars(env, path, 0);
    gnome_config_sync_file (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_sync_file
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1private_1sync_1file (JNIEnv 
    *env, jclass cls, jstring path) 
{
    gchar* path_g = (gchar*)(*env)->GetStringUTFChars(env, path, 0);
    gnome_config_private_sync_file (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_drop_file
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1drop_1file
(JNIEnv *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gnome_config_drop_file (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_drop_file
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1private_1drop_1file (JNIEnv *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gnome_config_private_drop_file (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_clean_file
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1clean_1file
(JNIEnv *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gnome_config_clean_file (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_clean_file
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1private_1clean_1file (JNIEnv 
    *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gnome_config_private_clean_file (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_clean_section
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1clean_1section (JNIEnv *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gnome_config_clean_section (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_clean_section
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1private_1clean_1section (JNIEnv 
    *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gnome_config_private_clean_section (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_clean_key
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1clean_1key
(JNIEnv *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gnome_config_clean_key (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_clean_key
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1private_1clean_1key (JNIEnv 
    *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gnome_config_private_clean_key (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_get_real_path
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_Config_gnome_1config_1get_1real_1path (JNIEnv 
    *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gchar *result_g = (gchar*)gnome_config_get_real_path (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_private_get_real_path
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_Config_gnome_1config_1private_1get_1real_1path (
    JNIEnv *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gchar *result_g = (gchar*)gnome_config_private_get_real_path (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_push_prefix
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1push_1prefix (JNIEnv *env, 
    jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, 0);
    gnome_config_push_prefix (path_g);
    if (path) (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.Config
 * Method:    gnome_config_pop_prefix
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Config_gnome_1config_1pop_1prefix (JNIEnv *env, 
    jclass cls) 
{
    gnome_config_pop_prefix ();
}


#ifdef __cplusplus
}

#endif
