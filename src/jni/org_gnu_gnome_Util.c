/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_Util.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.Util
 * Method:    gnome_util_home_file
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_Util_gnome_1util_1home_1file (JNIEnv *env, 
    jclass cls, jstring afile) 
{
    const gchar* afile_g = (*env)->GetStringUTFChars(env, afile, 0);
    const gchar *result_g = gnome_util_home_file (afile_g);
    (*env)->ReleaseStringUTFChars(env, afile, afile_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.Util
 * Method:    gnome_util_user_shell
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_Util_gnome_1util_1user_1shell (JNIEnv *env, 
    jclass cls) 
{
    const gchar *result_g = gnome_util_user_shell ();
    return (*env)->NewStringUTF(env, result_g);
}


#ifdef __cplusplus
}

#endif
