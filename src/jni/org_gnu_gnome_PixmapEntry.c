/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_PixmapEntry.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.PixmapEntry
 * Method:    gnome_pixmap_entry_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_PixmapEntry_gnome_1pixmap_1entry_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gnome_pixmap_entry_get_type ();
}

/*
 * Class:     org.gnu.gnome.PixmapEntry
 * Method:    gnome_pixmap_entry_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_PixmapEntry_gnome_1pixmap_1entry_1new (JNIEnv *env, 
    jclass cls, jstring historyId, jstring browseDialogTitle, jboolean doPreview) 
{
    gchar* historyId_g = (gchar*)(*env)->GetStringUTFChars(env, historyId, 0);
    gchar* browseDialogTitle_g = (gchar*)(*env)->GetStringUTFChars(env, browseDialogTitle, 0);
    jobject result = getHandleFromPointer(env, gnome_pixmap_entry_new (historyId_g, browseDialogTitle_g, (gboolean)doPreview));
    if (historyId) 
    	(*env)->ReleaseStringUTFChars(env, historyId, historyId_g);
    if (browseDialogTitle) 
    	(*env)->ReleaseStringUTFChars(env, browseDialogTitle, browseDialogTitle_g);
    return result;
}

/*
 * Class:     org.gnu.gnome.PixmapEntry
 * Method:    gnome_pixmap_entry_set_pixmap_subdir
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_PixmapEntry_gnome_1pixmap_1entry_1set_1pixmap_1subdir (
    JNIEnv *env, jclass cls, jobject pentry, jstring subdir) 
{
	GnomePixmapEntry* pentry_g = (GnomePixmapEntry*)getPointerFromHandle(env, pentry);
    gchar* subdir_g = (gchar*)(*env)->GetStringUTFChars(env, subdir, 0);
    gnome_pixmap_entry_set_pixmap_subdir (pentry_g, subdir_g);
    if (subdir) 
    	(*env)->ReleaseStringUTFChars(env, subdir, subdir_g);
}

/*
 * Class:     org.gnu.gnome.PixmapEntry
 * Method:    gnome_pixmap_entry_scrolled_window
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_PixmapEntry_gnome_1pixmap_1entry_1scrolled_1window (
    JNIEnv *env, jclass cls, jobject pentry) 
{
	GnomePixmapEntry* pentry_g = (GnomePixmapEntry*)getPointerFromHandle(env, pentry);
    return getHandleFromPointer(env, gnome_pixmap_entry_scrolled_window (pentry_g));
}

/*
 * Class:     org.gnu.gnome.PixmapEntry
 * Method:    gnome_pixmap_entry_preview_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_PixmapEntry_gnome_1pixmap_1entry_1preview_1widget (
    JNIEnv *env, jclass cls, jobject pentry) 
{
	GnomePixmapEntry* pentry_g = (GnomePixmapEntry*)getPointerFromHandle(env, pentry);
    return getHandleFromPointer(env, gnome_pixmap_entry_preview_widget (pentry_g));
}

/*
 * Class:     org.gnu.gnome.PixmapEntry
 * Method:    gnome_pixmap_entry_set_preview
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_PixmapEntry_gnome_1pixmap_1entry_1set_1preview (
    JNIEnv *env, jclass cls, jobject pentry, jboolean doPreview) 
{
	GnomePixmapEntry* pentry_g = (GnomePixmapEntry*)getPointerFromHandle(env, pentry);
    gboolean doPreview_g = (gboolean) doPreview;
    gnome_pixmap_entry_set_preview (pentry_g, doPreview_g);
}

/*
 * Class:     org.gnu.gnome.PixmapEntry
 * Method:    gnome_pixmap_entry_set_preview_size
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_PixmapEntry_gnome_1pixmap_1entry_1set_1preview_1size (
    JNIEnv *env, jclass cls, jobject pentry, jint width, jint height) 
{
	GnomePixmapEntry* pentry_g = (GnomePixmapEntry*)getPointerFromHandle(env, pentry);
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    gnome_pixmap_entry_set_preview_size (pentry_g, width_g, height_g);
}

/*
 * Class:     org.gnu.gnome.PixmapEntry
 * Method:    gnome_pixmap_entry_get_filename
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_PixmapEntry_gnome_1pixmap_1entry_1get_1filename (
    JNIEnv *env, jclass cls, jobject pentry) 
{
	GnomePixmapEntry* pentry_g = (GnomePixmapEntry*)getPointerFromHandle(env, pentry);
    gchar *result_g = (gchar*)gnome_pixmap_entry_get_filename (pentry_g);
    return (*env)->NewStringUTF(env, result_g);
}


#ifdef __cplusplus
}

#endif
