/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_About.h"

#ifdef __cplusplus
extern "C" 
{
#endif


/*
 * Class:     org.gnu.gnome.About
 * Method:    gnome_about_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_About_gnome_1about_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)gnome_about_get_type ();
}

/*
 * Class:     org.gnu.gnome.About
 * Method:    gnome_about_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_About_gnome_1about_1new (JNIEnv *env, jclass cls, 
    jstring title, jstring version, jstring copyright, jstring comments, jobjectArray 
    authors, jobjectArray documenters, jstring translatorCredits, jobject logo) 
{
    const gchar* title_g = (*env)->GetStringUTFChars(env, title, NULL);
    const gchar* version_g = (*env)->GetStringUTFChars(env, version, NULL);
    const gchar* copyright_g = (*env)->GetStringUTFChars(env, copyright, NULL);
    const gchar* comments_g = (*env)->GetStringUTFChars(env, comments, NULL);
    const gchar* translatorCredits_g = (*env)->GetStringUTFChars(env, translatorCredits, NULL);
    gchar **authors_g = getStringArray(env, authors);
    gchar**documenters_g = getStringArray(env, documenters);
    jobject retval;
	GdkPixbuf *logo_g;
	
	if (logo) {
	  logo_g = (GdkPixbuf *)getPointerFromHandle(env, logo);
	} else {
	  logo_g = NULL;
	}

   retval = getHandleFromPointer(env,
   		gnome_about_new (title_g, version_g, copyright_g, comments_g, (const gchar**)authors_g,  
   			(const gchar**)documenters_g, translatorCredits_g, logo_g));
   	
	(*env)->ReleaseStringUTFChars(env, title, title_g);
	(*env)->ReleaseStringUTFChars(env, version, version_g);
	(*env)->ReleaseStringUTFChars(env, copyright, copyright_g);
	(*env)->ReleaseStringUTFChars(env, comments, comments_g);
	(*env)->ReleaseStringUTFChars(env, translatorCredits, translatorCredits_g);
	
	freeStringArray(env, authors, authors_g);
	freeStringArray(env, documenters, documenters_g);

	return retval;
}


#ifdef __cplusplus
}

#endif
