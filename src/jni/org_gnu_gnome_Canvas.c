/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomecanvas/libgnomecanvas.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_Canvas.h"

#ifdef __cplusplus
extern "C" 
{
#endif

static gdouble GnomeCanvas_get_scroll_x1 (GnomeCanvas * cptr) 
{
    return cptr->scroll_x1;
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    getScrollX1
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_Canvas_getScrollX1 (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvas *cptr_g = (GnomeCanvas *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvas_get_scroll_x1 (cptr_g));
}

static gdouble GnomeCanvas_get_scroll_y1 (GnomeCanvas * cptr) 
{
    return cptr->scroll_y1;
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    getScrollY1
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_Canvas_getScrollY1 (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvas *cptr_g = (GnomeCanvas *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvas_get_scroll_y1 (cptr_g));
}

static gdouble GnomeCanvas_get_scroll_x2 (GnomeCanvas * cptr) 
{
    return cptr->scroll_x2;
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    getScrollX2
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_Canvas_getScrollX2 (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvas *cptr_g = (GnomeCanvas *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvas_get_scroll_x2 (cptr_g));
}

static gdouble GnomeCanvas_get_scroll_y2 (GnomeCanvas * cptr) 
{
    return cptr->scroll_y2;
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    getScrollY2
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_Canvas_getScrollY2 (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvas *cptr_g = (GnomeCanvas *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvas_get_scroll_y2 (cptr_g));
}

static gdouble GnomeCanvas_get_pixels_per_unit (GnomeCanvas * cptr) 
{
    return cptr->pixels_per_unit;
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    getPixelsPerUnit
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gnome_Canvas_getPixelsPerUnit (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeCanvas *cptr_g = (GnomeCanvas *)getPointerFromHandle(env, cptr);
    return (jdouble) (GnomeCanvas_get_pixels_per_unit (cptr_g));
}

static GnomeCanvasItem * GnomeCanvas_get_current_item (GnomeCanvas * cptr) 
{
    return cptr->current_item;
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    getCurrentItem
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Canvas_getCurrentItem (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvas *cptr_g = (GnomeCanvas *)getPointerFromHandle(env, cptr);
    return getGObjectHandleAndRef(env, G_OBJECT(GnomeCanvas_get_current_item (cptr_g)));
}

static GnomeCanvasItem * GnomeCanvas_get_focused_item (GnomeCanvas * cptr) 
{
    return cptr->focused_item;
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    getFocusedItem
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Canvas_getFocusedItem (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvas *cptr_g = (GnomeCanvas *)getPointerFromHandle(env, cptr);
    return getGObjectHandleAndRef(env, G_OBJECT(GnomeCanvas_get_focused_item (cptr_g)));
}

static GnomeCanvasItem * GnomeCanvas_get_grabbed_item (GnomeCanvas * cptr) 
{
    return cptr->grabbed_item;
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    getGrabbedItem
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Canvas_getGrabbedItem (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GnomeCanvas *cptr_g = (GnomeCanvas *)getPointerFromHandle(env, cptr);
    return getGObjectHandleAndRef(env, G_OBJECT(GnomeCanvas_get_grabbed_item (cptr_g)));
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    gnome_canvas_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)gnome_canvas_get_type ();
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    gnome_canvas_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, G_OBJECT(gnome_canvas_new ()));
}

/*
 * Class:     org_gnu_gnome_Canvas  
 * Method:    gnome_canvas_new_aa
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1new_1aa   
(JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, G_OBJECT(gnome_canvas_new_aa()));
}
                                                                                 

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    gnome_canvas_root
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1root (JNIEnv *env, jclass cls, 
    jobject canvas) 
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
    return getGObjectHandleAndRef(env, G_OBJECT(gnome_canvas_root (canvas_g)));
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    gnome_canvas_set_scroll_region
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1set_1scroll_1region (JNIEnv 
    *env, jclass cls, jobject canvas, jdouble x1, jdouble y1, jdouble x2, jdouble y2) 
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
    gdouble x1_g = (gdouble) x1;
    gdouble y1_g = (gdouble) y1;
    gdouble x2_g = (gdouble) x2;
    gdouble y2_g = (gdouble) y2;
    gnome_canvas_set_scroll_region (canvas_g, x1_g, y1_g, x2_g, y2_g);
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    gnome_canvas_get_scroll_region
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1get_1scroll_1region (JNIEnv 
    *env, jclass cls, jobject canvas, jdoubleArray x1, jdoubleArray y1, jdoubleArray x2, 
    jdoubleArray y2) 
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
    gdouble *x1_g = (gdouble *) (*env)->GetDoubleArrayElements (env, x1, NULL);
    gdouble *y1_g = (gdouble *) (*env)->GetDoubleArrayElements (env, y1, NULL);
    gdouble *x2_g = (gdouble *) (*env)->GetDoubleArrayElements (env, x2, NULL);
    gdouble *y2_g = (gdouble *) (*env)->GetDoubleArrayElements (env, y2, NULL);
    gnome_canvas_get_scroll_region (canvas_g, x1_g, y1_g, x2_g, y2_g);
    (*env)->ReleaseDoubleArrayElements (env, x1, (jdouble *) x1_g, 0);
    (*env)->ReleaseDoubleArrayElements (env, y1, (jdouble *) y1_g, 0);
    (*env)->ReleaseDoubleArrayElements (env, x2, (jdouble *) x2_g, 0);
    (*env)->ReleaseDoubleArrayElements (env, y2, (jdouble *) y2_g, 0);
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    gnome_canvas_set_pixels_per_unit
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1set_1pixels_1per_1unit (JNIEnv 
    *env, jclass cls, jobject canvas, jdouble n) 
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
    gdouble n_g = (gdouble) n;
    gnome_canvas_set_pixels_per_unit (canvas_g, n_g);
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    gnome_canvas_scroll_to
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1scroll_1to (JNIEnv *env, jclass 
    cls, jobject canvas, jint cx, jint cy) 
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
    gint32 cx_g = (gint32) cx;
    gint32 cy_g = (gint32) cy;
    gnome_canvas_scroll_to (canvas_g, cx_g, cy_g);
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    gnome_canvas_get_scroll_offsets
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1get_1scroll_1offsets (JNIEnv 
    *env, jclass cls, jobject canvas, jintArray cx, jintArray cy) 
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
    gint *cx_g = (gint *) (*env)->GetIntArrayElements (env, cx, NULL);
    gint *cy_g = (gint *) (*env)->GetIntArrayElements (env, cy, NULL);
    gnome_canvas_get_scroll_offsets (canvas_g, cx_g, cy_g);
    (*env)->ReleaseIntArrayElements (env, cx, (jint *) cx_g, 0);
    (*env)->ReleaseIntArrayElements (env, cy, (jint *) cy_g, 0);
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    gnome_canvas_update_now
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1update_1now (JNIEnv *env, 
    jclass cls, jobject canvas) 
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
    gnome_canvas_update_now (canvas_g);
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    gnome_canvas_get_item_at
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1get_1item_1at (JNIEnv *env, 
    jclass cls, jobject canvas, jdouble x, jdouble y) 
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
    gdouble x_g = (gdouble) x;
    gdouble y_g = (gdouble) y;
    return getGObjectHandleAndRef(env, G_OBJECT(gnome_canvas_get_item_at (canvas_g, x_g, y_g)));
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    gnome_canvas_get_color
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1get_1color (JNIEnv *env, 
    jclass cls, jobject canvas, jstring spec, jobjectArray color) 
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
    const gchar* spec_g = (*env)->GetStringUTFChars(env, spec, 0);
    GdkColor *color_g = (GdkColor *)getPointerArrayFromHandles(env, color);;
    jboolean result_j = (jboolean) (gnome_canvas_get_color(canvas_g, spec_g, color_g));
    (*env)->ReleaseStringUTFChars(env, spec, spec_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    gnome_canvas_get_color_pixel
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1get_1color_1pixel (JNIEnv 
    *env, jclass cls, jobject canvas, jint rgba) 
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
    gint32 rgba_g = (gint32) rgba;
    return (jlong) (gnome_canvas_get_color_pixel (canvas_g, rgba_g));
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    gnome_canvas_set_stipple_origin
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1set_1stipple_1origin (JNIEnv 
    *env, jclass cls, jobject canvas, jobject gc) 
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    gnome_canvas_set_stipple_origin (canvas_g, gc_g);
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    gnome_canvas_set_dither
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1set_1dither (JNIEnv *env, 
    jclass cls, jobject canvas, jint dither) 
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
    GdkRgbDither dither_g = (GdkRgbDither) dither;
    gnome_canvas_set_dither (canvas_g, dither_g);
}

/*
 * Class:     org.gnu.gnome.Canvas
 * Method:    gnome_canvas_get_dither
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1get_1dither (JNIEnv *env, 
    jclass cls, jobject canvas) 
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
    return (jint) (gnome_canvas_get_dither (canvas_g));
}

/*
 * Class:     org_gnu_gnome_Canvas
 * Method:    gnome_canvas_set_center_scroll_region
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1set_1center_1scroll_1region
  (JNIEnv *env, jclass cls, jobject canvas, jboolean center)
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
	gnome_canvas_set_center_scroll_region(canvas_g, (gboolean)center);
}
                                                                                
/*
 * Class:     org_gnu_gnome_Canvas
 * Method:    gnome_canvas_get_center_scroll_region
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1get_1center_1scroll_1region
  (JNIEnv *env, jclass cls, jobject canvas)
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
	return (jboolean)gnome_canvas_get_center_scroll_region(canvas_g);
}
                                                                                
/*
 * Class:     org_gnu_gnome_Canvas
 * Method:    gnome_canvas_request_redraw
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1request_1redraw 
  (JNIEnv *env, jclass cls, jobject canvas, jint x1, jint y1, jint x2, jint y2)
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
	gnome_canvas_request_redraw(canvas_g, x1, y1, x2, y2);
}
                                                                                
/*
 * Class:     org_gnu_gnome_Canvas
 * Method:    gnome_canvas_window_to_world
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1window_1to_1world
  (JNIEnv *env, jclass cls, jobject canvas, 
  jdouble winx, jdouble winy, jdoubleArray worldx, jdoubleArray worldy)
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
	jdouble *wx = (*env)->GetDoubleArrayElements(env, worldx, NULL);
	jdouble *wy = (*env)->GetDoubleArrayElements(env, worldy, NULL);
	gnome_canvas_window_to_world(canvas_g, winx, winy, wx, wy);
	(*env)->ReleaseDoubleArrayElements(env, worldx, wx, 0);
	(*env)->ReleaseDoubleArrayElements(env, worldy, wy, 0);
}
                                                                                
/*
 * Class:     org_gnu_gnome_Canvas
 * Method:    gnome_canvas_world_to_window
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Canvas_gnome_1canvas_1world_1to_1window
  (JNIEnv *env, jclass cls, jobject canvas, 
  jdouble worldx, jdouble worldy, jdoubleArray winx, jdoubleArray winy)
{
    GnomeCanvas *canvas_g = (GnomeCanvas *)getPointerFromHandle(env, canvas);
	jdouble *wx = (*env)->GetDoubleArrayElements(env, winx, NULL);
	jdouble *wy = (*env)->GetDoubleArrayElements(env, winy, NULL);
	gnome_canvas_window_to_world(canvas_g, worldx, worldy, wx, wy);
	(*env)->ReleaseDoubleArrayElements(env, winx, wx, 0);
	(*env)->ReleaseDoubleArrayElements(env, winy, wy, 0);
}
                                                                                
	
#ifdef __cplusplus
}

#endif
