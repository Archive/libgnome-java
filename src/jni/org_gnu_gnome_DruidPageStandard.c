/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gnome_DruidPageStandard.h"

#ifdef __cplusplus
extern "C" 
{
#endif

static GtkWidget * GnomeDruidPageStandard_get_vbox (GnomeDruidPageStandard * cptr) 
{
    return cptr->vbox;
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    getVbox
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageStandard_getVbox (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeDruidPageStandard *cptr_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, G_OBJECT(GnomeDruidPageStandard_get_vbox (cptr_g)));
}

static gchar * GnomeDruidPageStandard_get_title (GnomeDruidPageStandard * cptr) 
{
    return cptr->title;
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    getTitle
 * Signature: (I)java.lang.String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_DruidPageStandard_getTitle (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GnomeDruidPageStandard *cptr_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, cptr);
    gchar *result_g = (gchar*)GnomeDruidPageStandard_get_title (cptr_g);
    return (*env)->NewStringUTF(env, result_g);
}

static GdkPixbuf * GnomeDruidPageStandard_get_logo (GnomeDruidPageStandard * cptr) 
{
    return cptr->logo;
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    getLogo
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageStandard_getLogo (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GnomeDruidPageStandard *cptr_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, cptr);
    return getGObjectHandleAndRef(env, G_OBJECT(GnomeDruidPageStandard_get_logo (cptr_g)));
}

static GdkPixbuf * GnomeDruidPageStandard_get_top_watermark (GnomeDruidPageStandard * cptr) 
{
    return cptr->top_watermark;
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    getTopWatermark
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageStandard_getTopWatermark (JNIEnv *env, 
    jclass cls, jobject cptr) 
{
    GnomeDruidPageStandard *cptr_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, cptr);
    return getGObjectHandleAndRef(env, G_OBJECT(GnomeDruidPageStandard_get_top_watermark (cptr_g)));
}

static GdkColor * GnomeDruidPageStandard_get_title_foreground (GnomeDruidPageStandard * cptr) 
{
    return (GdkColor*)&cptr->title_foreground;
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    getTitleForeground
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageStandard_getTitleForeground (JNIEnv *env, 
    jclass cls, jobject cptr) 
{
    GnomeDruidPageStandard *cptr_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, cptr);
    return getGBoxedHandle(env, GnomeDruidPageStandard_get_title_foreground (cptr_g),
    		GDK_TYPE_COLOR, (GBoxedCopyFunc) gdk_color_copy,
    		(GBoxedFreeFunc) gdk_color_free);
}

static GdkColor * GnomeDruidPageStandard_get_background (GnomeDruidPageStandard * cptr) 
{
    return (GdkColor*)&cptr->background;
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    getBackground
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageStandard_getBackground (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GnomeDruidPageStandard *cptr_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, cptr);
    return getGBoxedHandle(env, GnomeDruidPageStandard_get_background (cptr_g),
    		GDK_TYPE_COLOR, (GBoxedCopyFunc) gdk_color_copy,
    		(GBoxedFreeFunc) gdk_color_free);
}

static GdkColor * GnomeDruidPageStandard_get_logo_background (GnomeDruidPageStandard * cptr) 
{
    return (GdkColor*)&cptr->logo_background;
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    getLogoBackground
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageStandard_getLogoBackground (JNIEnv *env, 
    jclass cls, jobject cptr) 
{
    GnomeDruidPageStandard *cptr_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, cptr);
    return getGBoxedHandle(env, GnomeDruidPageStandard_get_logo_background (cptr_g),
    		GDK_TYPE_COLOR, (GBoxedCopyFunc) gdk_color_copy,
    		(GBoxedFreeFunc) gdk_color_free);
}

static GdkColor * GnomeDruidPageStandard_get_contents_background (GnomeDruidPageStandard * cptr) 
{
    return (GdkColor*)&cptr->contents_background;
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    getContentsBackground
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageStandard_getContentsBackground (JNIEnv *env, 
    jclass cls, jobject cptr) 
{
    GnomeDruidPageStandard *cptr_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, cptr);
    return getGBoxedHandle(env, GnomeDruidPageStandard_get_contents_background (cptr_g),
    		GDK_TYPE_COLOR, (GBoxedCopyFunc) gdk_color_copy,
    		(GBoxedFreeFunc) gdk_color_free);
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    gnome_druid_page_standard_get_type
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_gnome_DruidPageStandard_gnome_1druid_1page_1standard_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gnome_druid_page_standard_get_type ();
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    gnome_druid_page_standard_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_DruidPageStandard_gnome_1druid_1page_1standard_1new (
    JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, G_OBJECT(gnome_druid_page_standard_new ()));
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    gnome_druid_page_standard_new_with_vals
 */
JNIEXPORT jobject JNICALL 
Java_org_gnu_gnome_DruidPageStandard_gnome_1druid_1page_1standard_1new_1with_1vals (JNIEnv 
    *env, jclass cls, jstring title, jobject logo, jobject topWatermark) 
{
    gchar* title_g = (gchar*)(*env)->GetStringUTFChars(env, title, 0);
    GdkPixbuf *logo_g = (GdkPixbuf *)getPointerFromHandle(env, logo);
    GdkPixbuf *topWatermark_g = (GdkPixbuf *)getPointerFromHandle(env, topWatermark);
    jobject result = getGObjectHandle(env, 
    		G_OBJECT(gnome_druid_page_standard_new_with_vals (title_g, logo_g,
    		topWatermark_g)));
    if (title) 
    	(*env)->ReleaseStringUTFChars(env, title, title_g);
    return result;
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    gnome_druid_page_standard_set_title
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_DruidPageStandard_gnome_1druid_1page_1standard_1set_1title (JNIEnv *env, 
    jclass cls, jobject page, jstring title) 
{
	GnomeDruidPageStandard *page_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, page);
    gchar* title_g = (gchar*)(*env)->GetStringUTFChars(env, title, 0);
    gnome_druid_page_standard_set_title (page_g, title_g);
    if (title) 
    	(*env)->ReleaseStringUTFChars(env, title, title_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    gnome_druid_page_standard_set_logo
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_DruidPageStandard_gnome_1druid_1page_1standard_1set_1logo (JNIEnv *env, 
    jclass cls, jobject page, jobject logoImage) 
{
    GnomeDruidPageStandard *page_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, page);
    GdkPixbuf *logoImage_g = (GdkPixbuf *)getPointerFromHandle(env, logoImage);
    gnome_druid_page_standard_set_logo (page_g, logoImage_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    gnome_druid_page_standard_set_top_watermark
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_DruidPageStandard_gnome_1druid_1page_1standard_1set_1top_1watermark (JNIEnv 
    *env, jclass cls, jobject page, jobject topWatermarkImage) 
{
    GnomeDruidPageStandard *page_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, page);
    GdkPixbuf *topWatermarkImage_g = (GdkPixbuf *)getPointerFromHandle(env, topWatermarkImage);
    gnome_druid_page_standard_set_top_watermark (page_g, topWatermarkImage_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    gnome_druid_page_standard_set_title_foreground
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_DruidPageStandard_gnome_1druid_1page_1standard_1set_1title_1foreground (
    JNIEnv *env, jclass cls, jobject page, jobject color) 
{
    GnomeDruidPageStandard *page_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, page);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gnome_druid_page_standard_set_title_foreground (page_g, color_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    gnome_druid_page_standard_set_background
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_DruidPageStandard_gnome_1druid_1page_1standard_1set_1background (JNIEnv 
    *env, jclass cls, jobject page, jobject color) 
{
    GnomeDruidPageStandard *page_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, page);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gnome_druid_page_standard_set_background (page_g, color_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    gnome_druid_page_standard_set_logo_background
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_DruidPageStandard_gnome_1druid_1page_1standard_1set_1logo_1background (
    JNIEnv *env, jclass cls, jobject page, jobject color) 
{
    GnomeDruidPageStandard *page_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, page);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gnome_druid_page_standard_set_logo_background (page_g, color_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    gnome_druid_page_standard_set_contents_background
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_DruidPageStandard_gnome_1druid_1page_1standard_1set_1contents_1background (
    JNIEnv *env, jclass cls, jobject page, jobject color) 
{
    GnomeDruidPageStandard *page_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, page);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gnome_druid_page_standard_set_contents_background (page_g, color_g);
}

/*
 * Class:     org.gnu.gnome.DruidPageStandard
 * Method:    gnome_druid_page_standard_append_item
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_DruidPageStandard_gnome_1druid_1page_1standard_1append_1item (JNIEnv *env, 
    jclass cls, jobject page, jstring questionMnemonic, jobject item, jstring additionalInfoMarkup) 
{
    GnomeDruidPageStandard *page_g = (GnomeDruidPageStandard *)getPointerFromHandle(env, page);
    gchar* questionMnemonic_g = (gchar*)(*env)->GetStringUTFChars(env, questionMnemonic, 0);
    GtkWidget *item_g = (GtkWidget *)getPointerFromHandle(env, item);
    gchar* additionalInfoMarkup_g = (gchar*)(*env)->GetStringUTFChars(env, additionalInfoMarkup, 0);
    gnome_druid_page_standard_append_item (page_g, questionMnemonic_g, item_g, 
            additionalInfoMarkup_g);
    if (questionMnemonic) 
    	(*env)->ReleaseStringUTFChars(env, questionMnemonic, questionMnemonic_g);
    if (additionalInfoMarkup) 
    	(*env)->ReleaseStringUTFChars(env, additionalInfoMarkup, additionalInfoMarkup_g);
}


#ifdef __cplusplus
}

#endif
