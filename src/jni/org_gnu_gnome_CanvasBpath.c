/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomecanvas/libgnomecanvas.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_CanvasBpath.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.CanvasBpath
 * Method:    gnome_canvas_bpath_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_CanvasBpath_gnome_1canvas_1bpath_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gnome_canvas_bpath_get_type ();
}


#ifdef __cplusplus
}

#endif
