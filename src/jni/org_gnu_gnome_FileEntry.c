/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_FileEntry.h"

#ifdef __cplusplus

extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.FileEntry
 * Method:    gnome_file_entry_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_FileEntry_gnome_1file_1entry_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gnome_file_entry_get_type ();
}

/*
 * Class:     org.gnu.gnome.FileEntry
 * Method:    gnome_file_entry_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_FileEntry_gnome_1file_1entry_1new (JNIEnv *env, 
    jclass cls, jstring historyId, jstring browseDialogTitle) 
{
    gchar* historyId_g = (gchar*)(*env)->GetStringUTFChars(env, historyId, 0);
    gchar* browseDialogTitle_g = (gchar*)(*env)->GetStringUTFChars(env, browseDialogTitle, 0);
    jobject result = getHandleFromPointer(env,
    		gnome_file_entry_new (historyId_g, browseDialogTitle_g));
    if (historyId) 
    	(*env)->ReleaseStringUTFChars(env, historyId, historyId_g);
    if (browseDialogTitle) 
    	(*env)->ReleaseStringUTFChars(env, browseDialogTitle, browseDialogTitle_g);
    return result;
}

/*
 * Class:     org.gnu.gnome.FileEntry
 * Method:    gnome_file_entry_gnome_entry
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_FileEntry_gnome_1file_1entry_1gnome_1entry (JNIEnv 
    *env, jclass cls, jobject fentry) 
{
    GnomeFileEntry *fentry_g = (GnomeFileEntry *)getPointerFromHandle(env, fentry);
    return getHandleFromPointer(env, gnome_file_entry_gnome_entry (fentry_g));
}

/*
 * Class:     org.gnu.gnome.FileEntry
 * Method:    gnome_file_entry_gtk_entry
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_FileEntry_gnome_1file_1entry_1gtk_1entry (JNIEnv 
    *env, jclass cls, jobject fentry) 
{
    GnomeFileEntry *fentry_g = (GnomeFileEntry *)getPointerFromHandle(env, fentry);
    return getHandleFromPointer(env, gnome_file_entry_gtk_entry (fentry_g));
}

/*
 * Class:     org.gnu.gnome.FileEntry
 * Method:    gnome_file_entry_set_title
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_FileEntry_gnome_1file_1entry_1set_1title (JNIEnv 
        *env, jclass cls, jobject fentry, jstring browseDialogTitle) 
{
    GnomeFileEntry *fentry_g = (GnomeFileEntry *)getPointerFromHandle(env, fentry);
    gchar* browseDialogTitle_g = (gchar*)(*env)->GetStringUTFChars(env, browseDialogTitle, 0);
    gnome_file_entry_set_title (fentry_g, browseDialogTitle_g);
    if (browseDialogTitle) 
    	(*env)->ReleaseStringUTFChars(env, browseDialogTitle, browseDialogTitle_g);
}

/*
 * Class:     org.gnu.gnome.FileEntry
 * Method:    gnome_file_entry_set_default_path
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_FileEntry_gnome_1file_1entry_1set_1default_1path (
    JNIEnv *env, jclass cls, jobject fentry, jstring path) 
{
    GnomeFileEntry *fentry_g = (GnomeFileEntry *)getPointerFromHandle(env, fentry);
    gchar* path_g = (gchar*)(*env)->GetStringUTFChars(env, path, 0);
    gnome_file_entry_set_default_path(fentry_g, path_g);
    if (path) 
    	(*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gnome.FileEntry
 * Method:    gnome_file_entry_set_directory_entry
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_FileEntry_gnome_1file_1entry_1set_1directory_1entry (
    JNIEnv *env, jclass cls, jobject fentry, jboolean directoryEntry) 
{
    GnomeFileEntry *fentry_g = (GnomeFileEntry *)getPointerFromHandle(env, fentry);
    gboolean directoryEntry_g = (gboolean) directoryEntry;
    gnome_file_entry_set_directory_entry (fentry_g, directoryEntry_g);
}

/*
 * Class:     org.gnu.gnome.FileEntry
 * Method:    gnome_file_entry_get_directory_entry
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gnome_FileEntry_gnome_1file_1entry_1get_1directory_1entry (JNIEnv *env, jclass 
    cls, jobject fentry) 
{
    GnomeFileEntry *fentry_g = (GnomeFileEntry *)getPointerFromHandle(env, fentry);
    return (jboolean) (gnome_file_entry_get_directory_entry (fentry_g));
}

/*
 * Class:     org.gnu.gnome.FileEntry
 * Method:    gnome_file_entry_get_full_path
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_FileEntry_gnome_1file_1entry_1get_1full_1path (
    JNIEnv *env, jclass cls, jobject fentry, jboolean fileMustExist) 
{
    GnomeFileEntry *fentry_g = (GnomeFileEntry *)getPointerFromHandle(env, fentry);
    gchar *result_g = (gchar*)gnome_file_entry_get_full_path(fentry_g, (gboolean)fileMustExist);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.FileEntry
 * Method:    gnome_file_entry_set_filename
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_FileEntry_gnome_1file_1entry_1set_1filename (JNIEnv 
    *env, jclass cls, jobject fentry, jstring filename) 
{
    GnomeFileEntry *fentry_g = (GnomeFileEntry *)getPointerFromHandle(env, fentry);
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
    gnome_file_entry_set_filename(fentry_g, filename_g);
    if (filename) 
    	(*env)->ReleaseStringUTFChars(env, filename, filename_g);
}

/*
 * Class:     org.gnu.gnome.FileEntry
 * Method:    gnome_file_entry_set_modal
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_FileEntry_gnome_1file_1entry_1set_1modal (JNIEnv 
    *env, jclass cls, jobject fentry, jboolean isModal) 
{
    GnomeFileEntry *fentry_g = (GnomeFileEntry *)getPointerFromHandle(env, fentry);
    gboolean isModal_g = (gboolean) isModal;
    gnome_file_entry_set_modal (fentry_g, isModal_g);
}

/*
 * Class:     org.gnu.gnome.FileEntry
 * Method:    gnome_file_entry_get_modal
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_FileEntry_gnome_1file_1entry_1get_1modal (JNIEnv 
    *env, jclass cls, jobject fentry) 
{
    GnomeFileEntry *fentry_g = (GnomeFileEntry *)getPointerFromHandle(env, fentry);
    return (jboolean) (gnome_file_entry_get_modal (fentry_g));
}


#ifdef __cplusplus
}

#endif
