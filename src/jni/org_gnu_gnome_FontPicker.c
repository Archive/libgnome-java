/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_FontPicker.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.FontPicker
 * Method:    gnome_font_picker_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_FontPicker_gnome_1font_1picker_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gnome_font_picker_get_type ();
}

/*
 * Class:     org.gnu.gnome.FontPicker
 * Method:    gnome_font_picker_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_FontPicker_gnome_1font_1picker_1new (JNIEnv *env, 
    jclass cls) 
{
    return getHandleFromPointer(env, gnome_font_picker_new ());
}

/*
 * Class:     org.gnu.gnome.FontPicker
 * Method:    gnome_font_picker_set_title
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_FontPicker_gnome_1font_1picker_1set_1title (JNIEnv 
    *env, jclass cls, jobject gfp, jstring title) 
{
	GnomeFontPicker* gfp_g = (GnomeFontPicker*)getPointerFromHandle(env, gfp);
    gchar* title_g = (gchar*)(*env)->GetStringUTFChars(env, title, 0);
    gnome_font_picker_set_title (gfp_g, title_g);
    if (title) 
    	(*env)->ReleaseStringUTFChars(env, title, title_g);
}

/*
 * Class:     org.gnu.gnome.FontPicker
 * Method:    gnome_font_picker_get_title
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_FontPicker_gnome_1font_1picker_1get_1title (
    JNIEnv *env, jclass cls, jobject gfp) 
{
	GnomeFontPicker* gfp_g = (GnomeFontPicker*)getPointerFromHandle(env, gfp);
    gchar *result_g = (gchar*)gnome_font_picker_get_title (gfp_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.FontPicker
 * Method:    gnome_font_picker_get_mode
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_FontPicker_gnome_1font_1picker_1get_1mode (JNIEnv 
    *env, jclass cls, jobject gfp) 
{
	GnomeFontPicker* gfp_g = (GnomeFontPicker*)getPointerFromHandle(env, gfp);
    return (jint) (gnome_font_picker_get_mode (gfp_g));
}

/*
 * Class:     org.gnu.gnome.FontPicker
 * Method:    gnome_font_picker_set_mode
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_FontPicker_gnome_1font_1picker_1set_1mode (JNIEnv 
    *env, jclass cls, jobject gfp, jint mode) 
{
	GnomeFontPicker* gfp_g = (GnomeFontPicker*)getPointerFromHandle(env, gfp);
    GnomeFontPickerMode mode_g = (GnomeFontPickerMode) mode;
    gnome_font_picker_set_mode (gfp_g, mode_g);
}

/*
 * Class:     org.gnu.gnome.FontPicker
 * Method:    gnome_font_picker_fi_set_use_font_in_label
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_FontPicker_gnome_1font_1picker_1fi_1set_1use_1font_1in_1label (JNIEnv *env, 
    jclass cls, jobject gfp, jboolean use_font_in_label, jint size) 
{
	GnomeFontPicker* gfp_g = (GnomeFontPicker*)getPointerFromHandle(env, gfp);
    gboolean use_font_in_label_g = (gboolean) use_font_in_label;
    gint32 size_g = (gint32) size;
    gnome_font_picker_fi_set_use_font_in_label (gfp_g, use_font_in_label_g, size_g);
}

/*
 * Class:     org.gnu.gnome.FontPicker
 * Method:    gnome_font_picker_fi_set_show_size
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_FontPicker_gnome_1font_1picker_1fi_1set_1show_1size (
    JNIEnv *env, jclass cls, jobject gfp, jboolean showSize) 
{
	GnomeFontPicker* gfp_g = (GnomeFontPicker*)getPointerFromHandle(env, gfp);
    gboolean showSize_g = (gboolean) showSize;
    gnome_font_picker_fi_set_show_size (gfp_g, showSize_g);
}

/*
 * Class:     org.gnu.gnome.FontPicker
 * Method:    gnome_font_picker_uw_set_widget
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_FontPicker_gnome_1font_1picker_1uw_1set_1widget (
    JNIEnv *env, jclass cls, jobject gfp, jobject widget) 
{
	GnomeFontPicker* gfp_g = (GnomeFontPicker*)getPointerFromHandle(env, gfp);
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gnome_font_picker_uw_set_widget (gfp_g, widget_g);
}

/*
 * Class:     org.gnu.gnome.FontPicker
 * Method:    gnome_font_picker_uw_get_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_FontPicker_gnome_1font_1picker_1uw_1get_1widget (
    JNIEnv *env, jclass cls, jobject gfp) 
{
	GnomeFontPicker* gfp_g = (GnomeFontPicker*)getPointerFromHandle(env, gfp);
    return getHandleFromPointer(env, gnome_font_picker_uw_get_widget (gfp_g));
}

/*
 * Class:     org.gnu.gnome.FontPicker
 * Method:    gnome_font_picker_get_font_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_FontPicker_gnome_1font_1picker_1get_1font_1name (
    JNIEnv *env, jclass cls, jobject gfp) 
{
	GnomeFontPicker* gfp_g = (GnomeFontPicker*)getPointerFromHandle(env, gfp);
    gchar *result_g = (gchar*)gnome_font_picker_get_font_name (gfp_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.FontPicker
 * Method:    gnome_font_picker_set_font_name
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnome_FontPicker_gnome_1font_1picker_1set_1font_1name (
    JNIEnv *env, jclass cls, jobject gfp, jstring fontname) 
{
	GnomeFontPicker* gfp_g = (GnomeFontPicker*)getPointerFromHandle(env, gfp);
    gchar* fontname_g = (gchar*)(*env)->GetStringUTFChars(env, fontname, 0);
    jboolean result_j = (jboolean) (gnome_font_picker_set_font_name (gfp_g, fontname_g));
    if (fontname) 
    	(*env)->ReleaseStringUTFChars(env, fontname, fontname_g);
    return result_j;
}

/*
 * Class:     org.gnu.gnome.FontPicker
 * Method:    gnome_font_picker_get_preview_text
 */
JNIEXPORT jstring JNICALL 
Java_org_gnu_gnome_FontPicker_gnome_1font_1picker_1get_1preview_1text (JNIEnv *env, jclass cls, 
    jobject gfp) 
{
	GnomeFontPicker* gfp_g = (GnomeFontPicker*)getPointerFromHandle(env, gfp);
    gchar *result_g = (gchar*)gnome_font_picker_get_preview_text (gfp_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.FontPicker
 * Method:    gnome_font_picker_set_preview_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_FontPicker_gnome_1font_1picker_1set_1preview_1text (
    JNIEnv *env, jclass cls, jobject gfp, jstring text) 
{
	GnomeFontPicker* gfp_g = (GnomeFontPicker*)getPointerFromHandle(env, gfp);
    gchar* text_g = (gchar*)(*env)->GetStringUTFChars(env, text, 0);
    gnome_font_picker_set_preview_text (gfp_g, text_g);
    if (text) 
    	(*env)->ReleaseStringUTFChars(env, text, text_g);
}


#ifdef __cplusplus
}

#endif
