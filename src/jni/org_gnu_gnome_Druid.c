/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>
#include <gtk_java.h>
#include "org_gnu_gnome_Druid.h"

#ifdef __cplusplus


extern "C" 
{
#endif

static GtkButton * GnomeDruid_get_help (GnomeDruid * cptr) 
{
    return (GtkButton*)cptr->help;
}

/*
 * Class:     org.gnu.gnome.Druid
 * Method:    getHelp
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Druid_getHelp (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeDruid *cptr_g = (GnomeDruid *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, G_OBJECT(GnomeDruid_get_help (cptr_g)));
}

static GtkButton * GnomeDruid_get_back (GnomeDruid * cptr) 
{
    return (GtkButton*)cptr->back;
}

/*
 * Class:     org.gnu.gnome.Druid
 * Method:    getBack
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Druid_getBack (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeDruid *cptr_g = (GnomeDruid *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, G_OBJECT(GnomeDruid_get_back (cptr_g)));
}

static GtkButton * GnomeDruid_get_next (GnomeDruid * cptr) 
{
    return (GtkButton*)cptr->next;
}

/*
 * Class:     org.gnu.gnome.Druid
 * Method:    getNext
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Druid_getNext (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeDruid *cptr_g = (GnomeDruid *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, G_OBJECT(GnomeDruid_get_next (cptr_g)));
}

static GtkButton * GnomeDruid_get_cancel (GnomeDruid * cptr) 
{
    return (GtkButton*)cptr->cancel;
}

/*
 * Class:     org.gnu.gnome.Druid
 * Method:    getCancel
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Druid_getCancel (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeDruid *cptr_g = (GnomeDruid *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, G_OBJECT(GnomeDruid_get_cancel (cptr_g)));
}

static GtkButton * GnomeDruid_get_finish (GnomeDruid * cptr) 
{
    return (GtkButton*)cptr->finish;
}

/*
 * Class:     org.gnu.gnome.Druid
 * Method:    getFinish
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Druid_getFinish (JNIEnv *env, jclass cls, jobject cptr) 
{
    GnomeDruid *cptr_g = (GnomeDruid *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, G_OBJECT(GnomeDruid_get_finish (cptr_g)));
}

/*
 * Class:     org.gnu.gnome.Druid
 * Method:    gnome_druid_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_Druid_gnome_1druid_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)gnome_druid_get_type ();
}

/*
 * Class:     org.gnu.gnome.Druid
 * Method:    gnome_druid_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Druid_gnome_1druid_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, G_OBJECT(gnome_druid_new ()));
}

/*
 * Class:     org.gnu.gnome.Druid
 * Method:    gnome_druid_set_buttons_sensitive
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Druid_gnome_1druid_1set_1buttons_1sensitive (JNIEnv 
    *env, jclass cls, jobject druid, jboolean backSensitive, jboolean nextSensitive, jboolean 
    cancelSensitive, jboolean helpSensitive) 
{
    GnomeDruid *druid_g = (GnomeDruid *)getPointerFromHandle(env, druid);
    gboolean backSensitive_g = (gboolean) backSensitive;
    gboolean nextSensitive_g = (gboolean) nextSensitive;
    gboolean cancelSensitive_g = (gboolean) cancelSensitive;
    gboolean helpSensitive_g = (gboolean) helpSensitive;
    gnome_druid_set_buttons_sensitive (druid_g, backSensitive_g, nextSensitive_g, 
            cancelSensitive_g, helpSensitive_g);
}

/*
 * Class:     org.gnu.gnome.Druid
 * Method:    gnome_druid_set_show_finish
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Druid_gnome_1druid_1set_1show_1finish (JNIEnv *env, 
    jclass cls, jobject druid, jboolean showFinish) 
{
    GnomeDruid *druid_g = (GnomeDruid *)getPointerFromHandle(env, druid);
    gboolean showFinish_g = (gboolean) showFinish;
    gnome_druid_set_show_finish (druid_g, showFinish_g);
}

/*
 * Class:     org.gnu.gnome.Druid
 * Method:    gnome_druid_set_show_help
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Druid_gnome_1druid_1set_1show_1help (JNIEnv *env, 
    jclass cls, jobject druid, jboolean showHelp) 
{
    GnomeDruid *druid_g = (GnomeDruid *)getPointerFromHandle(env, druid);
    gboolean showHelp_g = (gboolean) showHelp;
    gnome_druid_set_show_help (druid_g, showHelp_g);
}

/*
 * Class:     org.gnu.gnome.Druid
 * Method:    gnome_druid_prepend_page
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Druid_gnome_1druid_1prepend_1page (JNIEnv *env, 
    jclass cls, jobject druid, jobject page) 
{
    GnomeDruid *druid_g = (GnomeDruid *)getPointerFromHandle(env, druid);
    GnomeDruidPage *page_g = (GnomeDruidPage *)getPointerFromHandle(env, page);
    gnome_druid_prepend_page (druid_g, page_g);
}

/*
 * Class:     org.gnu.gnome.Druid
 * Method:    gnome_druid_insert_page
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Druid_gnome_1druid_1insert_1page (JNIEnv *env, jclass 
    cls, jobject druid, jobject backPage, jobject page) 
{
    GnomeDruid *druid_g = (GnomeDruid *)getPointerFromHandle(env, druid);
    GnomeDruidPage *backPage_g = (GnomeDruidPage *)getPointerFromHandle(env, backPage);
    GnomeDruidPage *page_g = (GnomeDruidPage *)getPointerFromHandle(env, page);
    gnome_druid_insert_page (druid_g, backPage_g, page_g);
}

/*
 * Class:     org.gnu.gnome.Druid
 * Method:    gnome_druid_append_page
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Druid_gnome_1druid_1append_1page (JNIEnv *env, jclass 
    cls, jobject druid, jobject page) 
{
    GnomeDruid *druid_g = (GnomeDruid *)getPointerFromHandle(env, druid);
    GnomeDruidPage *page_g = (GnomeDruidPage *)getPointerFromHandle(env, page);
    gnome_druid_append_page (druid_g, page_g);
}

/*
 * Class:     org.gnu.gnome.Druid
 * Method:    gnome_druid_set_page
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Druid_gnome_1druid_1set_1page (JNIEnv *env, jclass 
    cls, jobject druid, jobject page) 
{
    GnomeDruid *druid_g = (GnomeDruid *)getPointerFromHandle(env, druid);
    GnomeDruidPage *page_g = (GnomeDruidPage *)getPointerFromHandle(env, page);
    gnome_druid_set_page (druid_g, page_g);
}

/*
 * Class:     org.gnu.gnome.Druid
 * Method:    gnome_druid_new_with_window
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Druid_gnome_1druid_1new_1with_1window (JNIEnv *env, 
    jclass cls, jstring title, jobject parent, jboolean closeOnCancel, jobject window) 
{
    GtkWindow *parent_g = (GtkWindow *)getPointerFromHandle(env, parent);
    const gchar* title_g = (*env)->GetStringUTFChars(env, title, 0);
    GtkWidget *window_g = (GtkWidget *)getPointerFromHandle(env, window);
    jobject result = getGObjectHandle(env, 
    		G_OBJECT(gnome_druid_new_with_window (title_g, parent_g,
    		(gboolean)closeOnCancel, &window_g)));
    if (title) 
    	(*env)->ReleaseStringUTFChars(env, title, title_g);
    return result;
}


#ifdef __cplusplus
}

#endif
