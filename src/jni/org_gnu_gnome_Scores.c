/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_Scores.h"

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gnome.Scores
 * Method:    gnome_scores_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_Scores_gnome_1scores_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)gnome_scores_get_type ();
}

/*
 * Class:     org.gnu.gnome.Scores
 * Method:    gnome_scores_display
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_Scores_gnome_1scores_1display (JNIEnv *env, jclass 
    cls, jstring title, jstring app_name, jstring level, jint pos) 
{
    gchar* title_g = (gchar*)(*env)->GetStringUTFChars(env, title, 0);
    gchar* app_name_g = (gchar*)(*env)->GetStringUTFChars(env, app_name, 0);
    gchar* level_g = (gchar*)(*env)->GetStringUTFChars(env, level, 0);
    jobject result = getHandleFromPointer(env,
    		gnome_scores_display (title_g, app_name_g, level_g, (gint32)pos));
    if (title) 
    	(*env)->ReleaseStringUTFChars(env, title, title_g);
    if (app_name) (*env)->ReleaseStringUTFChars(env, app_name, app_name_g);
    if (level) (*env)->ReleaseStringUTFChars(env, level, level_g);
    return result;
}

/*
 * Class:     org.gnu.gnome.Scores
 * Method:    gnome_scores_set_logo_pixmap
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Scores_gnome_1scores_1set_1logo_1pixmap (JNIEnv *env, 
    jclass cls, jobject gs, jstring logo) 
{
	GnomeScores* gs_g = (GnomeScores*)getPointerFromHandle(env, gs);
    gchar* logo_g = (gchar*)(*env)->GetStringUTFChars(env, logo, 0);
    gnome_scores_set_logo_pixmap (gs_g, logo_g);
    if (logo) 
    	(*env)->ReleaseStringUTFChars(env, logo, logo_g);
}

/*
 * Class:     org.gnu.gnome.Scores
 * Method:    gnome_scores_set_logo_widget
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Scores_gnome_1scores_1set_1logo_1widget (JNIEnv *env, 
    jclass cls, jobject gs, jobject widget) 
{
	GnomeScores* gs_g = (GnomeScores*)getPointerFromHandle(env, gs);
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gnome_scores_set_logo_widget (gs_g, widget_g);
}

/*
 * Class:     org.gnu.gnome.Scores
 * Method:    gnome_scores_set_logo_label_title
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Scores_gnome_1scores_1set_1logo_1label_1title (JNIEnv *env, jclass cls, jobject gs, jstring text) 
{
	GnomeScores* gs_g = (GnomeScores*)getPointerFromHandle(env, gs);
    gchar* text_g = (gchar*)(*env)->GetStringUTFChars(env, text, 0);
    gnome_scores_set_logo_label_title (gs_g, text_g);
    if (text) 
    	(*env)->ReleaseStringUTFChars(env, text, text_g);
}

/*
 * Class:     org.gnu.gnome.Scores
 * Method:    gnome_scores_set_current_player
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_Scores_gnome_1scores_1set_1current_1player (JNIEnv 
    *env, jclass cls, jobject gs, jint i) 
{
	GnomeScores* gs_g = (GnomeScores*)getPointerFromHandle(env, gs);
    gint32 i_g = (gint32) i;
    gnome_scores_set_current_player (gs_g, i_g);
}


#ifdef __cplusplus
}

#endif
