/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_IconTextItem.h"

#ifdef __cplusplus

extern "C" 
{
#endif


/*
 * Class:     org.gnu.gnome.IconTextItem
 * Method:    gnome_icon_text_item_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnome_IconTextItem_gnome_1icon_1text_1item_1get_1type (
    JNIEnv *env, jclass cls) 
{
    return (jint)gnome_icon_text_item_get_type ();
}

/*
 * Class:     org.gnu.gnome.IconTextItem
 * Method:    gnome_icon_text_item_configure
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconTextItem_gnome_1icon_1text_1item_1configure (
    JNIEnv *env, jclass cls, jobject iti, jint x, jint y, jint width, jstring fontname, 
    jstring text, jboolean isEditable, jboolean isStatic) 
{
	GnomeIconTextItem* iti_g = (GnomeIconTextItem*)getPointerFromHandle(env, iti);
    gchar* fontname_g = (gchar*)(*env)->GetStringUTFChars(env, fontname, 0);
    gchar* text_g = (gchar*)(*env)->GetStringUTFChars(env, text, 0);
    gnome_icon_text_item_configure (iti_g, (gint32)x, 
    		(gint32)y, (gint32)width, fontname_g, text_g, 
            (gboolean)isEditable, (gboolean)isStatic);
    if (fontname) 
    	(*env)->ReleaseStringUTFChars(env, fontname, fontname_g);
    if (text) 
    	(*env)->ReleaseStringUTFChars(env, text, text_g);
}

/*
 * Class:     org.gnu.gnome.IconTextItem
 * Method:    gnome_icon_text_item_setxy
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconTextItem_gnome_1icon_1text_1item_1setxy (JNIEnv 
    *env, jclass cls, jobject iti, jint x, jint y) 
{
	GnomeIconTextItem* iti_g = (GnomeIconTextItem*)getPointerFromHandle(env, iti);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gnome_icon_text_item_setxy (iti_g, x_g, y_g);
}

/*
 * Class:     org.gnu.gnome.IconTextItem
 * Method:    gnome_icon_text_item_select
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconTextItem_gnome_1icon_1text_1item_1select (JNIEnv 
    *env, jclass cls, jobject iti, jint sel) 
{
	GnomeIconTextItem* iti_g = (GnomeIconTextItem*)getPointerFromHandle(env, iti);
    gint32 sel_g = (gint32) sel;
    gnome_icon_text_item_select (iti_g, sel_g);
}

/*
 * Class:     org.gnu.gnome.IconTextItem
 * Method:    gnome_icon_text_item_focus
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconTextItem_gnome_1icon_1text_1item_1focus (JNIEnv 
    *env, jclass cls, jobject iti, jint focused) 
{
	GnomeIconTextItem* iti_g = (GnomeIconTextItem*)getPointerFromHandle(env, iti);
    gint32 focused_g = (gint32) focused;
    gnome_icon_text_item_focus (iti_g, focused_g);
}

/*
 * Class:     org.gnu.gnome.IconTextItem
 * Method:    gnome_icon_text_item_get_text
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnome_IconTextItem_gnome_1icon_1text_1item_1get_1text (
    JNIEnv *env, jclass cls, jobject iti) 
{
	GnomeIconTextItem* iti_g = (GnomeIconTextItem*)getPointerFromHandle(env, iti);
    gchar *result_g = (gchar*)gnome_icon_text_item_get_text (iti_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gnome.IconTextItem
 * Method:    gnome_icon_text_item_start_editing
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconTextItem_gnome_1icon_1text_1item_1start_1editing (
    JNIEnv *env, jclass cls, jobject iti) 
{
	GnomeIconTextItem* iti_g = (GnomeIconTextItem*)getPointerFromHandle(env, iti);
    gnome_icon_text_item_start_editing (iti_g);
}

/*
 * Class:     org.gnu.gnome.IconTextItem
 * Method:    gnome_icon_text_item_stop_editing
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_IconTextItem_gnome_1icon_1text_1item_1stop_1editing (
    JNIEnv *env, jclass cls, jobject iti, jboolean accept) 
{
	GnomeIconTextItem* iti_g = (GnomeIconTextItem*)getPointerFromHandle(env, iti);
    gboolean accept_g = (gboolean) accept;
    gnome_icon_text_item_stop_editing (iti_g, accept_g);
}


/*
 * Class:     org_gnu_gnome_IconTextItem
 * Method:    gnome_icon_text_item_get_editable
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnome_IconTextItem_gnome_1icon_1text_1item_1get_1editable
  (JNIEnv *env, jclass cls, jobject iti)
{
	GnomeIconTextItem* iti_g = (GnomeIconTextItem*)getPointerFromHandle(env, iti);
	return getHandleFromPointer(env, gnome_icon_text_item_get_editable(iti_g));
}

#ifdef __cplusplus
}

#endif
