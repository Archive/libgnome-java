/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gnome.h>
#include <jg_jnu.h>

#include "org_gnu_gnome_WindowIcon.h"

#ifdef __cplusplus

extern "C" 
{
#endif


/*
 * Class:     org.gnu.gnome.WindowIcon
 * Method:    gnome_window_icon_set_from_default
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_WindowIcon_gnome_1window_1icon_1set_1from_1default (
    JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gnome_window_icon_set_from_default (window_g);
}

/*
 * Class:     org.gnu.gnome.WindowIcon
 * Method:    gnome_window_icon_set_from_file
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_WindowIcon_gnome_1window_1icon_1set_1from_1file (
    JNIEnv *env, jclass cls, jobject window, jstring filename) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    const gchar* filename_g = (*env)->GetStringUTFChars( env, filename, NULL );
	gnome_window_icon_set_from_file (window_g, filename_g);
	(*env)->ReleaseStringUTFChars(env, filename, filename_g);
}

/*
 * Class:     org.gnu.gnome.WindowIcon
 * Method:    gnome_window_icon_set_from_file_list
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_WindowIcon_gnome_1window_1icon_1set_1from_1file_1list (
    JNIEnv *env, jclass cls, jobject window, jobjectArray filenames) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gchar **filenames_g =  getStringArray(env, filenames);
    gnome_window_icon_set_from_file_list (window_g, (const gchar**)filenames_g);
    freeStringArray(env, filenames, filenames_g);
}

/*
 * Class:     org.gnu.gnome.WindowIcon
 * Method:    gnome_window_icon_set_default_from_file
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_WindowIcon_gnome_1window_1icon_1set_1default_1from_1file (JNIEnv *env, 
    jclass cls, jstring filename) 
{
    const gchar* filename_g = (*env)->GetStringUTFChars(env, filename, NULL);
	gnome_window_icon_set_default_from_file (filename_g);
	(*env)->ReleaseStringUTFChars(env, filename, filename_g);
}

/*
 * Class:     org.gnu.gnome.WindowIcon
 * Method:    gnome_window_icon_set_default_from_file_list
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gnome_WindowIcon_gnome_1window_1icon_1set_1default_1from_1file_1list (JNIEnv *env, 
    jclass cls, jobjectArray filenames) 
{
    gchar **filenames_g = getStringArray(env, filenames);
    gnome_window_icon_set_default_from_file_list ((const gchar**)filenames_g);
    freeStringArray(env, filenames, filenames_g);
}

/*
 * Class:     org.gnu.gnome.WindowIcon
 * Method:    gnome_window_icon_init
 */
JNIEXPORT void JNICALL Java_org_gnu_gnome_WindowIcon_gnome_1window_1icon_1init (JNIEnv *env, 
    jclass cls) 
{
    gnome_window_icon_init ();
}


#ifdef __cplusplus
}

#endif
