/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.gtk.GtkStockItem;

public class GnomeStockItem extends GtkStockItem {
    /**
     * Creates a new StockItem object, based on the id string for that object.
     * It is highly recommended that you use the static final strings which are
     * part of this class rather than quoting the strings directly.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public GnomeStockItem(String id) {
        super(id);
    }

    static final public GnomeStockItem TIMER = new GnomeStockItem(
            "gnome-stock-timer");

    static final public GnomeStockItem TIMER_STOP = new GnomeStockItem(
            "gnome-stock-timer-stop");

    static final public GnomeStockItem TRASH = new GnomeStockItem(
            "gnome-stock-trash");

    static final public GnomeStockItem TRASH_FULL = new GnomeStockItem(
            "gnome-stock-trash-full");

    static final public GnomeStockItem SCORES = new GnomeStockItem(
            "gnome-stock-scores");

    static final public GnomeStockItem ABOUT = new GnomeStockItem(
            "gnome-stock-about");

    static final public GnomeStockItem BLANK = new GnomeStockItem(
            "gnome-stock-blank");

    static final public GnomeStockItem VOLUME = new GnomeStockItem(
            "gnome-stock-volume");

    static final public GnomeStockItem MIDI = new GnomeStockItem(
            "gnome-stock-midi");

    static final public GnomeStockItem MIC = new GnomeStockItem(
            "gnome-stock-mic");

    static final public GnomeStockItem LINE_IN = new GnomeStockItem(
            "gnome-stock-line-in");

    static final public GnomeStockItem MAIL = new GnomeStockItem(
            "gnome-stock-mail");

    static final public GnomeStockItem MAIL_RCV = new GnomeStockItem(
            "gnome-stock-mail-rcv");

    static final public GnomeStockItem MAIL_SND = new GnomeStockItem(
            "gnome-stock-mail-snd");

    static final public GnomeStockItem MAIL_RPL = new GnomeStockItem(
            "gnome-stock-mail-rpl");

    static final public GnomeStockItem MAIL_FWD = new GnomeStockItem(
            "gnome-stock-mail-fwd");

    static final public GnomeStockItem MAIL_NEW = new GnomeStockItem(
            "gnome-stock-mail-new");

    static final public GnomeStockItem ATTACH = new GnomeStockItem(
            "gnome-stock-attach");

    static final public GnomeStockItem BOOK_RED = new GnomeStockItem(
            "gnome-stock-book-red");

    static final public GnomeStockItem BOOK_GREEN = new GnomeStockItem(
            "gnome-stock-book-green");

    static final public GnomeStockItem BOOK_BLUE = new GnomeStockItem(
            "gnome-stock-book-blue");

    static final public GnomeStockItem BOOK_YELLOW = new GnomeStockItem(
            "gnome-stock-book-yellow");

    static final public GnomeStockItem BOOK_OPEN = new GnomeStockItem(
            "gnome-stock-book-open");

    static final public GnomeStockItem MULTIPLE_FILE = new GnomeStockItem(
            "gnome-stock-multiple-file");

    static final public GnomeStockItem NOT = new GnomeStockItem(
            "gnome-stock-not");

    static final public GnomeStockItem TABLE_BORDERS = new GnomeStockItem(
            "gnome-stock-table-borders");

    static final public GnomeStockItem TABLE_FILL = new GnomeStockItem(
            "gnome-stock-table-fill");

    static final public GnomeStockItem TEXT_INDENT = new GnomeStockItem(
            "gnome-stock-text-indent");

    static final public GnomeStockItem TEXT_UNINDENT = new GnomeStockItem(
            "gnome-stock-text-unindent");

    static final public GnomeStockItem TEXT_BULLETED_LIST = new GnomeStockItem(
            "gnome-stock-text-bulleted-list");

    static final public GnomeStockItem TEXT_NUMBERED_LIST = new GnomeStockItem(
            "gnome-stock-text-numbered-list");

    static final public GnomeStockItem AUTHENTICATION = new GnomeStockItem(
            "gnome-stock-authentication");

}
