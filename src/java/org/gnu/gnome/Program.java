/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;
import org.gnu.glib.Struct;
import org.gnu.gtk.Config;

/**
 * This class takes care of handling application and library initialization and
 * command line parsing.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.Program</code>.
 */
public class Program extends Struct {

    protected Program(Handle handle) {
        super(handle);
    }

    /**
     * Initialize the gnome application specifying the directories used by the
     * app.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Program initGnomeUI(String appId, String appVersion,
            String[] argv, String prefix, String sysconfdir, String datadir,
            String libdir) {
        Handle handle = initLibgnome(appId, appVersion, argv.length + 1,
                makeArgv(argv), prefix, sysconfdir, datadir, libdir, 0);
        return new Program(handle);
    }

    /**
     * Initialize the gnome application using the default directories for the
     * app.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Program initGnomeUI(String appId, String appVersion,
            String[] argv) {
        Handle handle = initLibgnome(appId, appVersion, argv.length + 1,
                makeArgv(argv), "", "", "", "", 0);
        return new Program(handle);
    }

    /**
     * Initialize the gnome application specifying the directories use by the
     * app.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Program initGnome(String appId, String appVersion,
            String[] argv, String prefix, String sysconfdir, String datadir,
            String libdir) {
        Handle handle = initLibgnome(appId, appVersion, argv.length + 1,
                makeArgv(argv), prefix, sysconfdir, datadir, libdir, 1);
        return new Program(handle);
    }

    /**
     * Initialize the gnome application using the default directories for the
     * app.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Program initGnome(String appId, String appVersion,
            String[] argv) {
        Handle handle = initLibgnome(appId, appVersion, argv.length + 1,
                makeArgv(argv), "", "", "", "", 1);
        return new Program(handle);
    }

    public String getAppID() {
        return gnome_program_get_app_id(getHandle());
    }

    public String getName() {
        return gnome_program_get_human_readable_name(getHandle());
    }

    public String getVersion() {
        return gnome_program_get_app_version(getHandle());
    }

    // Load the library
    static {
        System.loadLibrary(Config.LIBRARY_NAME + Config.GTK_API_VERSION);
        System.loadLibrary(GnomeConfig.LIBRARY_NAME
                + GnomeConfig.GNOME_API_VERSION);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_program_get_type());
    }

    private static String[] makeArgv(String[] orig) {
        String appName = System.getProperty("gnome.appName", "java-gnome");

        if (orig == null) {
            orig = new String[] { appName };
        } else {
            String[] newArr = new String[orig.length + 1];
            System.arraycopy(orig, 0, newArr, 1, orig.length);
            newArr[0] = appName;
            orig = newArr;
        }
        return orig;
    }

    native static private Handle initLibgnome(String appId, String appVersion,
            int argc, String[] argv, String prefix, String sysconfdir,
            String datadir, String libdir, int type);

    native static final protected int gnome_program_get_type();

    native static final protected Handle gnome_program_get();

    native static final protected String gnome_program_get_human_readable_name(
            Handle program);

    native static final protected String gnome_program_get_app_id(Handle program);

    native static final protected String gnome_program_get_app_version(
            Handle program);

    native static final protected void gnome_program_module_register(
            Handle moduleInfo);

    native static final protected boolean gnome_program_module_registered(
            Handle moduleInfo);

    native static final protected Handle gnome_program_module_load(
            String modName);

}
