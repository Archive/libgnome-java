/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.gtk.VBox;
import org.gnu.glib.Handle;

/**
 * This widget provides an entry box with history and a button which can pop up
 * a file selector dialog box. It also accepts DND drops from the filemanager
 * and other sources.
 * 
 * @deprecated
 * @see org.gnu.gtk.FileChooser
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.FileEntry</code>.
 */
public class FileEntry extends VBox {
    /**
     * Create a new FileEntry widget
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public FileEntry(String historyID, String browseDialogTitle) {
        super(gnome_file_entry_new(historyID, browseDialogTitle));
    }

    /**
     * Construct a new FileEntry from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public FileEntry(Handle handle) {
        super(handle);
    }

    /**
     * Returns the Gnome Entry widget.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Entry getEntry() {
        Handle hndl = gnome_file_entry_gnome_entry(getHandle());
        GObject obj = getGObjectFromHandle(hndl);
        if (null != obj)
            return (Entry) obj;
        return new Entry(hndl);
    }

    /**
     * Sets the title of the browse dialog.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setTitle(String browseDialogTitle) {
        gnome_file_entry_set_title(getHandle(), browseDialogTitle);
    }

    /**
     * Sets the default path for the browse dialog.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setDefaultPath(String path) {
        gnome_file_entry_set_default_path(getHandle(), path);
    }

    /**
     * Sets whether this is a directory only entry.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setDirectorySelection(boolean directoryEntry) {
        gnome_file_entry_set_directory_entry(getHandle(), directoryEntry);
    }

    /**
     * Gets the full absolute path of the file from the entry.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getFullPath(boolean fileMustExist) {
        return gnome_file_entry_get_full_path(getHandle(), fileMustExist);
    }

    /**
     * Sets the modality of the browse dialog.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setModal(boolean isModal) {
        gnome_file_entry_set_modal(getHandle(), isModal);
    }

    /**
     * Returns if the browse dialog is modal.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public boolean getModal() {
        return gnome_file_entry_get_modal(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_file_entry_get_type());
    }

    native static final protected int gnome_file_entry_get_type();

    native static final protected Handle gnome_file_entry_new(String historyId,
            String browseDialogTitle);

    native static final protected Handle gnome_file_entry_gnome_entry(
            Handle fentry);

    native static final protected Handle gnome_file_entry_gtk_entry(
            Handle fentry);

    native static final protected void gnome_file_entry_set_title(
            Handle fentry, String browseDialogTitle);

    native static final protected void gnome_file_entry_set_default_path(
            Handle fentry, String path);

    native static final protected void gnome_file_entry_set_directory_entry(
            Handle fentry, boolean directoryEntry);

    native static final protected boolean gnome_file_entry_get_directory_entry(
            Handle fentry);

    native static final protected String gnome_file_entry_get_full_path(
            Handle fentry, boolean fileMustExist);

    native static final protected void gnome_file_entry_set_filename(
            Handle fentry, String filename);

    native static final protected void gnome_file_entry_set_modal(
            Handle fentry, boolean isModal);

    native static final protected boolean gnome_file_entry_get_modal(
            Handle fentry);

}
