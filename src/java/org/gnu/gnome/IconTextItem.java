/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.gtk.Editable;
import org.gnu.gtk.Entry;
import org.gnu.glib.Handle;

/**
 * @deprecated
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.IconTextItem</code>.
 */
public class IconTextItem extends CanvasItem {
    /**
     * Constructs an instance of IconTextItem from a native widget resource.
     * 
     * @param handle
     *            The handle to the native widget.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    IconTextItem(Handle handle) {
        super(handle);
    }

    public String getText() {
        return gnome_icon_text_item_get_text(getHandle());
    }

    public Editable getEditable() {
        Handle hndl = gnome_icon_text_item_get_editable(getHandle());
        GObject obj = getGObjectFromHandle(hndl);
        if (null != obj)
            return (Entry) obj;
        return new Entry(hndl);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_icon_text_item_get_type());
    }

    native static final protected int gnome_icon_text_item_get_type();

    native static final protected void gnome_icon_text_item_configure(
            Handle iti, int x, int y, int width, String fontname, String text,
            boolean isEditable, boolean isStatic);

    native static final protected void gnome_icon_text_item_setxy(Handle iti,
            int x, int y);

    native static final protected void gnome_icon_text_item_select(Handle iti,
            int sel);

    native static final protected void gnome_icon_text_item_focus(Handle iti,
            int focused);

    native static final protected String gnome_icon_text_item_get_text(
            Handle iti);

    native static final protected void gnome_icon_text_item_start_editing(
            Handle iti);

    native static final protected void gnome_icon_text_item_stop_editing(
            Handle iti, boolean accept);

    native static final protected Handle gnome_icon_text_item_get_editable(
            Handle iti);

}
