/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Enum;

/**
 * Identifies the item type that a UIInfo object specifies.
 * <p>
 * ENDOFINFO No more items. Use it at the end of an array. ITEM Normal item, or
 * radio item if it is inside a radioitems group. TOGGLEITEM Toggle (check box)
 * item. RADIOITEMS Radio item group. SUBTREE Item that defines a subtree /
 * submenu. SEPARATOR Separator lines (menu) or a blank space (toolbar). HELP
 * Create a list of help topics. Used in the Help menu. BUILDERDATA Specifies
 * the builder data. CONFIGURABLE A configurable menu item. SUBTREE_STOCK Item
 * that defines a subtree / submenu, same as SUBTREE, but the text should looked
 * up in the libgnome catalog. INCLUDE Almost like SUBTREE, but inserts items
 * into the current menu instead of making a submenu.
 * </p>
 * 
 * @see UIInfo
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.UIInfoType</code>.
 */
public class UIInfoType extends Enum {

    static final private int _ENDOFINFO = 0;

    static final public org.gnu.gnome.UIInfoType ENDOFINFO = new org.gnu.gnome.UIInfoType(
            _ENDOFINFO);

    static final private int _ITEM = 1;

    static final public org.gnu.gnome.UIInfoType ITEM = new org.gnu.gnome.UIInfoType(
            _ITEM);

    static final private int _TOGGLEITEM = 2;

    static final public org.gnu.gnome.UIInfoType TOGGLEITEM = new org.gnu.gnome.UIInfoType(
            _TOGGLEITEM);

    static final private int _RADIOITEMS = 3;

    static final public org.gnu.gnome.UIInfoType RADIOITEMS = new org.gnu.gnome.UIInfoType(
            _RADIOITEMS);

    static final private int _SUBTREE = 4;

    static final public org.gnu.gnome.UIInfoType SUBTREE = new org.gnu.gnome.UIInfoType(
            _SUBTREE);

    static final private int _SEPARATOR = 5;

    static final public org.gnu.gnome.UIInfoType SEPARATOR = new org.gnu.gnome.UIInfoType(
            _SEPARATOR);

    static final private int _HELP = 6;

    static final public org.gnu.gnome.UIInfoType HELP = new org.gnu.gnome.UIInfoType(
            _HELP);

    static final private int _BUILDER_DATA = 7;

    static final public org.gnu.gnome.UIInfoType BUILDER_DATA = new org.gnu.gnome.UIInfoType(
            _BUILDER_DATA);

    static final private int _ITEM_CONFIGURABLE = 8;

    static final public org.gnu.gnome.UIInfoType ITEM_CONFIGURABLE = new org.gnu.gnome.UIInfoType(
            _ITEM_CONFIGURABLE);

    static final private int _SUBTREE_STOCK = 9;

    static final public org.gnu.gnome.UIInfoType SUBTREE_STOCK = new org.gnu.gnome.UIInfoType(
            _SUBTREE_STOCK);

    static final private int _INCLUDE = 10;

    static final public org.gnu.gnome.UIInfoType INCLUDE = new org.gnu.gnome.UIInfoType(
            _INCLUDE);

    static final private org.gnu.gnome.UIInfoType[] theInterned = new org.gnu.gnome.UIInfoType[] {
            ENDOFINFO, ITEM, TOGGLEITEM, RADIOITEMS, SUBTREE, SEPARATOR, HELP,
            BUILDER_DATA, ITEM_CONFIGURABLE, SUBTREE_STOCK, INCLUDE };

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.UIInfoType theSacrificialOne = new org.gnu.gnome.UIInfoType(
            0);

    static public org.gnu.gnome.UIInfoType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.UIInfoType already = (org.gnu.gnome.UIInfoType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.UIInfoType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private UIInfoType(int value) {
        value_ = value;
    }

    public org.gnu.gnome.UIInfoType or(org.gnu.gnome.UIInfoType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.UIInfoType and(org.gnu.gnome.UIInfoType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.UIInfoType xor(org.gnu.gnome.UIInfoType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.UIInfoType other) {
        return (value_ & other.value_) == other.value_;
    }

}
