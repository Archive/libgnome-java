/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Type;
import org.gnu.gtk.Adjustment;
import org.gnu.gtk.SelectionMode;
import org.gnu.glib.Handle;

/**
 * @deprecated
 * @see org.gnu.gtk.IconView
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.IconList</code>.
 */
public class IconList extends Canvas {
    /**
     * Construct a new IconList object.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public IconList(int iconWidth, Adjustment adj, boolean isEditable,
            boolean isStaticText) {
        super(init(iconWidth, adj, isEditable, isStaticText));
    }

    private static Handle init(int iconWidth, Adjustment adj,
            boolean isEditable, boolean isStaticText) {
        int flags = 0;
        if (isEditable)
            flags = flags & 1 << 0;
        if (isStaticText)
            flags = flags & 1 << 1;
        return gnome_icon_list_new(iconWidth, adj.getHandle(), flags);
    }

    /**
     * Construct a new IconList from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public IconList(Handle handle) {
        super(handle);
    }

    /**
     * Avoid excessive recomputes during insertion and deletion. You should call
     * <i>thaw</i> once the activity is complete.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void freeze() {
        gnome_icon_list_freeze(getHandle());
    }

    /**
     * Thaw a previous frozen list.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void thaw() {
        gnome_icon_list_thaw(getHandle());
    }

    /**
     * Insert an icon into this list.
     * 
     * @param pos
     *            The position for the insertion.
     * @param iconFilename
     *            The file name for the icon to insert.
     * @param text
     *            The text to display below the icon.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void insert(int pos, String iconFilename, String text) {
        gnome_icon_list_insert(getHandle(), pos, iconFilename, text);
    }

    /**
     * Append an icon to the end of the list.
     * 
     * @param iconFilename
     *            The file name for the icon to insert.
     * @param text
     *            The text to display below the icon.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void append(String iconFilename, String text) {
        gnome_icon_list_append(getHandle(), iconFilename, text);
    }

    /**
     * Remove an icon from this list.
     * 
     * @param pos
     *            The position of the icon to remove
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void remove(int pos) {
        gnome_icon_list_remove(getHandle(), pos);
    }

    /**
     * Remove all icons from this list.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void clear() {
        gnome_icon_list_clear(getHandle());
    }

    /**
     * Get the number of icons in this list
     * 
     * @return The number of icons in this list.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public int getNumIcons() {
        return gnome_icon_list_get_num_icons(getHandle());
    }

    /**
     * Set the selection mode for this icon list.
     * 
     * @param mode
     *            The selection mode for this list.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setSelectionMode(SelectionMode mode) {
        gnome_icon_list_set_selection_mode(getHandle(), mode.getValue());
    }

    /**
     * Retrieve the selection mode for this icon list.
     * 
     * @return The selection mode for this list.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public SelectionMode getSelectionMode() {
        return SelectionMode
                .intern(gnome_icon_list_get_selection_mode(getHandle()));
    }

    /**
     * Select an icon in the list.
     * 
     * @param pos
     *            The position of the icon to select.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void selectIcon(int pos) {
        gnome_icon_list_select_icon(getHandle(), pos);
    }

    /**
     * Unselect an icon in the list.
     * 
     * @param pos
     *            The position of the icon to unselect.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void unselectIcon(int pos) {
        gnome_icon_list_unselect_icon(getHandle(), pos);
    }

    /**
     * Select all icons in the list.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void selectAll() {
        gnome_icon_list_select_all(getHandle());
    }

    /**
     * Unselect all icons in the list.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void unselectAll() {
        gnome_icon_list_unselect_all(getHandle());
    }

    // REDTAG: fix the getSelection method

    /**
     * Set the icon width for the list.
     * 
     * @param width
     *            The width for icons in the list.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setIconWidth(int width) {
        gnome_icon_list_set_icon_width(getHandle(), width);
    }

    /**
     * Set the row spacing for the list.
     * 
     * @param pixels
     *            The row spacing for the list.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setRowSpacing(int pixels) {
        gnome_icon_list_set_row_spacing(getHandle(), pixels);
    }

    /**
     * Set the column spacing for the list.
     * 
     * @param pixels
     *            The column spacing for the list.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setColumnSpacing(int pixels) {
        gnome_icon_list_set_col_spacing(getHandle(), pixels);
    }

    /**
     * Set the text spacing for the list.
     * 
     * @param pixels
     *            The text spacing for the list
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setTextSpacing(int pixels) {
        gnome_icon_list_set_text_spacing(getHandle(), pixels);
    }

    /**
     * Set the icon border for the list.
     * 
     * @param pixels
     *            The amount of pixels that surround the icons in the list.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setIconBorderWidth(int pixels) {
        gnome_icon_list_set_icon_border(getHandle(), pixels);
    }

    /**
     * Set the text that serves as the separator for this list. TODO: I don't
     * completely understand this method. Better javadocs are needed.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setSeparators(String sep) {
        gnome_icon_list_set_separators(getHandle(), sep);
    }

    /**
     * Retrieve the filename for an icon in the list.
     * 
     * @param pos
     *            The position of the icon to retrieve the filename.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getIconFilename(int pos) {
        return gnome_icon_list_get_icon_filename(getHandle(), pos);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_icon_list_get_type());
    }

    native static final protected int gnome_icon_list_get_type();

    native static final protected Handle gnome_icon_list_new(int iconWidth,
            Handle adj, int flags);

    native static final protected void gnome_icon_list_set_hadjustment(
            Handle gil, Handle hadj);

    native static final protected void gnome_icon_list_set_vadjustment(
            Handle gil, Handle vadj);

    native static final protected void gnome_icon_list_freeze(Handle gil);

    native static final protected void gnome_icon_list_thaw(Handle gil);

    native static final protected void gnome_icon_list_insert(Handle gil,
            int idx, String iconFilename, String text);

    native static final protected void gnome_icon_list_insert_pixbuf(
            Handle gil, int idx, Handle im, String iconFilename, String text);

    native static final protected int gnome_icon_list_append(Handle gil,
            String iconFilename, String text);

    native static final protected int gnome_icon_list_append_pixbuf(Handle gil,
            Handle im, String iconFilename, String text);

    native static final protected void gnome_icon_list_clear(Handle gil);

    native static final protected void gnome_icon_list_remove(Handle gil,
            int idx);

    native static final protected int gnome_icon_list_get_num_icons(Handle gil);

    native static final protected int gnome_icon_list_get_selection_mode(
            Handle gil);

    native static final protected void gnome_icon_list_set_selection_mode(
            Handle gil, int mode);

    native static final protected void gnome_icon_list_select_icon(Handle gil,
            int idx);

    native static final protected void gnome_icon_list_unselect_icon(
            Handle gil, int idx);

    native static final protected void gnome_icon_list_select_all(Handle gil);

    native static final protected int gnome_icon_list_unselect_all(Handle gil);

    native static final protected Handle[] gnome_icon_list_get_selection(
            Handle gil);

    native static final protected void gnome_icon_list_focus_icon(Handle gil,
            int idx);

    native static final protected void gnome_icon_list_set_icon_width(
            Handle gil, int w);

    native static final protected void gnome_icon_list_set_row_spacing(
            Handle gil, int pixels);

    native static final protected void gnome_icon_list_set_col_spacing(
            Handle gil, int pixels);

    native static final protected void gnome_icon_list_set_text_spacing(
            Handle gil, int pixels);

    native static final protected void gnome_icon_list_set_icon_border(
            Handle gil, int pixels);

    native static final protected void gnome_icon_list_set_separators(
            Handle gil, String sep);

    native static final protected String gnome_icon_list_get_icon_filename(
            Handle gil, int idx);

    native static final protected int gnome_icon_list_find_icon_from_filename(
            Handle gil, String filename);

    native static final protected void gnome_icon_list_moveto(Handle gil,
            int idx, double yalign);

    native static final protected int gnome_icon_list_icon_is_visible(
            Handle gil, int idx);

    native static final protected int gnome_icon_list_get_icon_at(Handle gil,
            int x, int y);

    native static final protected int gnome_icon_list_get_items_per_line(
            Handle gil);

    native static final protected Handle gnome_icon_list_get_icon_text_item(
            Handle gil, int idx);

    native static final protected Handle gnome_icon_list_get_icon_pixbuf_item(
            Handle gil, int idx);

}
