/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

/**
 * org.gnu.gnome.HelpException - An exception thrown when there is an error
 * displaying a help file.
 * 
 * <P>
 * <B>Revision History:</B>
 * <UL>
 * <LI>Nov 5, 2002 This class was created.</LI>
 * </UL>
 * </P>
 * 
 * @author Philip A. Chapman
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.HelpException</code>.
 */
public final class HelpException extends Exception {

    private HelpError error = null;

    /**
     * Constructor for HelpException.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    HelpException() {
        super();
    }

    /**
     * Constructor for HelpException.
     * 
     * @param message
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public HelpException(String message) {
        super(message);
    }

    // Constructors only available in java 1.4.0 and later
    /**
     * Constructor for HelpException.
     * 
     * @param message
     * @param cause
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    // public HelpException(String message, Throwable cause) {
    // super(message, cause);
    // }
    /**
     * Constructor for HelpException.
     * 
     * @param cause
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    // public HelpException(Throwable cause) {
    // super(cause);
    // }
}
