/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Type;
import org.gnu.gtk.Dialog;
import org.gnu.glib.Handle;

/**
 * The GnomeScores widget displays a list of names and scores for the players of
 * a game.
 * 
 * @deprecated
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.Scores</code>.
 */
public class Scores extends Dialog {
    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_scores_get_type());
    }

    native static final protected int gnome_scores_get_type();

    native static final protected Handle gnome_scores_display(String title,
            String app_name, String level, int pos);

    native static final protected void gnome_scores_set_logo_pixmap(Handle gs,
            String logo);

    native static final protected void gnome_scores_set_logo_widget(Handle gs,
            Handle widget);

    native static final protected void gnome_scores_set_logo_label_title(
            Handle gs, String title);

    native static final protected void gnome_scores_set_current_player(
            Handle gs, int i);

}
