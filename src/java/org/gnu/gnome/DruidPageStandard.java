/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.gdk.Color;
import org.gnu.gdk.Pixbuf;
import org.gnu.glib.Handle;
import org.gnu.glib.Type;
import org.gnu.gtk.VBox;
import org.gnu.gtk.Widget;

/**
 * A widget representing pages that are not initial or terminal pages of a
 * Druid.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.DruidPageStandard</code>.
 */
public class DruidPageStandard extends DruidPage {
    /**
     * Construct a new DruidPageStandard object
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public DruidPageStandard() {
        super(gnome_druid_page_standard_new());
    }

    /**
     * Construct a new DruidPageStandard object
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public DruidPageStandard(String title, Pixbuf logo, Pixbuf topWatermark) {
        super(gnome_druid_page_standard_new_with_vals(title, logo.getHandle(),
                topWatermark.getHandle()));
    }

    /**
     * Constructs a new DruidPageStandard object form a native resource.
     * 
     * @param handle
     *            The handle to the native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public DruidPageStandard(Handle handle) {
        super(handle);
    }

    /**
     * Set the title for the page.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setTitle(String title) {
        gnome_druid_page_standard_set_title(getHandle(), title);
    }

    /**
     * Set the logo for the page.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setLogo(Pixbuf logo) {
        gnome_druid_page_standard_set_logo(getHandle(), logo.getHandle());
    }

    /**
     * Set the watermark for the page.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setTopWatermark(Pixbuf watermark) {
        gnome_druid_page_standard_set_top_watermark(getHandle(), watermark
                .getHandle());
    }

    /**
     * Set the title foreground for the page.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setTitleForegroundColor(Color color) {
        gnome_druid_page_standard_set_title_foreground(getHandle(), color
                .getHandle());
    }

    /**
     * Set the background for the page.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setBackgroundColor(Color color) {
        gnome_druid_page_standard_set_background(getHandle(), color.getHandle());
    }

    /**
     * Set the logo background color.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setLogoBackgroundColor(Color color) {
        gnome_druid_page_standard_set_logo_background(getHandle(), color
                .getHandle());
    }

    /**
     * Set the contents background for the page.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setContentsBackgroundColor(Color color) {
        gnome_druid_page_standard_set_contents_background(getHandle(), color
                .getHandle());
    }

    /**
     * Get the layout control for the page. This can be used to add widgets the
     * the main area of the page.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public VBox getLayout() {
        Handle hndl = getVbox(getHandle());
        return VBox.getVBox(hndl);
    }

    /**
     * Convenience method to append a widget.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void appendItem(String question, Widget item, String additionalInfo) {
        gnome_druid_page_standard_append_item(getHandle(), question, item
                .getHandle(), additionalInfo);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_druid_page_standard_get_type());
    }

    native static final protected Handle getVbox(Handle cptr);

    native static final protected String getTitle(Handle cptr);

    native static final protected Handle getLogo(Handle cptr);

    native static final protected Handle getTopWatermark(Handle cptr);

    native static final protected Handle getTitleForeground(Handle cptr);

    native static final protected Handle getBackground(Handle cptr);

    native static final protected Handle getLogoBackground(Handle cptr);

    native static final protected Handle getContentsBackground(Handle cptr);

    native static final protected int gnome_druid_page_standard_get_type();

    native static final protected Handle gnome_druid_page_standard_new();

    native static final protected Handle gnome_druid_page_standard_new_with_vals(
            String title, Handle logo, Handle topWatermark);

    native static final protected void gnome_druid_page_standard_set_title(
            Handle page, String title);

    native static final protected void gnome_druid_page_standard_set_logo(
            Handle page, Handle logoImage);

    native static final protected void gnome_druid_page_standard_set_top_watermark(
            Handle page, Handle topWatermarkImage);

    native static final protected void gnome_druid_page_standard_set_title_foreground(
            Handle page, Handle color);

    native static final protected void gnome_druid_page_standard_set_background(
            Handle page, Handle color);

    native static final protected void gnome_druid_page_standard_set_logo_background(
            Handle page, Handle color);

    native static final protected void gnome_druid_page_standard_set_contents_background(
            Handle page, Handle color);

    native static final protected void gnome_druid_page_standard_append_item(
            Handle page, String questionMnemonic, Handle item,
            String additionalInfoMarkup);

}
