/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Enum;

public class RestartStyle extends Enum {

    static final private int _IF_RUNNING = 0;

    static final public org.gnu.gnome.RestartStyle IF_RUNNING = new org.gnu.gnome.RestartStyle(
            _IF_RUNNING);

    static final private int _ANYWAY = 1;

    static final public org.gnu.gnome.RestartStyle ANYWAY = new org.gnu.gnome.RestartStyle(
            _ANYWAY);

    static final private int _IMMEDIATELY = 2;

    static final public org.gnu.gnome.RestartStyle IMMEDIATELY = new org.gnu.gnome.RestartStyle(
            _IMMEDIATELY);

    static final private int _NEVER = 3;

    static final public org.gnu.gnome.RestartStyle NEVER = new org.gnu.gnome.RestartStyle(
            _NEVER);

    static final private org.gnu.gnome.RestartStyle[] theInterned = new org.gnu.gnome.RestartStyle[] {
            IF_RUNNING, ANYWAY, IMMEDIATELY, NEVER };

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.RestartStyle theSacrificialOne = new org.gnu.gnome.RestartStyle(
            0);

    static public org.gnu.gnome.RestartStyle intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.RestartStyle already = (org.gnu.gnome.RestartStyle) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.RestartStyle(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private RestartStyle(int value) {
        value_ = value;
    }

    public org.gnu.gnome.RestartStyle or(org.gnu.gnome.RestartStyle other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.RestartStyle and(org.gnu.gnome.RestartStyle other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.RestartStyle xor(org.gnu.gnome.RestartStyle other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.RestartStyle other) {
        return (value_ & other.value_) == other.value_;
    }

}
