/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Type;
import org.gnu.gtk.AnchorType;
import org.gnu.gtk.Widget;
import org.gnu.glib.Handle;

/**
 * The GnomeCanvasWidget is not a widget - it is a GnomeCanvasItem that is
 * capable of containing a widget. You can use a to display any widget in a
 * GnomeCanvas as if the widget were a GnomeCanvasItem.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.CanvasWidget</code>.
 */
public class CanvasWidget extends CanvasItem {

    public CanvasWidget(CanvasGroup group) {
        super(group, gnome_canvas_widget_get_type());
    }

    /**
     * Constructs an instance of CanvasWidget from a native widget resource.
     * 
     * @param handle
     *            The handle to the native widget.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    CanvasWidget(Handle handle) {
        super(handle);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_canvas_widget_get_type());
    }

    public Widget getWidget() {
        return Widget.getWidget(getWidget(getHandle()));
    }

    public void setWidget(Widget aWidget) {
        setWidget(getHandle(), aWidget.getHandle());
    }

    public double getHeight() {
        return getHeight(getHandle());
    }

    public void setHeight(double height) {
        setHeight(getHandle(), height);
    }

    public double getWidth() {
        return getWidth(getHandle());
    }

    public void setWidth(double width) {
        setWidth(getHandle(), width);
    }

    public AnchorType getAnchorType() {
        return AnchorType.intern(getAnchor(getHandle()));
    }

    public void setAnchorType(AnchorType aType) {
        setAnchor(getHandle(), aType.getValue());
    }

    native static final protected Handle getWidget(Handle cptr);

    native static final protected void setWidget(Handle cptr, Handle widget);

    native static final protected double getX(Handle cptr);

    native static final protected void setX(Handle cptr, double x);

    native static final protected double getY(Handle cptr);

    native static final protected void setY(Handle cptr, double y);

    native static final protected double getWidth(Handle cptr);

    native static final protected void setWidth(Handle cptr, double width);

    native static final protected double getHeight(Handle cptr);

    native static final protected void setHeight(Handle cptr, double height);

    native static final protected int getAnchor(Handle cptr);

    native static final protected void setAnchor(Handle cptr, int anchor);

    native static final protected boolean getSizePixels(Handle cptr);

    native static final protected void setSizePixels(Handle cptr, boolean size_pixels);

    native static final protected int gnome_canvas_widget_get_type();

}
