/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Enum;

public class InteractStyle extends Enum {
    static final private int _NONE = 0;

    static final public org.gnu.gnome.InteractStyle NONE = new org.gnu.gnome.InteractStyle(
            _NONE);

    static final private int _ERRORS = 1;

    static final public org.gnu.gnome.InteractStyle ERRORS = new org.gnu.gnome.InteractStyle(
            _ERRORS);

    static final private int _ANY = 2;

    static final public org.gnu.gnome.InteractStyle ANY = new org.gnu.gnome.InteractStyle(
            _ANY);

    static final private org.gnu.gnome.InteractStyle[] theInterned = new org.gnu.gnome.InteractStyle[] {
            NONE, ERRORS, ANY };

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.InteractStyle theSacrificialOne = new org.gnu.gnome.InteractStyle(
            0);

    static public org.gnu.gnome.InteractStyle intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.InteractStyle already = (org.gnu.gnome.InteractStyle) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.InteractStyle(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private InteractStyle(int value) {
        value_ = value;
    }

    public org.gnu.gnome.InteractStyle or(org.gnu.gnome.InteractStyle other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.InteractStyle and(org.gnu.gnome.InteractStyle other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.InteractStyle xor(org.gnu.gnome.InteractStyle other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.InteractStyle other) {
        return (value_ & other.value_) == other.value_;
    }

}
