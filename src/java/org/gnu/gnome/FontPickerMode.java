/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Enum;

public class FontPickerMode extends Enum {
    static final private int _PIXMAP = 0;

    static final public org.gnu.gnome.FontPickerMode PIXMAP = new org.gnu.gnome.FontPickerMode(
            _PIXMAP);

    static final private int _MODE_FONT_INFO = 1;

    static final public org.gnu.gnome.FontPickerMode MODE_FONT_INFO = new org.gnu.gnome.FontPickerMode(
            _MODE_FONT_INFO);

    static final private int _MODE_USER_WIDGET = 2;

    static final public org.gnu.gnome.FontPickerMode MODE_USER_WIDGET = new org.gnu.gnome.FontPickerMode(
            _MODE_USER_WIDGET);

    static final private int _MODE_UNKNOWN = 3;

    static final public org.gnu.gnome.FontPickerMode MODE_UNKNOWN = new org.gnu.gnome.FontPickerMode(
            _MODE_UNKNOWN);

    static final private org.gnu.gnome.FontPickerMode[] theInterned = new org.gnu.gnome.FontPickerMode[] {
            PIXMAP, MODE_FONT_INFO, MODE_USER_WIDGET, MODE_UNKNOWN }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.FontPickerMode theSacrificialOne = new org.gnu.gnome.FontPickerMode(
            0);

    static public org.gnu.gnome.FontPickerMode intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.FontPickerMode already = (org.gnu.gnome.FontPickerMode) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.FontPickerMode(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private FontPickerMode(int value) {
        value_ = value;
    }

    public org.gnu.gnome.FontPickerMode or(org.gnu.gnome.FontPickerMode other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.FontPickerMode and(org.gnu.gnome.FontPickerMode other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.FontPickerMode xor(org.gnu.gnome.FontPickerMode other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.FontPickerMode other) {
        return (value_ & other.value_) == other.value_;
    }

}
