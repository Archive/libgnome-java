/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.gdk.Bitmap;
import org.gnu.gdk.Color;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The GnomeCanvasEllipse is a GnomeCanvasItem that draws itself as an ellipse
 * on a GnomeCanvas.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.CanvasEllipse</code>.
 */
public class CanvasEllipse extends CanvasRE {

    public CanvasEllipse(CanvasGroup group) {
        super(group, gnome_canvas_ellipse_get_type());
    }

    /**
     * Constructs an instance of CanvasEllipse from a native widget resource.
     * 
     * @param handle
     *            The handle to the native widget.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    CanvasEllipse(Handle handle) {
        super(handle);
    }

    /**
     * Creates a new CanvasEllipse object.
     * 
     * @param group
     *            The parent group for the new CanvasItem.
     * @param x1
     *            The leftmost coordinate.
     * @param y1
     *            The topmost coordinate.
     * @param x2
     *            The rightmost coordinate.
     * @param y2
     *            The bottommost coordinate.
     * @param fill_color
     *            Fill color to be used.
     * @param outline_color
     *            Outline color or null for transparent.
     * @param fill_stipple
     *            Stipple used when drawing fill.
     * @param outline_stipple
     *            Stipple used when drawing outline.
     * @param width_pixels
     *            Outline width specified in pixels, independent of zoom factor.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public CanvasEllipse(CanvasGroup group, double x1, double y1, double x2,
            double y2, Color fill_color, Color outline_color,
            Bitmap fill_stipple, Bitmap outline_stipple, int width_pixels) {
        super(gnome_canvas_re_new_gdk_wpix(group.getHandle(),
                gnome_canvas_ellipse_get_type(), x1, y1, x2, y2, fill_color
                        .getHandle(), outline_color == null ? null
                        : outline_color.getHandle(),
                fill_stipple == null ? null : fill_stipple.getHandle(),
                outline_stipple == null ? null : outline_stipple.getHandle(),
                width_pixels));
    }

    /**
     * Creates a new CanvasEllipse object.
     * 
     * @param group
     *            The parent group for the new CanvasItem.
     * @param x1
     *            The leftmost coordinate.
     * @param y1
     *            The topmost coordinate.
     * @param x2
     *            The rightmost coordinate.
     * @param y2
     *            The bottommost coordinate.
     * @param fill_color
     *            Fill color to be used.
     * @param outline_color
     *            Outline color or null for transparent.
     * @param fill_stipple
     *            Stipple used when drawing fill.
     * @param outline_stipple
     *            Stipple used when drawing outline.
     * @param width_units
     *            Outline width specified in Canvas units.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public CanvasEllipse(CanvasGroup group, double x1, double y1, double x2,
            double y2, Color fill_color, Color outline_color,
            Bitmap fill_stipple, Bitmap outline_stipple, double width_units) {
        super(gnome_canvas_re_new_gdk_wunit(group.getHandle(),
                gnome_canvas_ellipse_get_type(), x1, y1, x2, y2, fill_color
                        .getHandle(), outline_color == null ? null
                        : outline_color.getHandle(),
                fill_stipple == null ? null : fill_stipple.getHandle(),
                outline_stipple == null ? null : outline_stipple.getHandle(),
                width_units));
    }

    /**
     * Creates a new CanvasEllipse object.
     * 
     * @param group
     *            The parent group for the new CanvasItem.
     * @param x1
     *            The leftmost coordinate.
     * @param y1
     *            The topmost coordinate.
     * @param x2
     *            The rightmost coordinate.
     * @param y2
     *            The bottommost coordinate.
     * @param fill_color
     *            Fill color to be used.
     * @param outline_color
     *            Outline color.
     * @param width_pixels
     *            Outline width specified in pixels, independent of zoom factor.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public CanvasEllipse(CanvasGroup group, double x1, double y1, double x2,
            double y2, int fill_color, int outline_color, int width_pixels) {
        super(gnome_canvas_re_new_rgba_wpix(group.getHandle(),
                gnome_canvas_ellipse_get_type(), x1, y1, x2, y2, fill_color,
                outline_color, width_pixels));
    }

    /**
     * Creates a new CanvasEllipse object.
     * 
     * @param group
     *            The parent group for the new CanvasItem.
     * @param x1
     *            The leftmost coordinate.
     * @param y1
     *            The topmost coordinate.
     * @param x2
     *            The rightmost coordinate.
     * @param y2
     *            The bottommost coordinate.
     * @param fill_color
     *            Fill color to be used.
     * @param outline_color
     *            Outline color.
     * @param width_units
     *            Outline width specified in Canvas units.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public CanvasEllipse(CanvasGroup group, double x1, double y1, double x2,
            double y2, int fill_color, int outline_color, double width_units) {
        super(gnome_canvas_re_new_rgba_wunit(group.getHandle(),
                gnome_canvas_ellipse_get_type(), x1, y1, x2, y2, fill_color,
                outline_color, width_units));
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_canvas_ellipse_get_type());
    }

    native static final protected int gnome_canvas_ellipse_get_type();

}
