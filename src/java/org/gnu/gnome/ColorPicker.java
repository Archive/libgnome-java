/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import java.util.Vector;

import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.glib.Type;
import org.gnu.gnome.event.ColorPickerEvent;
import org.gnu.gnome.event.ColorPickerListener;
import org.gnu.gtk.Button;
import org.gnu.glib.Handle;

/**
 * The ColorPicker button pops up a ColorSelectionDialog and allows a color to
 * be selected. The button changes color to match the currently selected color.
 * The colors can be set using doubles - values between 0.0 and 1.0, 8 bit
 * integers - values between 0 and 255, and 16 bit integers - values between 0
 * and 65535.
 * 
 * @deprecated
 * @see org.gnu.gtk.ColorSelectionDialog
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.ColorPicker</code>.
 */
public class ColorPicker extends Button {
    /**
     * Listeners for handling ColorPicker events
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private Vector colorPickerListeners = null;

    /**
     * Creates a new instance of ColorPicker
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public ColorPicker() {
        super(gnome_color_picker_new());
    }

    /**
     * Construct a ColorPicker using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public ColorPicker(Handle handle) {
        super(handle);
    }

    /**
     * Get the alpha of the color as a double value between 0.0 and 1.0.
     * 
     * @return The alpha of the color.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public double getAlphaDouble() {
        double alpha[] = new double[1];
        double blue[] = new double[1];
        double green[] = new double[1];
        double red[] = new double[1];

        gnome_color_picker_get_d(getHandle(), red, green, blue, alpha);

        return alpha[0];
    }

    /**
     * Get the blue element of the color as a double value between 0.0 and 1.0.
     * 
     * @return The blue element of the color.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public double getBlueDouble() {
        double alpha[] = new double[1];
        double blue[] = new double[1];
        double green[] = new double[1];
        double red[] = new double[1];

        gnome_color_picker_get_d(getHandle(), red, green, blue, alpha);

        return blue[0];
    }

    /**
     * Get the green element of the color as a double value between 0.0 and 1.0.
     * 
     * @return The green element of the color.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public double getGreenDouble() {
        double alpha[] = new double[1];
        double blue[] = new double[1];
        double green[] = new double[1];
        double red[] = new double[1];

        gnome_color_picker_get_d(getHandle(), red, green, blue, alpha);

        return green[0];
    }

    /**
     * Get the red element of the color as a double value between 0.0 and 1.0.
     * 
     * @return The red element of the color.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public double getRedDouble() {
        double alpha[] = new double[1];
        double blue[] = new double[1];
        double green[] = new double[1];
        double red[] = new double[1];

        gnome_color_picker_get_d(getHandle(), red, green, blue, alpha);

        return red[0];
    }

    /**
     * Get the alpha of the color as a 8 bit value between 0 and 255.
     * 
     * @return The alpha of the color.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public int getAlpha8Bit() {
        int alpha[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i8(getHandle(), red, green, blue, alpha);

        return alpha[0];
    }

    /**
     * Get the blue element of the color as a 8 bit value between 0 and 255.
     * 
     * @return The blue element of the color.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public int getBlue8Bit() {
        int alpha[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i8(getHandle(), red, green, blue, alpha);

        return blue[0];
    }

    /**
     * Get the green element of the color as a double value between 0 and 255.
     * 
     * @return The green element of the color.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public int getGreen8Bit() {
        int alpha[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i8(getHandle(), red, green, blue, alpha);

        return green[0];
    }

    /**
     * Get the red element of the color as a double value between 0 and 255.
     * 
     * @return The red element of the color.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public int getRed8Bit() {
        int alpha[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i8(getHandle(), red, green, blue, alpha);

        return red[0];
    }

    /**
     * Get the alpha of the color as a 16 bit value between 0 and 65535.
     * 
     * @return The alpha of the color.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public int getAlpha16Bit() {
        int alpha[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i16(getHandle(), red, green, blue, alpha);

        return alpha[0];
    }

    /**
     * Get the blue element of the color as a 16 bit value between 0 and 65535.
     * 
     * @return The blue element of the color.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public int getBlue16Bit() {
        int alpha[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i16(getHandle(), red, green, blue, alpha);

        return blue[0];
    }

    /**
     * Get the green element of the color as a double value between 0 and 65535.
     * 
     * @return The green element of the color.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public int getGreen16Bit() {
        int alpha[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i16(getHandle(), red, green, blue, alpha);

        return green[0];
    }

    /**
     * Get the red element of the color as a double value between 0 and 65535.
     * 
     * @return The red element of the color.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public int getRed16Bit() {
        int alpha[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i16(getHandle(), red, green, blue, alpha);

        return red[0];
    }

    /**
     * Returns whether ColorPicker uses alpha.
     * 
     * @return True if alpha is used. Otherwise, false.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public boolean getAlphaEnabled() {
        return gnome_color_picker_get_use_alpha(getHandle());
    }

    /**
     * Returns whether ColorPicker uses dither.
     * 
     * @return True if dither is used. Otherwise, false.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public boolean getDitherEnabled() {
        return gnome_color_picker_get_dither(getHandle());
    }

    /**
     * Gets the title of the color picker.
     * 
     * @return The title for the color picker.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getTitle() {
        return gnome_color_picker_get_title(getHandle());
    }

    /**
     * A shortcut method to set all the color values with one call. All values
     * must be a double value between 0.0 and 1.0.
     * 
     * @param alpha
     *            The alpha of the color.
     * @param blue
     *            The blue element of the color.
     * @param green
     *            The green element of the color.
     * @param red
     *            The red element of the color.
     * @exception IllegalArgumentException
     *                Thrown if any given value is outside of the valid range
     *                0.0 to 1.0.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setColorDouble(double alpha, double blue, double green,
            double red) throws IllegalArgumentException {
        if (alpha < 0.0 || alpha > 1.0) {
            throw new IllegalArgumentException(
                    "The value "
                            + alpha
                            + " is outside the acceptable range of 0.0 to 1.0 for alpha.");
        } else if (blue < 0.0 || blue > 1.0) {
            throw new IllegalArgumentException(
                    "The value "
                            + blue
                            + " is outside the acceptable range of 0.0 to 1.0 for blue.");
        } else if (green < 0.0 || green > 1.0) {
            throw new IllegalArgumentException(
                    "The value "
                            + green
                            + " is outside the acceptable range of 0.0 to 1.0 for green.");
        } else if (red < 0.0 || red > 1.0) {
            throw new IllegalArgumentException("The value " + red
                    + " is outside the acceptable range of 0.0 to 1.0 for red.");
        }

        gnome_color_picker_set_d(getHandle(), red, green, blue, alpha);
    }

    /**
     * Set the alpha of the color as a double value between 0.0 and 1.0.
     * 
     * @param alpha
     *            The alpha of the color.
     * @exception IllegalArgumentException
     *                Thrown if alpha is outside of the valid range 0.0 to 1.0.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setAlphaDouble(double alpha) throws IllegalArgumentException {
        if (alpha < 0.0 || alpha > 1.0) {
            throw new IllegalArgumentException("The value " + alpha
                    + " is outside the acceptable range of 0.0 to 1.0.");
        }

        double a[] = new double[1];
        double blue[] = new double[1];
        double green[] = new double[1];
        double red[] = new double[1];

        gnome_color_picker_get_d(getHandle(), red, green, blue, a);
        a[0] = alpha;
        gnome_color_picker_set_d(getHandle(), red[0], green[0], blue[0], a[0]);
    }

    /**
     * Set the blue element of the color as a double value between 0.0 and 1.0.
     * 
     * @param color
     *            The blue element of the color.
     * @exception IllegalArgumentException
     *                Thrown if color is outside of the valid range 0.0 to 1.0.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setBlueDouble(double color) throws IllegalArgumentException {
        if (color < 0.0 || color > 1.0) {
            throw new IllegalArgumentException("The value " + color
                    + " is outside the acceptable range of 0.0 to 1.0.");
        }

        double alpha[] = new double[1];
        double blue[] = new double[1];
        double green[] = new double[1];
        double red[] = new double[1];

        gnome_color_picker_get_d(getHandle(), red, green, blue, alpha);
        blue[0] = color;
        gnome_color_picker_set_d(getHandle(), red[0], green[0], blue[0],
                alpha[0]);
    }

    /**
     * Set the green element of the color as a double value between 0.0 and 1.0.
     * 
     * @param color
     *            The green element of the color.
     * @exception IllegalArgumentException
     *                Thrown if color is outside of the valid range 0.0 to 1.0.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setGreenDouble(double color) throws IllegalArgumentException {
        if (color < 0.0 || color > 1.0) {
            throw new IllegalArgumentException("The value " + color
                    + " is outside the acceptable range of 0.0 to 1.0.");
        }

        double alpha[] = new double[1];
        double blue[] = new double[1];
        double green[] = new double[1];
        double red[] = new double[1];

        gnome_color_picker_get_d(getHandle(), red, green, blue, alpha);
        green[0] = color;
        gnome_color_picker_set_d(getHandle(), red[0], green[0], blue[0],
                alpha[0]);
    }

    /**
     * Set the red element of the color as a double value between 0.0 and 1.0.
     * 
     * @param color
     *            The red element of the color.
     * @exception IllegalArgumentException
     *                Thrown if color is outside of the valid range 0.0 to 1.0.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setRedDouble(double color) throws IllegalArgumentException {
        if (color < 0.0 || color > 1.0) {
            throw new IllegalArgumentException("The value " + color
                    + " is outside the acceptable range of 0.0 to 1.0.");
        }

        double alpha[] = new double[1];
        double blue[] = new double[1];
        double green[] = new double[1];
        double red[] = new double[1];

        gnome_color_picker_get_d(getHandle(), red, green, blue, alpha);
        red[0] = color;
        gnome_color_picker_set_d(getHandle(), red[0], green[0], blue[0],
                alpha[0]);
    }

    /**
     * A shortcut method to set all the color values with one call. All values
     * must be a integer value between 0 and 255.
     * 
     * @param alpha
     *            The alpha of the color.
     * @param blue
     *            The blue element of the color.
     * @param green
     *            The green element of the color.
     * @param red
     *            The red element of the color.
     * @exception IllegalArgumentException
     *                Thrown if any given value is outside of the valid range 0
     *                to 255.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setColor8Bit(int alpha, int blue, int green, int red)
            throws IllegalArgumentException {
        if (alpha < 0 || alpha > 255) {
            throw new IllegalArgumentException("The value " + alpha
                    + " is outside the acceptable range of 0 to 255 for alpha.");
        } else if (blue < 0 || blue > 255) {
            throw new IllegalArgumentException("The value " + blue
                    + " is outside the acceptable range of 0 to 255 for blue.");
        } else if (green < 0 || green > 255) {
            throw new IllegalArgumentException("The value " + green
                    + " is outside the acceptable range of 0 to 255 for green.");
        } else if (red < 0 || red > 255) {
            throw new IllegalArgumentException("The value " + red
                    + " is outside the acceptable range of 0 to 255 for red.");
        }

        gnome_color_picker_set_i8(getHandle(), red, green, blue, alpha);
    }

    /**
     * Set the alpha of the color as a 8 bit value between 0 and 255.
     * 
     * @param alpha
     *            The alpha of the color.
     * @exception IllegalArgumentException
     *                Thrown if alpha is outside of the valid range 0 to 255.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setAlpha8Bit(int alpha) throws IllegalArgumentException {
        if (alpha < 0 || alpha > 255) {
            throw new IllegalArgumentException("The value " + alpha
                    + " is outside the acceptable range of 0 to 255.");
        }

        int a[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i8(getHandle(), red, green, blue, a);
        a[0] = alpha;
        gnome_color_picker_set_i8(getHandle(), red[0], green[0], blue[0], a[0]);
    }

    /**
     * Set the blue element of the color as a 8 bit value between 0 and 255.
     * 
     * @param color
     *            The blue element of the color.
     * @exception IllegalArgumentException
     *                Thrown if color is outside of the valid range 0 to 255.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setBlue8Bit(int color) throws IllegalArgumentException {
        if (color < 0 || color > 255) {
            throw new IllegalArgumentException("The value " + color
                    + " is outside the acceptable range of 0 to 255.");
        }

        int alpha[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i8(getHandle(), red, green, blue, alpha);
        blue[0] = color;
        gnome_color_picker_set_i8(getHandle(), red[0], green[0], blue[0],
                alpha[0]);
    }

    /**
     * Set the green element of the color as a integer value between 0 and 255.
     * 
     * @param color
     *            The green element of the color.
     * @exception IllegalArgumentException
     *                Thrown if color is outside of the valid range 0 to 255.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setGreen8Bit(int color) throws IllegalArgumentException {
        if (color < 0 || color > 255) {
            throw new IllegalArgumentException("The value " + color
                    + " is outside the acceptable range of 0 to 255.");
        }

        int alpha[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i8(getHandle(), red, green, blue, alpha);
        green[0] = color;
        gnome_color_picker_set_i8(getHandle(), red[0], green[0], blue[0],
                alpha[0]);
    }

    /**
     * Set the red element of the color as a integer value between 0 and 255.
     * 
     * @param color
     *            The red element of the color.
     * @exception IllegalArgumentException
     *                Thrown if color is outside of the valid range 0 to 255.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setRed8Bit(int color) throws IllegalArgumentException {
        if (color < 0 || color > 255) {
            throw new IllegalArgumentException("The value " + color
                    + " is outside the acceptable range of 0 to 255.");
        }

        int alpha[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i8(getHandle(), red, green, blue, alpha);
        red[0] = color;
        gnome_color_picker_set_i8(getHandle(), red[0], green[0], blue[0],
                alpha[0]);
    }

    /**
     * A shortcut method to set all the color values with one call. All values
     * must be a integer value between 0 and 65535.
     * 
     * @param alpha
     *            The alpha of the color.
     * @param blue
     *            The blue element of the color.
     * @param green
     *            The green element of the color.
     * @param red
     *            The red element of the color.
     * @exception IllegalArgumentException
     *                Thrown if any given value is outside of the valid range 0
     *                to 65535.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setColor16Bit(int alpha, int blue, int green, int red)
            throws IllegalArgumentException {
        if (alpha < 0 || alpha > 65535) {
            throw new IllegalArgumentException(
                    "The value "
                            + alpha
                            + " is outside the acceptable range of 0 to 65535 for alpha.");
        } else if (blue < 0 || blue > 65535) {
            throw new IllegalArgumentException(
                    "The value "
                            + blue
                            + " is outside the acceptable range of 0 to 65535 for blue.");
        } else if (green < 0 || green > 65535) {
            throw new IllegalArgumentException(
                    "The value "
                            + green
                            + " is outside the acceptable range of 0 to 65535 for green.");
        } else if (red < 0 || red > 65535) {
            throw new IllegalArgumentException("The value " + red
                    + " is outside the acceptable range of 0 to 65535 for red.");
        }

        gnome_color_picker_set_i8(getHandle(), red, green, blue, alpha);
    }

    /**
     * Set the alpha of the color as a 16 bit value between 0 and 65535.
     * 
     * @param alpha
     *            The alpha of the color.
     * @exception IllegalArgumentException
     *                Thrown if alpha is outside of the valid range 0 to 65535.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setAlpha16Bit(int alpha) throws IllegalArgumentException {
        if (alpha < 0 || alpha > 65535) {
            throw new IllegalArgumentException("The value " + alpha
                    + " is outside the acceptable range of 0 to 65535.");
        }

        int a[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i16(getHandle(), red, green, blue, a);
        a[0] = alpha;
        gnome_color_picker_set_i16(getHandle(), red[0], green[0], blue[0], a[0]);
    }

    /**
     * Set the blue element of the color as a 16 bit value between 0 and 65535.
     * 
     * @param color
     *            The blue element of the color.
     * @exception IllegalArgumentException
     *                Thrown if color is outside of the valid range 0 to 65535.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setBlue16Bit(int color) throws IllegalArgumentException {
        if (color < 0 || color > 65535) {
            throw new IllegalArgumentException("The value " + color
                    + " is outside the acceptable range of 0 to 65535.");
        }

        int alpha[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i16(getHandle(), red, green, blue, alpha);
        blue[0] = color;
        gnome_color_picker_set_i16(getHandle(), red[0], green[0], blue[0],
                alpha[0]);
    }

    /**
     * Set the green element of the color as a integer value between 0 and
     * 65535.
     * 
     * @param color
     *            The green element of the color.
     * @exception IllegalArgumentException
     *                Thrown if color is outside of the valid range 0 to 65535.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setGreen16Bit(int color) throws IllegalArgumentException {
        if (color < 0 || color > 65535) {
            throw new IllegalArgumentException("The value " + color
                    + " is outside the acceptable range of 0 to 65535.");
        }

        int alpha[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i16(getHandle(), red, green, blue, alpha);
        green[0] = color;
        gnome_color_picker_set_i16(getHandle(), red[0], green[0], blue[0],
                alpha[0]);
    }

    /**
     * Set the red element of the color as a integer value between 0 and 65535.
     * 
     * @param color
     *            The red element of the color.
     * @exception IllegalArgumentException
     *                Thrown if color is outside of the valid range 0 to 65535.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setRed16Bit(int color) throws IllegalArgumentException {
        if (color < 0 || color > 65535) {
            throw new IllegalArgumentException("The value " + color
                    + " is outside the acceptable range of 0 to 65535.");
        }

        int alpha[] = new int[1];
        int blue[] = new int[1];
        int green[] = new int[1];
        int red[] = new int[1];

        gnome_color_picker_get_i16(getHandle(), red, green, blue, alpha);
        red[0] = color;
        gnome_color_picker_set_i16(getHandle(), red[0], green[0], blue[0],
                alpha[0]);
    }

    /**
     * Sets whether ColorPicker is to use alpha.
     * 
     * @param enable
     *            True to use alpha. Otherwise, false.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setAlphaEnabled(boolean enable) {
        gnome_color_picker_set_use_alpha(getHandle(), enable);
    }

    /**
     * Sets whether ColorPicker is to use dither.
     * 
     * @param enable
     *            True to use dither. Otherwise, false.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setDitherEnabled(boolean enable) {
        gnome_color_picker_set_dither(getHandle(), enable);
    }

    /**
     * Sets the title of the color picker.
     * 
     * @param title
     *            The title for the color picker.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setTitle(String title) {
        gnome_color_picker_set_title(getHandle(), title);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_color_picker_get_type());
    }

    // ////////////////////////////////////////////////////
    // Event handling
    // ////////////////////////////////////////////////////
    /**
     * Register an object to handle ColorPicker events.
     * 
     * @see org.gnu.gnome.event.ColorPickerListener
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void addListener(ColorPickerListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(colorPickerListeners, listener);
        if (i == -1) {
            if (null == colorPickerListeners) {
                evtMap.initialize(this, ColorPickerEvent.Type.COLOR_SET);
                colorPickerListeners = new Vector();
            }
            colorPickerListeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(ColorPickerListener)
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(ColorPickerListener listener) {
        int i = findListener(colorPickerListeners, listener);
        if (i > -1) {
            colorPickerListeners.remove(i);
        }
        if (0 == colorPickerListeners.size()) {
            evtMap.uninitialize(this, ColorPickerEvent.Type.COLOR_SET);
            colorPickerListeners = null;
        }
    }

    protected void fireColorPickerEvent(ColorPickerEvent event) {
        if (null == colorPickerListeners) {
            return;
        }
        int size = colorPickerListeners.size();
        int i = 0;
        while (i < size) {
            ColorPickerListener cpl = (ColorPickerListener) colorPickerListeners
                    .elementAt(i);
            cpl.colorPickerEvent(event);
            i++;
        }
    }

    private void handleColorSet(int red, int green, int blue, int alpha) {
        fireColorPickerEvent(new ColorPickerEvent(this));
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    /**
     * Implementation method to build an EventMap for this widget class. Not
     * useful (or supported) for application use.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("color_set", "handleColorSet",
                ColorPickerEvent.Type.COLOR_SET, ColorPickerListener.class);
    }

    native static final protected int gnome_color_picker_get_type();

    native static final protected Handle gnome_color_picker_new();

    native static final protected void gnome_color_picker_set_d(Handle cp,
            double red, double green, double blue, double alpha);

    native static final protected void gnome_color_picker_get_d(Handle cp,
            double[] red, double[] green, double[] blue, double[] alpha);

    native static final protected void gnome_color_picker_set_i8(Handle cp,
            int red, int green, int blue, int alpha);

    native static final protected void gnome_color_picker_get_i8(Handle cp,
            int[] red, int[] green, int[] blue, int[] alpha);

    native static final protected void gnome_color_picker_set_i16(Handle cp,
            int red, int green, int blue, int alpha);

    native static final protected void gnome_color_picker_get_i16(Handle cp,
            int[] red, int[] green, int[] blue, int[] alpha);

    native static final protected void gnome_color_picker_set_dither(Handle cp,
            boolean dither);

    native static final protected boolean gnome_color_picker_get_dither(
            Handle cp);

    native static final protected void gnome_color_picker_set_use_alpha(
            Handle cp, boolean useAlpha);

    native static final protected boolean gnome_color_picker_get_use_alpha(
            Handle cp);

    native static final protected void gnome_color_picker_set_title(Handle cp,
            String title);

    native static final protected String gnome_color_picker_get_title(Handle cp);

}
