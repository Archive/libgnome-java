/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.CanvasPathDef</code>.
 */
public class CanvasPathDef extends GObject {

    native static final protected Handle gnome_canvas_path_def_new();

    native static final protected Handle gnome_canvas_path_def_new_sized(
            int length);

    native static final protected void gnome_canvas_path_def_ref(Handle path);

    native static final protected void gnome_canvas_path_def_finish(Handle path);

    native static final protected void gnome_canvas_path_def_ensure_space(
            Handle path, int space);

    native static final protected void gnome_canvas_path_def_copy(Handle dst,
            Handle src);

    native static final protected Handle gnome_canvas_path_def_duplicate(
            Handle path);

    native static final protected void gnome_canvas_path_def_unref(Handle path);

    native static final protected void gnome_canvas_path_def_reset(Handle path);

    native static final protected void gnome_canvas_path_def_moveto(
            Handle path, double x, double y);

    native static final protected void gnome_canvas_path_def_lineto(
            Handle path, double x, double y);

    native static final protected void gnome_canvas_path_def_lineto_moving(
            Handle path, double x, double y);

    native static final protected void gnome_canvas_path_def_curveto(
            Handle path, double x0, double y0, double x1, double y1, double x2,
            double y2);

    native static final protected void gnome_canvas_path_def_closepath(
            Handle path);

    native static final protected void gnome_canvas_path_def_closepath_current(
            Handle path);

    native static final protected int gnome_canvas_path_def_length(Handle path);

    native static final protected boolean gnome_canvas_path_def_is_empty(
            Handle path);

    native static final protected boolean gnome_canvas_path_def_has_currentpoint(
            Handle path);

    native static final protected boolean gnome_canvas_path_def_any_open(
            Handle path);

    native static final protected boolean gnome_canvas_path_def_all_open(
            Handle path);

    native static final protected boolean gnome_canvas_path_def_any_closed(
            Handle path);

    native static final protected boolean gnome_canvas_path_def_all_closed(
            Handle path);

}
