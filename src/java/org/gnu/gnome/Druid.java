/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Handle;
import org.gnu.glib.Type;
import org.gnu.gtk.Button;
import org.gnu.gtk.Container;
import org.gnu.gtk.Widget;
import org.gnu.gtk.Window;

/**
 * The GNOME druid is a system for assisting the user with installing a service.
 * It is roughly equivalent in functionality to the Wizards available in
 * Windows.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.Druid</code>.
 */
public class Druid extends Container {

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Druid getDruid(Handle handle) {
        if (handle == null)
            return null;
        
        Druid druid = (Druid) getGObjectFromHandle(handle);
        if (druid == null) {
            druid = new Druid(handle);
        }
        return druid;
    }

    /**
     * Construct a new Druid object.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Druid() {
        super(gnome_druid_new());
    }

    /**
     * Construct a new Druid from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Druid(Handle handle) {
        super(handle);
    }

    /**
     * Construct a new Druid object that already contains a window.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Druid(String title, Window parent, boolean closeOnCancel,
            Widget window) {
        super(gnome_druid_new_with_window(title, parent.getHandle(),
                closeOnCancel, window.getHandle()));
    }

    /**
     * Sets the sensitivity of the buttons on the Druid dialog.
     * 
     * @param backSensitive
     *            Determines if the "Back" button is sensitive.
     * @param nextSensitive
     *            Determines if the "Next" button is sensitive.
     * @param cancelSensitive
     *            Determines if the "Cancel" button is sensitive.
     * @param helpSensitive
     *            Determines if the "Help" button is sensitive.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setButtonsSensitive(boolean backSensitive,
            boolean nextSensitive, boolean cancelSensitive,
            boolean helpSensitive) {
        gnome_druid_set_buttons_sensitive(getHandle(), backSensitive,
                nextSensitive, cancelSensitive, helpSensitive);
    }

    /**
     * Sets the text on the last button of the dialog. If <i>showFinish</i> is
     * true the text becomes "Finish". Otherwise the text is set to "Cancel".
     * 
     * @param showFinish
     *            Determines the text of the last button on the dialog.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setShowFinish(boolean showFinish) {
        gnome_druid_set_show_finish(getHandle(), showFinish);
    }

    /**
     * Sets whether the help button should be displayed
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setShowHelp(boolean showHelp) {
        gnome_druid_set_show_help(getHandle(), showHelp);
    }

    /**
     * Prepends a DruidPage into the list of pages for this dialog.
     * 
     * @param page
     *            The page to prepend.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void prependPage(DruidPage page) {
        gnome_druid_prepend_page(getHandle(), page.getHandle());
    }

    /**
     * Append a DruidPage into the list of pages for this dialog.
     * 
     * @param page
     *            The page to append.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void appendPage(DruidPage page) {
        gnome_druid_append_page(getHandle(), page.getHandle());
    }

    /**
     * Insert <i>page</i> after <i>backPage</i> into the list of pages for
     * this dialog. If </i>backPage is not present or null, </i>page</i> will
     * be prepended.
     * 
     * @param backPage
     * @param page
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void insertPage(DruidPage backPage, DruidPage page) {
        gnome_druid_insert_page(getHandle(), backPage.getHandle(), page
                .getHandle());
    }

    /**
     * Set the currently displayed page to <i>page</i>.
     * 
     * @param page
     *            The page to make the current page.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setPage(DruidPage page) {
        gnome_druid_set_page(getHandle(), page.getHandle());
    }

    /**
     * Retrieve the "Help" button.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Button getHelpButton() {
        Handle hndl = getHelp(getHandle());
        return Button.getButton(hndl);
    }

    /**
     * Retrieve the "Back" button.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Button getBackButton() {
        Handle hndl = getBack(getHandle());
        return Button.getButton(hndl);
    }

    /**
     * Retrieve the "Next" button.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Button getNextButton() {
        Handle hndl = getNext(getHandle());
        return Button.getButton(hndl);
    }

    /**
     * Retrieve the "Cancel" button.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Button getCancelButton() {
        Handle hndl = getCancel(getHandle());
        return Button.getButton(hndl);
    }

    /**
     * Retrieve the "Finish" button.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Button getFinishButton() {
        Handle hndl = getFinish(getHandle());
        return Button.getButton(hndl);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_druid_get_type());
    }

    native static final protected Handle getHelp(Handle cptr);

    native static final protected Handle getBack(Handle cptr);

    native static final protected Handle getNext(Handle cptr);

    native static final protected Handle getCancel(Handle cptr);

    native static final protected Handle getFinish(Handle cptr);

    native static final protected int gnome_druid_get_type();

    native static final protected Handle gnome_druid_new();

    native static final protected void gnome_druid_set_buttons_sensitive(
            Handle druid, boolean backSensitive, boolean nextSensitive,
            boolean cancelSensitive, boolean helpSensitive);

    native static final protected void gnome_druid_set_show_finish(
            Handle druid, boolean showFinish);

    native static final protected void gnome_druid_set_show_help(Handle druid,
            boolean showHelp);

    native static final protected void gnome_druid_prepend_page(Handle druid,
            Handle page);

    native static final protected void gnome_druid_insert_page(Handle druid,
            Handle backPage, Handle page);

    native static final protected void gnome_druid_append_page(Handle druid,
            Handle page);

    native static final protected void gnome_druid_set_page(Handle druid,
            Handle page);

    native static final protected Handle gnome_druid_new_with_window(
            String title, Handle parent, boolean closeOnCancel, Handle window);

}
