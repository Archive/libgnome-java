/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import java.util.Vector;

import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.glib.GObject;
import org.gnu.glib.Handle;
import org.gnu.glib.Type;
import org.gnu.gnome.event.IconEntryEvent;
import org.gnu.gnome.event.IconEntryListener;
import org.gnu.gtk.VBox;

/**
 * A GnomeIconEntry is a button that pops up a window displaying a collection of
 * icons. The user can select one of the icons found in /usr/share/pixmaps or
 * browse the file system to find icons in other directories. The pull-down list
 * of directory names maintains a history of previously opened directories.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.IconEntry</code>.
 */
public class IconEntry extends VBox {

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static IconEntry getIconEntry(Handle handle) {
        if (handle == null) {
            return null;
        }

        IconEntry iconEntry = (IconEntry) GObject.getGObjectFromHandle(handle);
        if (iconEntry == null) {
            iconEntry = new IconEntry(handle);
        }
        return iconEntry;
    }

    private Vector listeners;

    /**
     * Construct a new IconEntry
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public IconEntry(String historyId, String dialogTitle) {
        super(gnome_icon_entry_new(historyId, dialogTitle));
    }

    /**
     * Construct a new IconEntry from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public IconEntry(Handle handle) {
        super(handle);
    }

    /**
     * Set the subdirectory in which to look for icons.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setSubdirectory(String subdir) {
        gnome_icon_entry_set_pixmap_subdir(getHandle(), subdir);
    }

    /**
     * Retrieve the filename of the icon selected
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getFilename() {
        return gnome_icon_entry_get_filename(getHandle());
    }

    /**
     * Set the filename of an icon.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setFilename(String filename) {
        gnome_icon_entry_set_filename(getHandle(), filename);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_icon_entry_get_type());
    }

    /**
     * Set the title for the Browse Dialog.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setBrowseDialogTitle(String browseDialogTitle) {
        gnome_icon_entry_set_browse_dialog_title(getHandle(), browseDialogTitle);
    }

    /**
     * Set the history id
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setHistoryId(String id) {
        gnome_icon_entry_set_history_id(getHandle(), id);
    }

    /**
     * Sets the maximum number of save entries in the browse dialog.
     * 
     * @param maxSaved
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setMaxSaved(int maxSaved) {
        gnome_icon_entry_set_max_saved(getHandle(), maxSaved);
    }

    // ////////////////////////////////////////////////////
    // Event handling
    // ////////////////////////////////////////////////////

    /**
     * Register an object to handle IconEntry events.
     * 
     * @see org.gnu.gnome.event.IconEntryListener
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void addListener(IconEntryListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(listeners, listener);
        if (i == -1) {
            if (null == listeners) {
                evtMap.initialize(this, IconEntryEvent.Type.BROWSE);
                evtMap.initialize(this, IconEntryEvent.Type.CHANGED);
                listeners = new Vector();
            }
            listeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(IconEntryListener)
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(IconEntryListener listener) {
        int i = findListener(listeners, listener);
        if (i > -1) {
            listeners.remove(i);
        }
        if (0 == listeners.size()) {
            evtMap.uninitialize(this, IconEntryEvent.Type.BROWSE);
            evtMap.uninitialize(this, IconEntryEvent.Type.CHANGED);
            listeners = null;
        }
    }

    protected void fireIconEntryEvent(IconEntryEvent event) {
        if (null == listeners) {
            return;
        }
        int size = listeners.size();
        int i = 0;
        while (i < size) {
            IconEntryListener lis = (IconEntryListener) listeners.elementAt(i);
            lis.iconEntryEvent(event);
            i++;
        }
    }

    private void handleChanged() {
        fireIconEntryEvent(new IconEntryEvent(this, IconEntryEvent.Type.CHANGED));
    }

    private void handleBrowse() {
        fireIconEntryEvent(new IconEntryEvent(this, IconEntryEvent.Type.BROWSE));
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    /**
     * Implementation method to build an EventMap for this widget class. Not
     * useful (or supported) for application use.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("changed", "handleChanged",
                IconEntryEvent.Type.CHANGED, IconEntryListener.class);
        anEvtMap.addEvent("browse", "handleBrowse", IconEntryEvent.Type.BROWSE,
                IconEntryListener.class);
    }

    native static final protected int gnome_icon_entry_get_type();

    native static final protected Handle gnome_icon_entry_new(String historyId,
            String browserDialogTitle);

    native static final protected void gnome_icon_entry_set_pixmap_subdir(
            Handle ientry, String subdir);

    native static final protected String gnome_icon_entry_get_filename(
            Handle ientry);

    native static final protected boolean gnome_icon_entry_set_filename(
            Handle ientry, String filename);

    native static final protected void gnome_icon_entry_set_browse_dialog_title(
            Handle ientry, String browseDialogTitle);

    native static final protected void gnome_icon_entry_set_history_id(
            Handle ientry, String historyId);

    native static final protected Handle gnome_icon_entry_pick_dialog(
            Handle ientry);

    native static final protected void gnome_icon_entry_set_max_saved(
            Handle ientry, int max);

}
