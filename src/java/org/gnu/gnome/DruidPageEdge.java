/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.gdk.Color;
import org.gnu.gdk.Pixbuf;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * A widget holding information about the overall look of the currently
 * displayed DruidPage.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.DruidPageEdge</code>.
 */
public class DruidPageEdge extends DruidPage {
    /**
     * Construct a new DruidPageEdge object
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public DruidPageEdge(EdgePosition position) {
        super(gnome_druid_page_edge_new(position.getValue()));
    }

    /**
     * Construct a new DruidPageEdge object
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public DruidPageEdge(EdgePosition position, boolean antialiased,
            String title, String text, Pixbuf logo, Pixbuf watermark,
            Pixbuf topWatermark) {
        super(gnome_druid_page_edge_new_with_vals(position.getValue(),
                antialiased, title, text, logo.getHandle(), watermark
                        .getHandle(), topWatermark.getHandle()));
    }

    /**
     * Set the background color for the page.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setBackgroundColor(Color color) {
        gnome_druid_page_edge_set_bg_color(getHandle(), color.getHandle());
    }

    /**
     * Set the color for the textbox.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setTextboxColor(Color color) {
        gnome_druid_page_edge_set_textbox_color(getHandle(), color.getHandle());
    }

    /**
     * Set the background color for the logo.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setLogoBackgroundColor(Color color) {
        gnome_druid_page_edge_set_logo_bg_color(getHandle(), color.getHandle());
    }

    /**
     * Set the title color.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setTitleColor(Color color) {
        gnome_druid_page_edge_set_title_color(getHandle(), color.getHandle());
    }

    /**
     * Set the text color.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setTextColor(Color color) {
        gnome_druid_page_edge_set_text_color(getHandle(), color.getHandle());
    }

    /**
     * Set the text to be displayed
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setText(String text) {
        gnome_druid_page_edge_set_text(getHandle(), text);
    }

    /**
     * Set the title to be displayed.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setTitle(String title) {
        gnome_druid_page_edge_set_title(getHandle(), title);
    }

    /**
     * Set the logo for the page.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setLogo(Pixbuf logo) {
        gnome_druid_page_edge_set_logo(getHandle(), logo.getHandle());
    }

    /**
     * Set the watermark for the page.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setWatermark(Pixbuf watermark) {
        gnome_druid_page_edge_set_watermark(getHandle(), watermark.getHandle());
    }

    /**
     * Set the top watermark for the page.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setTopWatermark(Pixbuf watermark) {
        gnome_druid_page_edge_set_top_watermark(getHandle(), watermark
                .getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_druid_page_edge_get_type());
    }

    native static final protected String getTitle(Handle cptr);

    native static final protected String getText(Handle cptr);

    native static final protected Handle getLogoImage(Handle cptr);

    native static final protected Handle getWatermarkImage(Handle cptr);

    native static final protected Handle getTopWatermarkImage(Handle cptr);

    native static final protected Handle getBackgroundColor(Handle cptr);

    native static final protected Handle getTextboxColor(Handle cptr);

    native static final protected Handle getLogoBackgroundColor(Handle cptr);

    native static final protected Handle getTitleColor(Handle cptr);

    native static final protected Handle getTextColor(Handle cptr);

    native static final protected int getPosition(Handle cptr);

    native static final protected int gnome_druid_page_edge_get_type();

    native static final protected Handle gnome_druid_page_edge_new(int position);

    native static final protected Handle gnome_druid_page_edge_new_with_vals(
            int position, boolean antialiased, String title, String text,
            Handle logo, Handle watermark, Handle topWatermark);

    native static final protected void gnome_druid_page_edge_set_bg_color(
            Handle dpe, Handle color);

    native static final protected void gnome_druid_page_edge_set_textbox_color(
            Handle dpe, Handle color);

    native static final protected void gnome_druid_page_edge_set_logo_bg_color(
            Handle dpe, Handle color);

    native static final protected void gnome_druid_page_edge_set_title_color(
            Handle dpe, Handle color);

    native static final protected void gnome_druid_page_edge_set_text_color(
            Handle dpe, Handle color);

    native static final protected void gnome_druid_page_edge_set_text(
            Handle dpe, String text);

    native static final protected void gnome_druid_page_edge_set_title(
            Handle dpe, String title);

    native static final protected void gnome_druid_page_edge_set_logo(
            Handle dpe, Handle logoImage);

    native static final protected void gnome_druid_page_edge_set_watermark(
            Handle dpe, Handle watermark);

    native static final protected void gnome_druid_page_edge_set_top_watermark(
            Handle dpe, Handle topWatermark);

}
