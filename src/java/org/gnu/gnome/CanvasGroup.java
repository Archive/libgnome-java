/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * Objects of this class are created and managed inside a GnomeCanvas widget.
 * Its purpose is to contain and control a collection of GnomeCanvasItem
 * objects, displayed by the GnomeCanvas. The only way to add a GnomeCanvasItem
 * to a GnomeCanvas is to add it to a GnomeCanvasGroup object. Because the
 * GnomeCanvasGroup is also a GnomeCanvasItem it can contain other
 * GnomeCanvasGroups. There is always one GnomeCanvasGroup in a GnomeCanvas.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.CanvasGroup</code>.
 */
public class CanvasGroup extends CanvasItem {

    /**
     * Constructs an instance of CanvasGroup from a native widget resource.
     * 
     * @param handle
     *            The handle to the native widget.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    CanvasGroup(Handle handle) {
        super(handle);
    }

    static CanvasGroup getCanvasGroup(Handle handle) {
        if (handle == null) {
            return null;
        }

        CanvasGroup obj = (CanvasGroup) GObject.getGObjectFromHandle(handle);

        if (obj == null) {
            obj = new CanvasGroup(handle);
        }

        return obj;
    }

    public CanvasItem[] getItems() {
        Handle[] hndls = get_item_list(getHandle());
        if (null == hndls)
            return null;
        CanvasItem[] values = new CanvasItem[hndls.length];
        for (int i = 0; i < hndls.length; i++) {
            values[i] = CanvasItem.getCanvasItem(hndls[i]);
        }
        return values;
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_canvas_group_get_type());
    }

    native static final protected int gnome_canvas_group_get_type();

    native static final protected Handle[] get_item_list(Handle group);

}
