/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Enum;

public class ClientState extends Enum {
    static final private int _IDLE = 0;

    static final public org.gnu.gnome.ClientState IDLE = new org.gnu.gnome.ClientState(
            _IDLE);

    static final private int _SAVING_PHASE_1 = 1;

    static final public org.gnu.gnome.ClientState SAVING_PHASE_1 = new org.gnu.gnome.ClientState(
            _SAVING_PHASE_1);

    static final private int _WAITING_FOR_PHASE_2 = 2;

    static final public org.gnu.gnome.ClientState WAITING_FOR_PHASE_2 = new org.gnu.gnome.ClientState(
            _WAITING_FOR_PHASE_2);

    static final private int _SAVING_PHASE_2 = 3;

    static final public org.gnu.gnome.ClientState SAVING_PHASE_2 = new org.gnu.gnome.ClientState(
            _SAVING_PHASE_2);

    static final private int _FROZEN = 4;

    static final public org.gnu.gnome.ClientState FROZEN = new org.gnu.gnome.ClientState(
            _FROZEN);

    static final private int _DISCONNECTED = 5;

    static final public org.gnu.gnome.ClientState DISCONNECTED = new org.gnu.gnome.ClientState(
            _DISCONNECTED);

    static final private int _REGISTERING = 6;

    static final public org.gnu.gnome.ClientState REGISTERING = new org.gnu.gnome.ClientState(
            _REGISTERING);

    static final private org.gnu.gnome.ClientState[] theInterned = new org.gnu.gnome.ClientState[] {
            IDLE, SAVING_PHASE_1, WAITING_FOR_PHASE_2, SAVING_PHASE_2, FROZEN,
            DISCONNECTED, REGISTERING }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.ClientState theSacrificialOne = new org.gnu.gnome.ClientState(
            0);

    static public org.gnu.gnome.ClientState intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.ClientState already = (org.gnu.gnome.ClientState) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.ClientState(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ClientState(int value) {
        value_ = value;
    }

    public org.gnu.gnome.ClientState or(org.gnu.gnome.ClientState other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.ClientState and(org.gnu.gnome.ClientState other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.ClientState xor(org.gnu.gnome.ClientState other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.ClientState other) {
        return (value_ & other.value_) == other.value_;
    }

}
