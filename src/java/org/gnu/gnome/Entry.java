/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Type;
import org.gnu.gtk.Combo;
import org.gnu.glib.Handle;

/**
 * The Entry widget accepts user input - once accepted, the entry is included in
 * the history of items previously entered. You can save this history
 * information so it can be restored the next time the widget appears.
 * 
 * @deprecated
 * @see org.gnu.gtk.Entry
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.Entry</code>.
 */
public class Entry extends Combo {

    // reference to the gtk entry widget
    private org.gnu.gtk.Entry myEntry = null;

    /**
     * Constructs an Entry object referencing an existing gnome Entry native
     * resource.
     * 
     * @param handle
     *            The handle to the gnome Entry widget.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Entry(Handle handle) {
        super(handle);
    }

    /**
     * Constructs a new Entry object.
     * 
     * @param historyID
     *            The unique Id used by gnome to save and load history list
     *            info.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Entry(String historyID) {
        super(gnome_entry_new(historyID));
    }

    /**
     * Add a string to the end of the list.
     * 
     * @param text
     *            The string to append.
     * @param save
     *            If true, the list will be saved to history after the append.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void appendHistory(String text, boolean save) {
        gnome_entry_append_history(getHandle(), save, text);
    }

    /**
     * Clear the history list.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void clearHistory() {
        gnome_entry_clear_history(getHandle());
    }

    /**
     * Gets the unique Id used by gnome to save and load history info.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getHistoryID() {
        return gnome_entry_get_history_id(getHandle());
    }

    // The include file doesn't really say what max_saved does.
    public int getMaxSaved() {
        return gnome_entry_get_max_saved(getHandle());
    }

    /**
     * Add a string to the beginning of the list.
     * 
     * @param text
     *            The string to prepend.
     * @param save
     *            If true, the list will be saved to history after the prepend.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void prependHistory(String text, boolean save) {
        gnome_entry_prepend_history(getHandle(), save, text);
    }

    /**
     * Sets the unique Id used by gnome to save and load history info. This is
     * usefull if you want to save and load history lists interactively. No no
     * need to create a new control with the appropriate historyID, just change
     * it.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setHistoryID(String historyID) {
        gnome_entry_set_history_id(getHandle(), historyID);
    }

    // The include file doesn't really say what max_saved does.
    public void setMaxSaved(int maxSaved) {
        gnome_entry_set_max_saved(getHandle(), maxSaved);
    }

    /**
     * Return the Gtk Entry for this widget.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public org.gnu.gtk.Entry getEntry() {
        if (null == myEntry) {
            myEntry = new org.gnu.gtk.Entry(gnome_entry_gtk_entry(getHandle()));
        }
        return myEntry;
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_entry_get_type());
    }

    native static final protected int gnome_entry_get_type();

    native static final protected Handle gnome_entry_new(String historyId);

    native static final protected Handle gnome_entry_gtk_entry(Handle gentry);

    native static final protected void gnome_entry_set_history_id(
            Handle gentry, String historyId);

    native static final protected String gnome_entry_get_history_id(
            Handle gentry);

    native static final protected void gnome_entry_set_max_saved(Handle gentry,
            int maxSaved);

    native static final protected int gnome_entry_get_max_saved(Handle gentry);

    native static final protected void gnome_entry_prepend_history(
            Handle gentry, boolean save, String text);

    native static final protected void gnome_entry_append_history(
            Handle gentry, boolean save, String text);

    native static final protected void gnome_entry_clear_history(Handle gentry);

}
