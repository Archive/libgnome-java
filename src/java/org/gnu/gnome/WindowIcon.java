/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.gtk.Window;
import org.gnu.glib.Handle;
import org.gnu.glib.Struct;

/**
 * @deprecated
 * @see org.gnu.gtk.Window
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.WindowIcon</code>.
 */
public class WindowIcon extends Struct {
    // no instantiations
    private WindowIcon() {
    }

    /**
     * Set an icon for a window.
     * 
     * @param window
     *            The window to set the icon.
     * @param filename
     *            The filename of the icon
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static void setIcon(Window window, String filename) {
        gnome_window_icon_set_from_file(window.getHandle(), filename);
    }

    /**
     * Sets the default icon to be used in all windows of the application
     * 
     * @param filename
     *            The filename of the icon.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static void setDefaultIcon(String filename) {
        gnome_window_icon_set_default_from_file(filename);
    }

    native static final protected void gnome_window_icon_set_from_default(
            Handle window);

    native static final protected void gnome_window_icon_set_from_file(
            Handle window, String filename);

    native static final protected void gnome_window_icon_set_from_file_list(
            Handle window, String[] filenames);

    native static final protected void gnome_window_icon_set_default_from_file(
            String filename);

    native static final protected void gnome_window_icon_set_default_from_file_list(
            String[] filenames);

    native static final protected void gnome_window_icon_init();

}
