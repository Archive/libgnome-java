/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Enum;

public class FileDomain extends Enum {
    static final private int _UNKNOWN = 0;

    static final public org.gnu.gnome.FileDomain UNKNOWN = new org.gnu.gnome.FileDomain(
            _UNKNOWN);

    static final private int _LIBDIR = 1;

    static final public org.gnu.gnome.FileDomain LIBDIR = new org.gnu.gnome.FileDomain(
            _LIBDIR);

    static final private int _DATADIR = 2;

    static final public org.gnu.gnome.FileDomain DATADIR = new org.gnu.gnome.FileDomain(
            _DATADIR);

    static final private int _SOUND = 3;

    static final public org.gnu.gnome.FileDomain SOUND = new org.gnu.gnome.FileDomain(
            _SOUND);

    static final private int _PIXMAP = 4;

    static final public org.gnu.gnome.FileDomain PIXMAP = new org.gnu.gnome.FileDomain(
            _PIXMAP);

    static final private int _CONFIG = 5;

    static final public org.gnu.gnome.FileDomain CONFIG = new org.gnu.gnome.FileDomain(
            _CONFIG);

    static final private int _HELP = 6;

    static final public org.gnu.gnome.FileDomain HELP = new org.gnu.gnome.FileDomain(
            _HELP);

    static final private int _APP_LIBDIR = 7;

    static final public org.gnu.gnome.FileDomain APP_LIBDIR = new org.gnu.gnome.FileDomain(
            _APP_LIBDIR);

    static final private int _APP_DATADIR = 8;

    static final public org.gnu.gnome.FileDomain APP_DATADIR = new org.gnu.gnome.FileDomain(
            _APP_DATADIR);

    static final private int _APP_SOUND = 9;

    static final public org.gnu.gnome.FileDomain APP_SOUND = new org.gnu.gnome.FileDomain(
            _APP_SOUND);

    static final private int _APP_PIXMAP = 10;

    static final public org.gnu.gnome.FileDomain APP_PIXMAP = new org.gnu.gnome.FileDomain(
            _APP_PIXMAP);

    static final private int _APP_CONFIG = 11;

    static final public org.gnu.gnome.FileDomain APP_CONFIG = new org.gnu.gnome.FileDomain(
            _APP_CONFIG);

    static final private int _APP_HELP = 12;

    static final public org.gnu.gnome.FileDomain APP_HELP = new org.gnu.gnome.FileDomain(
            _APP_HELP);

    static final private org.gnu.gnome.FileDomain[] theInterned = new org.gnu.gnome.FileDomain[] {
            UNKNOWN, LIBDIR, DATADIR, SOUND, PIXMAP, CONFIG, HELP, APP_LIBDIR,
            APP_DATADIR, APP_SOUND, APP_PIXMAP, APP_CONFIG, APP_HELP }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.FileDomain theSacrificialOne = new org.gnu.gnome.FileDomain(
            0);

    static public org.gnu.gnome.FileDomain intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.FileDomain already = (org.gnu.gnome.FileDomain) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.FileDomain(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private FileDomain(int value) {
        value_ = value;
    }

    public org.gnu.gnome.FileDomain or(org.gnu.gnome.FileDomain other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.FileDomain and(org.gnu.gnome.FileDomain other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.FileDomain xor(org.gnu.gnome.FileDomain other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.FileDomain other) {
        return (value_ & other.value_) == other.value_;
    }

}
