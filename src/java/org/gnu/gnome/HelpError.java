/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Enum;

public class HelpError extends Enum {
    static final private int _INTERNAL = 0;

    static final public org.gnu.gnome.HelpError INTERNAL = new org.gnu.gnome.HelpError(
            _INTERNAL);

    static final private int _NOT_FOUND = 1;

    static final public org.gnu.gnome.HelpError NOT_FOUND = new org.gnu.gnome.HelpError(
            _NOT_FOUND);

    static final private org.gnu.gnome.HelpError[] theInterned = new org.gnu.gnome.HelpError[] {
            INTERNAL, NOT_FOUND };

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.HelpError theSacrificialOne = new org.gnu.gnome.HelpError(
            0);

    static public org.gnu.gnome.HelpError intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.HelpError already = (org.gnu.gnome.HelpError) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.HelpError(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private HelpError(int value) {
        value_ = value;
    }

    public org.gnu.gnome.HelpError or(org.gnu.gnome.HelpError other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.HelpError and(org.gnu.gnome.HelpError other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.HelpError xor(org.gnu.gnome.HelpError other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.HelpError other) {
        return (value_ & other.value_) == other.value_;
    }

}
