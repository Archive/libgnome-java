/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.gtk.Button;
import org.gnu.glib.Handle;

/**
 * The HRef widget displays a frameless button that, when selected, issues a
 * command for the web browser to connect to a web site. The actual command to
 * the browser is defined globally as the default member of URL Handler list in
 * the GNOME Control Center.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.HRef</code>.
 */
public class HRef extends Button {

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static HRef getHRef(Handle handle) {
        if (handle == null) {
            return null;
        }

        HRef href = (HRef) GObject.getGObjectFromHandle(handle);
        if (href == null) {
            href = new HRef(handle);
        }
        return href;
    }

    /**
     * Creates a new HRef with the given URL and label.
     * 
     * @param url
     *            The URL to show in the web browser when the HRef is clicked.
     * @param lable
     *            The caption for the HRef.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public HRef(String url, String lable) {
        super(gnome_href_new(url, lable));
    }

    /**
     * Construct a HRef using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public HRef(Handle handle) {
        super(handle);
    }

    /**
     * Returns the caption for the HRef.
     * 
     * @return The text displayed by the HRef.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getText() {
        return new String(gnome_href_get_text(getHandle()));
    }

    /**
     * Returns the URL referenced by the HRef.
     * 
     * @return The url to show when the HRef is clicked.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getURL() {
        return new String(gnome_href_get_url(getHandle()));
    }

    /**
     * Returns the caption for the HRef.
     * 
     * @param text
     *            The text displayed by the HRef.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setText(String text) {
        gnome_href_set_text(getHandle(), text);
    }

    /**
     * Returns the URL referenced by the HRef.
     * 
     * @param url
     *            The url to show when the HRef is clicked.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setURL(String url) {
        gnome_href_set_url(getHandle(), url);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_href_get_type());
    }

    native static final protected int gnome_href_get_type();

    native static final protected Handle gnome_href_new(String url, String lable);

    native static final protected void gnome_href_set_url(Handle href,
            String url);

    native static final protected String gnome_href_get_url(Handle href);

    native static final protected void gnome_href_set_text(Handle href,
            String text);

    native static final protected String gnome_href_get_text(Handle label);

}
