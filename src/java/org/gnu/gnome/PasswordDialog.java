/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.gtk.Dialog;
import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.PasswordDialog</code>.
 */
public class PasswordDialog extends Dialog {

    /**
     * Construct a new PasswordDialog widget
     * 
     * @param title
     *            The title for the dialog.
     * @param message
     *            A message that is displayed on the dialog.
     * @param username
     *            The username to set in the username entry widget.
     * @param password
     *            The password to set in the password entry widget.
     * @param readonlyUsername
     *            Is the username editable?
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public PasswordDialog(String title, String message, String username,
            String password, boolean readonlyUsername) {
        super(gnome_password_dialog_new(title, message, username, password,
                readonlyUsername));
    }

    public boolean runAndBlock() {
        checkState();
        return gnome_password_dialog_run_and_block(getHandle());
    }

    public void setUsername(String username) {
        checkState();
        gnome_password_dialog_set_username(getHandle(), username);
    }

    public String getUsername() {
        checkState();
        return gnome_password_dialog_get_username(getHandle());
    }

    public void setPassword(String password) {
        checkState();
        gnome_password_dialog_set_password(getHandle(), password);
    }

    public String getPassword() {
        checkState();
        return gnome_password_dialog_get_password(getHandle());
    }

    public void setReadonlyUsername(boolean readonly) {
        checkState();
        gnome_password_dialog_set_readonly_username(getHandle(), readonly);
    }

    public void setRemember(boolean remember) {
        checkState();
        gnome_password_dialog_set_remember(getHandle(), remember);
    }

    public boolean getRemember() {
        checkState();
        return gnome_password_dialog_get_remember(getHandle());
    }

    public void setShowUserpassButtons(boolean show) {
        checkState();
        gnome_password_dialog_set_show_userpass_buttons(getHandle(), show);
    }

    native static final protected int gnome_password_dialog_get_type();

    native static final protected Handle gnome_password_dialog_new(
            String title, String message, String username, String password,
            boolean readonlyUsername);

    native static final protected boolean gnome_password_dialog_run_and_block(
            Handle dialog);

    native static final protected void gnome_password_dialog_set_username(
            Handle dialog, String username);

    native static final protected void gnome_password_dialog_set_password(
            Handle dialog, String password);

    native static final protected void gnome_password_dialog_set_readonly_username(
            Handle dialog, boolean readonly);

    native static final protected void gnome_password_dialog_set_remember(
            Handle dialog, boolean remember);

    native static final protected void gnome_password_dialog_set_show_userpass_buttons(
            Handle dialog, boolean show);

    native static final protected String gnome_password_dialog_get_username(
            Handle dialog);

    native static final protected String gnome_password_dialog_get_password(
            Handle dialog);

    native static final protected boolean gnome_password_dialog_get_remember(
            Handle dialog);

}
