/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;
import org.gnu.glib.Type;
import org.gnu.gtk.AccelGroup;
import org.gnu.gtk.Container;
import org.gnu.gtk.MenuBar;
import org.gnu.gtk.MenuShell;
import org.gnu.gtk.StatusBar;
import org.gnu.gtk.ToolBar;
import org.gnu.gtk.VBox;
import org.gnu.gtk.Widget;
import org.gnu.gtk.Window;

/**
 * The App widget is the main window of a GNOME application. It is a container
 * widget that can hold a single child widget. It also includes facilities for
 * attaching menus, toolbars, a status bar, and widgets that can be docked.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.App</code>.
 */

public class App extends Window {

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static App getApp(Handle handle) {
        if (handle == null)
            return null;
        
        App app = (App) getGObjectFromHandle(handle);
        if (app == null) {
            app = new App(handle);
        }
        return app;
    }

    /**
     * Instantiates a new Application window with the indicated name and title.
     * 
     * @param appname
     *            The name of the application.
     * @param title
     *            The title of the application window. The title can be null, in
     *            which case the window's title will not be set.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public App(String appname, String title) {
        super(gnome_app_new(appname, title == null ? null : title));
    }

    public App(Handle handle) {
        super(handle);
    }

    /**
     * Returns the dock.
     * 
     * @return The dock.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Widget getDock() {
        checkState();
        Handle hndl = getDock(getHandle());
        return Widget.getWidget(hndl);
    }

    /**
     * Main accelerator group for this window (hotkeys live here).
     * 
     * @return The accelorator group.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public AccelGroup getAccelGroup() {
        checkState();
        return AccelGroup.getAccelGroup(getAccelGroup(getHandle()));
    }

    /**
     * Returns the menu bar.
     * 
     * @return The MenuBar.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public MenuBar getMenuBar() {
        checkState();
        Handle hndl = getMenubar(getHandle());
        return MenuBar.getMenuBar(hndl);
    }

    /**
     * Returns the contents of the App.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Widget getContents() {
        checkState();
        Handle hndl = getContents(getHandle());
        return Widget.getWidget(hndl);
    }

    /**
     * Gets the name of the application.
     * 
     * @return The name of the application.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getName() {
        checkState();
        return getName(getHandle());
    }

    /**
     * Gets the prefix for gnome-config, which is used to save the layout.
     * 
     * @return The prefix for gnome-config.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getPrefix() {
        checkState();
        return getPrefix(getHandle());
    }

    /**
     * Gets the status bar of the application windows.
     * 
     * @return The status bar.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public StatusBar getStatusBar() {
        checkState();
        Handle hndl = getStatusbar(getHandle());
        return StatusBar.getStatusBar(hndl);
    }

    /**
     * <I>From gnome-app.h</I>The vbox widget that ties them.
     * 
     * @return The VBox.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public VBox getVBox() {
        checkState();
        Handle hndl = getVbox(getHandle());
        return VBox.getVBox(hndl);
    }

    /**
     * If true, the application uses gnome-config to retrieve and save the
     * docking configuration automatically.
     * 
     * @return True if the docking configuration is to be handled &
     *         automatically, else false.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public boolean layoutConfig() {
        checkState();
        return getEnableLayoutConfig(getHandle());
    }

    /**
     * If true, the application uses gnome-config to retrieve and save the
     * docking configuration automatically.
     * 
     * @param enabled
     *            True if the docking configuration is to be handled
     *            automatically, else false.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void layoutConfig(boolean enabled) {
        checkState();
        gnome_app_enable_layout_config(getHandle(), enabled);
    }

    /**
     * Sets the content area of the application window.
     * 
     * @param contents
     *            The widget that contains the content of the window.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setContent(Widget contents) {
        checkState();
        gnome_app_set_contents(getHandle(), contents.getHandle());
    }

    /**
     * Sets the menu bar of the window.
     * 
     * @param menuBar
     *            The menu bar to be used for the window.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setMenuBar(MenuBar menuBar) {
        checkState();
        gnome_app_set_menus(getHandle(), menuBar.getHandle());
    }

    /**
     * Construct a menu bar and attach it to the specified application window.
     * 
     * @param uiinfos
     *            An array of UIInfo objects that define the menu.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void createMenus(UIInfo[] uiinfos) {
        checkState();
        if (uiinfos.length < 1)
            return;
        Handle[] hndls = new Handle[uiinfos.length];
        for (int i = 0; i < uiinfos.length; i++)
            hndls[i] = uiinfos[i].getHandle();
        gnome_app_create_menus(getHandle(), hndls);
    }

    /**
     * Removes <i>num</i> items from the existing app's menu structure
     * beginning with the item described by <i>path</i>.
     * 
     * @param path
     *            The path to first item to remove.
     * @param num
     *            The number of items to remove.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void removeMenus(String path, int num) {
        checkState();
        gnome_app_remove_menus(getHandle(), path, num);
    }

    /**
     * Inserts a menu in the existing app's menu structure right after the item
     * described by <i>path</i>.
     * 
     * @param path
     *            The path to the item that preceeds the insertion.
     * @param uiinfos
     *            An array of UIInfo objects that describe the menu to be
     *            inserted.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void insertMenu(String path, UIInfo[] uiinfos) {
        checkState();
        if (uiinfos.length < 1)
            return;
        Handle[] hndls = new Handle[uiinfos.length];
        for (int i = 0; i < uiinfos.length; i++)
            hndls[i] = uiinfos[i].getHandle();
        gnome_app_insert_menus(getHandle(), path, hndls);
    }

    /**
     * Sets the tool bar of the window.
     * 
     * @param toolBar
     *            The tool bar to be used for the window.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setToolBar(ToolBar toolBar) {
        checkState();
        gnome_app_set_toolbar(getHandle(), toolBar.getHandle());
    }

    /**
     * Construct a toolbar and attach it to the specified application window.
     * 
     * @param uiinfos
     *            An array of UIInfo objects that define the toolbar.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void createToolBar(UIInfo[] uiinfos) {
        checkState();
        if (uiinfos.length < 1)
            return;
        Handle[] hndls = new Handle[uiinfos.length];
        for (int i = 0; i < uiinfos.length; i++)
            hndls[i] = uiinfos[i].getHandle();
        gnome_app_create_toolbar(getHandle(), hndls);
    }

    /**
     * Sets the status bar of the application window.
     * 
     * @param appBar
     *            The app bar to use for the window.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setStatusBar(AppBar appBar) {
        checkState();
        gnome_app_set_statusbar(getHandle(), appBar.getHandle());
    }

    /**
     * Sets the status bar of the application window.
     * 
     * @param statusBar
     *            The status bar to use for the window.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setStatusBar(StatusBar statusBar) {
        checkState();
        gnome_app_set_statusbar(getHandle(), statusBar.getHandle());
    }

    /**
     * Sets the status bar of the application window, but uses the given
     * container widget rather than creating a new one.
     * 
     * @param appBar
     *            The app bar to use for the window.
     * @param container
     *            The container for the status bar.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setStatusBar(AppBar appBar, Container container) {
        checkState();
        gnome_app_set_statusbar_custom(getHandle(), container.getHandle(),
                appBar.getHandle());
    }

    /**
     * Sets the status bar of the application window, but uses the given
     * container widget rather than creating a new one.
     * 
     * @param statusBar
     *            The status bar to use for the window.
     * @param container
     *            The container for the status bar.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setStatusBar(StatusBar statusBar, Container container) {
        checkState();
        gnome_app_set_statusbar_custom(getHandle(), container.getHandle(),
                statusBar.getHandle());
    }

    /**
     * Activate the menu item hints, displaying in an appbar.
     * 
     * @param appBar
     *            The AppBar to install the hints.
     * @param uiinfos
     *            An array of UIInfo objects that contain the menu items for
     *            which the hints will be created.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void installAppBarMenuHints(AppBar appBar, UIInfo[] uiinfos) {
        checkState();
        Handle[] hndls = new Handle[uiinfos.length];
        for (int i = 0; i < uiinfos.length; i++) {
            hndls[i] = uiinfos[i].getHandle();
        }
        gnome_app_install_appbar_menu_hints(appBar.getHandle(), hndls);
    }

    /**
     * Activate the menu item hints, displaying in a statusbar.
     * 
     * @param statusBar
     *            The StatusBar to install the hints.
     * @param uiinfos
     *            An array of UIInfo objects that contain the menu items for
     *            which the hints will be created.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void installStatusBarMenuHints(StatusBar statusBar, UIInfo[] uiinfos) {
        checkState();
        Handle[] hndls = new Handle[uiinfos.length];
        for (int i = 0; i < uiinfos.length; i++) {
            hndls[i] = uiinfos[i].getHandle();
        }
        gnome_app_install_statusbar_menu_hints(statusBar.getHandle(), hndls);
    }

    /**
     * Activate the menu item hings, displaying in the statusbar or appbar.
     * 
     * @param uiinfos
     *            An array of UIInfo objects that contain the menu items for
     *            which the hints will be created.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void installMenuHints(UIInfo[] uiinfos) {
        checkState();
        Handle[] hndls = new Handle[uiinfos.length];
        for (int i = 0; i < uiinfos.length; i++) {
            hndls[i] = uiinfos[i].getHandle();
        }
        gnome_app_install_menu_hints(getHandle(), hndls);
    }

    /**
     * Fills the specified MenuShell with items created from the UIInfo array,
     * inserting them from the item number <i>position</i> on. The AccelGroup
     * will be used for all newly created sub menus and servers as the global
     * AccelGroup for all menu item hotkeys.
     * 
     * @param menuShell
     * @param uiinfos
     * @param accelGroup
     * @param underline
     * @param position
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void fillMenu(MenuShell menuShell, UIInfo[] uiinfos,
            AccelGroup accelGroup, boolean underline, int position) {
        checkState();
        Handle[] hndls = new Handle[uiinfos.length];
        for (int i = 0; i < uiinfos.length; i++)
            hndls[i] = uiinfos[i].getHandle();
        gnome_app_fill_menu(menuShell.getHandle(), hndls, accelGroup
                .getHandle(), underline, position);
    }

    /**
     * Fills a ToolBar with the specified UIInfos.
     * 
     * @param toolbar
     * @param uiinfos
     * @param accelGroup
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void fillToolbar(ToolBar toolbar, UIInfo[] uiinfos,
            AccelGroup accelGroup) {
        checkState();
        Handle[] hndls = new Handle[uiinfos.length];
        for (int i = 0; i < uiinfos.length; i++)
            hndls[i] = uiinfos[i].getHandle();
        gnome_app_fill_toolbar(toolbar.getHandle(), hndls, accelGroup
                .getHandle());
    }

    /**
     * Removes items from the Menu.
     * 
     * @param path
     * @param start
     * @param numItems
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void removeMenuRange(String path, int start, int numItems) {
        checkState();
        gnome_app_remove_menu_range(getHandle(), path, start, numItems);
    }

    /**
     * Display a simple message in an OK dialog or the status bar. Requires
     * confirmation from the user before it goes away.
     * 
     * @param message
     *            The text to display.
     * @return The dialog created or null.
     * @deprecated
     * @see org.gnu.gtk.MessageDialog
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Widget message(String message) {
        Handle hndl = gnome_app_message(getHandle(), message);
        if (null == hndl)
            return null;
        GObject obj = getGObjectFromHandle(hndl);
        if (null != obj)
            return (Widget) obj;
        return new Widget(hndl);
    }

    /**
     * Flash a message in the status bar for a few moments. If this app doesn't
     * have a status bar this method will do nothing.
     * 
     * @param flash
     *            The message to flash.
     * @deprecated
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void flash(String flash) {
        gnome_app_flash(getHandle(), flash);
    }

    /**
     * Display a not-so-important error message in an OK dialog or the status
     * bar.
     * 
     * @param warning
     *            The text to display.
     * @return The dialog created or null.
     * @deprecated
     * @see org.gnu.gtk.MessageDialog
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Widget warning(String warning) {
        Handle hndl = gnome_app_warning(getHandle(), warning);
        if (null == hndl)
            return null;
        GObject obj = getGObjectFromHandle(hndl);
        if (null != obj)
            return (Widget) obj;
        return new Widget(hndl);
    }

    /**
     * Display an important error message in an OK dialog or the status bar.
     * 
     * @param error
     *            The text to display.
     * @return The dialog created or null.
     * @deprecated
     * @see org.gnu.gtk.MessageDialog
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Widget error(String error) {
        Handle hndl = gnome_app_error(getHandle(), error);
        if (null == hndl)
            return null;
        GObject obj = getGObjectFromHandle(hndl);
        if (null != obj)
            return (Widget) obj;
        return new Widget(hndl);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_app_get_type());
    }

    native static final protected String getName(Handle cptr);

    native static final protected String getPrefix(Handle cptr);

    native static final protected Handle getDock(Handle cptr);

    native static final protected Handle getStatusbar(Handle cptr);

    native static final protected Handle getVbox(Handle cptr);

    native static final protected Handle getMenubar(Handle cptr);

    native static final protected Handle getContents(Handle cptr);

    native static final protected Handle getAccelGroup(Handle cptr);

    native static final protected boolean getEnableLayoutConfig(Handle cptr);

    native static final protected int gnome_app_get_type();

    native static final protected Handle gnome_app_new(String appname,
            String title);

    native static final protected void gnome_app_set_menus(Handle app,
            Handle menubar);

    native static final protected void gnome_app_set_toolbar(Handle app,
            Handle toolbar);

    native static final protected void gnome_app_set_statusbar(Handle app,
            Handle statusbar);

    native static final protected void gnome_app_set_statusbar_custom(
            Handle app, Handle container, Handle statusbar);

    native static final protected void gnome_app_set_contents(Handle app,
            Handle contents);

    native static final protected void gnome_app_enable_layout_config(
            Handle app, boolean enable);

    native static final protected void gnome_app_fill_menu(Handle menuShell,
            Handle[] uiInfo, Handle accelGroup, boolean ulineAccels, int pos);

    native static final protected void gnome_app_fill_menu_with_data(
            Handle menuShell, Handle[] uiInfo, Handle accelGroup,
            boolean ulineAccels, int pos, Object userData);

    native static final protected void gnome_app_fill_menu_custom(
            Handle menuShell, Handle[] uiInfo, Handle uiBData,
            Handle accelGroup, boolean ulineAccels, int pos);

    native static final protected void gnome_app_ui_configure_configurable(
            Handle uiinfo);

    native static final protected void gnome_app_create_menus(Handle app,
            Handle[] uiinfo);

    native static final protected void gnome_app_create_menus_custom(
            Handle app, Handle[] uiinfo, Handle uibdate);

    native static final protected void gnome_app_create_toolbar(Handle app,
            Handle[] uiinfo);

    native static final protected void gnome_app_create_toolbar_custom(
            Handle app, Handle[] uiinfo, Handle uibdata);

    native static final protected void gnome_app_fill_toolbar(Handle toolbar,
            Handle[] uiInfo, Handle accelGroup);

    native static final protected void gnome_app_fill_toolbar_with_data(
            Handle toolbar, Handle[] uiInfo, Handle accelGroup, Object userData);

    native static final protected void gnome_app_fill_toolbar_custom(
            Handle toolbar, Handle[] uiInfo, Handle uiBData, Handle accelGroup);

    native static final protected Handle gnome_app_find_menu_pos(Handle parent,
            String path, int pos);

    native static final protected void gnome_app_remove_menus(Handle app,
            String path, int items);

    native static final protected void gnome_app_remove_menu_range(Handle app,
            String path, int start, int items);

    native static final protected void gnome_app_insert_menus_custom(
            Handle app, String path, Handle menuinfo, Handle uibdata);

    native static final protected void gnome_app_insert_menus(Handle app,
            String path, Handle[] menuinfo);

    native static final protected void gnome_app_install_appbar_menu_hints(
            Handle appbar, Handle[] uiinfo);

    native static final protected void gnome_app_install_statusbar_menu_hints(
            Handle bar, Handle[] uiinfo);

    native static final protected void gnome_app_install_menu_hints(Handle app,
            Handle[] uiinfo);

    native static final protected Handle gnome_app_message(Handle app,
            String message);

    native static final protected void gnome_app_flash(Handle app, String flash);

    native static final protected Handle gnome_app_error(Handle app,
            String error);

    native static final protected Handle gnome_app_warning(Handle app,
            String warning);

}
