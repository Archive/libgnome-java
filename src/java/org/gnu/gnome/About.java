/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.gdk.Pixbuf;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The About widget dialog window is designed to pop up from the About button on
 * an application menu. Its purpose is to standardize the form and content.
 * 
 * @deprecated
 * @see org.gnu.gtk.AboutDialog
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.About</code>.
 */
public class About extends org.gnu.gtk.Dialog {

    /**
     * Constructor to create a new About dialog.
     * 
     * @param title
     *            The title of the application.
     * @param version
     *            The version of the application.
     * @param license
     *            The license.
     * @param comments
     *            Miscellaneous comments about the application.
     * @param authors
     *            The author(s) of the application.
     * @param documenters
     *            The documents(s) of the application.
     * @param translator
     *            The translator.
     * @param pixbuf
     *            A pixmap to display on the about dialiag.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public About(String title, String version, String license, String comments,
            String authors[], String documenters[], String translator,
            Pixbuf pixbuf) {
        super(init(title, version, license, comments, authors, documenters,
                translator, pixbuf));
    }

    private static Handle init(String title, String version, String license,
            String comments, String authors[], String documenters[],
            String translator, Pixbuf pixbuf) {
        Handle pixbufHandle;
        if (pixbuf == null) {
            pixbufHandle = null;
        } else {
            pixbufHandle = pixbuf.getHandle();
        }
        return gnome_about_new(title, version, license, comments, authors,
                documenters, translator, pixbufHandle);
    }

    /**
     * Construct an About box using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public About(Handle handle) {
        super(handle);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_about_get_type());
    }

    native static final protected int gnome_about_get_type();

    native static final protected Handle gnome_about_new(String title,
            String version, String copyright, String comments,
            String[] authors, String[] documenters, String translatorCredits,
            Handle logo);

}
