/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.GObject;
import org.gnu.gtk.AccelGroup;
import org.gnu.gtk.Menu;
import org.gnu.gtk.Widget;
import org.gnu.glib.Handle;

/**
 * @deprecated
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.PopupMenu</code>.
 */
public class PopupMenu extends Menu {

    /**
     * Construct a new PopupMenu object.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public PopupMenu(UIInfo[] uiinfos) {
        super(init1(uiinfos));
    }

    private static Handle init1(UIInfo[] uiinfos) {
        Handle[] hndls = new Handle[uiinfos.length];
        for (int i = 0; i < uiinfos.length; i++) {
            hndls[i] = uiinfos[i].getHandle();
        }
        return gnome_popup_menu_new(hndls);
    }

    /**
     * Construct a new PopupMenu object.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public PopupMenu(UIInfo[] uiinfos, AccelGroup accelGroup) {
        super(init2(uiinfos, accelGroup));
    }

    private static Handle init2(UIInfo[] uiinfos, AccelGroup accelGroup) {
        Handle[] hndls = new Handle[uiinfos.length];
        for (int i = 0; i < uiinfos.length; i++) {
            hndls[i] = uiinfos[i].getHandle();
        }
        return gnome_popup_menu_new_with_accelgroup(hndls, accelGroup
                .getHandle());
    }

    /**
     * Return the AccelGroup associated with this PopupMenu.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public AccelGroup getAccelGroup() {
        Handle handle = gnome_popup_menu_get_accel_group(getHandle());
        return AccelGroup.getAccelGroup(handle);
    }

    /**
     * Attach this menu to a Widget.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void attach(Widget widget) {
        gnome_popup_menu_attach(getHandle(), widget.getHandle(), null);
    }

    /**
     * Append new menu items to the PopupMenu.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void append(UIInfo[] uiinfos) {
        Handle[] hndls = new Handle[uiinfos.length];
        for (int i = 0; i < uiinfos.length; i++) {
            hndls[i] = uiinfos[i].getHandle();
        }
        gnome_popup_menu_append(getHandle(), hndls);
    }

    native static final protected Handle gnome_popup_menu_new(Handle[] uiinfo);

    native static final protected Handle gnome_popup_menu_new_with_accelgroup(
            Handle[] uiinfo, Handle accelgroup);

    native static final protected Handle gnome_popup_menu_get_accel_group(
            Handle menu);

    native static final protected void gnome_popup_menu_attach(Handle menu,
            Handle widget, Object userData);

    native static final protected void gnome_popup_menu_append(Handle menu,
            Handle[] uiinfo);

}
