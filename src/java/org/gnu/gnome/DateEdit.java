/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import java.util.Date;
import java.util.Vector;

import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.glib.GObject;
import org.gnu.glib.Handle;
import org.gnu.glib.Type;
import org.gnu.gnome.event.DateEditEvent;
import org.gnu.gnome.event.DateEditListener;
import org.gnu.gtk.HBox;

/**
 * A control which allows the user to select a date.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.DateEdit</code>.
 */
public class DateEdit extends HBox {
    /**
     * Listeners for handling DateEdit events
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private Vector dateEditListeners = null;

    /**
     * Creates a new DateEdit control initialized to the given date.
     * 
     * @param date
     *            The date to which the control should be initilized.
     * @param timeDisplay
     *            True if the time should be shown. False if the date only
     *            should be shown.
     * @param use24fmt
     *            True if the control should display time in the 24 hour format.
     *            False if the 24 hour format should not be used.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public DateEdit(java.util.Date date, boolean timeDisplay, boolean use24fmt) {
        super(gnome_date_edit_new(date.getTime() / 1000, timeDisplay, use24fmt));
    }

    /**
     * Construct a new DateEdit from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public DateEdit(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static DateEdit getDateEdit(Handle handle) {
        if (handle == null)
            return null;

        DateEdit obj = (DateEdit) GObject.getGObjectFromHandle(handle);

        if (obj == null)
            obj = new DateEdit(handle);

        return obj;
    }

    /**
     * Returns true if the control shows the time in the 24 hour format.
     * 
     * @return True if time is being shown in the 24 hour format. Flase if the
     *         time is being shown in the 12 hour format.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public boolean get24HourFormat() {
        int flags = gnome_date_edit_get_flags(getHandle());
        int formatFlag = DateEditFlags.HR24.getValue();

        return ((flags & formatFlag) == formatFlag);
    }

    public Date getInitialTime() {
        long time_t = gnome_date_edit_get_initial_time(getHandle()) * 1000;
        return new Date(time_t);
    }

    /**
     * Get the date/time indicated in the DateEdit control.
     * 
     * @return the date/time.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Date getTime() {
        long time_t = gnome_date_edit_get_time(getHandle()) * 1000;
        return new Date(time_t);
    }

    /**
     * Returns true if the control shows the time as well as date.
     * 
     * @return True if time is being shown. Flase if the date only is being
     *         shown.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public boolean getTimeDisplay() {
        int flags = gnome_date_edit_get_flags(getHandle());
        int showTimeBitValue = DateEditFlags.SHOW_TIME.getValue();

        return ((flags & showTimeBitValue) == showTimeBitValue);
    }

    public void setPopupRange(int lowHour, int upHour) {
        gnome_date_edit_set_popup_range(getHandle(), lowHour, upHour);
    }

    /**
     * Set the date/time indicated in the DateEdit control.
     * 
     * @param date
     *            The date/time to display.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setTime(Date date) {
        long time_t = date.getTime() / 1000;
        gnome_date_edit_set_time(getHandle(), time_t);
    }

    /**
     * Change the way the time display is formated.
     * 
     * @param enabled
     *            True if time should be shown in 24 hour format. Flase if the
     *            date should be shown in 12 hour format.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void set24HourFormat(boolean enabled) {
        int flags = gnome_date_edit_get_flags(getHandle());
        int formatFlag = DateEditFlags.HR24.getValue();

        if (enabled && (flags & formatFlag) == 0) {
            flags = flags | formatFlag;
        } else if ((!enabled) && (flags & formatFlag) > 0) {
            flags = flags - formatFlag;
        } else {
            // The flag does not need to be changed.
            return;
        }
        gnome_date_edit_set_flags(getHandle(), flags);
    }

    /**
     * Enabled/Disable the functionallity which shows time as well as date.
     * 
     * @param enabled
     *            True if time should be shown. Flase if the date only should be
     *            shown.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setTimeDisplay(boolean enabled) {
        int flags = gnome_date_edit_get_flags(getHandle());
        int showTimeBitValue = DateEditFlags.SHOW_TIME.getValue();

        if (enabled && (flags & showTimeBitValue) == 0) {
            flags = flags | showTimeBitValue;
        } else if ((!enabled) && (flags & showTimeBitValue) > 0) {
            flags = flags - showTimeBitValue;
        } else {
            // The flag does not need to be changed.
            return;
        }
        gnome_date_edit_set_flags(getHandle(), flags);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_date_edit_get_type());
    }

    // //////////////////////////////////////
    // Event handling
    // //////////////////////////////////////

    /**
     * Register an object to handle DateEdit events.
     * 
     * @see org.gnu.gnome.event.DateEditListener
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void addListener(DateEditListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(dateEditListeners, listener);
        if (i == -1) {
            if (null == dateEditListeners) {
                evtMap.initialize(this, DateEditEvent.Type.DATE_CHANGED);
                evtMap.initialize(this, DateEditEvent.Type.TIME_CHANGED);
                dateEditListeners = new Vector();
            }
            dateEditListeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(DateEditListener)
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(DateEditListener listener) {
        int i = findListener(dateEditListeners, listener);
        if (i > -1) {
            dateEditListeners.remove(i);
        }
        if (0 == dateEditListeners.size()) {
            evtMap.uninitialize(this, DateEditEvent.Type.DATE_CHANGED);
            evtMap.uninitialize(this, DateEditEvent.Type.TIME_CHANGED);
            dateEditListeners = null;
        }
    }

    protected void fireDateEditEvent(DateEditEvent event) {
        if (null == dateEditListeners) {
            return;
        }
        int size = dateEditListeners.size();
        int i = 0;
        while (i < size) {
            DateEditListener del = (DateEditListener) dateEditListeners
                    .elementAt(i);
            del.dateEditEvent(event);
            i++;
        }
    }

    private void handleDateChanged() {
        fireDateEditEvent(new DateEditEvent(this,
                DateEditEvent.Type.DATE_CHANGED));
    }

    private void handleTimeChanged() {
        fireDateEditEvent(new DateEditEvent(this,
                DateEditEvent.Type.TIME_CHANGED));
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    /**
     * Implementation method to build an EventMap for this widget class. Not
     * useful (or supported) for application use.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("date_changed", "handleDateChanged",
                DateEditEvent.Type.DATE_CHANGED, DateEditListener.class);
        anEvtMap.addEvent("time_changed", "handleTimeChanged",
                DateEditEvent.Type.TIME_CHANGED, DateEditListener.class);
    }

    native static final protected int gnome_date_edit_get_type();

    native static final protected Handle gnome_date_edit_new(long the_time,
            boolean show_time, boolean use_24_format);

    native static final protected Handle gnome_date_edit_new_flags(
            long the_time, int flags);

    native static final protected void gnome_date_edit_set_time(Handle gde,
            long the_time);

    native static final protected long gnome_date_edit_get_time(Handle gde);

    native static final protected void gnome_date_edit_set_popup_range(
            Handle gde, int low_hour, int up_hour);

    native static final protected void gnome_date_edit_set_flags(Handle gde,
            int flags);

    native static final protected int gnome_date_edit_get_flags(Handle gde);

    native static final protected long gnome_date_edit_get_initial_time(
            Handle gde);

}
