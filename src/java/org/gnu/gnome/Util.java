/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Struct;

public class Util extends Struct {
    /**
     * Pass in a string, and it will prepend $HOME/.gnome2/ to it.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static String getHomeFile(String afile) {
        return gnome_util_home_file(afile);
    }

    /**
     * Returns the name of the user's shell.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static String getUserShell() {
        return gnome_util_user_shell();
    }

    native static final protected String gnome_util_home_file(String afile);

    native static final protected String gnome_util_user_shell();

}
