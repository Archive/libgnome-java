/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import java.util.Vector;

import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.gnome.event.ClientEvent;
import org.gnu.gnome.event.ClientListener;
import org.gnu.gtk.Dialog;
import org.gnu.glib.Handle;

/**
 * The GnomeClient object makes it possible for your application to save session
 * information when the user logs out. If, when logging out, the user chooses to
 * save the current settings, a save_yourself signal is sent to each
 * application. Then an application can save information in such a way that it
 * is supported on the command line whenever the program automatically restarts
 * a user login.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.Client</code>.
 */
public class Client extends GObject {
    /**
     * Listeners for handling Client events
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private Vector clientListeners = null;

    /**
     * Create a new session management client and try to connect to a session
     * manager.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Client() {
        super(gnome_client_new());
    }

    /**
     * Initialize a client from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    protected Client(Handle handle) {
        super(handle);
    }

    protected static Client getClient(Handle handle) {
        if (handle == null) {
            return null;
        }

        Client obj = (Client) GObject.getGObjectFromHandle(handle);

        if (obj == null) {
            obj = new Client(handle);
        }

        return obj;
    }

    /**
     * Get the config prefix for a client. The config prefix provides a suitable
     * place to store any details about the state of the client which can not be
     * described using the app's command line arguments (as set in the restart
     * command).
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getConfigPrefix() {
        return gnome_client_get_config_prefix(getHandle());
    }

    /**
     * Get the config prefix for clients which have NOT been restarted or
     * cloned. This config prefix may be used to write the user's preferred
     * config for these "new" clients.
     * <p>
     * You may also use this prefix as a place to store and retrieve config
     * details that you wish to apply to ALL instances of the app. However, this
     * practice limits user's freedom to configure each instance in a different
     * way so it should be used with caution.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getGlobalConfigPrefix() {
        return gnome_client_get_global_config_prefix(getHandle());
    }

    /**
     * Set the value used for the global config prefix. The config prefixes
     * returned by getGlobalConfigPrefix() are formed by extending this prefix
     * with a unique identifier.
     * <p>
     * The global config prefix defaults to a name based on the name of the
     * executable. This function allows you to set it to a different value. It
     * should be called BEFORE retrieving the config prefix for the first time.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setGlobalConfigPrefix(String prefix) {
        gnome_client_set_global_config_prefix(getHandle(), prefix);
    }

    /**
     * Returns some flags, that give additional information about this client.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public ClientFlags getFlags() {
        return ClientFlags.intern(gnome_client_get_flags(getHandle()));
    }

    /**
     * The session manager usually only restarts clients which are running when
     * the session was last saved. You can set the restart style to make the
     * manager restart the client: - at the start of every session
     * (RESTART_AWAY) - whenever the client dies (RESTART_IMMEDIATELY) - never
     * (RESTART_NEVER)
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setRestartStyle(RestartStyle style) {
        gnome_client_set_restart_style(getHandle(), style.getValue());
    }

    /**
     * The gnome-session manager includes an extension to the protocol which
     * allows the order in which clients are started up to be organized into a
     * number of run levels. This method may be used to inform the gnome-session
     * manager of where this client should appear in this run level ordering.
     * The priority runs from 0 (started first) to 99 (started last) and
     * defaults to 50.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setPriority(int priority) {
        gnome_client_set_priority(getHandle(), priority);
    }

    /**
     * Sets the directory where all commands are executed.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setCurrentDirectory(String dir) {
        gnome_client_set_current_directory(getHandle(), dir);
    }

    /**
     * Provide the command that should be used to restart this application.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setRestartCommand(String[] args) {
        gnome_client_set_restart_command(getHandle(), args.length, args);
    }

    public void setDiscardCommand(String[] args) {
        gnome_client_set_discard_command(getHandle(), args.length, args);
    }

    public void setResignCommand(String[] args) {
        gnome_client_set_resign_command(getHandle(), args.length, args);
    }

    public void setShutdownCommand(String[] args) {
        gnome_client_set_shutdown_command(getHandle(), args.length, args);
    }

    public void setEnvironment(String name, String value) {
        gnome_client_set_environment(getHandle(), name, value);
    }

    public void setCloneCommand(String[] args) {
        gnome_client_set_clone_command(getHandle(), args.length, args);
    }

    public void setProcessID(int id) {
        gnome_client_set_process_id(getHandle(), id);
    }

    public void setProgram(String program) {
        gnome_client_set_program(getHandle(), program);
    }

    public void setUserID(String id) {
        gnome_client_set_user_id(getHandle(), id);
    }

    public void saveAnyDialog(Dialog dialog) {
        gnome_client_save_any_dialog(getHandle(), dialog.getHandle());
    }

    public void saveErrorDialog(Dialog dialog) {
        gnome_client_save_error_dialog(getHandle(), dialog.getHandle());
    }

    public void requestPhase2() {
        gnome_client_request_phase_2(getHandle());
    }

    /**
     * Request the session manager to save the session in some way.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void requestSave(SaveStyle saveStyle, boolean shutdown,
            InteractStyle interactStyle, boolean fast, boolean global) {
        gnome_client_request_save(getHandle(), saveStyle.getValue(), shutdown,
                interactStyle.getValue(), fast, global);
    }

    /**
     * Flush the underlying connection to the connection manager. This is useful
     * if you have some pending changes that you want to make sure get
     * committed.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void flush() {
        gnome_client_flush(getHandle());
    }

    /**
     * Try to connect to a session manager. This should only be called after a
     * disconnect().
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void connect() {
        gnome_client_connect(getHandle());
    }

    /**
     * Disconnect from the session manager.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void disconnect() {
        gnome_client_disconnect(getHandle());
    }

    public void setID(String id) {
        gnome_client_set_id(getHandle(), id);
    }

    public String getID() {
        return gnome_client_get_id(getHandle());
    }

    public String getPreviousID() {
        return gnome_client_get_previous_id(getHandle());
    }

    public String getDesktopID() {
        return gnome_client_get_desktop_id(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_client_get_type());
    }

    // ////////////////////////////////////////////////
    // Event handling
    // ///////////////////////////////////////////////
    /**
     * Register an object to handle Client events.
     * 
     * @see org.gnu.gnome.event.ClientListener
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void addListener(ClientListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(clientListeners, listener);
        if (i == -1) {
            if (null == clientListeners) {
                evtMap.initialize(this, ClientEvent.Type.CONNECT);
                evtMap.initialize(this, ClientEvent.Type.DIE);
                evtMap.initialize(this, ClientEvent.Type.DISCONNECT);
                evtMap.initialize(this, ClientEvent.Type.SAVE_COMPLETE);
                evtMap.initialize(this, ClientEvent.Type.SAVE_YOURSELF);
                evtMap.initialize(this, ClientEvent.Type.SHUTDOWN_CANCELLED);
                clientListeners = new Vector();
            }
            clientListeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(ClientListener)
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(ClientListener listener) {
        int i = findListener(clientListeners, listener);
        if (i > -1) {
            clientListeners.remove(i);
        }
        if (0 == clientListeners.size()) {
            evtMap.uninitialize(this, ClientEvent.Type.CONNECT);
            evtMap.uninitialize(this, ClientEvent.Type.DIE);
            evtMap.uninitialize(this, ClientEvent.Type.DISCONNECT);
            evtMap.uninitialize(this, ClientEvent.Type.SAVE_COMPLETE);
            evtMap.uninitialize(this, ClientEvent.Type.SAVE_YOURSELF);
            evtMap.uninitialize(this, ClientEvent.Type.SHUTDOWN_CANCELLED);
            clientListeners = null;
        }
    }

    /**
     * Give us a way to locate a specific listener in a Vector.
     * 
     * @param list
     *            The Vector of listeners to search.
     * @param listener
     *            The object that is to be located in the Vector.
     * @return Returns the index of the listener in the Vector, or -1 if the
     *         listener is not contained in the Vector.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    protected static int findListener(Vector list, Object listener) {
        if (null == list || null == listener)
            return -1;
        return list.indexOf(listener);
    }

    protected void fireClientEvent(ClientEvent event) {
        if (null == clientListeners) {
            return;
        }
        int size = clientListeners.size();
        int i = 0;
        while (i < size) {
            ClientListener cl = (ClientListener) clientListeners.elementAt(i);
            cl.clientEvent(event);
            i++;
        }
    }

    private void handleConnect(boolean restarted) {
        fireClientEvent(new ClientEvent(this, ClientEvent.Type.CONNECT));
    }

    private void handleDie() {
        fireClientEvent(new ClientEvent(this, ClientEvent.Type.DIE));
    }

    private void handleDisconnect() {
        fireClientEvent(new ClientEvent(this, ClientEvent.Type.DISCONNECT));
    }

    private void handleSaveComplete() {
        fireClientEvent(new ClientEvent(this, ClientEvent.Type.SAVE_COMPLETE));
    }

    private void handleSaveYourself(int phase, int saveStyle, boolean shutdown,
            int interactStyle, boolean fase) {
        fireClientEvent(new ClientEvent(this, ClientEvent.Type.SAVE_YOURSELF));
    }

    private void handleShutdownCancelled() {
        fireClientEvent(new ClientEvent(this,
                ClientEvent.Type.SHUTDOWN_CANCELLED));
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    /**
     * Implementation method to build an EventMap for this widget class. Not
     * useful (or supported) for application use.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("connect", "handleConnect", ClientEvent.Type.CONNECT,
                ClientListener.class);
        anEvtMap.addEvent("die", "handleDie", ClientEvent.Type.DIE,
                ClientListener.class);
        anEvtMap.addEvent("disconnect", "handleDisconnect",
                ClientEvent.Type.DISCONNECT, ClientListener.class);
        anEvtMap.addEvent("save_complete", "handleSaveComplete",
                ClientEvent.Type.SAVE_COMPLETE, ClientListener.class);
        anEvtMap.addEvent("save_yourself", "handleSaveYourself",
                ClientEvent.Type.SAVE_YOURSELF, ClientListener.class);
        anEvtMap.addEvent("shutdown_cancelled", "handleShutdownCancelled",
                ClientEvent.Type.SHUTDOWN_CANCELLED, ClientListener.class);
    }

    native static final protected int gnome_client_get_type();

    native static final protected String gnome_client_get_config_prefix(
            Handle client);

    native static final protected String gnome_client_get_global_config_prefix(
            Handle client);

    native static final protected void gnome_client_set_global_config_prefix(
            Handle client, String prefix);

    native static final protected int gnome_client_get_flags(Handle client);

    native static final protected void gnome_client_set_restart_style(
            Handle client, int style);

    native static final protected void gnome_client_set_priority(Handle client,
            int priority);

    native static final protected void gnome_client_set_restart_command(
            Handle client, int argc, String[] args);

    native static final protected void gnome_client_set_discard_command(
            Handle client, int argc, String[] args);

    native static final protected void gnome_client_set_resign_command(
            Handle client, int argc, String[] args);

    native static final protected void gnome_client_set_shutdown_command(
            Handle client, int argc, String[] args);

    native static final protected void gnome_client_set_current_directory(
            Handle client, String dir);

    native static final protected void gnome_client_set_environment(
            Handle client, String name, String value);

    native static final protected void gnome_client_set_clone_command(
            Handle client, int argc, String[] args);

    native static final protected void gnome_client_set_process_id(
            Handle client, int id);

    native static final protected void gnome_client_set_program(Handle client,
            String program);

    native static final protected void gnome_client_set_user_id(Handle client,
            String id);

    native static final protected void gnome_client_save_any_dialog(
            Handle client, Handle dialog);

    native static final protected void gnome_client_save_error_dialog(
            Handle client, Handle dialog);

    native static final protected void gnome_client_request_phase_2(
            Handle client);

    native static final protected void gnome_client_request_save(Handle client,
            int save_style, boolean shutdown, int interact_style, boolean fast,
            boolean global);

    native static final protected void gnome_client_flush(Handle client);

    native static final protected Handle gnome_client_new();

    native static final protected void gnome_client_connect(Handle client);

    native static final protected void gnome_client_disconnect(Handle client);

    native static final protected void gnome_client_set_id(Handle client,
            String id);

    native static final protected String gnome_client_get_id(Handle client);

    native static final protected String gnome_client_get_previous_id(
            Handle client);

    native static final protected String gnome_client_get_desktop_id(
            Handle client);

}
