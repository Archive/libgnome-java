/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Enum;

public class IconListMode extends Enum {
    static final private int _ICONS = 0;

    static final public org.gnu.gnome.IconListMode ICONS = new org.gnu.gnome.IconListMode(
            _ICONS);

    static final private int _TEXT_BELOW = 1;

    static final public org.gnu.gnome.IconListMode TEXT_BELOW = new org.gnu.gnome.IconListMode(
            _TEXT_BELOW);

    static final private int _TEXT_RIGHT = 2;

    static final public org.gnu.gnome.IconListMode TEXT_RIGHT = new org.gnu.gnome.IconListMode(
            _TEXT_RIGHT);

    static final private org.gnu.gnome.IconListMode[] theInterned = new org.gnu.gnome.IconListMode[] {
            ICONS, TEXT_BELOW, TEXT_RIGHT }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.IconListMode theSacrificialOne = new org.gnu.gnome.IconListMode(
            0);

    static public org.gnu.gnome.IconListMode intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.IconListMode already = (org.gnu.gnome.IconListMode) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.IconListMode(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private IconListMode(int value) {
        value_ = value;
    }

    public org.gnu.gnome.IconListMode or(org.gnu.gnome.IconListMode other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.IconListMode and(org.gnu.gnome.IconListMode other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.IconListMode xor(org.gnu.gnome.IconListMode other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.IconListMode other) {
        return (value_ & other.value_) == other.value_;
    }

}
