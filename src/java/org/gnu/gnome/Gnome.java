/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Handle;
import org.gnu.gtk.Config;

public class Gnome {

    /**
     * Get the master session management client. This master client gets a
     * client id that may be specified by the '--sm-client-id' command line
     * option. A master client will be generated during application startup. If
     * possible the master client will contact the session manager after
     * command-line parsing is finished.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Client getMasterClient() {
        return Client.getClient(gnome_master_client());
    }

    /**
     * Flush the accelerator definitions into the application specific
     * configuration file ~/.gnome/accels/&lt;appid&gt;
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static void syncAccelerators() {
        gnome_accelerators_sync();
    }

    static {
        System.loadLibrary(Config.LIBRARY_NAME + Config.GTK_API_VERSION);
        System.loadLibrary(GnomeConfig.LIBRARY_NAME
                + GnomeConfig.GNOME_API_VERSION);
    }

    native static final protected Handle gnome_master_client();

    native static final protected void gnome_accelerators_sync();

    private Gnome() {
        // prevent instantiation
    }
}
