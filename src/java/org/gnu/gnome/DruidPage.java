/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import java.util.Vector;

import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.glib.Type;
import org.gnu.gnome.event.DruidPageChangeEvent;
import org.gnu.gnome.event.DruidPageChangeListener;
import org.gnu.gnome.event.DruidPageSetupEvent;
import org.gnu.gnome.event.DruidPageSetupListener;
import org.gnu.gtk.Bin;
import org.gnu.glib.Handle;

/**
 * This widget is a virtual widget to define the interface to a druid page.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.DruidPage</code>.
 */
public class DruidPage extends Bin {

    /**
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private Vector druidPageChangeListeners = null;

    private Vector druidPageSetupListeners = null;

    /**
     * Construct a new DruidPage.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public DruidPage() {
        super(gnome_druid_page_new());
    }

    /**
     * Constructs a new DruidPage object form a native resource.
     * 
     * @param handle
     *            The handle to the native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public DruidPage(Handle handle) {
        super(handle);
    }

    public boolean pageNext() {
        return gnome_druid_page_next(getHandle());
    }

    public void pagePrepare() {
        gnome_druid_page_prepare(getHandle());
    }

    public boolean pageBack() {
        return gnome_druid_page_back(getHandle());
    }

    public boolean pageCancel() {
        return gnome_druid_page_cancel(getHandle());
    }

    public void pageFinish() {
        gnome_druid_page_finish(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_druid_page_get_type());
    }

    // ////////////////////////////
    // Event handling
    // ////////////////////////////
    /**
     * Register an object to handle DruidPageChange events.
     * 
     * @see org.gnu.gnome.event.DruidPageChangeListener
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void addListener(DruidPageChangeListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(druidPageChangeListeners, listener);
        if (i == -1) {
            if (null == druidPageChangeListeners) {
                evtMap.initialize(this, DruidPageChangeEvent.Type.BACK);
                evtMap.initialize(this, DruidPageChangeEvent.Type.CANCEL);
                evtMap.initialize(this, DruidPageChangeEvent.Type.NEXT);
                druidPageChangeListeners = new Vector();
            }
            druidPageChangeListeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(DruidPageChangeListener)
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(DruidPageChangeListener listener) {
        int i = findListener(druidPageChangeListeners, listener);
        if (i > -1) {
            druidPageChangeListeners.remove(i);
        }
        if (0 == druidPageChangeListeners.size()) {
            evtMap.uninitialize(this, DruidPageChangeEvent.Type.BACK);
            evtMap.uninitialize(this, DruidPageChangeEvent.Type.CANCEL);
            evtMap.uninitialize(this, DruidPageChangeEvent.Type.NEXT);
            druidPageChangeListeners = null;
        }
    }

    protected boolean fireDruidPageChangeEvent(DruidPageChangeEvent event) {
        if (null == druidPageChangeListeners) {
            return false;
        }
        int size = druidPageChangeListeners.size();
        int i = 0;
        while (i < size) {
            DruidPageChangeListener dpl = (DruidPageChangeListener) druidPageChangeListeners
                    .elementAt(i);
            if (dpl.druidPageChangeEvent(event))
                return true;
            i++;
        }
        return false;
    }

    /**
     * Register an object to handle DruidPageSetup events.
     * 
     * @see org.gnu.gnome.event.DruidPageSetupListener
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void addListener(DruidPageSetupListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(druidPageSetupListeners, listener);
        if (i == -1) {
            if (null == druidPageSetupListeners) {
                evtMap.initialize(this, DruidPageSetupEvent.Type.FINISH);
                evtMap.initialize(this, DruidPageSetupEvent.Type.PREPARE);
                druidPageSetupListeners = new Vector();
            }
            druidPageSetupListeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(DruidPageSetupListener)
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(DruidPageSetupListener listener) {
        int i = findListener(druidPageSetupListeners, listener);
        if (i > -1) {
            druidPageSetupListeners.remove(i);
        }
        if (0 == druidPageSetupListeners.size()) {
            evtMap.uninitialize(this, DruidPageSetupEvent.Type.FINISH);
            evtMap.uninitialize(this, DruidPageSetupEvent.Type.PREPARE);
            druidPageSetupListeners = null;
        }
    }

    protected void fireDruidPageSetupEvent(DruidPageSetupEvent event) {
        if (null == druidPageSetupListeners) {
            return;
        }
        int size = druidPageSetupListeners.size();
        int i = 0;
        while (i < size) {
            DruidPageSetupListener dpl = (DruidPageSetupListener) druidPageSetupListeners
                    .elementAt(i);
            dpl.druidPageSetupEvent(event);
            i++;
        }
    }

    private boolean handleNext(Handle widget) {
        return fireDruidPageChangeEvent(new DruidPageChangeEvent(this,
                DruidPageChangeEvent.Type.NEXT));
    }

    private boolean handleBack(Handle widget) {
        return fireDruidPageChangeEvent(new DruidPageChangeEvent(this,
                DruidPageChangeEvent.Type.BACK));
    }

    private void handleFinish(Handle widget) {
        fireDruidPageSetupEvent(new DruidPageSetupEvent(this,
                DruidPageSetupEvent.Type.FINISH));
    }

    private boolean handleCancel(Handle widget) {
        return fireDruidPageChangeEvent(new DruidPageChangeEvent(this,
                DruidPageChangeEvent.Type.CANCEL));
    }

    private void handlePrepare(Handle widget) {
        fireDruidPageSetupEvent(new DruidPageSetupEvent(this,
                DruidPageSetupEvent.Type.PREPARE));
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("next", "handleNext", DruidPageChangeEvent.Type.NEXT,
                DruidPageChangeListener.class);
        anEvtMap.addEvent("back", "handleBack", DruidPageChangeEvent.Type.BACK,
                DruidPageChangeListener.class);
        anEvtMap.addEvent("finish", "handleFinish",
                DruidPageSetupEvent.Type.FINISH, DruidPageSetupListener.class);
        anEvtMap
                .addEvent("cancel", "handleCancel",
                        DruidPageChangeEvent.Type.CANCEL,
                        DruidPageChangeListener.class);
        anEvtMap.addEvent("prepare", "handlePrepare",
                DruidPageSetupEvent.Type.PREPARE, DruidPageSetupListener.class);
    }

    native static final protected int gnome_druid_page_get_type();

    native static final protected Handle gnome_druid_page_new();

    native static final protected boolean gnome_druid_page_next(Handle page);

    native static final protected void gnome_druid_page_prepare(Handle page);

    native static final protected boolean gnome_druid_page_back(Handle page);

    native static final protected boolean gnome_druid_page_cancel(Handle page);

    native static final protected void gnome_druid_page_finish(Handle page);

}
