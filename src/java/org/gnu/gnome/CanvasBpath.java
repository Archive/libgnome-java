/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.CanvasBpath</code>.
 */
public class CanvasBpath extends CanvasShape {

    public CanvasBpath(CanvasGroup group) {
        super(group, gnome_canvas_bpath_get_type());
    }

    /**
     * Constructs an instance of CanvasBpath from a native widget resource.
     * 
     * @param handle
     *            The handle to the native widget.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    CanvasBpath(Handle handle) {
        super(handle);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_canvas_bpath_get_type());
    }

    native static final protected int gnome_canvas_bpath_get_type();

}
