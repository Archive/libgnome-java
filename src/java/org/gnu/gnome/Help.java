/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Handle;
import org.gnu.glib.Struct;

public class Help extends Struct {
    public static boolean display(String filename, String linkId)
            throws HelpException {
        int[] error = new int[1];
        boolean ret = gnome_help_display(filename, linkId, error);
        if (error[0] == HelpError.INTERNAL.getValue()) {
            throw new HelpException(
                    "An internal error occurred opening help file: " + filename
                            + ", link: " + linkId + ".");
        } else if (error[0] == HelpError.NOT_FOUND.getValue()) {
            throw new HelpException(
                    "Page not found error occurred opening help file: "
                            + filename + ", link: " + linkId + ".");
        }
        return ret;
    }

    public static boolean display(Program program, String docId,
            String filename, String lineId) throws HelpException {
        int[] error = new int[1];
        boolean ret = gnome_help_display_with_doc_id(program.getHandle(),
                docId, filename, lineId, error);
        if (error[0] == HelpError.INTERNAL.getValue()) {
            throw new HelpException(
                    "An internal error occurred opening help file: " + filename
                            + ", line ID: " + lineId + ".");
        } else if (error[0] == HelpError.NOT_FOUND.getValue()) {
            throw new HelpException(
                    "Page not found error occurred opening help file: "
                            + filename + ", line ID: " + lineId + ".");
        }
        return ret;
    }

    public static boolean displayDesktop(Program program, String docId,
            String filename, String linkId) throws HelpException {
        int[] error = new int[1];
        boolean ret = gnome_help_display_desktop(program.getHandle(), docId,
                filename, linkId, error);
        if (error[0] == HelpError.INTERNAL.getValue()) {
            throw new HelpException(
                    "An internal error occurred opening help file: " + filename
                            + ", link: " + linkId + ".");
        } else if (error[0] == HelpError.NOT_FOUND.getValue()) {
            throw new HelpException(
                    "Page not found error occurred opening help file: "
                            + filename + ", link: " + linkId + ".");
        }
        return ret;
    }

    public static boolean display(String helpUri) throws HelpException {
        int[] error = new int[1];
        boolean ret = gnome_help_display_uri(helpUri, error);
        if (error[0] == HelpError.INTERNAL.getValue()) {
            throw new HelpException(
                    "An internal error occurred opening help uri: " + helpUri
                            + ".");
        } else if (error[0] == HelpError.NOT_FOUND.getValue()) {
            throw new HelpException(
                    "Page not found error occurred opening help uri: "
                            + helpUri + ".");
        }
        return ret;
    }

    native static final protected boolean gnome_help_display(String filename,
            String linkId, int[] error);

    native static final protected boolean gnome_help_display_with_doc_id(
            Handle program, String docId, String filename, String lineId,
            int[] error);

    native static final protected boolean gnome_help_display_desktop(
            Handle program, String docId, String filename, String linkId,
            int[] error);

    native static final protected boolean gnome_help_display_uri(
            String helpUri, int[] error);

}
