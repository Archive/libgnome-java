/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Enum;

/**
 * The standard items that can be added into menus and toolbars.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.UIInfoConfigurableTypes</code>.
 */
public class UIInfoConfigurableTypes extends Enum {

    static final private int _NEW = 0;

    static final public org.gnu.gnome.UIInfoConfigurableTypes NEW = new org.gnu.gnome.UIInfoConfigurableTypes(
            _NEW);

    static final private int _OPEN = 1;

    static final public org.gnu.gnome.UIInfoConfigurableTypes OPEN = new org.gnu.gnome.UIInfoConfigurableTypes(
            _OPEN);

    static final private int _SAVE = 2;

    static final public org.gnu.gnome.UIInfoConfigurableTypes SAVE = new org.gnu.gnome.UIInfoConfigurableTypes(
            _SAVE);

    static final private int _SAVE_AS = 3;

    static final public org.gnu.gnome.UIInfoConfigurableTypes SAVE_AS = new org.gnu.gnome.UIInfoConfigurableTypes(
            _SAVE_AS);

    static final private int _REVERT = 4;

    static final public org.gnu.gnome.UIInfoConfigurableTypes REVERT = new org.gnu.gnome.UIInfoConfigurableTypes(
            _REVERT);

    static final private int _PRINT = 5;

    static final public org.gnu.gnome.UIInfoConfigurableTypes PRINT = new org.gnu.gnome.UIInfoConfigurableTypes(
            _PRINT);

    static final private int _PRINT_SETUP = 6;

    static final public org.gnu.gnome.UIInfoConfigurableTypes PRINT_SETUP = new org.gnu.gnome.UIInfoConfigurableTypes(
            _PRINT_SETUP);

    static final private int _CLOSE = 7;

    static final public org.gnu.gnome.UIInfoConfigurableTypes CLOSE = new org.gnu.gnome.UIInfoConfigurableTypes(
            _CLOSE);

    static final private int _QUIT = 8;

    static final public org.gnu.gnome.UIInfoConfigurableTypes QUIT = new org.gnu.gnome.UIInfoConfigurableTypes(
            _QUIT);

    static final private int _CUT = 9;

    static final public org.gnu.gnome.UIInfoConfigurableTypes CUT = new org.gnu.gnome.UIInfoConfigurableTypes(
            _CUT);

    static final private int _COPY = 10;

    static final public org.gnu.gnome.UIInfoConfigurableTypes COPY = new org.gnu.gnome.UIInfoConfigurableTypes(
            _COPY);

    static final private int _PASTE = 11;

    static final public org.gnu.gnome.UIInfoConfigurableTypes PASTE = new org.gnu.gnome.UIInfoConfigurableTypes(
            _PASTE);

    static final private int _CLEAR = 12;

    static final public org.gnu.gnome.UIInfoConfigurableTypes CLEAR = new org.gnu.gnome.UIInfoConfigurableTypes(
            _CLEAR);

    static final private int _UNDO = 13;

    static final public org.gnu.gnome.UIInfoConfigurableTypes UNDO = new org.gnu.gnome.UIInfoConfigurableTypes(
            _UNDO);

    static final private int _REDO = 14;

    static final public org.gnu.gnome.UIInfoConfigurableTypes REDO = new org.gnu.gnome.UIInfoConfigurableTypes(
            _REDO);

    static final private int _FIND = 15;

    static final public org.gnu.gnome.UIInfoConfigurableTypes FIND = new org.gnu.gnome.UIInfoConfigurableTypes(
            _FIND);

    static final private int _FIND_AGAIN = 16;

    static final public org.gnu.gnome.UIInfoConfigurableTypes FIND_AGAIN = new org.gnu.gnome.UIInfoConfigurableTypes(
            _FIND_AGAIN);

    static final private int _REPLACE = 17;

    static final public org.gnu.gnome.UIInfoConfigurableTypes REPLACE = new org.gnu.gnome.UIInfoConfigurableTypes(
            _REPLACE);

    static final private int _PROPERTIES = 18;

    static final public org.gnu.gnome.UIInfoConfigurableTypes PROPERTIES = new org.gnu.gnome.UIInfoConfigurableTypes(
            _PROPERTIES);

    static final private int _PREFERENCES = 19;

    static final public org.gnu.gnome.UIInfoConfigurableTypes PREFERENCES = new org.gnu.gnome.UIInfoConfigurableTypes(
            _PREFERENCES);

    static final private int _ABOUT = 20;

    static final public org.gnu.gnome.UIInfoConfigurableTypes ABOUT = new org.gnu.gnome.UIInfoConfigurableTypes(
            _ABOUT);

    static final private int _SELECT_ALL = 21;

    static final public org.gnu.gnome.UIInfoConfigurableTypes SELECT_ALL = new org.gnu.gnome.UIInfoConfigurableTypes(
            _SELECT_ALL);

    static final private int _NEW_WINDOW = 22;

    static final public org.gnu.gnome.UIInfoConfigurableTypes NEW_WINDOW = new org.gnu.gnome.UIInfoConfigurableTypes(
            _NEW_WINDOW);

    static final private int _CLOSE_WINDOW = 23;

    static final public org.gnu.gnome.UIInfoConfigurableTypes CLOSE_WINDOW = new org.gnu.gnome.UIInfoConfigurableTypes(
            _CLOSE_WINDOW);

    static final private int _NEW_GAME = 24;

    static final public org.gnu.gnome.UIInfoConfigurableTypes NEW_GAME = new org.gnu.gnome.UIInfoConfigurableTypes(
            _NEW_GAME);

    static final private int _PAUSE_GAME = 25;

    static final public org.gnu.gnome.UIInfoConfigurableTypes PAUSE_GAME = new org.gnu.gnome.UIInfoConfigurableTypes(
            _PAUSE_GAME);

    static final private int _RESTART_GAME = 26;

    static final public org.gnu.gnome.UIInfoConfigurableTypes RESTART_GAME = new org.gnu.gnome.UIInfoConfigurableTypes(
            _RESTART_GAME);

    static final private int _UNDO_MOVE = 27;

    static final public org.gnu.gnome.UIInfoConfigurableTypes UNDO_MOVE = new org.gnu.gnome.UIInfoConfigurableTypes(
            _UNDO_MOVE);

    static final private int _REDO_MOVE = 28;

    static final public org.gnu.gnome.UIInfoConfigurableTypes REDO_MOVE = new org.gnu.gnome.UIInfoConfigurableTypes(
            _REDO_MOVE);

    static final private int _HINT = 29;

    static final public org.gnu.gnome.UIInfoConfigurableTypes HINT = new org.gnu.gnome.UIInfoConfigurableTypes(
            _HINT);

    static final private int _SCORES = 30;

    static final public org.gnu.gnome.UIInfoConfigurableTypes SCORES = new org.gnu.gnome.UIInfoConfigurableTypes(
            _SCORES);

    static final private int _END_GAME = 31;

    static final public org.gnu.gnome.UIInfoConfigurableTypes END_GAME = new org.gnu.gnome.UIInfoConfigurableTypes(
            _END_GAME);

    static final private org.gnu.gnome.UIInfoConfigurableTypes[] theInterned = new org.gnu.gnome.UIInfoConfigurableTypes[] {
            NEW, OPEN, SAVE, SAVE_AS, REVERT, PRINT, PRINT_SETUP, CLOSE, QUIT,
            CUT, COPY, PASTE, CLEAR, UNDO, REDO, FIND, FIND_AGAIN, REPLACE,
            PROPERTIES, PREFERENCES, ABOUT, SELECT_ALL, NEW_WINDOW,
            CLOSE_WINDOW, NEW_GAME, PAUSE_GAME, RESTART_GAME, UNDO_MOVE,
            REDO_MOVE, HINT, SCORES, END_GAME };

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.UIInfoConfigurableTypes theSacrificialOne = new org.gnu.gnome.UIInfoConfigurableTypes(
            0);

    static public org.gnu.gnome.UIInfoConfigurableTypes intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.UIInfoConfigurableTypes already = (org.gnu.gnome.UIInfoConfigurableTypes) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.UIInfoConfigurableTypes(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private UIInfoConfigurableTypes(int value) {
        value_ = value;
    }

    public org.gnu.gnome.UIInfoConfigurableTypes or(
            org.gnu.gnome.UIInfoConfigurableTypes other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.UIInfoConfigurableTypes and(
            org.gnu.gnome.UIInfoConfigurableTypes other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.UIInfoConfigurableTypes xor(
            org.gnu.gnome.UIInfoConfigurableTypes other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.UIInfoConfigurableTypes other) {
        return (value_ & other.value_) == other.value_;
    }

}
