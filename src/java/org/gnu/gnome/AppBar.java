/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import java.util.Vector;

import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.glib.Handle;
import org.gnu.glib.Type;
import org.gnu.gnome.event.AppBarEvent;
import org.gnu.gnome.event.AppBarListener;
import org.gnu.gtk.HBox;
import org.gnu.gtk.ProgressBar;
import org.gnu.gtk.StatusBar;

/**
 * The AppBar widget is a progress bar with a textual annotation.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.AppBar</code>.
 */
public class AppBar extends HBox {

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static AppBar getAppBar(Handle handle) {
        if (handle == null)
            return null;
        
        AppBar appBar = (AppBar) getGObjectFromHandle(handle);
        if (appBar == null) {
            appBar = new AppBar(handle);
        }
        return appBar;
    }
    
    /**
     * Listeners for handling AppBar events
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private Vector appBarListeners = null;

    /**
     * Creates new instance of AppBar.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public AppBar(boolean hasProgress, boolean hasStatus, PreferencesType type) {
        super(gnome_appbar_new(hasProgress, hasStatus, type.getValue()));
    }

    /**
     * Construct an AppBar using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public AppBar(Handle handle) {
        super(handle);
    }

    /**
     * Clears any prompt.
     * 
     * @see #setPrompt(String, boolean)
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void clearPrompt() {
        gnome_appbar_clear_prompt(getHandle());
    }

    /**
     * Clears the stack.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void clearStack() {
        gnome_appbar_clear_stack(getHandle());
    }

    /**
     * Returns the progress bar used by AppBar.
     * 
     * @return The progress bar.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public ProgressBar getProgressBar() {
        Handle hndl = gnome_appbar_get_progress(getHandle());
        return ProgressBar.getProgressBar(hndl);
    }

    /**
     * Gets the response to setPrompt
     * 
     * @see #setPrompt(String, boolean)
     * @return The user's response.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getResponse() {
        return gnome_appbar_get_response(getHandle());
    }

    /**
     * Pops the top status off the stack. Calling this on an empty stack causes
     * no problems.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void popStack() {
        gnome_appbar_pop(getHandle());
    }

    /**
     * Pushes the status onto the stack.
     * 
     * @param status
     *            The status text. Not null.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void pushStack(String status) {
        gnome_appbar_push(getHandle(), status);
    }

    /**
     * Refreshes to the current state of stack/default. Useful for forcing the
     * text set by a call to setStatusText to disappear.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void refresh() {
        gnome_appbar_refresh(getHandle());
    }

    /**
     * Sets the percetage to show for progress.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setProgressPercentage(double percentage) {
        gnome_appbar_set_progress_percentage(getHandle(), percentage);
    }

    /**
     * Sets a prompt in the appbar and waits for a response. When the user
     * responds or cancels, a user_response signal is emitted.
     * 
     * @param prompt
     *            The message used to prompt the user.
     * @param modal
     *            True if all user input to the application should be blocked
     *            until the user responds to the prompt or cancels.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setPrompt(String prompt, boolean modal) {
        gnome_appbar_set_prompt(getHandle(), prompt, modal);
    }

    /**
     * What to show when showing nothing else. Defaults to nothing.
     * 
     * @param status
     *            The text to use as default. Not null.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setStatusDefault(String status) {
        gnome_appbar_set_default(getHandle(), status);
    }

    /**
     * Sets the status text without changing the widget state. The next set or
     * push will destroy this permanently.
     * 
     * @param status
     *            The text to set. Not null.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setStatusText(String status) {
        gnome_appbar_set_status(getHandle(), status);
    }

    /**
     * Return the StatusBar widget contained in this AppBar.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public StatusBar getStatusBar() {
        return StatusBar.getStatusBar(gnome_appbar_get_status(getHandle()));
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_appbar_get_type());
    }

    // //////////////////////////////////
    // Event handling
    // /////////////////////////////////

    /**
     * Register an object to handle AppBar events.
     * 
     * @see org.gnu.gnome.event.AppBarListener
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void addListener(AppBarListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(appBarListeners, listener);
        if (i == -1) {
            if (null == appBarListeners) {
                evtMap.initialize(this, AppBarEvent.Type.CLEAR_PROMPT);
                evtMap.initialize(this, AppBarEvent.Type.USER_RESPONSE);
                appBarListeners = new Vector();
            }
            appBarListeners.addElement(listener);
        }
    }

    /**
     * Removes a listener.
     * 
     * @see #addListener(AppBarListener)
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(AppBarListener listener) {
        int i = findListener(appBarListeners, listener);
        if (i > -1) {
            appBarListeners.remove(i);
        }
        if (0 == appBarListeners.size()) {
            evtMap.uninitialize(this, AppBarEvent.Type.CLEAR_PROMPT);
            evtMap.uninitialize(this, AppBarEvent.Type.USER_RESPONSE);
            appBarListeners = null;
        }
    }

    protected void fireAppBarEvent(AppBarEvent event) {
        if (null == appBarListeners) {
            return;
        }
        int size = appBarListeners.size();
        int i = 0;
        while (i < size) {
            AppBarListener abl = (AppBarListener) appBarListeners.elementAt(i);
            abl.appBarEvent(event);
            i++;
        }
    }

    private void handleUserResponse() {
        fireAppBarEvent(new AppBarEvent(this, AppBarEvent.Type.USER_RESPONSE));
    }

    private void handleClearPrompt() {
        fireAppBarEvent(new AppBarEvent(this, AppBarEvent.Type.CLEAR_PROMPT));
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    /**
     * Implementation method to build an EventMap for this widget class. Not
     * useful (or supported) for application use.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("user_response", "handleUserResponse",
                AppBarEvent.Type.USER_RESPONSE, AppBarListener.class);
        anEvtMap.addEvent("clear_prompt", "handleClearPrompt",
                AppBarEvent.Type.CLEAR_PROMPT, AppBarListener.class);
    }

    native static final protected int gnome_appbar_get_type();

    native static final protected Handle gnome_appbar_new(boolean hasProgress,
            boolean hasStatus, int interactivity);

    native static final protected void gnome_appbar_set_status(Handle appbar,
            String status);

    native static final protected Handle gnome_appbar_get_status(Handle appbar);

    native static final protected void gnome_appbar_set_default(Handle appbar,
            String defaultStatus);

    native static final protected void gnome_appbar_push(Handle appbar,
            String status);

    native static final protected void gnome_appbar_pop(Handle appbar);

    native static final protected void gnome_appbar_clear_stack(Handle appbar);

    native static final protected void gnome_appbar_set_progress_percentage(
            Handle appbar, double percentage);

    native static final protected Handle gnome_appbar_get_progress(Handle appbar);

    native static final protected void gnome_appbar_refresh(Handle appbar);

    native static final protected void gnome_appbar_set_prompt(Handle appbar,
            String prompt, boolean modal);

    native static final protected void gnome_appbar_clear_prompt(Handle appbar);

    native static final protected String gnome_appbar_get_response(Handle appbar);

}
