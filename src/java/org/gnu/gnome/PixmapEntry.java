/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * @deprecated
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.PixmapEntry</code>.
 */
public class PixmapEntry extends FileEntry {
    /**
     * Create a new Pixmap Entry.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public PixmapEntry(String historyID, String browseDialogTitle,
            boolean doPreview) {
        super(gnome_pixmap_entry_new(historyID, browseDialogTitle, doPreview));
    }

    /**
     * Construct a new PixmapEntry from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public PixmapEntry(Handle handle) {
        super(handle);
    }

    /**
     * Set the directory for the lookup
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setDirectory(String directory) {
        gnome_pixmap_entry_set_pixmap_subdir(getHandle(), directory);
    }

    /**
     * Retrun a file if it is possible to load it with gdk-pixbuf.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getFilename() {
        return gnome_pixmap_entry_get_filename(getHandle());
    }

    /**
     * Set if a preview should be displayed.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setPreview(boolean doPreview) {
        gnome_pixmap_entry_set_preview(getHandle(), doPreview);
    }

    /**
     * Set the size of the preview.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setPreviewSize(int width, int height) {
        gnome_pixmap_entry_set_preview_size(getHandle(), width, height);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_pixmap_entry_get_type());
    }

    native static final protected int gnome_pixmap_entry_get_type();

    native static final protected Handle gnome_pixmap_entry_new(
            String historyId, String browseDialogTitle, boolean doPreview);

    native static final protected void gnome_pixmap_entry_set_pixmap_subdir(
            Handle pentry, String subdir);

    native static final protected Handle gnome_pixmap_entry_scrolled_window(
            Handle pentry);

    native static final protected Handle gnome_pixmap_entry_preview_widget(
            Handle pentry);

    native static final protected void gnome_pixmap_entry_set_preview(
            Handle pentry, boolean doPreview);

    native static final protected void gnome_pixmap_entry_set_preview_size(
            Handle pentry, int width, int height);

    native static final protected String gnome_pixmap_entry_get_filename(
            Handle pentry);

}
