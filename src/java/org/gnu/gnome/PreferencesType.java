/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Enum;

public class PreferencesType extends Enum {
    static final private int _NEVER = 0;

    static final public org.gnu.gnome.PreferencesType NEVER = new org.gnu.gnome.PreferencesType(
            _NEVER);

    static final private int _USER = 1;

    static final public org.gnu.gnome.PreferencesType USER = new org.gnu.gnome.PreferencesType(
            _USER);

    static final private int _ALWAYS = 2;

    static final public org.gnu.gnome.PreferencesType ALWAYS = new org.gnu.gnome.PreferencesType(
            _ALWAYS);

    static final private org.gnu.gnome.PreferencesType[] theInterned = new org.gnu.gnome.PreferencesType[] {
            NEVER, USER, ALWAYS };

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.PreferencesType theSacrificialOne = new org.gnu.gnome.PreferencesType(
            0);

    static public org.gnu.gnome.PreferencesType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.PreferencesType already = (org.gnu.gnome.PreferencesType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.PreferencesType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private PreferencesType(int value) {
        value_ = value;
    }

    public org.gnu.gnome.PreferencesType or(org.gnu.gnome.PreferencesType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.PreferencesType and(org.gnu.gnome.PreferencesType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.PreferencesType xor(org.gnu.gnome.PreferencesType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.PreferencesType other) {
        return (value_ & other.value_) == other.value_;
    }

}
