/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.gdk.ModifierType;
import org.gnu.glib.Handle;
import org.gnu.glib.Struct;
import org.gnu.gtk.GtkStockItem;
import org.gnu.gtk.Widget;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.MenuItemEvent;
import org.gnu.gtk.event.MenuItemListener;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.UIInfo</code>.
 */
public class UIInfo extends Struct {

    public int getType() {
        return getType(getHandle());
    }

    /**
     * Listener to handle MenuEvents
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private MenuItemListener menuListener;

    /**
     * Listener to handle ButtonEvents.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private ButtonListener buttonListener;

    private static final byte[] zeroBytes = new byte[0];

    /**
     * This constructor should be used when the UIInfo type is a subtree or a
     * radioitem lead entry and the UIPixmapType is stock or a filename. The
     * moreInfo argument is an array of subitems that are under the subtree or
     * radioitem and the name argument is the name of the stock icon or the name
     * of the file.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    protected UIInfo(UIInfoType type, String label, String hint,
            UIInfo[] moreInfo, UIPixmapType pixmapType, String name,
            int acceleratorKey, ModifierType acMods) {
        super(init(type, label, hint, pixmapType, acceleratorKey, acMods));
        if (moreInfo == null)
            return;
        Handle[] hndls = new Handle[moreInfo.length];
        for (int i = 0; i < moreInfo.length; i++)
            hndls[i] = moreInfo[i].getHandle();
        if (null != hndls)
            setSubmenuInfo(getHandle(), hndls);
        if (null != name && 0 < name.length())
            setPixmapInfo(getHandle(), name);
    }

    /**
     * This constructor should be used when the UIInfo type is a subtree or a
     * radioitem lead entry and the UIPixmapType is data. The moreInfo argument
     * is an array of subitems that are under the subtree or radioitem and the
     * name argument is inline xpm data.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    protected UIInfo(UIInfoType type, String label, String hint,
            UIInfo[] moreInfo, UIPixmapType pixmapType, byte data[],
            int acceleratorKey, ModifierType acMods) {
        super(init(type, label, hint, pixmapType, acceleratorKey, acMods));
        Handle[] hndls = null;
        for (int i = 0; i < moreInfo.length; i++)
            hndls[i] = moreInfo[i].getHandle();
        if (null != hndls)
            setSubmenuInfo(getHandle(), hndls);
        if (null != data)
            setPixmapInfo(getHandle(), data);
    }

    /**
     * This constructor should be used when the UIInfo type is a help item. The
     * moreInfo argument is a string that specifies the help node to load.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    protected UIInfo(String helpNode) {
        super(uiinfo_new(UIInfoType.HELP.getValue(), "", "", UIPixmapType.NONE
                .getValue(), 0, 0));
        setMoreInfo(getHandle(), helpNode);
        setPixmapInfo(getHandle(), zeroBytes);
    }

    /**
     * This constructor should be used when the UIInfoType is item, toggleitem,
     * or radioitem and the UIPixmapType is file or stock. The name name
     * argument holds the filename or stock name of the pixmap.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    protected UIInfo(UIInfoType type, String label, String hint,
            MenuItemListener listener, UIPixmapType pixmapType, String name,
            int acceleratorKey, ModifierType acMods) {
        super(init(type, label, hint, pixmapType, acceleratorKey, acMods));
        menuListener = listener;
        setCallbackInfo(getHandle(), "handleMenuEvent", this);
        setPixmapInfo(getHandle(), name);
    }

    /**
     * This constructor should be used when the UIInfoType is item, toggleitem,
     * or radioitem and the UIPixmapType is file or stock. The name name
     * argument holds the filename or stock name of the pixmap.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    protected UIInfo(UIInfoType type, String label, String hint,
            ButtonListener listener, UIPixmapType pixmapType, String name,
            int acceleratorKey, ModifierType acMods) {
        super(init(type, label, hint, pixmapType, acceleratorKey, acMods));
        buttonListener = listener;
        setCallbackInfo(getHandle(), "handleButtonEvent", this);
        setPixmapInfo(getHandle(), name);
    }

    /**
     * This constructor should be used when the UIInfoType is item, toggleitem,
     * or radioitem and the UIPixmapType is data. The data argument is an inline
     * xpm.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    protected UIInfo(UIInfoType type, String label, String hint,
            MenuItemListener listener, UIPixmapType pixmapType, byte data[],
            int acceleratorKey, ModifierType acMods) {
        super(init(type, label, hint, pixmapType, acceleratorKey, acMods));
        if (null == data) {
            data = zeroBytes;
        }
        menuListener = listener;
        setCallbackInfo(getHandle(), "handleMenuEvent", this);
        setPixmapInfo(getHandle(), data);
    }

    /**
     * This constructor should be used when the UIInfoType is item, toggleitem,
     * or radioitem and the UIPixmapType is data. The data argument is an inline
     * xpm.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    protected UIInfo(UIInfoType type, String label, String hint,
            ButtonListener listener, UIPixmapType pixmapType, byte data[],
            int acceleratorKey, ModifierType acMods) {
        super(init(type, label, hint, pixmapType, acceleratorKey, acMods));
        if (null == data) {
            data = zeroBytes;
        }
        buttonListener = listener;
        setCallbackInfo(getHandle(), "handleButtonEvent", this);
        setPixmapInfo(getHandle(), data);
    }

    protected UIInfo(Handle hndl) {
        super(hndl);
    }

    private static Handle init(UIInfoType type, String label, String hint,
            UIPixmapType pixmapType, int acceleratorKey, ModifierType acMods) {
        if (null == acMods) {
            acMods = ModifierType.intern(0);
        }
        if (null == label) {
            label = "";
        }
        if (null == hint) {
            hint = "";
        }
        return uiinfo_new(type.getValue(), label, hint, pixmapType.getValue(),
                acceleratorKey, acMods.getValue());
    }

    public Widget getWidget() {
        Handle hndl = getWidget(getHandle());
        return Widget.getWidget(hndl);
    }

    protected void handleMenuEvent() {
        menuListener.menuItemEvent(new MenuItemEvent(getWidget()));
    }

    protected void handleButtonEvent() {
        ButtonEvent be = new ButtonEvent(getWidget(), ButtonEvent.Type.CLICK);
        buttonListener.buttonEvent(be);
    }

    /**
     * Use this method to end an array.
     * 
     * @return A UIInfo that should be used to terminate an array of UIInfo
     *         objects.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo end() {
        return new UIInfo(UIInfoType.ENDOFINFO, null, null, new UIInfo[0],
                UIPixmapType.NONE, "", 0, null);
    }

    /**
     * Use this method to include a separator in your menu or toolbar
     * 
     * @return A UIInfo that represents a separator.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo separator() {
        return new UIInfo(UIInfoType.SEPARATOR, null, null, new UIInfo[0],
                UIPixmapType.NONE, "", 0, null);
    }

    /**
     * Generic menu or toolbar item which includes a label, a tooltip and an
     * optional pixmap (in xpm format) to be displayed next to the entry.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo item(String label, String tooltip,
            MenuItemListener listener, byte[] xpmData) {
        return new UIInfo(UIInfoType.ITEM, label, tooltip, listener,
                UIPixmapType.DATA, xpmData, 0, null);
    }

    /**
     * Generic menu or toolbar item which includes a label, a tooltip and an
     * optional pixmap (in xpm format) to be displayed next to the entry.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo item(String label, String tooltip,
            ButtonListener listener, byte[] xpmData) {
        return new UIInfo(UIInfoType.ITEM, label, tooltip, listener,
                UIPixmapType.DATA, xpmData, 0, null);
    }

    /**
     * Use this method to include an item in your menu that includes a
     * GNOME-stock image.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo itemStock(String label, String tooltip,
            MenuItemListener listener, GtkStockItem stockID) {
        return new UIInfo(UIInfoType.ITEM, label, tooltip, listener,
                UIPixmapType.STOCK, stockID.getString(), 0, null);
    }

    /**
     * Use this method to include an item in your menu that includes a
     * GNOME-stock image.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo itemStock(String label, String tooltip,
            ButtonListener listener, GtkStockItem stockID) {
        return new UIInfo(UIInfoType.ITEM, label, tooltip, listener,
                UIPixmapType.STOCK, stockID.getString(), 0, null);
    }

    /**
     * Use this method to add an item to the menu that has no pixmap associated
     * with it.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo item(String label, String tooltip,
            MenuItemListener listener) {
        return new UIInfo(UIInfoType.ITEM, label, tooltip, listener,
                UIPixmapType.NONE, "", 0, null);
    }

    /**
     * Use this method to add an item to the menu that has no pixmap associated
     * with it.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo item(String label, String tooltip,
            ButtonListener listener) {
        return new UIInfo(UIInfoType.ITEM, label, tooltip, listener,
                UIPixmapType.NONE, "", 0, null);
    }

    /**
     * Creates a toggle-item (a checkbox) in a menu, this includes an xpm
     * graphic.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo toggleItem(String label, String tooltip,
            MenuItemListener listener, byte[] xpmData) {
        return new UIInfo(UIInfoType.TOGGLEITEM, label, tooltip, listener,
                UIPixmapType.DATA, xpmData, 0, null);
    }

    /**
     * Creates a toggle-item (a checkbox) in a menu, this includes an xpm
     * graphic.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo toggleItem(String label, String tooltip,
            ButtonListener listener, byte[] xpmData) {
        return new UIInfo(UIInfoType.TOGGLEITEM, label, tooltip, listener,
                UIPixmapType.DATA, xpmData, 0, null);
    }

    /**
     * Creates a toggle-item (a checkbox) in a menu, this includes an stock
     * icon.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo toggleItemStock(String label, String tooltip,
            MenuItemListener listener, GtkStockItem stock) {
        return new UIInfo(UIInfoType.TOGGLEITEM, label, tooltip, listener,
                UIPixmapType.STOCK, stock.getString(), 0, null);
    }

    /**
     * Creates a toggle-item (a checkbox) in a menu, this includes an stock
     * icon.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo toggleItemStock(String label, String tooltip,
            ButtonListener listener, GtkStockItem stock) {
        return new UIInfo(UIInfoType.TOGGLEITEM, label, tooltip, listener,
                UIPixmapType.STOCK, stock.getString(), 0, null);
    }

    /**
     * Creates a toggle-item (a checkbox) in a menu with no icon.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo toggleItem(String label, String tooltip,
            MenuItemListener listener) {
        return new UIInfo(UIInfoType.TOGGLEITEM, label, tooltip, listener,
                UIPixmapType.NONE, "", 0, null);
    }

    /**
     * Creates a toggle-item (a checkbox) in a menu with no icon.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo toggleItem(String label, String tooltip,
            ButtonListener listener) {
        return new UIInfo(UIInfoType.TOGGLEITEM, label, tooltip, listener,
                UIPixmapType.NONE, "", 0, null);
    }

    /**
     * Creates a structure to insert a list of radio items.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo radioList(UIInfo list[]) {
        return new UIInfo(UIInfoType.RADIOITEMS, null, null, list,
                UIPixmapType.NONE, "", 0, null);
    }

    public static UIInfo radioItem(String label, String tooltip,
            MenuItemListener listener, byte[] xpmData) {
        return new UIInfo(UIInfoType.ITEM, label, tooltip, listener,
                UIPixmapType.DATA, xpmData, 0, null);
    }

    public static UIInfo radioItem(String label, String tooltip,
            ButtonListener listener, byte[] xpmData) {
        return new UIInfo(UIInfoType.ITEM, label, tooltip, listener,
                UIPixmapType.DATA, xpmData, 0, null);
    }

    public static UIInfo radioItem(String label, String tooltip,
            MenuItemListener listener) {
        return new UIInfo(UIInfoType.ITEM, label, tooltip, listener,
                UIPixmapType.NONE, "", 0, null);
    }

    public static UIInfo radioItem(String label, String tooltip,
            ButtonListener listener) {
        return new UIInfo(UIInfoType.ITEM, label, tooltip, listener,
                UIPixmapType.NONE, "", 0, null);
    }

    public static UIInfo radioItemStock(String label, String tooltip,
            MenuItemListener listener, GtkStockItem stock) {
        return new UIInfo(UIInfoType.ITEM, label, tooltip, listener,
                UIPixmapType.STOCK, stock.getString(), 0, null);
    }

    public static UIInfo radioItemStock(String label, String tooltip,
            ButtonListener listener, GtkStockItem stock) {
        return new UIInfo(UIInfoType.ITEM, label, tooltip, listener,
                UIPixmapType.STOCK, stock.getString(), 0, null);
    }

    /**
     * Creates a submenu or a sub-tree in the menu stracture.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo subtree(String label, UIInfo tree[]) {
        return new UIInfo(UIInfoType.SUBTREE, label, null, tree,
                UIPixmapType.NONE, "", 0, null);
    }

    /**
     * Creates a submenu or a sub-tree in the menu stracture with a stock pixmap
     * displayed.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static UIInfo subtree(String label, UIInfo tree[], String stockID) {
        return new UIInfo(UIInfoType.SUBTREE, label, null, tree,
                UIPixmapType.STOCK, stockID, 0, null);
    }

    public static UIInfo help(String helpNode) {
        return new UIInfo(helpNode);
    }

    // ///////////////////////////////
    // Stock menu items
    // ///////////////////////////////
    public static UIInfo newItem(String label, String tip,
            MenuItemListener listener) {
        return new UIInfo(UIInfoType.ITEM_CONFIGURABLE, label, tip, listener,
                UIPixmapType.NONE, "", UIInfoConfigurableTypes.NEW.getValue(),
                (ModifierType) null);
    }

    public static UIInfo newSubtree(UIInfo[] tree) {
        return new UIInfo(UIInfoType.SUBTREE_STOCK, "_NEW", "", tree,
                UIPixmapType.STOCK, new String("Menu_New"), 0,
                (ModifierType) null);
    }

    public static UIInfo openItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.OPEN.getValue());
    }

    public static UIInfo openItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.OPEN.getValue());
    }

    public static UIInfo saveItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.SAVE.getValue());
    }

    public static UIInfo saveItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.SAVE.getValue());
    }

    public static UIInfo saveAsItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.SAVE_AS
                .getValue());
    }

    public static UIInfo saveAsItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.SAVE_AS
                .getValue());
    }

    public static UIInfo revertItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.REVERT
                .getValue());
    }

    public static UIInfo revertItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.REVERT
                .getValue());
    }

    public static UIInfo printItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.PRINT.getValue());
    }

    public static UIInfo printItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.PRINT.getValue());
    }

    public static UIInfo printSetupItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.PRINT_SETUP
                .getValue());
    }

    public static UIInfo printSetupItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.PRINT_SETUP
                .getValue());
    }

    public static UIInfo closeItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.CLOSE.getValue());
    }

    public static UIInfo closeItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.CLOSE.getValue());
    }

    public static UIInfo quitItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.QUIT.getValue());
    }

    public static UIInfo quitItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.QUIT.getValue());
    }

    public static UIInfo cutItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.CUT.getValue());
    }

    public static UIInfo cutItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.CUT.getValue());
    }

    public static UIInfo copyItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.COPY.getValue());
    }

    public static UIInfo copyItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.COPY.getValue());
    }

    public static UIInfo pasteItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.PASTE.getValue());
    }

    public static UIInfo pasteItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.PASTE.getValue());
    }

    public static UIInfo selectAllItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.SELECT_ALL
                .getValue());
    }

    public static UIInfo selectAllItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.SELECT_ALL
                .getValue());
    }

    public static UIInfo clearItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.CLEAR.getValue());
    }

    public static UIInfo clearItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.CLEAR.getValue());
    }

    public static UIInfo undoItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.UNDO.getValue());
    }

    public static UIInfo undoItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.UNDO.getValue());
    }

    public static UIInfo redoItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.REDO.getValue());
    }

    public static UIInfo redoItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.REDO.getValue());
    }

    public static UIInfo findItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.FIND.getValue());
    }

    public static UIInfo findItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.FIND.getValue());
    }

    public static UIInfo findAgainItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.FIND_AGAIN
                .getValue());
    }

    public static UIInfo findAgainItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.FIND_AGAIN
                .getValue());
    }

    public static UIInfo replaceItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.REPLACE
                .getValue());
    }

    public static UIInfo replaceItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.REPLACE
                .getValue());
    }

    public static UIInfo propertiesItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.PROPERTIES
                .getValue());
    }

    public static UIInfo propertiesItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.PROPERTIES
                .getValue());
    }

    public static UIInfo preferencesItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.PREFERENCES
                .getValue());
    }

    public static UIInfo preferencesItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.PREFERENCES
                .getValue());
    }

    public static UIInfo newWindowItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.NEW_WINDOW
                .getValue());
    }

    public static UIInfo newWindowItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.NEW_WINDOW
                .getValue());
    }

    public static UIInfo closeWindowItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.CLOSE_WINDOW
                .getValue());
    }

    public static UIInfo closeWindowItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.CLOSE_WINDOW
                .getValue());
    }

    public static UIInfo aboutItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.ABOUT.getValue());
    }

    public static UIInfo aboutItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.ABOUT.getValue());
    }

    public static UIInfo newGameItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.NEW_GAME
                .getValue());
    }

    public static UIInfo newGameItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.NEW_GAME
                .getValue());
    }

    public static UIInfo pauseGameItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.PAUSE_GAME
                .getValue());
    }

    public static UIInfo pauseGameItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.PAUSE_GAME
                .getValue());
    }

    public static UIInfo restartGameItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.RESTART_GAME
                .getValue());
    }

    public static UIInfo restartGameItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.RESTART_GAME
                .getValue());
    }

    public static UIInfo undoMoveItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.UNDO_MOVE
                .getValue());
    }

    public static UIInfo undoMoveItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.UNDO_MOVE
                .getValue());
    }

    public static UIInfo redoMoveItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.REDO_MOVE
                .getValue());
    }

    public static UIInfo redoMoveItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.REDO_MOVE
                .getValue());
    }

    public static UIInfo hintItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.HINT.getValue());
    }

    public static UIInfo hintItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.HINT.getValue());
    }

    public static UIInfo scoresItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.SCORES
                .getValue());
    }

    public static UIInfo scoresItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.SCORES
                .getValue());
    }

    public static UIInfo endGameItem(MenuItemListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.END_GAME
                .getValue());
    }

    public static UIInfo endGameItem(ButtonListener listener) {
        return createDefault(listener, UIInfoConfigurableTypes.END_GAME
                .getValue());
    }

    protected static UIInfo createDefault(MenuItemListener listener, int type) {
        return new UIInfo(UIInfoType.ITEM_CONFIGURABLE, null, null, listener,
                UIPixmapType.NONE, "", type, (ModifierType) null);
    }

    protected static UIInfo createDefault(ButtonListener listener, int type) {
        return new UIInfo(UIInfoType.ITEM_CONFIGURABLE, null, null, listener,
                UIPixmapType.NONE, "", type, (ModifierType) null);
    }

    // protected static void makeNative( UIInfo[] array ){
    // if (array.length <= 0)
    // return;
    // int[] handleArray = new int[array.length];
    // for(int i = 0; i < array.length; i++)
    // handleArray[i] = array[i].getHandle();
    //		
    // int[] newHandles;
    // newHandles = toNativeArray( handleArray );
    //		
    // for(int i =0; i < array.length; i++)
    // //array[i].setHandle(newHandles[i]);
    // array[i] = new UIInfo(newHandles[i]);
    //
    // }

    native static final protected int getType(Handle cptr);

    native static final protected String getLabel(Handle cptr);

    native static final protected String getHint(Handle cptr);

    native static final protected Handle getMoreinfo(Handle cptr);

    native static final protected Handle getUserData(Handle cptr);

    native static final protected int getPixmapType(Handle cptr);

    native static final protected Handle getPixmapInfo(Handle cptr);

    native static final protected int getAcceleratorKey(Handle cptr);

    native static final protected int getAcMods(Handle cptr);

    native static final protected Handle getWidget(Handle cptr);

    native static final protected Handle uiinfo_new(int type, String label,
            String hint, int pixmapType, int acceleratorKey, int acMods);

    native static final protected void setPixmapInfo(Handle handle, String name);

    native static final protected void setPixmapInfo(Handle handle, byte[] data);

    native static final protected void setCallbackInfo(Handle handle,
            String callbackFunc, Object callbackObj);

    native static final protected void setMoreInfo(Handle handle,
            String moreInfo);

    native static final protected void setSubmenuInfo(Handle handle,
            Handle[] moreInfo);

}
