/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Flags;

public class DateEditFlags extends Flags {
    static final private int _SHOW_TIME = 1 << 0;

    static final public org.gnu.gnome.DateEditFlags SHOW_TIME = new org.gnu.gnome.DateEditFlags(
            _SHOW_TIME);

    static final private int _HR24 = 1 << 1;

    static final public org.gnu.gnome.DateEditFlags HR24 = new org.gnu.gnome.DateEditFlags(
            _HR24);

    static final private int _WEEK_STARTS_ON_MONDAY = 1 << 2;

    static final public org.gnu.gnome.DateEditFlags WEEK_STARTS_ON_MONDAY = new org.gnu.gnome.DateEditFlags(
            _WEEK_STARTS_ON_MONDAY);

    static final private org.gnu.gnome.DateEditFlags[] theInterned = new org.gnu.gnome.DateEditFlags[] {
            new org.gnu.gnome.DateEditFlags(0), SHOW_TIME, HR24,
            new org.gnu.gnome.DateEditFlags(3), WEEK_STARTS_ON_MONDAY }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.DateEditFlags theSacrificialOne = new org.gnu.gnome.DateEditFlags(
            0);

    static public org.gnu.gnome.DateEditFlags intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.DateEditFlags already = (org.gnu.gnome.DateEditFlags) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.DateEditFlags(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private DateEditFlags(int value) {
        value_ = value;
    }

    public org.gnu.gnome.DateEditFlags or(org.gnu.gnome.DateEditFlags other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.DateEditFlags and(org.gnu.gnome.DateEditFlags other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.DateEditFlags xor(org.gnu.gnome.DateEditFlags other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.DateEditFlags other) {
        return (value_ & other.value_) == other.value_;
    }

}
