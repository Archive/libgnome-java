/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Enum;

public class SaveStyle extends Enum {
    static final private int _GLOBAL = 0;

    static final public org.gnu.gnome.SaveStyle GLOBAL = new org.gnu.gnome.SaveStyle(
            _GLOBAL);

    static final private int _LOCAL = 1;

    static final public org.gnu.gnome.SaveStyle LOCAL = new org.gnu.gnome.SaveStyle(
            _LOCAL);

    static final private int _BOTH = 2;

    static final public org.gnu.gnome.SaveStyle BOTH = new org.gnu.gnome.SaveStyle(
            _BOTH);

    static final private org.gnu.gnome.SaveStyle[] theInterned = new org.gnu.gnome.SaveStyle[] {
            GLOBAL, LOCAL, BOTH };

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.SaveStyle theSacrificialOne = new org.gnu.gnome.SaveStyle(
            0);

    static public org.gnu.gnome.SaveStyle intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.SaveStyle already = (org.gnu.gnome.SaveStyle) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.SaveStyle(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private SaveStyle(int value) {
        value_ = value;
    }

    public org.gnu.gnome.SaveStyle or(org.gnu.gnome.SaveStyle other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.SaveStyle and(org.gnu.gnome.SaveStyle other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.SaveStyle xor(org.gnu.gnome.SaveStyle other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.SaveStyle other) {
        return (value_ & other.value_) == other.value_;
    }

}
