/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The GnomeCanvasLine is a GnomeCanvasItem that draws a line on a GnomeCanvas.
 * The line can contain multiple segments.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.CanvasLine</code>.
 */
public class CanvasLine extends CanvasItem {

    public CanvasLine(CanvasGroup group) {
        super(group, gnome_canvas_line_get_type());
    }

    /**
     * Constructs an instance of CanvasLine from a native widget resource.
     * 
     * @param handle
     *            The handle to the native widget.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    CanvasLine(Handle handle) {
        super(handle);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_canvas_line_get_type());
    }

    native static final protected int getNumPoints(Handle cptr);

    native static final protected double getWidth(Handle cptr);

    native static final protected Handle getStipple(Handle cptr);

    native static final protected void setStipple(Handle cptr, Handle stipple);

    native static final protected long getFillPixel(Handle cptr);

    native static final protected void setFillPixel(Handle cptr, long fill_pixel);

    native static final protected int getFillColor(Handle cptr);

    native static final protected void setFillColor(Handle cptr, int fill_color);

    native static final protected Handle getGc(Handle cptr);

    native static final protected boolean getWidthPixels(Handle cptr);

    native static final protected boolean getFirstArrow(Handle cptr);

    native static final protected void setFirstArrow(Handle cptr, boolean first_arrow);

    native static final protected boolean getLastArrow(Handle cptr);

    native static final protected void setLastArrow(Handle cptr, boolean last_arrow);

    native static final protected boolean getSmooth(Handle cptr);

    native static final protected void setSmooth(Handle cptr, boolean smooth);

    native static final protected int getCap(Handle cptr);

    native static final protected void setCap(Handle cptr, int cap);

    native static final protected int getLineStyle(Handle cptr);

    native static final protected void setLineStyle(Handle cptr, int line_style);

    native static final protected int getJoin(Handle cptr);

    native static final protected void setJoin(Handle cptr, int join);

    native static final protected int getFillRgba(Handle cptr);

    native static final protected void setFillRgba(Handle cptr, int fill_rgba);

    native static final protected int getSplineSteps(Handle cptr);

    native static final protected void setSplineSteps(Handle cptr, int spline_steps);

    native static final protected double getShapeA(Handle cptr);

    native static final protected void setShapeA(Handle cptr, double shape_a);

    native static final protected double getShapeB(Handle cptr);

    native static final protected void setShapeB(Handle cptr, double shape_b);

    native static final protected double getShapeC(Handle cptr);

    native static final protected void setShapeC(Handle cptr, double shape_c);

    native static final protected int gnome_canvas_line_get_type();

}
