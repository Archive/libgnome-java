/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Handle;
import org.gnu.glib.Struct;

public class ModuleInfo extends Struct {

    native static final protected String getName(Handle cptr);

    native static final protected String getVersion(Handle cptr);

    native static final protected String getDescription(Handle cptr);

}
