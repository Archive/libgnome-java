/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.gdk.Color;
import org.gnu.gdk.RgbDither;
import org.gnu.glib.GObject;
import org.gnu.glib.Handle;
import org.gnu.glib.Type;
import org.gnu.gtk.Layout;

/**
 * You can use the GnomeCanvas widget to draw figures, display graphics,
 * position widgets, and more.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.Canvas</code>.
 */
public class Canvas extends Layout {
    
    /**
     * Constructs a new Canvas.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Canvas(boolean useAntialiased) {
        super(init(useAntialiased));
    }

    /**
     * Construct a new Canvas.
     * 
     * @deprecated
     * 
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Canvas() {
        super(gnome_canvas_new());
    }

    /**
     * Constructs a new Canvas object form a native resource.
     * 
     * @param handle
     *            The handle to the native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Canvas(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Canvas getCanvas(Handle handle) {
        if (handle == null) {
            return null;
        }

        Canvas obj = (Canvas) GObject.getGObjectFromHandle(handle);

        if (obj == null) {
            obj = new Canvas(handle);
        }

        return obj;
    }

    private static Handle init(boolean useAntialiased) {
        if (useAntialiased)
            return gnome_canvas_new_aa();
        else
            return gnome_canvas_new();
    }

    /**
     * Takes a string specification for a color and allocates it into the
     * specified GdkColor.
     * 
     * @param spec
     *            The color to allocate.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Color getColor(String spec) {
        Handle[] ch = new Handle[1];
        if (gnome_canvas_get_color(getHandle(), spec, ch)) {
            return Color.getColorFromHandle(ch[0]);
        } else {
            return null;
        }
    }

    /**
     * Allocates a color from the RGB value passed into this function.
     * 
     * @param rgba
     *            The RGB value.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public long getColorPixel(int rgba) {
        return gnome_canvas_get_color_pixel(getHandle(), rgba);
    }

    /**
     * The item containing the mouse pointer, or NULL if none.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public CanvasItem getCurrentCanvasItem() {
        Handle cih = getCurrentItem(getHandle());
        return CanvasItem.getCanvasItem(cih);
    }

    /**
     * Returns the dither mode of an antialiased canvas. Only applicable to
     * antialiased canvases - ignored by non-antialiased convases.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public RgbDither getDither() {
        int dh = gnome_canvas_get_dither(getHandle());
        return RgbDither.intern(dh);
    }

    /**
     * Controls the dithering used when the canvas renders. Only applicable to
     * antialiased canvases - ignored by non-antialiased canvases.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setDither(RgbDither dither) {
        gnome_canvas_set_dither(getHandle(), dither.getValue());
    }

    /**
     * The currently focused item, or NULL if none.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public CanvasItem getFocusedCanvasItem() {
        Handle cih = getFocusedItem(getHandle());
        return CanvasItem.getCanvasItem(cih);
    }

    /**
     * The item that holds a pointer grab, or NULL if none.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public CanvasItem getGrabbedCanvasItem() {
        Handle cih = getGrabbedItem(getHandle());
        return CanvasItem.getCanvasItem(cih);
    }

    /**
     * Returns the item that is at the specified position in world coordinates,
     * or NULL if no item is there.
     * 
     * @param x
     *            The horizontal coordinate.
     * @param y
     *            The vertical coordinate.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public CanvasItem getItemAt(double x, double y) {
        Handle cih = gnome_canvas_get_item_at(getHandle(), x, y);
        return CanvasItem.getCanvasItem(cih);
    }

    /**
     * Scaling factor to be used for display.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public double getPixelsPerUnit() {
        return (getPixelsPerUnit(getHandle()));
    }

    /**
     * Sets the number of pixels that correspond to one unit in world
     * coordinates.
     * 
     * @param n
     *            The number of pixels
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setPixelsPerUnit(double n) {
        gnome_canvas_set_pixels_per_unit(getHandle(), n);
    }

    /**
     * Returns the root canvas item group of the canvas
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public CanvasGroup getRoot() {
        Handle hndl = gnome_canvas_root(getHandle());
        return CanvasGroup.getCanvasGroup(hndl);
    }

    /**
     * Returns the horizontal scroll offset of the canvas in canvas pixel
     * coordinates.
     * 
     * @return The scroll offset x coordinate.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public double getScrollOffsetX() {
        int[] x = new int[1];
        int[] y = new int[1];
        gnome_canvas_get_scroll_offsets(getHandle(), x, y);
        return x[0];
    }

    /**
     * Returns the vertical scroll offset of the canvas in canvas pixel
     * coordinates.
     * 
     * @return The scroll offset y coordinate.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public double getScrollOffsetY() {
        int[] x = new int[1];
        int[] y = new int[1];
        gnome_canvas_get_scroll_offsets(getHandle(), x, y);
        return y[0];
    }

    /**
     * The x1 coordinate of the scrolling region.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public double getScrollX1() {
        return (getScrollX1(getHandle()));
    }

    /**
     * Set the scroll region's X1 coordinate.
     * 
     * @param x1
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setScrollX1(double x1) {
        gnome_canvas_set_scroll_region(getHandle(), x1, getScrollY1(),
                getScrollX2(), getScrollY2());
    }

    /**
     * The x2 coordinate of the scrolling region.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public double getScrollX2() {
        return (getScrollX2(getHandle()));
    }

    /**
     * Set the scroll region's X2 coordinate.
     * 
     * @param x2
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setScrollX2(double x2) {
        gnome_canvas_set_scroll_region(getHandle(), getScrollX1(),
                getScrollY1(), x2, getScrollY2());
    }

    /**
     * The y1 coordinate of the scrolling region.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public double getScrollY1() {
        return (getScrollY1(getHandle()));
    }

    /**
     * Set the scroll region's Y1 coordinate.
     * 
     * @param y1
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setScrollY1(double y1) {
        gnome_canvas_set_scroll_region(getHandle(), getScrollX1(), y1,
                getScrollX2(), getScrollY2());
    }

    /**
     * The y2 coordinate of the scrolling region.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public double getScrollY2() {
        return (getScrollY2(getHandle()));
    }

    /**
     * Set the scroll region's Y2 coordinate.
     * 
     * @param y2
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setScrollY2(double y2) {
        gnome_canvas_set_scroll_region(getHandle(), getScrollX1(),
                getScrollY1(), getScrollX2(), y2);
    }

    /**
     * Scrolls the canvas to the specified offsets, given in canvas pixel
     * coordinates.
     * 
     * @param cx
     *            Horizontal coordinate.
     * @param cy
     *            Vertical coordinate.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void scrollTo(int cx, int cy) {
        gnome_canvas_scroll_to(getHandle(), cx, cy);
    }

    /**
     * Convenience method to set all 4 coordinates of the scroll region at once.
     * 
     * @param x1
     *            First horizontal coordinate.
     * @param y1
     *            First vertical coordinate.
     * @param x2
     *            Second horizontal coordinate.
     * @param y2
     *            Second vertical coordinate.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setScrollRegion(double x1, double y1, double x2, double y2) {
        gnome_canvas_set_scroll_region(getHandle(), x1, y1, x2, y2);
    }

    /**
     * Requests that the canvas be repainted immediately instead of during the
     * idle loop.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void update() {
        gnome_canvas_update_now(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_canvas_get_type());
    }

    public boolean getCenterScrollRegion() {
        return gnome_canvas_get_center_scroll_region(getHandle());
    }

    public void setCenterScrollRegion(boolean center) {
        gnome_canvas_set_center_scroll_region(getHandle(), center);
    }

    public void requestRedraw(int x1, int y1, int x2, int y2) {
        gnome_canvas_request_redraw(getHandle(), x1, y1, x2, y2);
    }

    public double windowToWorldX(double winx) {
        double[] x = new double[1];
        double[] y = new double[1];
        gnome_canvas_window_to_world(getHandle(), winx, 0.0, x, y);
        return x[0];
    }

    public double windowToWorldY(double winy) {
        double[] x = new double[1];
        double[] y = new double[1];
        gnome_canvas_window_to_world(getHandle(), 0.0, winy, x, y);
        return y[0];
    }

    public double worldToWindowX(double worldx) {
        double[] x = new double[1];
        double[] y = new double[1];
        gnome_canvas_world_to_window(getHandle(), worldx, 0.0, x, y);
        return x[0];
    }

    public double worldToWindowY(double worldy) {
        double[] x = new double[1];
        double[] y = new double[1];
        gnome_canvas_world_to_window(getHandle(), 0.0, worldy, x, y);
        return y[0];
    }

    native static final protected double getScrollX1(Handle cptr);

    native static final protected double getScrollY1(Handle cptr);

    native static final protected double getScrollX2(Handle cptr);

    native static final protected double getScrollY2(Handle cptr);

    native static final protected double getPixelsPerUnit(Handle cptr);

    native static final protected Handle getCurrentItem(Handle cptr);

    native static final protected Handle getFocusedItem(Handle cptr);

    native static final protected Handle getGrabbedItem(Handle cptr);

    native static final protected int gnome_canvas_get_type();

    native static final protected Handle gnome_canvas_new();

    native static final protected Handle gnome_canvas_new_aa();

    native static final protected Handle gnome_canvas_root(Handle canvas);

    native static final protected void gnome_canvas_set_scroll_region(
            Handle canvas, double x1, double y1, double x2, double y2);

    native static final protected void gnome_canvas_get_scroll_region(
            Handle canvas, double[] x1, double[] y1, double[] x2, double[] y2);

    native static final protected void gnome_canvas_set_center_scroll_region(
            Handle canvas, boolean center);

    native static final protected boolean gnome_canvas_get_center_scroll_region(
            Handle canvas);

    native static final protected void gnome_canvas_set_pixels_per_unit(
            Handle canvas, double n);

    native static final protected void gnome_canvas_scroll_to(Handle canvas,
            int cx, int cy);

    native static final protected void gnome_canvas_get_scroll_offsets(
            Handle canvas, int[] cx, int[] cy);

    native static final protected void gnome_canvas_update_now(Handle canvas);

    native static final protected Handle gnome_canvas_get_item_at(
            Handle canvas, double x, double y);

    native static final protected void gnome_canvas_request_redraw(
            Handle canvas, int x1, int y1, int x2, int y2);

    native static final protected void gnome_canvas_window_to_world(
            Handle canvas, double winx, double winy, double[] worldx,
            double[] worldy);

    native static final protected void gnome_canvas_world_to_window(
            Handle canvas, double worldx, double worldy, double[] winx,
            double[] winy);

    native static final protected boolean gnome_canvas_get_color(Handle canvas,
            String spec, Handle[] color);

    native static final protected long gnome_canvas_get_color_pixel(
            Handle canvas, int rgba);

    native static final protected void gnome_canvas_set_stipple_origin(
            Handle canvas, Handle gc);

    native static final protected void gnome_canvas_set_dither(Handle canvas,
            int dither);

    native static final protected int gnome_canvas_get_dither(Handle canvas);

}
