/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome.event;

/**
 * Listener for the {@link org.gnu.gnome.FontPicker} widget.
 * 
 * @deprecated
 * @see org.gnu.gtk.events.FontButtonListener
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling has been completely re-designed 
 *             in java-gnome 4.0, so there will be no direct correspondant
 *             for this class. See individual inner interfaces in classes
 *             within <code>org.gnome.vte</code>
 */
public interface FontPickerListener {
    /**
     * This method is called whenever a FontPicker event occurs.
     * @deprecated Superceeded by java-gnome 4.0; as signal handling are
     *             implemented differently, you will need to reimplement any
     *             code using these callbacks.
     */
    public void fontPickerEvent(FontPickerEvent event);

}
