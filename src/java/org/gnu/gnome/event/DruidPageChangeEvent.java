/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.event.GtkEvent;

/**
 * An event represeting change of page actions by a {@link
 * org.gnu.gnome.DruidPage} widget.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling has been completely re-designed 
 *             in java-gnome 4.0, so there will be no direct correspondant
 *             for this class. See individual inner interfaces in classes
 *             within <code>org.gnome.vte</code>
 */
public class DruidPageChangeEvent extends GtkEvent {
    /**
     * Type of a DruidPageChangeEvent
     * @deprecated Superceeded by java-gnome 4.0; as signal handling are
     *             implemented differently, you will need to reimplement any
     *             code using these callbacks.
     */
    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        public static final Type NEXT = new Type(1, "NEXT");

        public static final Type BACK = new Type(2, "BACK");

        public static final Type CANCEL = new Type(3, "CANCEL");

    }

    /**
     * Creates a new DruidPageChangeEvent. This is used internally by
     * java-gnome. Users only have to deal with listeners.
     * @deprecated Superceeded by java-gnome 4.0; as signal handling are
     *             implemented differently, you will need to reimplement any
     *             code using these callbacks.
     */
    public DruidPageChangeEvent(Object source, DruidPageChangeEvent.Type type) {
        super(source, type);
    }

    /**
     * @return True if the type of this event is the same as that stated.
     * @deprecated Superceeded by java-gnome 4.0; as signal handling are
     *             implemented differently, you will need to reimplement any
     *             code using these callbacks.
     */
    public boolean isOfType(DruidPageChangeEvent.Type aType) {
        return (type.getID() == aType.getID());
    }

}
