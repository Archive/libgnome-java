/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Struct;

/**
 * Class that can be used to play a sound.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.Sound</code>.
 */
public class Sound extends Struct {

    /**
     * Initialize the esd connection
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void initialize(String hostname) {
        gnome_sound_init(hostname);
    }

    /**
     * Play the specifyed sound file.
     * 
     * @param filename
     *            The file to play
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    static public void play(String filename) {
        gnome_sound_play(filename);
    }

    /**
     * Load the sample provided
     * 
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    static public int loadSample(String sampleName, String fileName) {
        return gnome_sound_sample_load(sampleName, fileName);
    }

    /**
     * Shutdown the sound system.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    static public void shutdown() {
        gnome_sound_shutdown();
    }

    native static final protected int gnome_sound_connection_get();

    native static final protected void gnome_sound_init(String hostname);

    native static final protected void gnome_sound_shutdown();

    native static final protected int gnome_sound_sample_load(
            String sample_name, String filename);

    native static final protected void gnome_sound_play(String filename);

}
