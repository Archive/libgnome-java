/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * This is a base class that acts as a parent for both GnomeCanvasRect and
 * GnomeCanvasEllipse. This base class includes information pertaining to colors
 * and sizes, but the two decendents contain the drawing details.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.CanvasRE</code>.
 */
public class CanvasRE extends CanvasShape {
    protected static String ARG_NAME_X1 = "x1";

    protected static String ARG_NAME_Y1 = "y1";

    protected static String ARG_NAME_X2 = "x2";

    protected static String ARG_NAME_Y2 = "y2";

    protected static String ARG_NAME_FILL_COLOR = "fill_color";

    protected static String ARG_NAME_FILL_COLOR_GDK = "fill_color_gdk";

    protected static String ARG_NAME_FILL_COLOR_RGBA = "fill_color_rgba";

    protected static String ARG_NAME_OUTLINE_COLOR = "outline_color";

    protected static String ARG_NAME_OUTLINE_COLOR_GDK = "outline_gdk_color";

    protected static String ARG_NAME_OUTLINE_COLOR_RGBA = "outline_color_rgba";

    protected static String ARG_NAME_FILL_STIPPLE = "fill_stipple";

    protected static String ARG_NAME_OUTLINE_STIPPLE = "outline_stipple";

    protected static String ARG_NAME_WIDTH_PIXELS = "width_pixels";

    protected static String ARG_NAME_WIDTH_UNITS = "width_units";

    protected CanvasRE(CanvasGroup group, int type) {
        super(group, type);
    }

    /**
     * Constructs an instance of CanvasRE from a native widget resource.
     * 
     * @param handle
     *            The handle to the native widget.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    CanvasRE(Handle handle) {
        super(handle);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_canvas_re_get_type());
    }

    native static final protected void gnome_canvas_request_redraw(
            Handle canvas, int x1, int y1, int x2, int y2);

    native static final protected int gnome_canvas_re_get_type();

    native static final protected Handle gnome_canvas_re_new_gdk_wpix(
            Handle group, int type, double x1, double y1, double x2, double y2,
            Handle fill_color, Handle outline_color, Handle fill_stipple,
            Handle outline_stipple, int width_pixels);

    native static final protected Handle gnome_canvas_re_new_gdk_wunit(
            Handle group, int type, double x1, double y1, double x2, double y2,
            Handle fill_color, Handle outline_color, Handle fill_stipple,
            Handle outline_stipple, double width_units);

    native static final protected Handle gnome_canvas_re_new_rgba_wpix(
            Handle group, int type, double x1, double y1, double x2, double y2,
            int fill_color, int outline_color, int width_pixels);

    native static final protected Handle gnome_canvas_re_new_rgba_wunit(
            Handle group, int type, double x1, double y1, double x2, double y2,
            int fill_color, int outline_color, double width_units);

}
