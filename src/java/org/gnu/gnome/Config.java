/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Struct;

/**
 * @deprecated
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.Config</code>.
 */
public class Config extends Struct {

    static public String getString(String path) {
        return gnome_config_get_string(path);
    }

    static public int getInt(String path) {
        return gnome_config_get_int(path);
    }

    static public double getDouble(String path) {
        return gnome_config_get_float(path);
    }

    static public boolean getBoolean(String path) {
        return gnome_config_get_bool(path);
    }

    static public String getPrivateString(String path) {
        return gnome_config_private_get_string(path);
    }

    static public int getPrivateInt(String path) {
        return gnome_config_private_get_int(path);
    }

    static public double getPrivateDouble(String path) {
        return gnome_config_private_get_float(path);
    }

    static public boolean getPrivateBoolean(String path) {
        return gnome_config_private_get_bool(path);
    }

    static public void setString(String path, String value) {
        gnome_config_set_string(path, value);
    }

    static public void setInt(String path, int value) {
        gnome_config_set_int(path, value);
    }

    static public void setDouble(String path, double value) {
        gnome_config_set_float(path, value);
    }

    static public void setBoolean(String path, boolean value) {
        gnome_config_set_bool(path, value);
    }

    static public void setPrivateString(String path, String value) {
        gnome_config_private_set_string(path, value);
    }

    static public void setPrivateInt(String path, int value) {
        gnome_config_private_set_int(path, value);
    }

    static public void setPrivateDouble(String path, double value) {
        gnome_config_private_set_float(path, value);
    }

    static public void setPrivateBoolean(String path, boolean value) {
        gnome_config_private_set_bool(path, value);
    }

    static public void sync() {
        gnome_config_sync();
    }

    native static final protected String gnome_config_get_string(String path);

    native static final protected String gnome_config_get_translated_string(
            String path);

    native static final protected int gnome_config_get_int(String path);

    native static final protected double gnome_config_get_float(String path);

    native static final protected boolean gnome_config_get_bool(String path);

    native static final protected String gnome_config_private_get_string(
            String path);

    native static final protected String gnome_config_private_get_translated_string(
            String path);

    native static final protected int gnome_config_private_get_int(String path);

    native static final protected double gnome_config_private_get_float(
            String path);

    native static final protected boolean gnome_config_private_get_bool(
            String path);

    native static final protected void gnome_config_set_string(String path,
            String value);

    native static final protected void gnome_config_set_translated_string(
            String path, String value);

    native static final protected void gnome_config_set_int(String path,
            int value);

    native static final protected void gnome_config_set_float(String path,
            double value);

    native static final protected void gnome_config_set_bool(String path,
            boolean value);

    native static final protected void gnome_config_private_set_string(
            String path, String value);

    native static final protected void gnome_config_private_set_translated_string(
            String path, String value);

    native static final protected void gnome_config_private_set_int(
            String path, int value);

    native static final protected void gnome_config_private_set_float(
            String path, double value);

    native static final protected void gnome_config_private_set_bool(
            String path, boolean value);

    native static final protected boolean gnome_config_has_section(String path);

    native static final protected boolean gnome_config_private_has_section(
            String path);

    native static final protected void gnome_config_drop_all();

    native static final protected void gnome_config_sync();

    native static final protected void gnome_config_sync_file(String path);

    native static final protected void gnome_config_private_sync_file(
            String path);

    native static final protected void gnome_config_drop_file(String path);

    native static final protected void gnome_config_private_drop_file(
            String path);

    native static final protected void gnome_config_clean_file(String path);

    native static final protected void gnome_config_private_clean_file(
            String path);

    native static final protected void gnome_config_clean_section(String path);

    native static final protected void gnome_config_private_clean_section(
            String path);

    native static final protected void gnome_config_clean_key(String path);

    native static final protected void gnome_config_private_clean_key(
            String path);

    native static final protected String gnome_config_get_real_path(String path);

    native static final protected String gnome_config_private_get_real_path(
            String path);

    native static final protected void gnome_config_push_prefix(String path);

    native static final protected void gnome_config_pop_prefix();

}
