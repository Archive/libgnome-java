/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Enum;

/**
 * Identifies the type of pixmap used in an item.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.UIPixmapType</code>.
 */
public class UIPixmapType extends Enum {
    static final private int _NONE = 0;

    static final public org.gnu.gnome.UIPixmapType NONE = new org.gnu.gnome.UIPixmapType(
            _NONE);

    static final private int _STOCK = 1;

    static final public org.gnu.gnome.UIPixmapType STOCK = new org.gnu.gnome.UIPixmapType(
            _STOCK);

    static final private int _DATA = 2;

    static final public org.gnu.gnome.UIPixmapType DATA = new org.gnu.gnome.UIPixmapType(
            _DATA);

    static final private int _FILENAME = 3;

    static final public org.gnu.gnome.UIPixmapType FILENAME = new org.gnu.gnome.UIPixmapType(
            _FILENAME);

    static final private org.gnu.gnome.UIPixmapType[] theInterned = new org.gnu.gnome.UIPixmapType[] {
            NONE, STOCK, DATA, FILENAME };

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.UIPixmapType theSacrificialOne = new org.gnu.gnome.UIPixmapType(
            0);

    static public org.gnu.gnome.UIPixmapType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.UIPixmapType already = (org.gnu.gnome.UIPixmapType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.UIPixmapType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private UIPixmapType(int value) {
        value_ = value;
    }

    public org.gnu.gnome.UIPixmapType or(org.gnu.gnome.UIPixmapType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.UIPixmapType and(org.gnu.gnome.UIPixmapType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.UIPixmapType xor(org.gnu.gnome.UIPixmapType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.UIPixmapType other) {
        return (value_ & other.value_) == other.value_;
    }

}
