/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Struct;

public class Url extends Struct {
    /**
     * This method displays the given URL in he appropriate viewer. The
     * appropriate viewer is user defined, according to these rules:<br>
     * <br>
     * 1. Extract the protocol from URL. This is defined as everything before
     * the first colon<br>
     * <br>
     * 2. Check if the key /desktop/gnome/url-handlers/<i>protocol</i>-show
     * exists in the gnome config database. If it does, use this as a command
     * template. If it doesn't, check for the key
     * /desktop/gnome/url-handlers/default-show, and if that doesn't exist fall
     * back on the compiled in default.<br>
     * <br>
     * 3. Substitute the %s in the template with the URL.<br>
     * <br>
     * 4.Call GnomeExecute.shell, with the expanded command as the second
     * argument.
     * 
     * @param url
     *            The url.
     * @return True if the url was shown. False if there was an error
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static boolean show(String url) {
        // FixMe: Add error handling. Awaiting the definition of the glib
        // GSpawnError enumeration.
        int errors[] = new int[1];
        return gnome_url_show(url, errors);
    }

    native static final protected boolean gnome_url_show(String url, int[] error);

}
