/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

public class CanvasPoints extends MemStruct {
    /**
     * Constructs a new CanvasPoints object with memory reserved for the given
     * number of points.
     * 
     * @param numPoints
     *            The number of points to be represented by the object.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public CanvasPoints(int numPoints) {
        super(gnome_canvas_points_new(numPoints));
    }

    // I could have created a Point class, and taken an array of Point objects,
    // but that seemed a little heavy-weight for this simple constructor.
    // Especially considering that the Point class would have to be immutable.
    // PAC.
    /**
     * Constructs a new CanvasPoints object with the given points. Point n would
     * be (xCoordinates[n], yCoordinates[n]).
     * 
     * @param xCoordinates
     *            an array containing the x coordinates of the points.
     * @param yCoordinates
     *            an array containing the y coordinates of the points.
     * @exception IllegalArgumentException
     *                Indicates that the number of elements in the arrays are
     *                not the same.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public CanvasPoints(double xCoordinates[], double yCoordinates[])
            throws IllegalArgumentException {
        if (xCoordinates.length != yCoordinates.length) {
            throw new IllegalArgumentException(
                    "The number of X coordinates and Y coordinates must be the same.");
        } else {
            setHandle(gnome_canvas_points_new(xCoordinates.length));
            for (int i = 0; i < xCoordinates.length; i++) {
                gnome_canvas_points_set_point(getHandle(), i, xCoordinates[i],
                        yCoordinates[i]);
            }
        }
    }

    /**
     * Sets the value of the point at the given index.
     * 
     * @param index
     *            The index of the point. This value is 0 to
     *            {@link #CanvasPoints(int) numPoints} - 1.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setPoint(int index, double x, double y) {
        gnome_canvas_points_set_point(getHandle(), index, x, y);
    }

    native static final protected Handle gnome_canvas_points_new(int numPoints);

    native static final protected void gnome_canvas_points_ref(Handle points);

    native static final protected void gnome_canvas_points_free(Handle points);

    native static final protected void gnome_canvas_points_set_point(
            Handle points, int index, double x, double y);

}
