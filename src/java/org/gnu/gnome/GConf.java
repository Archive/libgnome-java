/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Handle;
import org.gnu.glib.Struct;

public class GConf extends Struct {
    public static String getGnomeLibsSettings(String subkey) {
        return gnome_gconf_get_gnome_libs_settings_relative(subkey);
    }

    public static String getAppSettings(Program program, String subkey) {
        return gnome_gconf_get_app_settings_relative(program.getHandle(),
                subkey);
    }

    native static final protected String gnome_gconf_get_gnome_libs_settings_relative(
            String subkey);

    native static final protected String gnome_gconf_get_app_settings_relative(
            Handle program, String subkey);

}
