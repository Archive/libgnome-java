/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Enum;

public class EdgePosition extends Enum {
    static final private int _START = 0;

    static final public org.gnu.gnome.EdgePosition START = new org.gnu.gnome.EdgePosition(
            _START);

    static final private int _FINISH = 1;

    static final public org.gnu.gnome.EdgePosition FINISH = new org.gnu.gnome.EdgePosition(
            _FINISH);

    static final private int _OTHER = 2;

    static final public org.gnu.gnome.EdgePosition OTHER = new org.gnu.gnome.EdgePosition(
            _OTHER);

    static final private int _LAST = 3;

    static final public org.gnu.gnome.EdgePosition LAST = new org.gnu.gnome.EdgePosition(
            _LAST);

    static final private org.gnu.gnome.EdgePosition[] theInterned = new org.gnu.gnome.EdgePosition[] {
            START, FINISH, OTHER, LAST }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.EdgePosition theSacrificialOne = new org.gnu.gnome.EdgePosition(
            0);

    static public org.gnu.gnome.EdgePosition intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.EdgePosition already = (org.gnu.gnome.EdgePosition) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.EdgePosition(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private EdgePosition(int value) {
        value_ = value;
    }

    public org.gnu.gnome.EdgePosition or(org.gnu.gnome.EdgePosition other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.EdgePosition and(org.gnu.gnome.EdgePosition other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.EdgePosition xor(org.gnu.gnome.EdgePosition other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.EdgePosition other) {
        return (value_ & other.value_) == other.value_;
    }

}
