/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Flags;

public class ClientFlags extends Flags {
    static final private int _IS_CONNECTED = 1 << 0;

    static final public org.gnu.gnome.ClientFlags IS_CONNECTED = new org.gnu.gnome.ClientFlags(
            _IS_CONNECTED);

    static final private int _RESTARTED = 1 << 1;

    static final public org.gnu.gnome.ClientFlags RESTARTED = new org.gnu.gnome.ClientFlags(
            _RESTARTED);

    static final private int _RESTORED = 1 << 2;

    static final public org.gnu.gnome.ClientFlags RESTORED = new org.gnu.gnome.ClientFlags(
            _RESTORED);

    static final private org.gnu.gnome.ClientFlags[] theInterned = new org.gnu.gnome.ClientFlags[] {
            new org.gnu.gnome.ClientFlags(0), IS_CONNECTED, RESTARTED,
            new org.gnu.gnome.ClientFlags(3), RESTORED }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.ClientFlags theSacrificialOne = new org.gnu.gnome.ClientFlags(
            0);

    static public org.gnu.gnome.ClientFlags intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.ClientFlags already = (org.gnu.gnome.ClientFlags) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.ClientFlags(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ClientFlags(int value) {
        value_ = value;
    }

    public org.gnu.gnome.ClientFlags or(org.gnu.gnome.ClientFlags other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.ClientFlags and(org.gnu.gnome.ClientFlags other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.ClientFlags xor(org.gnu.gnome.ClientFlags other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.ClientFlags other) {
        return (value_ & other.value_) == other.value_;
    }

}
