/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Enum;

public class DialogType extends Enum {
    static final private int _ERROR = 0;

    static final public org.gnu.gnome.DialogType ERROR = new org.gnu.gnome.DialogType(
            _ERROR);

    static final private int _NORMAL = 1;

    static final public org.gnu.gnome.DialogType NORMAL = new org.gnu.gnome.DialogType(
            _NORMAL);

    static final private org.gnu.gnome.DialogType[] theInterned = new org.gnu.gnome.DialogType[] {
            ERROR, NORMAL }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gnome.DialogType theSacrificialOne = new org.gnu.gnome.DialogType(
            0);

    static public org.gnu.gnome.DialogType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gnome.DialogType already = (org.gnu.gnome.DialogType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gnome.DialogType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private DialogType(int value) {
        value_ = value;
    }

    public org.gnu.gnome.DialogType or(org.gnu.gnome.DialogType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gnome.DialogType and(org.gnu.gnome.DialogType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gnome.DialogType xor(org.gnu.gnome.DialogType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gnome.DialogType other) {
        return (value_ & other.value_) == other.value_;
    }

}
