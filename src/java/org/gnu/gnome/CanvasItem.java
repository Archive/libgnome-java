/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The CanvasItem is the base class of all items that a Canvas displays.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.CanvasItem</code>.
 */
public class CanvasItem extends GObject {

    protected CanvasItem(CanvasGroup group, int type) {
        super(gnome_canvas_item_new(group.getHandle(), type));
    }

    /**
     * Constructs an instance of CanvasItem from a native widget resource.
     * 
     * @param handle
     *            The handle to the native widget.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    CanvasItem(Handle handle) {
        super(handle);
    }

    static CanvasItem getCanvasItem(Handle handle) {
        if (handle == null) {
            return null;
        }

        CanvasItem obj = (CanvasItem) GObject.getGObjectFromHandle(handle);

        if (obj == null) {
            obj = new CanvasItem(handle);
        }

        return obj;
    }

    /**
     * Apply an absolute affine transformation to the item.
     * 
     * @param affine
     *            the affine to apply.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void affineAbsolute(double[] affine) {
        gnome_canvas_item_affine_absolute(getHandle(), affine);
    }

    /**
     * Apply a relative affine transformation to the item.
     * 
     * @param affine
     *            The affine to apply.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void affineRelative(double[] affine) {
        gnome_canvas_item_affine_relative(getHandle(), affine);
    }

    /**
     * Returns the parent canvas for this item.
     * 
     * @return The parent canvas.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Canvas getCanvas() {
        Handle hndl = getCanvas(getHandle());
        return Canvas.getCanvas(hndl);
    }

    /**
     * Return the parent canvas group for this item.
     * 
     * @return The parent cavas group.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public CanvasGroup getParent() {
        Handle hndl = getParent(getHandle());
        return CanvasGroup.getCanvasGroup(hndl);
    }

    /**
     * X1 coordinate fo the bounding box for this item (in canvas coordinates).
     * 
     * @return The X1 coordinate.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public double getX1() {
        return getX1(getHandle());
    }

    /**
     * X2 coordinate fo the bounding box for this item (in canvas coordinates).
     * 
     * @return The X2 coordinate.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public double getX2() {
        return getX2(getHandle());
    }

    /**
     * Y1 coordinate fo the bounding box for this item (in canvas coordinates).
     * 
     * @return The Y1 coordinate.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public double getY1() {
        return getY1(getHandle());
    }

    /**
     * Y2 coordinate fo the bounding box for this item (in canvas coordinates).
     * 
     * @return The Y2 coordinate.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public double getY2() {
        return getY2(getHandle());
    }

    /**
     * Hide the item (make it invisible). If the item is already invisible, it
     * has no effect.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void hide() {
        gnome_canvas_item_hide(getHandle());
    }

    /**
     * Lower the item in the z-order of its parent group by the specified number
     * of positions.
     * 
     * @param positions
     *            The number of positions to lower the item.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void lower(int positions) {
        gnome_canvas_item_lower(getHandle(), positions);
    }

    /**
     * Lower the item to the bottom of its parent group's z-order.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void lowerToBotton() {
        gnome_canvas_item_lower_to_bottom(getHandle());
    }

    /**
     * Move the item by the specified amount.
     * 
     * @param x
     *            The amount to move the item horizontally.
     * @param y
     *            The amount to move the item vertically.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void move(double x, double y) {
        gnome_canvas_item_move(getHandle(), x, y);
    }

    /**
     * Raise the item in the z-order of its parent group by the specified number
     * of positions.
     * 
     * @param positions
     *            The number of positions to raise the item.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void raise(int positions) {
        gnome_canvas_item_raise(getHandle(), positions);
    }

    /**
     * Raise the item to the top of its parent group's z-order.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void raiseToTop() {
        gnome_canvas_item_raise_to_top(getHandle());
    }

    /**
     * Show the item (make it visible). If the item is already shown, it has no
     * effect.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void show() {
        gnome_canvas_item_show(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_canvas_item_get_type());
    }

    native static final protected Handle getCanvas(Handle cptr);

    native static final protected Handle getParent(Handle cptr);

    native static final protected double getX1(Handle cptr);

    native static final protected double getY1(Handle cptr);

    native static final protected double getX2(Handle cptr);

    native static final protected double getY2(Handle cptr);

    native static final protected int gnome_canvas_item_get_type();

    native static final protected void gnome_canvas_item_move(Handle item,
            double dx, double dy);

    native static final protected void gnome_canvas_item_affine_relative(
            Handle item, double[] affine);

    native static final protected void gnome_canvas_item_affine_absolute(
            Handle item, double[] affine);

    native static final protected void gnome_canvas_item_raise(Handle item,
            int position);

    native static final protected void gnome_canvas_item_raise_to_top(
            Handle item);

    native static final protected void gnome_canvas_item_lower(Handle item,
            int positions);

    native static final protected void gnome_canvas_item_lower_to_bottom(
            Handle item);

    native static final protected void gnome_canvas_item_show(Handle item);

    native static final protected void gnome_canvas_item_hide(Handle item);

    native static final protected int gnome_canvas_item_grab(Handle item,
            int event_mask, Handle cursor, int etime);

    native static final protected void gnome_canvas_item_ungrab(Handle item,
            int etime);

    native static final protected void gnome_canvas_item_reparent(Handle item,
            Handle new_group);

    native static final protected void gnome_canvas_item_grab_focus(Handle item);

    native static final protected void gnome_canvas_item_request_update(
            Handle item);

    native static final protected Handle gnome_canvas_item_new(Handle group,
            int type);

}
