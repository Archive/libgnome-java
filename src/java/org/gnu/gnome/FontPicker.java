/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import java.util.Vector;

import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.gnome.event.FontPickerEvent;
import org.gnu.gnome.event.FontPickerListener;
import org.gnu.gtk.Button;
import org.gnu.gtk.Widget;
import org.gnu.glib.Handle;

/**
 * The FontPicker widget is a button that, when selected, presents a window that
 * enables the user to select from among the many available fonts.
 * 
 * @deprecated
 * @see org.gnu.gtk.FontButton
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.FontPicker</code>.
 */
public class FontPicker extends Button {
    /**
     * Listeners for handling FontPicker events
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private Vector fontPickerListeners = null;

    /**
     * Creates a new font picker.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public FontPicker() {
        super(gnome_font_picker_new());
    }

    /**
     * Construct a FontPicker using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public FontPicker(Handle handle) {
        super(handle);
    }

    /**
     * Enables/Disables thre rendering of the font's name in its font.
     * 
     * @param enable
     *            True if the font name should be rendered in its font. False
     *            otherwise.
     * @param size
     *            The font size to use when rendering the font names.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void enableFontPreview(boolean enable, int size) {
        gnome_font_picker_fi_set_use_font_in_label(getHandle(), enable, size);
    }

    /**
     * Enable/Disable the display of font sizes.
     * 
     * @param showSize
     *            True if font sizes should be shown. False otherwise.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void enableSize(boolean showSize) {
        gnome_font_picker_fi_set_show_size(getHandle(), showSize);
    }

    /**
     * Get the name of the selected font.
     * 
     * @return The name of the selected font.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getFontName() {
        return gnome_font_picker_get_font_name(getHandle());
    }

    /**
     * Get the mode.
     * 
     * @return The mode.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public FontPickerMode getMode() {
        return FontPickerMode.intern(gnome_font_picker_get_mode(getHandle()));

    }

    /**
     * Get the text showen in the preview.
     * 
     * @return The text in the preview.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getPreviewText() {
        return gnome_font_picker_get_preview_text(getHandle());
    }

    /**
     * Get the title of the dialog.
     * 
     * @return The title.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getTitle() {
        return gnome_font_picker_get_title(getHandle());
    }

    /**
     * Get the user defined widget. For use with USER_WIDGET mode.
     * 
     * @return The widget.
     * @see #getMode()
     * @see #setMode(FontPickerMode)
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Widget getUserWidget() {
        Handle hndl = gnome_font_picker_uw_get_widget(getHandle());
        if (null == hndl)
            return null;
        GObject obj = getGObjectFromHandle(hndl);
        if (null != obj)
            return (Widget) obj;
        return new Widget(hndl);
    }

    /**
     * Set the name of the font to be selected. (Select the specified font)
     * 
     * @param fontName
     *            The name of the font to select.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setFontName(String fontName) {
        gnome_font_picker_set_font_name(getHandle(), fontName);
    }

    /**
     * Set the mode.
     * 
     * @param mode
     *            The mode.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setMode(FontPickerMode mode) {
        gnome_font_picker_set_mode(getHandle(), mode.getValue());
    }

    /**
     * Set the text to show in the preview.
     * 
     * @param text
     *            The text to preview.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setPreviewText(String text) {
        gnome_font_picker_set_preview_text(getHandle(), text);
    }

    /**
     * Set the title of the dialog.
     * 
     * @param title
     *            The title.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setTitle(String title) {
        gnome_font_picker_set_title(getHandle(), title);
    }

    /**
     * Set the user defined widget. For use with USER_WIDGET mode.
     * 
     * @param widget
     *            The widget.
     * @see #getMode()
     * @see #setMode(FontPickerMode)
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void setUserWidget(Widget widget) {
        gnome_font_picker_uw_set_widget(getHandle(), widget.getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_font_picker_get_type());
    }

    // ////////////////////////////////////////////////////
    // Event handling
    // ////////////////////////////////////////////////////

    /**
     * Register an object to handle FontPicker events.
     * 
     * @see org.gnu.gnome.event.FontPickerListener
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void addListener(FontPickerListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(fontPickerListeners, listener);
        if (i == -1) {
            if (null == fontPickerListeners) {
                evtMap.initialize(this, FontPickerEvent.Type.FONT_SET);
                fontPickerListeners = new Vector();
            }
            fontPickerListeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(FontPickerListener)
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(FontPickerListener listener) {
        int i = findListener(fontPickerListeners, listener);
        if (i > -1) {
            fontPickerListeners.remove(i);
        }
        if (0 == fontPickerListeners.size()) {
            evtMap.uninitialize(this, FontPickerEvent.Type.FONT_SET);
            fontPickerListeners = null;
        }
    }

    protected void fireFontPickerEvent(FontPickerEvent event) {
        if (null == fontPickerListeners) {
            return;
        }
        int size = fontPickerListeners.size();
        int i = 0;
        while (i < size) {
            FontPickerListener fpl = (FontPickerListener) fontPickerListeners
                    .elementAt(i);
            fpl.fontPickerEvent(event);
            i++;
        }
    }

    private void handleFontSet(String fontName) {
        fireFontPickerEvent(new FontPickerEvent(this));
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    /**
     * Implementation method to build an EventMap for this widget class. Not
     * useful (or supported) for application use.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("font_set", "handleFontSet",
                FontPickerEvent.Type.FONT_SET, FontPickerListener.class);
    }

    native static final protected int gnome_font_picker_get_type();

    native static final protected Handle gnome_font_picker_new();

    native static final protected void gnome_font_picker_set_title(Handle gfp,
            String title);

    native static final protected String gnome_font_picker_get_title(Handle gfp);

    native static final protected int gnome_font_picker_get_mode(Handle gfp);

    native static final protected void gnome_font_picker_set_mode(Handle gfp,
            int mode);

    native static final protected void gnome_font_picker_fi_set_use_font_in_label(
            Handle gfp, boolean use_font_in_label, int size);

    native static final protected void gnome_font_picker_fi_set_show_size(
            Handle gfp, boolean showSize);

    native static final protected void gnome_font_picker_uw_set_widget(
            Handle gfp, Handle widget);

    native static final protected Handle gnome_font_picker_uw_get_widget(
            Handle gfp);

    native static final protected String gnome_font_picker_get_font_name(
            Handle gfp);

    native static final protected boolean gnome_font_picker_set_font_name(
            Handle gfp, String fontname);

    native static final protected String gnome_font_picker_get_preview_text(
            Handle gfp);

    native static final protected void gnome_font_picker_set_preview_text(
            Handle gfp, String text);

}
