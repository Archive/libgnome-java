/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.gtk.VBox;
import org.gnu.glib.Handle;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.IconSelection</code>.
 */
public class IconSelection extends VBox {

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static IconSelection getIconSelection(Handle handle) {
        if (handle == null) {
            return null;
        }

        IconSelection obj = (IconSelection) GObject
                .getGObjectFromHandle(handle);

        if (obj == null) {
            obj = new IconSelection(handle);
        }

        return obj;
    }

    /**
     * Construct a new IconSelection object.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public IconSelection() {
        super(gnome_icon_selection_new());
    }

    /**
     * Construct a new IconSelection from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public IconSelection(Handle handle) {
        super(handle);
    }

    /**
     * Add the default Gnome icon directories.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void addDefaults() {
        gnome_icon_selection_add_defaults(getHandle());
    }

    /**
     * Add icons from the specified directory.
     * 
     * @param dir
     *            The director to add.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void addDirectory(String dir) {
        gnome_icon_selection_add_directory(getHandle(), dir);
    }

    /**
     * Load and display the icons.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void showIcons() {
        gnome_icon_selection_show_icons(getHandle());
    }

    /**
     * Clear all icons (even the icons that are not shown if <i>notShown</i> is
     * true.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void clear(boolean notShown) {
        gnome_icon_selection_clear(getHandle(), notShown);
    }

    /**
     * Get the filename of the selected icon. If <i>fullPath</i> is true the
     * full path of the filename is returned.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getIcon(boolean fullPath) {
        return gnome_icon_selection_get_icon(getHandle(), fullPath);
    }

    /**
     * Select the icon specified by the filename provided. The filename is not
     * the full path, just the last component.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public void selectIcon(String filename) {
        gnome_icon_selection_select_icon(getHandle(), filename);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_icon_selection_get_type());
    }

    native static final protected int gnome_icon_selection_get_type();

    native static final protected Handle gnome_icon_selection_new();

    native static final protected void gnome_icon_selection_add_defaults(
            Handle gis);

    native static final protected void gnome_icon_selection_add_directory(
            Handle gis, String dir);

    native static final protected void gnome_icon_selection_show_icons(
            Handle gis);

    native static final protected void gnome_icon_selection_clear(Handle gis,
            boolean notShown);

    native static final protected String gnome_icon_selection_get_icon(
            Handle gis, boolean fullPath);

    native static final protected void gnome_icon_selection_select_icon(
            Handle gis, String filename);

    native static final protected void gnome_icon_selection_stop_loading(
            Handle gis);

    native static final protected Handle gnome_icon_selection_get_gil(Handle gis);

    native static final protected Handle gnome_icon_selection_get_box(Handle gis);

}
