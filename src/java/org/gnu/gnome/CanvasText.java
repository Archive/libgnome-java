/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gnome;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * A GnomeCanvasText is a GnomeCanvasItem that displays a line of text of a
 * GnomeCanvas.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gnome.CanvasText</code>.
 */
public class CanvasText extends CanvasItem {

    public CanvasText(CanvasGroup group) {
        super(group, gnome_canvas_text_get_type());
    }

    /**
     * Constructs an instance of CanvasText from a native widget resource.
     * 
     * @param handle
     *            The handle to the native widget.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    CanvasText(Handle handle) {
        super(handle);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gnome_canvas_text_get_type());
    }

    native static final protected String getText(Handle cptr);

    native static final protected void setText(Handle cptr, String text);

    native static final protected double getX(Handle cptr);

    native static final protected void setX(Handle cptr, double x);

    native static final protected double getY(Handle cptr);

    native static final protected void setY(Handle cptr, double y);

    native static final protected int getAnchor(Handle cptr);

    native static final protected void setAnchor(Handle cptr, int anchor);

    native static final protected int getJustification(Handle cptr);

    native static  final protected void setJustification(Handle cptr, int justification);

    native static final protected double getClipWidth(Handle cptr);

    native static final protected void setClipWidth(Handle cptr, double clip_width);

    native static final protected double getClipHeight(Handle cptr);

    native static final protected void setClipHeight(Handle cptr, double clip_height);

    native static final protected long getPixel(Handle cptr);

    native static final protected void setPixel(Handle cptr, long pixel);

    native static final protected Handle getStipple(Handle cptr);

    native static final protected void setStipple(Handle cptr, Handle stipple);

    native static final protected Handle getGc(Handle cptr);

    native static final protected int getMaxWidth(Handle cptr);

    native static final protected int getHeight(Handle cptr);

    native static final protected boolean getClip(Handle cptr);

    native static final protected void setClip(Handle cptr, boolean clip);

    native static final protected Handle getFontDesc(Handle cptr);

    native static  final protected void setFontDesc(Handle cptr, Handle font_desc);

    native static final protected Handle getAttrList(Handle cptr);

    native static final protected void setAttrList(Handle cptr, Handle attr_list);

    native static final protected int getUnderline(Handle cptr);

    native static final protected void setUnderline(Handle cptr, int underline);

    native static final protected boolean getStrikethrough(Handle cptr);

    native static final protected void setStrikethrough(Handle cptr, boolean strikethrough);

    native static final protected int getRise(Handle cptr);

    native static final protected void setRise(Handle cptr, int rise);

    native static final protected double getScale(Handle cptr);

    native static final protected void setScale(Handle cptr, double scale);

    native static final protected Handle getLayout(Handle cptr);

    native static final protected void setLayout(Handle cptr, Handle layout);

    native static final protected double getXofs(Handle cptr);

    native static final protected double getYofs(Handle cptr);

    native static final protected int getCx(Handle cptr);

    native static final protected int getCy(Handle cptr);

    native static final protected boolean getUnderlineSet(Handle cptr);

    native static final protected boolean getStrikeSet(Handle cptr);

    native static final protected boolean getRiseSet(Handle cptr);

    native static final protected boolean getScaleSet(Handle cptr);

    native static final protected int getClipCx(Handle cptr);

    native static final protected int getClipCy(Handle cptr);

    native static final protected int getClipCheight(Handle cptr);

    native static final protected long getRgba(Handle cptr);

    native static final protected int gnome_canvas_text_get_type();

}
